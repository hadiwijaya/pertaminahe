package beans;

import com.utils.ADFUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichColumn;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.data.RichTree;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanRadio;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelSplitter;
import oracle.adf.view.rich.component.rich.layout.RichPanelTabbed;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;
import oracle.adf.view.rich.component.rich.output.RichOutputText;
import oracle.adf.view.rich.component.rich.output.RichPanelCollection;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.event.AttributeChangeEvent;
import org.apache.myfaces.trinidad.event.DisclosureEvent;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;

import java.io.FileOutputStream;
import java.io.InputStream;

import java.sql.Date;

import java.sql.ResultSet;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;

import java.util.Calendar;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.FacesMessage;

import javax.faces.component.UIComponent;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.share.ADFContext;

import oracle.binding.AttributeBinding;

import oracle.jbo.RowSetIterator;

import oracle.stellent.ridc.IdcClientException;
import oracle.stellent.ridc.model.DataBinder;
import oracle.stellent.ridc.model.DataObject;

import org.apache.myfaces.trinidad.component.UIXTree;
import org.apache.myfaces.trinidad.model.UploadedFile;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.ComponentReference;
import org.apache.myfaces.trinidad.util.Service;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;

public class requestClass {
    private RichSelectOneChoice dropDownViaBind;
    private String currentAddress;
    private String weblogicusername;
    private String weblogicpassword;
    private RichTable bindTable;
    private String Userlogin;
    private String UserloginFullName;
    private String idrequest;
    private String tableStatus;
    private String RemarksRewrite;
    private String contractorRewrite;
    private String contractorUser;
    private String countList;
    private RichCommandButton bindSaveDocRequest;
    private RichCommandButton bindCreateInserDocList;
    private RichPanelTabbed bindInputList;
    private RichPanelSplitter bindPanelSplitter;
    private RichPanelGroupLayout bindTabPage;
    private RichPanelFormLayout bindForm;
    private RichCommandButton bindNewInsert;
    private RichInputText bindSV;
    private RichInputText bindDocSource;
    private RichInputText bindDocStatus;
    private RichCommandButton bindCancelInput;
    private RichTable bindTableCreateRequest;
    private RichCommandButton bindCreateRequest;
    private RichPanelGroupLayout bindFromRequest;
    private RichOutputText bindOutput;
    private RichInputText bindTdcLead;
    private RichInputText bindStatusRequest;
    private RichInputText bindRequestor;
    private RichInputText bindIdRequestCreate;
    private RichInputText bindIdRequest;
    private RichTree bindTreeEdms;
    private RichTable bindTableFilesEDMS;
    private List<ChildFiles> childFiles = new ArrayList<ChildFiles>();
    private RichInputText bindSearchNameInternalUser;
    private RichCommandButton btnSearchInternalUser;
    private RichTable bindTableInternalUser;
    private RichColumn bindSelectedInternalUsername;
    private RichPopup bindPopupInternalUser;
    private RichDialog dialogLsnrSelectInternalUser;
    private RichColumn bindSelectedInternalFullname;
    private RichInputText bindSelectSVFullname;
    private RichInputText bindSelectUserFullName;
    private RichInputText bindSelectUserEmail;
    private RichInputText bindSelectUserName;
    private RichInputText bindSVName;
    private RichInputText bindSVLabel;
    private RichInputText bindPurposeLabel;
    private RichCommandButton bindSend;
    private RichOutputText bindTextStatusDocRequest;
    private RichPopup bindPopupCIDSend;
    private RichCommandButton bindNextFlow;
    private RichPopup bindPopupNoItem;
    private RichPopup bindPopupSelectedItem;
    private RichInputText bindRequestCIDRequestorPosition;
    private RichPopup bindPopupFillForm;
    private RichPopup popupRequestCreated;
    private RichPopup bindPopupRequestCreated;
    private RichInputText bindRequestCIDPurpose;
    private RichInputText bindRequestCIDSVPosition;
    private RichInputDate bindRequestCIDDateRequest;
    private RichCommandButton bindSubmitForm;
    String reqStatus = "";
    private RichCommandButton bindUpdate;
    private RichCommandButton bindSaveButton;
    private RichCommandButton bindUpdateButton;
    private RichCommandButton bindCreateButton;
    Log log = new Log();
    private RichInputText bindRemarksRequestor;
    private RichPanelCollection bindTableHistory;
    private Date date;
    private UploadedFile file;
    private long fileLength;
    private InputStream fileInputStream;
    private String fileName;
    private String fileContentType;
    private RichInputFile bindUploadFile;
    private String selectedEDMSFile;
    private String userSelected;
    private RichSelectBooleanRadio bindRadioEDMS;
    private ComponentReference bindEdmsDocNumber;
    private ComponentReference bindEdmsDocTitle;
    private RichPanelGroupLayout bindFormRequestEDMS;
    private RichInputText bindDidDocEDMS;
    private RichInputText bindDocNumberEDMS;
    private RichInputText bindDocTitleEDMS;
    private RichInputText bindDocSourceEDMS;
    private RichInputText bindStatusDocRequestEDMS;
    private RichInputFile bindAttachFile;
    private RichInputText bindAttachFormIdrequest;
    private RichInputDate bindAttachFormDate;
    private RichInputText bindAttachFormDID;
    private RichInputText bindAttachFormDocName;
    private RichInputText bindAttachFormDocFormat;
    private RichInputText bindAttachFormDocSize;
    private RichInputText bindAttachFormStatus;
    private RichInputText bindAttachFormFolderGuid;
    private RichPopup bindPopupInputAttach;
    private RichPopup bindPopupAttachInput;
    private RichInputText bindIdRequestEDMS;
    private RichTable bindTableAttach;
    private RichCommandButton bindAttachAdd;
    private RichCommandButton bindAttachUpload;
    private RichCommandButton bindCancelInputRequest;
    private RichCommandButton bindCancelEditRequest;
    private RichInputText bindAttachcoloum;
    private RichCommandButton bindCancelInputRequestCID;
    private RichCommandButton bindCancelEditRequestCID;
    private RichOutputText bindLabelUSer;
    private RichOutputText bindLabelUSer2;
    private RichInputText bindContractorRewrite;
    private RichInputText bindRemarksRewrite;
    private RichInputText bindUserContractor;
    private RichInputText bindInternalUser;
    private RichCommandButton bindDeleteDocList;
    private RichCommandButton bindEditDocList;
    private RichCommandButton bindSendRequest;
    private RichCommandButton bindFinishDocList;
    private RichCommandButton bindAddtoList;
    private RichCommandButton bindCreateRequestButton;
    private RichCommandButton bindEditRequestButton;
    private RichCommandButton bindDeleteRequestButton;
    private RichCommandButton bindDocumentListButton;
    private RichPanelFormLayout bindFormCheckbox;
    private RichSelectBooleanCheckbox bindCheckbox;
    private RichPopup bindPopupAlreadyRequested;
    private RichTable bindTableDocRequest;
    private RichPanelFormLayout bindInputForm;
    private RichPopup bindPopupValidationDocList;
    private RichPopup bindPopupValidationDelList;
    private RichPopup bindPopupValidationDel;
    private RichPopup bindPopupValidationSend;
    private RichPopup bindPopupValidationDelCreate;
    private RichPopup bindPopupSend;
    private RichPopup bindpopupDeleteRequestList;
    ArrayList<String> validationDocList = new ArrayList<String>();
    private RichPopup bindPopupValidationEdit;
    private RichCommandButton bindBackToReqList;
    private RichCommandButton bindHome;
    private RichPanelTabbed bindTabInputType;
    private RichInputText bindDocNumNonEDMS;
    private RichInputText bindDocTitleNonEDMS;
    private RichTable bindTableRequest;
    private RichPanelFormLayout bindFormCreateRequest;
    private RichInputText bindAttachUploadForm;
    private RichCommandButton bindSelectDocument;
    private RichPanelGroupLayout bindAddTolist;
    private RichPopup bindPopupNullSelect;
    private RichOutputText bindCountList;
    private RichOutputText doccount;
    private RichOutputText tyestboiuy;
    private RichOutputText sampah;
    private RichOutputText bindIteratorRowCount;
    private RichInputText bindApNamaCompany;
    private RichSelectOneChoice bindSocApName;
    private RichInputText bindApNama;
    private String IDC_SERVER;
    private String URL_CID;
    private String Url_Database;
    private String User_Database;
    private String Pass_Database;
    private String roleTdcReview;

    public requestClass() {
        HttpServletRequest request =
            (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();

        setCurrentAddress(getValueInDB("AppModuleDataControl", "PheConfigVO1",
                                       "KEY_CONFIG = 'RIDC_URL'", "KeyValue"));
        setWeblogicusername(getValueInDB("AppModuleDataControl",
                                         "PheConfigVO1",
                                         "KEY_CONFIG = 'WEBLOGIC_USER'",
                                         "KeyValue"));
        setWeblogicpassword(getValueInDB("AppModuleDataControl",
                                         "PheConfigVO1",
                                         "KEY_CONFIG = 'WEBLOGIC_PASS'",
                                         "KeyValue"));
        setIDC_SERVER(getValueInDB("AppModuleDataControl",
                                         "PheConfigVO1",
                                         "KEY_CONFIG = 'IDC_SERVER'",
                                         "KeyValue"));
        setURL_CID(getValueInDB("AppModuleDataControl",
                                         "PheConfigVO1",
                                         "KEY_CONFIG = 'URL_CID'",
                                         "KeyValue"));
        setUrl_Database(getValueInDB("AppModuleDataControl",
                                         "PheConfigVO1",
                                         "KEY_CONFIG = 'URL_DATABASE'",
                                         "KeyValue"));
        setUser_Database(getValueInDB("AppModuleDataControl",
                                         "PheConfigVO1",
                                         "KEY_CONFIG = 'USER_DATABASE'",
                                         "KeyValue"));
        setPass_Database(getValueInDB("AppModuleDataControl",
                                         "PheConfigVO1",
                                         "KEY_CONFIG = 'PASS_DATABASE'",
                                         "KeyValue"));
        
        try {
            Userlogin =request.getParameter("username");//"PHEH_User";//"PHEH_User";//request.getParameter("username");//"tdc_drawing";"NSO_Admin";
            ApplicationModule am =
                ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
            ViewObject voRequestInput = am.findViewObject("PheApListVO1");
            voRequestInput.setNamedWhereClauseParam("dUser", Userlogin);
            voRequestInput.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("Error get Username");
        }
        try {
            UserloginFullName =
                    getValueInDB("AppModuleDataControl", "UsersVO1",
                                 "DNAME = '" + Userlogin + "'", "Dfullname");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("Error get fullname");
        }
        try {
            bindLabelUSer = new RichOutputText();
            bindLabelUSer2 = new RichOutputText();
            bindLabelUSer.setValue(UserloginFullName);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindLabelUSer);
            bindLabelUSer2.setValue(UserloginFullName);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindLabelUSer2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //main method
        //UserloginFullName = "Yudhi Widhiyana";
        //Userlogin = "yudhi.widhiyana";
        //UserloginFullName = "weblogic";
        //Userlogin = "weblogic";
        //UserloginFullName = "indra.fajar";
        //Userlogin = "indra.fajar";
        log.log("0", "Login User", UserloginFullName);
        ApplicationModule am =
            ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject voRequestInput = am.findViewObject("PheCidListrequestVO1");
        voRequestInput.setWhereClause("CIDREQUESTOR = '" + UserloginFullName +
                                      "' AND Cidstatusrequest <> 'Publish' AND Cidstatusrequest <> 'Approve TDC Lead'");
        voRequestInput.executeQuery();
        tableStatus = voRequestInput.getEstimatedRowCount() + "";
        refresh();
    }

    public Void createInput() {
        // tombol add to list di form input doc list, nambahin list di doc request
        bindFinishDocList.setVisible(true);
        bindFormRequestEDMS.setVisible(false);
        //log.log(idrequest, "remarks requestor",bindRemarksRequestor.getValue().toString());
        bindTabPage.setVisible(false);
        bindCancelEditRequestCID.setVisible(false); //Cancel Edit
        bindSaveDocRequest.setVisible(false); // Add To List
        buttonDocListOn();
        refreshTable();
        return null;
    }

    public String createRequest() {
        bindCreateRequest.setVisible(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindCreateRequest);
        bindSaveButton.setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindSaveButton);
        bindUpdateButton.setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindUpdateButton);
        bindCancelEditRequest.setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindCancelEditRequest);
        bindCancelInputRequest.setVisible(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindCancelInputRequest);
        
        // membuat request CID
        bindFromRequest.setVisible(true);
        // rama , pakai yg di comment jg bs, cmn biar irit codingan pake yg bawahnya aj
        //        Row keyValue =
        //            ADFUtils.findIterator("tdcLead1Iterator").getCurrentRow();
        //        String tdcLead = keyValue.getAttribute("KeyValue").toString();
        /* */
        
        ApplicationModule am =
            ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject voRequestInput = am.findViewObject("PheApListVO1");
        voRequestInput.setNamedWhereClauseParam("dUser", Userlogin);
        voRequestInput.executeQuery();
        Row r=voRequestInput.first();
        
        if(voRequestInput.getEstimatedRowCount() <=1 ){
            String tdcCompany=r.getAttribute("Dattributename").toString();
            String tdclabel=getValueInDB("AppModuleDataControl", "PheConfigVO1", "UPPER(KEY_CONFIG) = UPPER('TDCLEAD_"+tdcCompany.substring(4).replaceAll("\\s+","")+"')",
                                     "KeyDisplay");
            String tdcLead =
                        getValueInDB("AppModuleDataControl", "PheConfigVO1", "UPPER(KEY_CONFIG) = UPPER('TDCLEAD_"+tdcCompany.substring(4).replaceAll("\\s+","")+"')",
                                     "KeyValue");
//                    System.out.print("\ntdcLead = " + tdcLead + ' '+"nama AP = "+tdcCompany.substring(4).replaceAll("\\s+",""));
                    bindTdcLead.setValue(tdcLead); 
                    bindTdcLead.setLabel(tdclabel);
                    bindApNama.setValue(tdcCompany);
                    if(tdcCompany.substring(4).replaceAll("\\s+","").equalsIgnoreCase("ONWJ")){
                        setRoleTdcReview("TDC");
                    }else{
                        setRoleTdcReview(tdcCompany.substring(4).replaceAll("\\s+",""));
                    }
                    System.out.println("roleTdcReviewAp = "+getRoleTdcReview());
                 AdfFacesContext.getCurrentInstance().addPartialTarget(bindApNama);
        }
        
//        AdfFacesContext.getCurrentInstance().addPartialTarget(bindSocApName);
        
        bindStatusRequest.setValue("Draft");
        bindRequestor.setValue(UserloginFullName);
        oracle.jbo.domain.Timestamp datetime =
            new oracle.jbo.domain.Timestamp(System.currentTimeMillis());
        bindRequestCIDDateRequest.setValue(datetime);
        //System.out.print(datetime);
        buttonReqListOFF();
        refreshTableRequest();

        return null;
    }

    public void refreshTable() {

        ApplicationModule am =
            ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject voDocInput = am.findViewObject("PheCidListdocrequestVO1");
        voDocInput.setWhereClause("IDREQUEST = '" + idrequest + "'");
        voDocInput.executeQuery();
        countList = voDocInput.getEstimatedRowCount() + "";

        try {
            bindCountList.setValue(countList);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindCountList);
        } catch (Exception e) {
        }

        System.out.print("\nIDREQUEST = '" + idrequest + "'");

        ViewObject voRequestInput = am.findViewObject("PheCidAttachmentVO1");
        voRequestInput.setWhereClause("IDREQUEST = '" + idrequest + "' ");
        voRequestInput.executeQuery();

        ViewObject voRequestLog = am.findViewObject("PheCidLogVO1");
        voRequestLog.setWhereClause("IDREQUEST = '" + idrequest +
                                    "' AND ACTION <> 'getValueInDB' AND ACTION <> 'Send Email'");
        voRequestLog.executeQuery();
        //AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableHistory);
        refresh();
    }

    public void refreshTableRequest() {
        idrequest = bindIdRequest.getValue().toString();
        ApplicationModule am =
            ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject voDocInput = am.findViewObject("PheCidListdocrequestVO1");
        voDocInput.setWhereClause("IDREQUEST = '" + idrequest + "'");
        voDocInput.executeQuery();

        countList = voDocInput.getEstimatedRowCount() + "";

        try {
            bindCountList.setValue(countList);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindCountList);
        } catch (Exception e) {
        }

        System.out.print("\nIDREQUEST = '" + idrequest + "'");

        ViewObject voRequestInput = am.findViewObject("PheCidAttachmentVO1");
        voRequestInput.setWhereClause("IDREQUEST = '" + idrequest + "' ");
        voRequestInput.executeQuery();

        ViewObject voRequestLog = am.findViewObject("PheCidLogVO1");
        voRequestLog.setWhereClause("IDREQUEST = '" + idrequest +
                                    "' AND ACTION <> 'getValueInDB' AND ACTION <> 'Send Email'");
        //voRequestInput.setWhereClause("upper(action) like upper('%remarks%') or upper(action) like upper('%approval%')");
        voRequestLog.executeQuery();
        //AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableHistory);
        refresh();

    }

    public String inputList() {
        //validasiInputDoc
        System.out.print("===Testing====");
        DCIteratorBinding dci =
            ADFUtils.findIterator("PheCidListdocrequestVO1Iterator");
        System.out.println("DCI COUNT: " + dci.getEstimatedRowCount());

        //proses simpen temporary di PageFlowScope
        ADFContext actx = ADFContext.getCurrent();
        Map pfscount = actx.getPageFlowScope();
        Object var = pfscount.put("doccount", dci.getEstimatedRowCount());
        
        long a = dci.getEstimatedRowCount();
        String isBlocked = "true";
        if (a >= 109) {
            isBlocked = "false";
        } else {
            isBlocked = "true";
        }

        if (isBlocked.equalsIgnoreCase("true")) {
            BindingContainer bindings1 =
                BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding operationbinding =
                bindings1.getOperationBinding("CreateInsert");
            operationbinding.execute();

            String Cidstatusrequest =
                getValueInDB("AppModuleDataControl", "PheCidListrequestVO1",
                             "Idrequest= '" + idrequest + "'",
                             "Cidstatusrequest");
            if ((Cidstatusrequest.equals("Draft"))) {
                buttonDocListOFF();
                bindIteratorRowCount.setVisible(false);
                bindUpdate.setVisible(false);
                bindSaveDocRequest.setVisible(false);
                bindAddtoList.setVisible(true);
                bindCancelInputRequestCID.setVisible(true);
                bindCancelEditRequestCID.setVisible(false);
                bindTabPage.setVisible(true);
                bindDocSource.setValue("Non EDMS");
                bindDocStatus.setValue("Draft");
                bindIdRequestCreate.setValue(idrequest);
                bindAddtoList.setDisabled(true);
                commit();
            } else {
                RichPopup.PopupHints ph = new RichPopup.PopupHints();
                bindPopupValidationDocList.show(ph);
                refreshTable();
            }
        } else {
            BindingContainer bindings1 =
                BindingContext.getCurrent().getCurrentBindingsEntry();
            OperationBinding operationbinding =
                bindings1.getOperationBinding("Rollback");
            operationbinding.execute();

            FacesContext ctx = FacesContext.getCurrentInstance();
            FacesMessage fm =
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Document sudah terinput sebanyak 109, Document berikutnya harap membuat CID baru.",
                                 "");
            ctx.addMessage(null, fm);
        }
        // bindNewInsert.setVisible(false);

        //debug hasil print count list
        //String test = pfscount.get("doccount") == null ? "" : pfscount.get("doccount").toString();
        //System.out.println("****### "+test);
        return null;
    }

    public void buttonDocListOFF() {
        bindBackToReqList.setVisible(false);
        bindCreateInserDocList.setVisible(false);
        bindEditDocList.setVisible(false);
        bindDeleteDocList.setVisible(false);
        bindSendRequest.setVisible(false);
    }

    public void buttonDocListOn() {
        bindSelectDocument.setVisible(true);
        bindBackToReqList.setVisible(true);
        bindCreateInserDocList.setVisible(true);
        bindEditDocList.setVisible(true);
        bindDeleteDocList.setVisible(true);
        bindSendRequest.setVisible(true);
    }

    public void buttonReqListOn() {
        bindHome.setVisible(true);
        bindCreateRequestButton.setVisible(true);
        bindEditRequestButton.setVisible(true);
        bindDeleteRequestButton.setVisible(true);
        bindDocumentListButton.setVisible(true);
    }

    public void buttonReqListOFF() {
        bindHome.setVisible(false);
        bindCreateRequestButton.setVisible(false);
        bindEditRequestButton.setVisible(false);
        bindDeleteRequestButton.setVisible(false);
        bindDocumentListButton.setVisible(false);
    }

    public void getValue(ActionEvent actionEvent) {
        //Add event code here...
        bindOutput.setValue(bindIdRequest.getValue().toString());
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindOutput);
        System.out.print(actionEvent);
    }

    public String cancelInput() {
        bindTabPage.setVisible(false);
        bindCreateInserDocList.setVisible(true);
        bindCancelEditRequestCID.setVisible(false); //Cancel Edit
        bindSaveDocRequest.setVisible(false); // Add To List
        buttonDocListOn();
        refreshTable();
        // Add event code here...
        return null;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 30);
        date = (Date)cal.getTime();
        return date;
    }

    public void setDropDownViaBind(RichSelectOneChoice dropDownViaBind) {
        this.dropDownViaBind = dropDownViaBind;
    }

    public RichSelectOneChoice getDropDownViaBind() {
        return dropDownViaBind;
    }

    public void setBindTable(RichTable bindTable) {
        this.bindTable = bindTable;
    }

    public RichTable getBindTable() {
        return bindTable;
    }

    public void setBindSaveDocRequest(RichCommandButton bindSaveDocRequest) {
        this.bindSaveDocRequest = bindSaveDocRequest;
    }

    public RichCommandButton getBindSaveDocRequest() {
        return bindSaveDocRequest;
    }

    public void setBindCreateInserDocList(RichCommandButton bindCreateInserDocList) {
        this.bindCreateInserDocList = bindCreateInserDocList;
    }

    public RichCommandButton getBindCreateInserDocList() {
        return bindCreateInserDocList;
    }

    public void setBindInputList(RichPanelTabbed bindInputList) {
        this.bindInputList = bindInputList;
    }

    public RichPanelTabbed getBindInputList() {
        return bindInputList;
    }

    public void setBindPanelSplitter(RichPanelSplitter bindPanelSplitter) {
        this.bindPanelSplitter = bindPanelSplitter;
    }

    public RichPanelSplitter getBindPanelSplitter() {
        return bindPanelSplitter;
    }

    public void setBindTabPage(RichPanelGroupLayout bindTabPage) {
        this.bindTabPage = bindTabPage;
    }

    public RichPanelGroupLayout getBindTabPage() {
        return bindTabPage;
    }

    public void setBindForm(RichPanelFormLayout bindForm) {
        this.bindForm = bindForm;
    }

    public RichPanelFormLayout getBindForm() {
        return bindForm;
    }

    public void setBindNewInsert(RichCommandButton bindNewInsert) {
        this.bindNewInsert = bindNewInsert;
    }

    public RichCommandButton getBindNewInsert() {
        return bindNewInsert;
    }

    public void setBindSV(RichInputText bindSV) {
        this.bindSV = bindSV;
    }

    public RichInputText getBindSV() {
        return bindSV;
    }

    public void setBindDocSource(RichInputText bindDocSource) {
        this.bindDocSource = bindDocSource;
    }

    public RichInputText getBindDocSource() {
        return bindDocSource;
    }

    public void setBindDocStatus(RichInputText bindDocStatus) {
        this.bindDocStatus = bindDocStatus;
    }

    public RichInputText getBindDocStatus() {
        return bindDocStatus;
    }

    public void setBindCancelInput(RichCommandButton bindCancelInput) {
        this.bindCancelInput = bindCancelInput;
    }

    public RichCommandButton getBindCancelInput() {
        return bindCancelInput;
    }

    public void setBindTableCreateRequest(RichTable bindTableCreateRequest) {
        this.bindTableCreateRequest = bindTableCreateRequest;
    }

    public RichTable getBindTableCreateRequest() {
        return bindTableCreateRequest;
    }

    public void setBindCreateRequest(RichCommandButton bindCreateRequest) {
        this.bindCreateRequest = bindCreateRequest;
    }

    public RichCommandButton getBindCreateRequest() {
        return bindCreateRequest;
    }

    public void setBindFromRequest(RichPanelGroupLayout bindFromRequest) {
        this.bindFromRequest = bindFromRequest;
    }

    public RichPanelGroupLayout getBindFromRequest() {
        return bindFromRequest;
    }

    public String cancelRequest() {
        // Add event code here...
        bindFromRequest.setVisible(false);
        buttonReqListOn();
        return null;
    }

    public void setBindOutput(RichOutputText bindOutput) {
        this.bindOutput = bindOutput;
    }

    public RichOutputText getBindOutput() {
        return bindOutput;
    }

    public void setBindTdcLead(RichInputText bindTdcLead) {
        this.bindTdcLead = bindTdcLead;
    }

    public RichInputText getBindTdcLead() {
        return bindTdcLead;
    }

    public void setBindStatusRequest(RichInputText bindStatusRequest) {
        this.bindStatusRequest = bindStatusRequest;
    }

    public RichInputText getBindStatusRequest() {
        return bindStatusRequest;
    }

    public void setBindRequestor(RichInputText bindRequestor) {
        this.bindRequestor = bindRequestor;
    }

    public RichInputText getBindRequestor() {
        return bindRequestor;
    }

    public void setBindIdRequestCreate(RichInputText bindIdRequestCreate) {
        this.bindIdRequestCreate = bindIdRequestCreate;
    }

    public RichInputText getBindIdRequestCreate() {
        return bindIdRequestCreate;
    }

    public void setBindIdRequest(RichInputText bindIdRequest) {
        this.bindIdRequest = bindIdRequest;
    }

    public RichInputText getBindIdRequest() {
        return bindIdRequest;
    }

    public void nextFlow(ActionEvent actionEvent) {
        
        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        if (tableStatus.equals("0")) {
            bindPopupNoItem.show(ph);
        } else {
            bindPopupSelectedItem.show(ph);
            refreshTableRequest();
        }
        System.out.print(actionEvent);
    }

    public void setBindTreeEdms(RichTree bindTreeEdms) {
        this.bindTreeEdms = bindTreeEdms;
    }

    public RichTree getBindTreeEdms() {
        return bindTreeEdms;
    }

    public void setSelectedEDMSFile(String selectedEDMSFile) {
        this.selectedEDMSFile = selectedEDMSFile;
    }

    public String getSelectedEDMSFile() {
        return selectedEDMSFile;
    }

    public void OnChangeRadio(ValueChangeEvent valueChangeEvent) {
        if (valueChangeEvent.getNewValue().toString().equalsIgnoreCase("true")) {
            valueChangeEvent.getComponent().processUpdates(FacesContext.getCurrentInstance());
            Map p =
                ((UIComponent)valueChangeEvent.getSource()).getAttributes();
            System.out.println("rowvalIndex = " +
                               p.get("rowvalIndex").toString());
            setSelectedEDMSFile(p.get("rowvalIndex").toString());
        }
    }

    public BindingContainer GetBindings() {
        BindingContext bindingctx = BindingContext.getCurrent();
        BindingContainer bindings = bindingctx.getCurrentBindingsEntry();
        return bindings;
    }

    public BindingContainer getBinding() {
        BindingContainer bindings =
            BindingContext.getCurrent().getCurrentBindingsEntry();
        return bindings;
    }

    public RichInputText getBindEdmsDocTitle() {
        if (bindEdmsDocTitle != null) {
            return (RichInputText)bindEdmsDocTitle.getComponent();
        }
        return null;
    }

    public void setBindEdmsDocTitle(RichInputText bindEdmsDocTitle) {
        this.bindEdmsDocTitle =
                ComponentReference.newUIComponentReference(bindEdmsDocTitle);
    }

    public RichInputText getBindEdmsDocNumber() {
        if (bindEdmsDocNumber != null) {
            return (RichInputText)bindEdmsDocNumber.getComponent();
        }
        return null;
    }

    private static Object evaluateEL(String el) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ELContext elContext = facesContext.getELContext();
        ExpressionFactory expressionFactory =
            (ExpressionFactory)facesContext.getApplication().getExpressionFactory();
        ValueExpression exp =
            expressionFactory.createValueExpression(elContext, el,
                                                    Object.class);
        return exp.getValue(elContext);
    }

    public void ActionSelectEdms(ActionEvent actionEvent) {
        String dID = "";
        bindAddtoList.setDisabled(false);
        try {
            dID =
                childFiles.get(Integer.parseInt(getSelectedEDMSFile())).getDID() + "";
        } catch (Exception e) {
        }
        if (dID.equals("")) {
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            bindPopupNullSelect.show(ph);
        } else {
            String result = select(dID, idrequest);
            System.out.println("sudah ada atau belum = " + result);
            if (result.equals("")) {
                bindFormRequestEDMS.setVisible(true);
                bindStatusDocRequestEDMS.setValue("Draft");
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindStatusDocRequestEDMS);
                bindIdRequestEDMS.setValue(idrequest);
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindIdRequestEDMS);
                bindDidDocEDMS.setValue(childFiles.get(Integer.parseInt(getSelectedEDMSFile())).getDID());
                bindDocNumberEDMS.setValue(childFiles.get(Integer.parseInt(getSelectedEDMSFile())).getXDocNumber());
                bindDocTitleEDMS.setValue(childFiles.get(Integer.parseInt(getSelectedEDMSFile())).getDDocTitle());
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindDidDocEDMS);
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindDocNumberEDMS);
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindDocTitleEDMS);
                bindDocSourceEDMS.setValue("EDMS : " +
                                           childFiles.get(Integer.parseInt(getSelectedEDMSFile())).getXDocPurpose());
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindDocSourceEDMS);
                try {
                    bindContractorRewrite.setValue(contractorRewrite);
                    bindRemarksRewrite.setValue(RemarksRewrite);
                    bindUserContractor.setValue(contractorUser);
                } catch (Exception e) {
                }
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindUserContractor);
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindContractorRewrite);
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindRemarksRewrite);

                System.out.print("\n selected EDMS XDocNumber file = " +
                                 childFiles.get(Integer.parseInt(getSelectedEDMSFile())).getXDocNumber());
                System.out.print("\n selected EDMS DDocName file = " +
                                 childFiles.get(Integer.parseInt(getSelectedEDMSFile())).getDDocName());
                System.out.print("\n selected EDMS XDocName file = " +
                                 childFiles.get(Integer.parseInt(getSelectedEDMSFile())).getXDocName());
                System.out.print("\n selected EDMS DID file = " +
                                 childFiles.get(Integer.parseInt(getSelectedEDMSFile())).getDID());
            } else {
                RichPopup.PopupHints ph = new RichPopup.PopupHints();
                bindPopupAlreadyRequested.show(ph);
            }
        }
    }

    public void SelectTreeLsnr(SelectionEvent selectionEvent) {
        RichTree _tree = (RichTree)selectionEvent.getSource();
        RowKeySet _rkset = _tree.getSelectedRowKeys();

        Iterator _rkIterate = _rkset.iterator();

        if (_rkIterate.hasNext()) {
            List key = (List)_rkIterate.next();
            JUCtrlHierBinding treeBinds = null;
            CollectionModel collectionModel =
                (CollectionModel)_tree.getValue();
            treeBinds = (JUCtrlHierBinding)collectionModel.getWrappedData();
            JUCtrlHierNodeBinding nodeBinding = null;
            nodeBinding = treeBinds.findNodeByKeyPath(key);
            Row rw = nodeBinding.getRow();
            // Get Document ID
            String ID = rw.getAttribute("Ffolderguid").toString();
            System.out.println(ID);
            RIDCClass ridc =
                new RIDCClass(getWeblogicusername(), getWeblogicpassword());
            childFiles.clear();
            try {
                for (DataObject object : ridc.FolderBrowseFile(ID).getRows()) {
                    if (object.get("xStatus") != "Registered") {
                        System.out.println(object.get("xDocName").toString() +
                                           "--" +
                                           object.get("xOwnerName").toString());
                        childFiles.add(new ChildFiles(object.get("fFileGUID").toString(),
                                                      object.get("dID").toString(),
                                                      object.get("dDocName").toString(),
                                                      object.get("dFormat").toString(),
                                                      object.get("xDocNumber").toString(),
                                                      object.get("dDocTitle").toString(),
                                                      object.get("xDocName").toString(),
                                                      object.get("xOwnerName").toString(),
                                                      object.get("xDocPurpose").toString(),
                                                      object.get("fFileName").toString()));
                    }
                }
            } catch (Exception e) {
                System.out.println("Error select tree listener select file EDMS " +
                                   e.getMessage());
            }
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableFilesEDMS);
        }
    }

    public void setBindTableFilesEDMS(RichTable bindTableFilesEDMS) {
        this.bindTableFilesEDMS = bindTableFilesEDMS;
    }

    public RichTable getBindTableFilesEDMS() {
        return bindTableFilesEDMS;
    }

    public void setChildFiles(List<ChildFiles> childFiles) {
        this.childFiles = childFiles;
    }

    public List<ChildFiles> getChildFiles() {
        return childFiles;
    }

    public HttpSession GetSession() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest request =
            (HttpServletRequest)ctx.getExternalContext().getRequest();
        HttpSession session = request.getSession(true);
        return session;
    }

    public void setBindSearchNameInternalUser(RichInputText bindSearchNameInternalUser) {
        this.bindSearchNameInternalUser = bindSearchNameInternalUser;
    }

    public RichInputText getBindSearchNameInternalUser() {
        return bindSearchNameInternalUser;
    }

    public void setBtnSearchInternalUser(RichCommandButton btnSearchInternalUser) {
        this.btnSearchInternalUser = btnSearchInternalUser;
    }

    public RichCommandButton getBtnSearchInternalUser() {
        return btnSearchInternalUser;
    }

    public void searchInternalUser(ActionEvent actionEvent) {
        ApplicationModule app =
            ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject voParIn = app.findViewObject("UserInternalVO1");
        voParIn.setWhereClause("upper(FULLNAME) like upper('%" +
                               bindSearchNameInternalUser.getValue().toString() +
                               "%')");
        voParIn.executeQuery();
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableInternalUser);
    }

    public void setBindTableInternalUser(RichTable bindTableInternalUser) {
        this.bindTableInternalUser = bindTableInternalUser;
    }

    public RichTable getBindTableInternalUser() {
        return bindTableInternalUser;
    }

    public void setBindSelectedInternalUsername(RichColumn bindSelectedInternalUsername) {
        this.bindSelectedInternalUsername = bindSelectedInternalUsername;
    }

    public RichColumn getBindSelectedInternalUsername() {
        return bindSelectedInternalUsername;
    }

    public void setBindPopupInternalUser(RichPopup bindPopupInternalUser) {
        this.bindPopupInternalUser = bindPopupInternalUser;
    }

    public RichPopup getBindPopupInternalUser() {
        return bindPopupInternalUser;
    }

    public void setDialogLsnrSelectInternalUser(RichDialog dialogLsnrSelectInternalUser) {
        this.dialogLsnrSelectInternalUser = dialogLsnrSelectInternalUser;
    }

    public RichDialog getDialogLsnrSelectInternalUser() {
        return dialogLsnrSelectInternalUser;
    }

    public void DialogLsnrSelectInternalUser(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().equals(DialogEvent.Outcome.ok)) {
            bindSVName.setValue(bindSelectUserFullName.getValue().toString());
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindSVName);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableInternalUser);
        }
    }

    public void email(String EmailTo, String Subject, String HtmlText) {
        BindingContext bindingctx = BindingContext.getCurrent();
        System.out.print("\nbindingctx = " +
                         bindingctx.getCurrentDataControlFrame().toString());
        BindingContainer bindings = bindingctx.getCurrentBindingsEntry();
        OperationBinding sendEmail = bindings.getOperationBinding("SendEmail");
        System.out.print("\nsendEmail = " + sendEmail);
        Map paramEmail = sendEmail.getParamsMap();
        paramEmail.put("EmailTo", EmailTo);
        paramEmail.put("Subject", Subject);
        paramEmail.put("HtmlText", HtmlText);
        try {
            sendEmail.execute();
            log.log(idrequest, "Send Email",
                    "Sender : " + UserloginFullName + ", Reciever : " +
                    EmailTo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void emailWithCC(String EmailTo, String EmailCc, String Subject,
                            String HtmlText) {
        BindingContext bindingctx = BindingContext.getCurrent();
        System.out.print("\nbindingctx = " +
                         bindingctx.getCurrentDataControlFrame().toString());
        BindingContainer bindings = bindingctx.getCurrentBindingsEntry();
        OperationBinding sendEmail =
            bindings.getOperationBinding("SendEmailTo");
        System.out.print("\nsendEmail = " + sendEmail);
        Map paramEmail = sendEmail.getParamsMap();
        paramEmail.put("EmailTo", EmailTo);
        paramEmail.put("EmailCc", EmailCc);
        paramEmail.put("Subject", Subject);
        paramEmail.put("HtmlText", HtmlText);
        try {
            sendEmail.execute();
            log.log(idrequest, "Send Email",
                    "Sender : " + UserloginFullName + ", Reciever : " +
                    EmailTo + ", CC : " + EmailCc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void btnSearchInternalUser(ActionEvent actionEvent) {
        // Add event code here...
        System.out.print(actionEvent);
    }

    public void setBindSelectedInternalFullname(RichColumn bindSelectedInternalFullname) {
        this.bindSelectedInternalFullname = bindSelectedInternalFullname;
    }

    public RichColumn getBindSelectedInternalFullname() {
        return bindSelectedInternalFullname;
    }

    public void setBindSelectSVFullname(RichInputText bindSelectSVFullname) {
        this.bindSelectSVFullname = bindSelectSVFullname;
    }

    public RichInputText getBindSelectSVFullname() {
        return bindSelectSVFullname;
    }

    public void setBindSelectUserFullName(RichInputText bindSelectUserFullName) {
        this.bindSelectUserFullName = bindSelectUserFullName;
    }

    public RichInputText getBindSelectUserFullName() {
        return bindSelectUserFullName;
    }

    public void setBindSelectUserEmail(RichInputText bindSelectUserEmail) {
        this.bindSelectUserEmail = bindSelectUserEmail;
    }

    public RichInputText getBindSelectUserEmail() {
        return bindSelectUserEmail;
    }

    public void setBindSelectUserName(RichInputText bindSelectUserName) {
        this.bindSelectUserName = bindSelectUserName;
    }

    public RichInputText getBindSelectUserName() {
        return bindSelectUserName;
    }

    public void setBindSVName(RichInputText bindSVName) {
        this.bindSVName = bindSVName;
    }

    public RichInputText getBindSVName() {
        return bindSVName;
    }

    public void setBindSVLabel(RichInputText bindSVLabel) {
        this.bindSVLabel = bindSVLabel;
    }

    public RichInputText getBindSVLabel() {
        return bindSVLabel;
    }

    public void setBindPurposeLabel(RichInputText bindPurposeLabel) {
        this.bindPurposeLabel = bindPurposeLabel;
    }

    public RichInputText getBindPurposeLabel() {
        return bindPurposeLabel;
    }

    public void GoToHome(ActionEvent actionEvent) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        try {
            ctx.getExternalContext().redirect(getIDC_SERVER()+"?IdcService=GET_DOC_PAGE&Action=GetTemplatePage&Page=HOME_PAGE");
            //    ctx.getExternalContext().redirect("http://kponwjap005.pertamina.com:16300/urm/idcplg?IdcService=GET_DOC_PAGE&Action=GetTemplatePage&Page=HOME_PAGE");

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print(actionEvent);
    }

    public void gotoInbox() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        try {
            // ctx.getExternalContext().redirect("http://kponwjap005.pertamina.com:7070/CID/faces/pages/request.jspx?username=" +Userlogin);
            ctx.getExternalContext().redirect(getURL_CID()+"/CID/faces/pages/request.jspx?username=" +
                                              Userlogin);
            // ctx.getExternalContext().redirect("http://127.0.0.1:7101/CID/faces/pages/request.jspx");
        } catch (IOException e) {
            e.printStackTrace();
        }
        refreshTable();
    }

    public void GoToCID(ActionEvent actionEvent) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        try {
            ctx.getExternalContext().redirect(getURL_CID()+"/CID/faces/pages/request.jspx?username=" +
                                              Userlogin);
            //ctx.getExternalContext().redirect("http://kponwjis013.pertamina.com:7070/CID/faces/pages/request.jspx?username=" +Userlogin);
            // ctx.getExternalContext().redirect("http://127.0.0.1:7101/CID/faces/pages/request.jspx");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print(actionEvent);
    }

    public void setBindSend(RichCommandButton bindSend) {
        this.bindSend = bindSend;
    }

    public RichCommandButton getBindSend() {
        return bindSend;
    }

    public void sendRequest(ActionEvent actionEvent) {
        System.out.print(actionEvent);
        String docStatus = "";
        String status =
            getValueInDB("AppModuleDataControl", "PheCidListrequestVO1",
                         "IDREQUEST = '" + idrequest + "'",
                         "Cidstatusrequest");
        if ((status.equals("TDC Review")) ||
            (status.equals("Approve TDC Lead")) ||
            (status.equals("Approve SPV")) ||
            (status.equals("Requested")) ||
            (status.equals("Reject TDC Lead With Remarks"))) {
            if(status.equals("Reject TDC Lead With Remarks")){
                update("req", "Requested",idrequest); 
                update("doc", "Requested", idrequest);
                emailSubmit("single", "Cidrequestorsupervisor");
//                RichPopup.PopupHints ph = new RichPopup.PopupHints();
//                bindPopupSend.show(ph);
            }
            bindSendRequest.setVisible(false);
            refreshTable();
        } else if ((status.equals("Draft")) ||
            (status.equals("Reject SPV With Remarks"))) {
            if (status.equals("Draft")) {
                update("req", "Requested",
                       idrequest); //saat pertama kali buat request CID
                       RichPopup.PopupHints ph = new RichPopup.PopupHints();
                       bindPopupSend.show(ph);
            }
            try {
                docStatus = bindDocStatus.getValue().toString();
                System.out.print("\ndocStatus = " + docStatus);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if ((docStatus.equals("Draft")) ||
                (docStatus.equals("Reject SPV With Remarks"))) {
                update("doc", "Requested", idrequest);
                log.log(idrequest, "Update doc",
                        "User : " + UserloginFullName + " - " +
                        "(Reject SPV With Remarks) or (Draft) change to (Requested)");
                RichPopup.PopupHints ph = new RichPopup.PopupHints();
                bindPopupSend.show(ph);
            }
            if ((reqStatus.equals("Draft")) ||
                (reqStatus.equals("Reject SPV With Remarks"))) {
                update("req", "Requested", idrequest);
                log.log(idrequest, "Update Request",
                        "User : " + UserloginFullName + " - " +
                        "(Reject SPV With Remarks) or (Draft) change to (Requested)");
                RichPopup.PopupHints ph = new RichPopup.PopupHints();
                bindPopupSend.show(ph);
            }
            log.log(idrequest, "Request Send",
                    "User : " + UserloginFullName + " - " +
                    "Request CID has been requested");
            commit();
            refresh();
            String statusRequest =
                getValueInDB("AppModuleDataControl", "PheCidListrequestVO1",
                             "IDREQUEST = '" + idrequest + "'",
                             "Cidstatusrequest");
            if ((statusRequest.equals("Requested")) ||
                (statusRequest.equals("Reject SPV With Remarks"))) {
                emailSubmit("single", "Cidrequestorsupervisor");
                RichPopup.PopupHints ph = new RichPopup.PopupHints();
                bindPopupSend.show(ph);
            } else if ((statusRequest.equals("Approve SPV")) ||
                       (statusRequest.equals("Reject TDC Review With Remarks"))) {
                ApplicationModule am =
                    ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
                ViewObject voUserTDCReview =
                    am.findViewObject("UsersecurityattributesVO1");
                voUserTDCReview.setWhereClause("upper (dattributetype)=upper('role') and upper(dattributename)=upper('CID_"+getRoleTdcReview().trim()+"_REVIEW')");
                voUserTDCReview.executeQuery();
                // String countUserTDCReview = voUserTDCReview.getEstimatedRowCount() + "";
                String first = "";
                String tdcDusername = "";
                String tdcFullname = "";
                while (voUserTDCReview.hasNext()) {
                    Row row = null;
                    if (first.equals("")) {
                        row = voUserTDCReview.first();
                        first = "1";
                    } else {
                        row = voUserTDCReview.next();
                    }
                    try {
                        tdcDusername =
                                row.getAttribute("Dusername").toString();
                        tdcFullname =
                                getValueInDB("AppModuleDataControl", "UsersVO1",
                                             "DNAME = '" + tdcDusername + "'",
                                             "Dfullname");
                        emailSubmit("multiple", tdcFullname);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // untuk TDC Review harus define dengan banyak user
            } else if ((statusRequest.equals("Approve SPV")) ||
                       (statusRequest.equals("TDC Review")) ||
                       (statusRequest.equals("Reject TDC Lead With Remarks"))) {
                        if(statusRequest.equals("Reject TDC Lead With Remarks")){
                            update("req", "Requested",idrequest); 
                            update("doc", "Requested", idrequest);
                            emailSubmit("single", "Cidrequestorsupervisor");
                        }
                        
                    emailSubmit("single", "Cidtdclead");

                RichPopup.PopupHints ph = new RichPopup.PopupHints();
                bindPopupSend.show(ph);
            }
        }
    }

    public void emailSubmit(String people, String to) {
        String toUser = ""; // user full name
        if (people.equals("single")) {
            toUser =
                    getValueInDB("AppModuleDataControl", "PheCidListrequestVO1",
                                 "IDREQUEST = '" + idrequest + "'", to);
        } else {
            toUser = to;
        }
        String EmailAddress =
            getValueInDB("AppModuleDataControl", "UsersVO1", "DFULLNAME = '" +
                         toUser + "'", "Demail");
        String username =
            getValueInDB("AppModuleDataControl", "UsersVO1", "DFULLNAME = '" +
                         toUser + "'", "Dname");
        System.out.print("\nCidrequestorsupervisor = " + toUser);
        System.out.print("\nEmailAddress = " + EmailAddress);

        ApplicationModule am =
            ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
        ViewObject voGetPar = am.findViewObject("PheCidListdocrequestVO1");
        voGetPar.setWhereClause("IDREQUEST = '" + idrequest + "'");
        voGetPar.executeQuery();

        ViewObject voGetParReq = am.findViewObject("PheCidListrequestVO1");
        voGetParReq.setWhereClause("IDREQUEST = '" + idrequest + "'");
        voGetParReq.executeQuery();
        //Row rowReq = voGetParReq.first();
        String countDocList = voGetPar.getEstimatedRowCount() + "";

        String first = "";
        // String Ciddocnumber = "";
        String Ciddocname = "";
        String Ciddoctitle = "";
        String documents = "";
        String purpose = "";
        try {
            Row row = voGetParReq.first();
            purpose = row.getAttribute("Cidpurpose").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (countDocList.equals("1")) {
            Row row = voGetPar.first();
            try {
                //Ciddocnumber = row.getAttribute("Ciddocnumber").toString();
                String DID = row.getAttribute("Diddoc").toString();
                Ciddocname =
                        getValueInDB("AppModuleDataControl", "DocmetaVO1", "DID = '" +
                                     DID + "'", "Xdocname");
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Ciddoctitle = row.getAttribute("Ciddoctitle").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            documents =
                    "<tr><th style=\"background-color:gainsboro;\">" + Ciddocname +
                    "</th><th></th><th style=\"background-color:gainsboro;\">" +
                    Ciddoctitle + "</th></tr>";
        } else {
            while (voGetPar.hasNext()) {
                Row row = null;
                if (first.equals("")) {
                    row = voGetPar.first();
                    first = "1";
                } else {
                    row = voGetPar.next();
                }
                try {
                    //Ciddocnumber = row.getAttribute("Ciddocnumber").toString();
                    String DID = row.getAttribute("Diddoc").toString();
                    Ciddocname =
                            getValueInDB("AppModuleDataControl", "DocmetaVO1",
                                         "DID = '" + DID + "'", "Xdocname");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Ciddoctitle = row.getAttribute("Ciddoctitle").toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                documents =
                        documents + "<tr><th style=\"background-color:gainsboro;\">" +
                        Ciddocname +
                        "</th><th></th><th style=\"background-color:gainsboro;\">" +
                        Ciddoctitle + "</th></tr>";
            }
        }
        String body = "<html>\n" +
            "<head></head>\n" +
            "<body style=\"font-family:arial\">Dear Sir/Madam<br/><br/>You have received a CID request from <b>" +
            UserloginFullName +
            "</b>  for following approved document(s):<br/><br/>\n<table>" +
            "<tr><th style=\"text-align:center;background-color:khaki;padding:8px 10px;\" align=\"center\">Document Name</th><th></th><th style=\"text-align:center;background-color:khaki;padding:8px 10px;\" align=\"center\">Document Title</th></tr>" +
            documents + "</table><br/><br/>" +
            "Please see click this <a href=" +getIDC_SERVER()+
            "?IdcService=PHE_LIST_CID&startRow=1&endRow=100&keyword=request&username=" +
            username + "&numcid=" +
            // "http://kponwjis013.pertamina.com:7070/CID/faces/pages/inboxApproval.jspx?username=" +
            //"http://kponwjap005.pertamina.com:7070/CID/faces/pages/inboxApproval.jspx?username="
            //username +
            ">link</a> to open CID request file(s) into the system." +
            "<br/>Thank you for your attention.</body></html>";


        email(EmailAddress, "Request CID : " + purpose, body);
        //bikin email body
    }
    
    public String getValueInDB2(String module, String datacontrolVO,
                               String whereClause) {
        ApplicationModule am =
            ADFUtils.getApplicationModuleForDataControl(module);
        ViewObject svname = am.findViewObject(datacontrolVO);
        svname.setNamedWhereClauseParam("NAMA",whereClause);
        svname.executeQuery();
        String value = "";
            if(svname.getEstimatedRowCount()>0){
                value = "ada";
            }else{
                value = "tidakada";
            }
        return value;
    }
    
    public String getValueInDB(String module, String datacontrolVO,
                               String whereClause, String getAttributeName) {
        ApplicationModule am =
            ADFUtils.getApplicationModuleForDataControl(module);
        ViewObject svname = am.findViewObject(datacontrolVO);
        svname.setWhereClause(whereClause);
        svname.executeQuery();

        String value = "";
        Row row = null;
        String first = "";
        try {
            row = svname.first();
            value = row.getAttribute(getAttributeName).toString();
        } catch (Exception e) {
        }
        while (svname.hasNext()) {
            if (first.equals("")) {
                row = svname.first();
                first = "1";
            } else {
                row = svname.next();
            }
            value = row.getAttribute(getAttributeName).toString();

        }
        //System.out.print("\nvalue = " + value);
        // log.log(idrequest, "getValueInDB", value);
        return value;
    }

    public void setBindTextStatusDocRequest(RichOutputText bindTextStatusDocRequest) {
        this.bindTextStatusDocRequest = bindTextStatusDocRequest;
    }

    public RichOutputText getBindTextStatusDocRequest() {
        return bindTextStatusDocRequest;
    }

    public void update(String method, String status, String idrequest) {
        Connection dbCon = null;
        Statement stat = null;
        String UpdateQuery = "";
        //ResultSet result = null;
        try {
            //DEV
            dbCon =
            DriverManager.getConnection(getUrl_Database(),getUser_Database(), getPass_Database());
//                    DriverManager.getConnection("jdbc:oracle:thin:@10.252.4.193:1521:OWCDEV",
//                                                "DEV_URMSERVER", "welcome1");

            //dbCon = DriverManager.getConnection(
            //      "jdbc:oracle:thin:@10.252.4.112:1521:owcprod",
            //    "owc_staging", "staging2014owc");
        } catch (SQLException e) {
            e.printStackTrace();
            if (dbCon != null) {
                System.out.println("Connected to Database!");
            } else {
                System.out.println("Failed to make connection!");
            }
            return;
        }

        try {
            if (method.equals("doc")) {
                UpdateQuery =
                        "update phe_cid_listdocrequest set cidstatusdocrequest='" +
                        status + "' where idrequest=" + idrequest;
            } else if (method.equals("req")) {
                UpdateQuery =
                        "update phe_cid_listrequest set cidstatusrequest='" +
                        status + "' where idrequest=" + idrequest;
            } else if (method.equals("attach")) {
                UpdateQuery =
                        "update phe_cid_create set attachphysical='" + status +
                        "' where idrequest=" + idrequest;
            } else if (method.equals("publish")) {
                UpdateQuery =
                        "update phe_cid_create set cidstatuscid='" + status +
                        "' where idrequest=" + idrequest;
            } else if (method.equals("publishRequest")) {
                UpdateQuery =
                        "update phe_cid_listrequest set cidstatusrequest='" +
                        status + "' where idrequest=" + idrequest;
            }
            System.out.println("\nQuery = " + UpdateQuery);
            stat = dbCon.createStatement();
            stat.executeUpdate(UpdateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            stat.close();
            dbCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String select(String Diddoc, String idrequest) {
        Connection dbCon = null;
        Statement stat = null;
        String SelectQuery = "";
        String value = "";
        ResultSet result = null;
        try {
            //DEV
            dbCon = DriverManager.getConnection(getUrl_Database(),getUser_Database(), getPass_Database());
//                    DriverManager.getConnection("jdbc:oracle:thin:@10.252.4.193:1521:OWCDEV",
//                                                "DEV_URMSERVER", "welcome1");

            //dbCon = DriverManager.getConnection(
            //      "jdbc:oracle:thin:@10.252.4.112:1521:owcprod",
            //    "owc_staging", "staging2014owc");
        } catch (SQLException e) {
            e.printStackTrace();
            if (dbCon != null) {
                System.out.println("Connected to Database!");
            } else {
                System.out.println("Failed to make connection!");
            }
        }

        try {
            SelectQuery =
                    "select idlist as IDLIST from phe_cid_listdocrequest where idRequest='" +
                    idrequest + "' and diddoc='" + Diddoc + "'";
            //System.out.println("\nQuery = " + SelectQuery);
            stat = dbCon.createStatement();
            result = stat.executeQuery(SelectQuery);
            while (result.next()) {
                value = result.getString("IDLIST") + "";
            }
            //System.out.println("value = "+value);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            stat.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            dbCon.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return value;
    }

    public void setBindPopupCIDSend(RichPopup bindPopupCIDSend) {
        this.bindPopupCIDSend = bindPopupCIDSend;
    }

    public RichPopup getBindPopupCIDSend() {
        return bindPopupCIDSend;
    }

    public void refresh() {
        bindTable = new RichTable();
        bindTableCreateRequest = new RichTable();
        bindTableAttach = new RichTable();
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableAttach);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindTable);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableCreateRequest);
    }

    public void commit() {
        DCBindingContainer bindings =
            (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        OperationBinding method = bindings.getOperationBinding("Commit");
        method.execute();
    }

    public String bindDeleteRequest() {
        System.out.print("===Testing====");
        DCIteratorBinding dci =
            ADFUtils.findIterator("PheCidListdocrequestVO1Iterator");
        System.out.println("DCI COUNT: " + dci.getEstimatedRowCount());
        
        ADFContext actx = ADFContext.getCurrent();
        Map pfscount = actx.getPageFlowScope();
        Object var = pfscount.put("doccount", dci.getEstimatedRowCount());
        
        commit();
        refresh();
        // Add event code here...
        return null;
    }

    public void setBindNextFlow(RichCommandButton bindNextFlow) {
        this.bindNextFlow = bindNextFlow;
    }

    public RichCommandButton getBindNextFlow() {
        return bindNextFlow;
    }

    public void setBindPopupNoItem(RichPopup bindPopupNoItem) {
        this.bindPopupNoItem = bindPopupNoItem;
    }

    public RichPopup getBindPopupNoItem() {
        return bindPopupNoItem;
    }

    public void setBindPopupSelectedItem(RichPopup bindPopupSelectedItem) {
        this.bindPopupSelectedItem = bindPopupSelectedItem;
    }

    public RichPopup getBindPopupSelectedItem() {
        return bindPopupSelectedItem;
    }

    public void setBindRequestCIDRequestorPosition(RichInputText bindRequestCIDRequestorPosition) {
        this.bindRequestCIDRequestorPosition = bindRequestCIDRequestorPosition;
    }

    public RichInputText getBindRequestCIDRequestorPosition() {
        return bindRequestCIDRequestorPosition;
    }

    public void setBindPopupFillForm(RichPopup bindPopupFillForm) {
        this.bindPopupFillForm = bindPopupFillForm;
    }

    public RichPopup getBindPopupFillForm() {
        return bindPopupFillForm;
    }

    public void setPopupRequestCreated(RichPopup popupRequestCreated) {
        this.popupRequestCreated = popupRequestCreated;
    }

    public RichPopup getPopupRequestCreated() {
        return popupRequestCreated;
    }

    public void setBindPopupRequestCreated(RichPopup bindPopupRequestCreated) {
        this.bindPopupRequestCreated = bindPopupRequestCreated;
    }

    public RichPopup getBindPopupRequestCreated() {
        return bindPopupRequestCreated;
    }

    public void setBindRequestCIDPurpose(RichInputText bindRequestCIDPurpose) {
        this.bindRequestCIDPurpose = bindRequestCIDPurpose;
    }

    public RichInputText getBindRequestCIDPurpose() {
        return bindRequestCIDPurpose;
    }

    public void setBindRequestCIDSVPosition(RichInputText bindRequestCIDSVPosition) {
        this.bindRequestCIDSVPosition = bindRequestCIDSVPosition;
    }

    public RichInputText getBindRequestCIDSVPosition() {
        return bindRequestCIDSVPosition;
    }

    public void setBindRequestCIDDateRequest(RichInputDate bindRequestCIDDateRequest) {
        this.bindRequestCIDDateRequest = bindRequestCIDDateRequest;
    }

    public RichInputDate getBindRequestCIDDateRequest() {
        return bindRequestCIDDateRequest;
    }

    public void setBindSubmitForm(RichCommandButton bindSubmitForm) {
        this.bindSubmitForm = bindSubmitForm;
    }

    public RichCommandButton getBindSubmitForm() {
        return bindSubmitForm;
    }

    public void bindEditButton(ActionEvent actionEvent) {
        String Cidstatusrequest =
            getValueInDB("AppModuleDataControl", "PheCidListrequestVO1",
                         "Idrequest= '" + idrequest + "'", "Cidstatusrequest");
        System.out.println("Cidstatusrequest btn edit "+Cidstatusrequest);
        if ((Cidstatusrequest.equals("TDC Review")) ||
            (Cidstatusrequest.equals("Approve TDC Lead")) ||
            (Cidstatusrequest.equals("Approve SPV")) ||
            (Cidstatusrequest.equals("Requested"))) {
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            bindPopupValidationDocList.show(ph);
            refreshTable();
        } else {
            bindIteratorRowCount.setVisible(false);
            bindFormRequestEDMS.setVisible(true);
            bindCancelInputRequestCID.setVisible(false);
            bindCancelEditRequestCID.setVisible(true); //Cancel Edit
            bindSaveDocRequest.setVisible(true); // Add To List
            bindAddtoList.setVisible(false);
            System.out.print(actionEvent);
            bindFinishDocList.setVisible(false);
            bindUpdate.setVisible(true);
            bindTabPage.setVisible(true);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindTabPage);
            buttonDocListOFF();

        }
    }

    public void bindDeleteButton(ActionEvent actionEvent) {

        String Cidstatusrequest =
            getValueInDB("AppModuleDataControl", "PheCidListrequestVO1",
                         "Idrequest= '" + idrequest + "'", "Cidstatusrequest");
        System.out.println("Cidstatusrequest del "+Cidstatusrequest);
        if ((Cidstatusrequest.equals("TDC Review")) ||
            (Cidstatusrequest.equals("Approve TDC Lead")) ||
            (Cidstatusrequest.equals("Approve SPV")) ||
            (Cidstatusrequest.equals("Requested"))) {
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            bindPopupValidationDel.show(ph);
            refreshTable();
        } else {
            RichPopup.PopupHints dh = new RichPopup.PopupHints();
            bindPopupValidationDelList.show(dh);
            bindIteratorRowCount.setVisible(false);
        }
    }

    public void bindDeleteButtonRequest(ActionEvent actionEvent) {
        //validationEdit
        String Cidstatusrequest = bindStatusRequest.getValue().toString();
        System.out.println("Cidstatusrequest del request "+Cidstatusrequest);
        if ((Cidstatusrequest.equals("TDC Review")) ||
            (Cidstatusrequest.equals("Approve TDC Lead")) ||
            (Cidstatusrequest.equals("Approve SPV")) ||
            (Cidstatusrequest.equals("Requested"))) {
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            bindPopupValidationDelCreate.show(ph);
        } else {
            RichPopup.PopupHints dh = new RichPopup.PopupHints();
            bindpopupDeleteRequestList.show(dh);
        }
    }

    public void setBindUpdate(RichCommandButton bindUpdate) {
        this.bindUpdate = bindUpdate;
    }

    public RichCommandButton getBindUpdate() {
        return bindUpdate;
    }

    public void bindUpdateButtonRequest(ActionEvent actionEvent) {
        //validationEdit
        String Cidstatusrequest = bindStatusRequest.getValue().toString();
         System.out.println("Cidstatusrequest update "+Cidstatusrequest);
        if ((Cidstatusrequest.equals("Draft")) ||
            (Cidstatusrequest.equals("Reject SPV With Remarks")) ||
            (Cidstatusrequest.equals("Reject TDC Lead With Remarks"))) {
            System.out.print(actionEvent);
            bindCancelEditRequest.setVisible(true);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindCancelEditRequest);
            bindCancelInputRequest.setVisible(false);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindCancelInputRequest);
            bindCreateRequest.setVisible(false);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindCreateRequest);
            //bindSaveButton.setVisible(true);
            //AdfFacesContext.getCurrentInstance().addPartialTarget(bindSaveButton);
            bindUpdateButton.setVisible(true);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindUpdateButton);
            bindFromRequest.setVisible(true);
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindFromRequest);
            buttonReqListOFF();
            refreshTableRequest();
        } else {
            RichPopup.PopupHints ph = new RichPopup.PopupHints();
            bindPopupValidationEdit.show(ph);
        }
    }

    public String bindSaveButtonRequest() {
        // Add event code here...
        bindTableRequest = new RichTable();
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableRequest);
        bindSaveButton.setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindSaveButton);
        bindUpdateButton.setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindUpdateButton);
        bindFromRequest.setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindFromRequest);
        buttonReqListOn();
        return null;
    }

    public void ResetTree() {
        System.out.println("reset tree");
        childFiles.clear();
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableFilesEDMS);
        CollapseAllNodes();
        try {
            bindTreeEdms.getSelectedRowKeys().removeAll();
            AdfFacesContext.getCurrentInstance().addPartialTarget(bindTreeEdms);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void CollapseAllNodes() {
        UIXTree tree = getBindTreeEdms();
        RowKeySet _disclosedRowKeys = tree.getDisclosedRowKeys();
        if (_disclosedRowKeys != null && _disclosedRowKeys.size() > 0) {
            _disclosedRowKeys.clear();
        }
        tree.setDisclosedRowKeys(_disclosedRowKeys);
    }

    public void setBindSaveButton(RichCommandButton bindSaveButton) {
        this.bindSaveButton = bindSaveButton;
    }

    public RichCommandButton getBindSaveButton() {
        return bindSaveButton;
    }

    public void setBindUpdateButton(RichCommandButton bindUpdateButton) {
        this.bindUpdateButton = bindUpdateButton;
    }

    public RichCommandButton getBindUpdateButton() {
        return bindUpdateButton;
    }

    public void setBindCreateButton(RichCommandButton bindCreateButton) {
        this.bindCreateButton = bindCreateButton;
    }

    public RichCommandButton getBindCreateButton() {
        return bindCreateButton;
    }


    public void setBindRemarksRequestor(RichInputText bindRemarksRequestor) {
        this.bindRemarksRequestor = bindRemarksRequestor;
    }

    public RichInputText getBindRemarksRequestor() {
        return bindRemarksRequestor;
    }

    public void setBindTableHistory(RichPanelCollection bindTableHistory) {
        this.bindTableHistory = bindTableHistory;
    }

    public RichPanelCollection getBindTableHistory() {
        return bindTableHistory;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }


    public void setWeblogicusername(String weblogicusername) {
        this.weblogicusername = weblogicusername;
    }

    public String getWeblogicusername() {
        return weblogicusername;
    }

    public void setWeblogicpassword(String weblogicpassword) {
        this.weblogicpassword = weblogicpassword;
    }

    public String getWeblogicpassword() {
        return weblogicpassword;
    }

    public void bindHistoryButton(ActionEvent actionEvent) {
    }

    public void setBindUploadFile(RichInputFile bindUploadFile) {
        this.bindUploadFile = bindUploadFile;
    }

    public RichInputFile getBindUploadFile() {
        return bindUploadFile;
    }

    public void setBindRadioEDMS(RichSelectBooleanRadio bindRadioEDMS) {
        this.bindRadioEDMS = bindRadioEDMS;
    }

    public RichSelectBooleanRadio getBindRadioEDMS() {
        return bindRadioEDMS;
    }

    public void setBindFormRequestEDMS(RichPanelGroupLayout bindFormRequestEDMS) {
        this.bindFormRequestEDMS = bindFormRequestEDMS;
    }

    public RichPanelGroupLayout getBindFormRequestEDMS() {
        return bindFormRequestEDMS;
    }

    public void setBindDidDocEDMS(RichInputText bindDidDocEDMS) {
        this.bindDidDocEDMS = bindDidDocEDMS;
    }

    public RichInputText getBindDidDocEDMS() {
        return bindDidDocEDMS;
    }

    public void setBindDocNumberEDMS(RichInputText bindDocNumberEDMS) {
        this.bindDocNumberEDMS = bindDocNumberEDMS;
    }

    public RichInputText getBindDocNumberEDMS() {
        return bindDocNumberEDMS;
    }

    public void setBindDocTitleEDMS(RichInputText bindDocTitleEDMS) {
        this.bindDocTitleEDMS = bindDocTitleEDMS;
    }

    public RichInputText getBindDocTitleEDMS() {
        return bindDocTitleEDMS;
    }

    public void setBindDocSourceEDMS(RichInputText bindDocSourceEDMS) {
        this.bindDocSourceEDMS = bindDocSourceEDMS;
    }

    public RichInputText getBindDocSourceEDMS() {
        return bindDocSourceEDMS;
    }

    public void setBindStatusDocRequestEDMS(RichInputText bindStatusDocRequestEDMS) {
        this.bindStatusDocRequestEDMS = bindStatusDocRequestEDMS;
    }

    public RichInputText getBindStatusDocRequestEDMS() {
        return bindStatusDocRequestEDMS;
    }

    public void setBindAttachFile(RichInputFile bindAttachFile) {
        this.bindAttachFile = bindAttachFile;
    }

    public RichInputFile getBindAttachFile() {
        return bindAttachFile;
    }

    public void onChangeAttachFile(ValueChangeEvent valueChangeEvent) throws IOException {
        // Add event code here...
        file = (UploadedFile)valueChangeEvent.getNewValue();

        fileLength = file.getLength();
        fileInputStream = file.getInputStream();
        fileName = file.getFilename();
        fileContentType = file.getContentType();

        System.out.print("\nfileLength = " + fileLength);
        System.out.print("\nfileInputStream =" + fileInputStream);
        System.out.print("\nfileName = " + fileName);
        System.out.print("\nfileContentType =" + fileContentType);
    }

    public void setBindAttachFormIdrequest(RichInputText bindAttachFormIdrequest) {
        this.bindAttachFormIdrequest = bindAttachFormIdrequest;
    }

    public RichInputText getBindAttachFormIdrequest() {
        return bindAttachFormIdrequest;
    }

    public void setBindAttachFormDate(RichInputDate bindAttachFormDate) {
        this.bindAttachFormDate = bindAttachFormDate;
    }

    public RichInputDate getBindAttachFormDate() {
        return bindAttachFormDate;
    }

    public void setBindAttachFormDID(RichInputText bindAttachFormDID) {
        this.bindAttachFormDID = bindAttachFormDID;
    }

    public RichInputText getBindAttachFormDID() {
        return bindAttachFormDID;
    }

    public void setBindAttachFormDocName(RichInputText bindAttachFormDocName) {
        this.bindAttachFormDocName = bindAttachFormDocName;
    }

    public RichInputText getBindAttachFormDocName() {
        return bindAttachFormDocName;
    }

    public void setBindAttachFormDocFormat(RichInputText bindAttachFormDocFormat) {
        this.bindAttachFormDocFormat = bindAttachFormDocFormat;
    }

    public RichInputText getBindAttachFormDocFormat() {
        return bindAttachFormDocFormat;
    }

    public void setBindAttachFormDocSize(RichInputText bindAttachFormDocSize) {
        this.bindAttachFormDocSize = bindAttachFormDocSize;
    }

    public RichInputText getBindAttachFormDocSize() {
        return bindAttachFormDocSize;
    }

    public void setBindAttachFormStatus(RichInputText bindAttachFormStatus) {
        this.bindAttachFormStatus = bindAttachFormStatus;
    }

    public RichInputText getBindAttachFormStatus() {
        return bindAttachFormStatus;
    }

    public void setBindAttachFormFolderGuid(RichInputText bindAttachFormFolderGuid) {
        this.bindAttachFormFolderGuid = bindAttachFormFolderGuid;
    }

    public RichInputText getBindAttachFormFolderGuid() {
        return bindAttachFormFolderGuid;
    }

    public void setBindPopupInputAttach(RichPopup bindPopupInputAttach) {
        this.bindPopupInputAttach = bindPopupInputAttach;
    }

    public RichPopup getBindPopupInputAttach() {
        return bindPopupInputAttach;
    }

    public String bindUploadAttach() throws IOException {

        RIDCClass ridc =
            new RIDCClass(getWeblogicusername(), getWeblogicpassword());
        InputStream is = null;
        String dDID = "";
        String folderGuid = "";
        Timestamp date = null;
        try {
            is = fileInputStream;
            dDID =
ridc.CheckinCIDForm(is, fileName, fileLength, "Confidential Information Distribution/Attachment Documents");
            log.log(idrequest, "Check in",
                    "User : " + UserloginFullName + " - " +
                    "CheckIN Attch file CID to EDMS in folder Confidential Information Distribution/Attachment Documents and get dID : " +
                    dDID);
            System.out.print("\ndDID = " + dDID);
            date = new Timestamp(System.currentTimeMillis());
            DataObject ob =
                ridc.FolderInfo("Confidential Information Distribution/Attachment Documents");
            folderGuid = ob.get("fFolderGUID").toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IdcClientException e) {
            StackTraceElement[] element = e.getStackTrace();
            System.out.print(element);
        }
        bindAttachFormIdrequest.setValue(idrequest);
        bindAttachFormDate.setValue(date);
        bindAttachFormDID.setValue(dDID);
        bindAttachFormDocName.setValue(fileName);
        bindAttachFormDocFormat.setValue(fileContentType);
        bindAttachFormDocSize.setValue(fileLength);
        bindAttachFormStatus.setValue("Draft");
        bindAttachFormFolderGuid.setValue(folderGuid);
        bindAttachcoloum.setValue("Yes");
        bindAttachUploadForm.setValue("Yes");
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindAttachUploadForm);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindFormCreateRequest);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindAttachcoloum);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindAttachFile);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableAttach);

        RichPopup.PopupHints ph = new RichPopup.PopupHints();
        bindPopupInputAttach.show(ph);
        return null;

    }

    public void bindInputAttachOK(ActionEvent actionEvent) {
        // Add event code here...
        bindPopupAttachInput.cancel();
    }

    public void setBindPopupAttachInput(RichPopup bindPopupAttachInput) {
        this.bindPopupAttachInput = bindPopupAttachInput;
    }

    public RichPopup getBindPopupAttachInput() {
        return bindPopupAttachInput;
    }

    public String bindAttachSubmitForm() {
        // Add event code here...
        bindAttachFile.resetValue();
        bindAttachFile = new RichInputFile();
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindAttachFile);
        return null;
    }

    public void setBindIdRequestEDMS(RichInputText bindIdRequestEDMS) {
        this.bindIdRequestEDMS = bindIdRequestEDMS;
    }

    public RichInputText getBindIdRequestEDMS() {
        return bindIdRequestEDMS;
    }

    public void setBindTableAttach(RichTable bindTableAttach) {
        this.bindTableAttach = bindTableAttach;
    }

    public RichTable getBindTableAttach() {
        return bindTableAttach;
    }

    public void setBindAttachAdd(RichCommandButton bindAttachAdd) {
        this.bindAttachAdd = bindAttachAdd;
    }

    public RichCommandButton getBindAttachAdd() {
        return bindAttachAdd;
    }

    public void setBindAttachUpload(RichCommandButton bindAttachUpload) {
        this.bindAttachUpload = bindAttachUpload;
    }

    public RichCommandButton getBindAttachUpload() {
        return bindAttachUpload;
    }

    public String bindCheckForm() {
        String requestorPosition = "";
        String SVname = "";
        String dateRequest = "";
        String SVPoristion = "";
        String purpose = "";
      
        try {
            reqStatus = bindStatusRequest.getValue().toString();
            purpose = bindRequestCIDPurpose.getValue().toString();
            SVPoristion = bindRequestCIDSVPosition.getValue().toString();
            dateRequest = bindRequestCIDDateRequest.getValue().toString();
            SVname = bindSVName.getValue().toString();
            requestorPosition =
                    bindRequestCIDRequestorPosition.getValue().toString();
            ApplicationModule am =
                ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
           String apName=bindApNama.getValue().toString();
            ViewObject svname = am.findViewObject("ParentMenuVo1");
            svname.setNamedWhereClauseParam("groupName",apName.replaceAll("\\s+",""));
            svname.executeQuery();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        RichPopup.PopupHints pop = new RichPopup.PopupHints();
        if ((requestorPosition.equals("")) || (purpose.equals("")) ||
            (SVPoristion.equals("")) || (dateRequest.equals("")) ||
            (SVname.equals(""))) {
            bindPopupFillForm.show(pop);
        } else {
            bindPopupRequestCreated.show(pop);
        }
        buttonReqListOn();
        return null;
    }

    public void bindCancelEdit(ActionEvent actionEvent) {
        // Add event code here...
        buttonReqListOn();
        bindFromRequest.setVisible(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindFromRequest);
    }

    public void setBindCancelInputRequest(RichCommandButton bindCancelInputRequest) {
        this.bindCancelInputRequest = bindCancelInputRequest;
    }

    public RichCommandButton getBindCancelInputRequest() {
        return bindCancelInputRequest;
    }

    public void setBindCancelEditRequest(RichCommandButton bindCancelEditRequest) {
        this.bindCancelEditRequest = bindCancelEditRequest;
    }

    public RichCommandButton getBindCancelEditRequest() {
        return bindCancelEditRequest;
    }

    public void setBindAttachcoloum(RichInputText bindAttachcoloum) {
        this.bindAttachcoloum = bindAttachcoloum;
    }

    public RichInputText getBindAttachcoloum() {
        return bindAttachcoloum;
    }

    public void printDraftCID(String username, String password) {
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
        System.out.print("\ndate = " + sdf.format(new java.util.Date()));
        String numberCid = "Draft - " + sdf.format(new java.util.Date());
        try {
            System.out.print("\nprint idrequest  = " + idrequest);
            RIDCClass ridc = new RIDCClass(username, password);

            String numberFile = numberCid + " - " + UserloginFullName;
            System.out.print("\nnumberFile  = " + numberFile);


            File fileasli = null;
            log.log(idrequest, "Create File CID",
                    "User : " + UserloginFullName + " - " +
                    "find base template" + "for ID Request " + idrequest);
            //fileasli = new File("C:\\CID_temp\\PHEONWJ-CID-yyyymmdd-0000 - Requestor Name.xls");
            fileasli =
                    new File("D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\CID_temp\\PHEONWJ-CID-yyyymmdd-0000 - Requestor Name.xls");
            FileInputStream inp = null;
            //bikin atau edit template
            inp = new FileInputStream(fileasli);

            log.log(idrequest, "Create File CID",
                    "User : " + UserloginFullName + " - " +
                    "remake file CID from base template" + "for ID Request " +
                    idrequest);
            //System.out.print("inp = "+inp.available());
            HSSFWorkbook my_xls_workbook = new HSSFWorkbook(inp);

            HSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);
            Cell cell = null;
            ApplicationModule am =
                ADFUtils.getApplicationModuleForDataControl("AppModuleDataControl");
            ViewObject voGetPar = am.findViewObject("PheCidListdocrequestVO1");
            voGetPar.setWhereClause("IDREQUEST = '" + idrequest + "'");
            voGetPar.executeQuery();

            ViewObject voGetParReq = am.findViewObject("PheCidListrequestVO1");
            voGetPar.setWhereClause("IDREQUEST = '" + idrequest + "'");
            voGetPar.executeQuery();
            Row rowReq = voGetParReq.first();

            int i = 17;
            int j = 2;
            int r = 1;
            String first = "";
            String Ciddocnumber = "";
            String Ciddoctitle = "";
            String Cidremarkrequestor = "";
            String Cidcontractor = "";
            String Cidrequestor = "";
            String Cidrequestorsupervisor = "";
            String Cidrequestorsupervisorposition = "";
            String Cidrequestorposition = "";
            String Cidpurpose = "";
            String Cidtdclead = "";
            while (voGetPar.hasNext()) {
                Row row = null;
                if (first.equals("")) {
                    row = voGetPar.first();
                    first = "1";
                } else {
                    row = voGetPar.next();
                }

                // System.out.print("\nCiddocnumber = "+Ciddocnumber);
                System.out.print("\n list create = " + r);
                try {
                    Ciddocnumber = row.getAttribute("Ciddocnumber").toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Ciddoctitle = row.getAttribute("Ciddoctitle").toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    Cidremarkrequestor =
                            row.getAttribute("Cidremarkrequestor").toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (first.equals("1")) {
                        Cidcontractor =
                                row.getAttribute("Cidcontractor").toString();
                        first = "2";
                    } else {
                        Cidcontractor =
                                Cidcontractor + ", " + row.getAttribute("Cidcontractor").toString();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                cell = my_worksheet.getRow(i).getCell(j);
                cell.setCellValue(r + "");
                cell = my_worksheet.getRow(i).getCell(j + 1);
                cell.setCellValue(Ciddocnumber);
                cell = my_worksheet.getRow(i).getCell(j + 3);
                cell.setCellValue(Ciddoctitle);
                cell = my_worksheet.getRow(i).getCell(j + 5);
                cell.setCellValue(Cidremarkrequestor);
                i++;
                r++;
            }
            try {
                Cidpurpose = rowReq.getAttribute("Cidpurpose").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Cidtdclead = rowReq.getAttribute("Cidtdclead").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Cidrequestor = rowReq.getAttribute("Cidrequestor").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Cidrequestorposition =
                        rowReq.getAttribute("Cidrequestorposition").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Cidrequestorsupervisor =
                        rowReq.getAttribute("Cidrequestorsupervisor").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Cidrequestorsupervisorposition =
                        rowReq.getAttribute("Cidrequestorsupervisorposition").toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            cell = my_worksheet.getRow(6).getCell(5);
            cell.setCellValue(numberCid);
            cell = my_worksheet.getRow(7).getCell(5);
            cell.setCellValue(sdf.format(new java.util.Date()));
            cell = my_worksheet.getRow(8).getCell(5);
            cell.setCellValue(Cidrequestor);
            cell = my_worksheet.getRow(9).getCell(5);
            cell.setCellValue(Cidtdclead);
            cell = my_worksheet.getRow(29).getCell(5);
            cell.setCellValue(Cidpurpose);
            cell = my_worksheet.getRow(31).getCell(5);
            cell.setCellValue(Cidcontractor);

            cell = my_worksheet.getRow(42).getCell(2);
            cell.setCellValue(Cidtdclead);
            cell = my_worksheet.getRow(48).getCell(2);
            cell.setCellValue(Cidrequestorsupervisor);
            cell = my_worksheet.getRow(48).getCell(5);
            cell.setCellValue(Cidrequestorsupervisorposition);
            cell = my_worksheet.getRow(53).getCell(2);
            cell.setCellValue(Cidrequestor);
            cell = my_worksheet.getRow(53).getCell(5);
            cell.setCellValue(Cidrequestorposition);
            cell = my_worksheet.getRow(53).getCell(7);
            cell.setCellValue(Cidcontractor);


            //java.net.URL imgURL = getClass().getResource("Image/approve.png");
            // aboutFrame.setIconImage(new ImageIcon(imgURL).getImage());

            log.log(idrequest, "Create File CID",
                    "User : " + UserloginFullName + " - " +
                    "input data to template" + "for ID Request " + idrequest);
            inp.close();

            //FileOutputStream output_file = new FileOutputStream(new File("C:\\CID_temp\\" + numberFile +".xls"));
            FileOutputStream output_file =
                new FileOutputStream(new File("D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\CID_temp\\temp\\" +
                                              numberFile + ".xls"));
            //write changes
            my_xls_workbook.write(output_file);
            //close the stream
            output_file.close();
            log.log(idrequest, "Create File CID", "new file CID created");

            //uploadfile ke EDMS
            //File file = new File("C:\\CID_temp\\" + numberFile + ".xls");
            File file =
                new File("D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\CID_temp\\temp\\" +
                         numberFile + ".xls");
            InputStream is = null;
            try {
                is = new FileInputStream(file);

                String dDID =
                    ridc.CheckinCIDForm(is, file.getName(), file.length(),
                                        "Confidential Information Distribution/Draft Documents");
                log.log(idrequest, "Check in",
                        "CheckIN file CID to EDMS in folder Confidential Information Distribution/Supporting Documents and get dID : " +
                        dDID);
                System.out.print("\ndDID = " + dDID);
                inboxClass inboxClass = new inboxClass();
                inboxClass.insert(idrequest, numberCid, "SYSDATE", "Draft",
                                  getIDC_SERVER()+"?IdcService=VIEW_IN_AUTOVUE&dID=" +
                        //"http://kponwjap005.pertamina.com:16300/urm/idcplg?IdcService=VIEW_IN_AUTOVUE&dID="
                        dDID, "");
                // openfile(dDID);
                download(dDID);
                gotoInbox();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IdcClientException e) {
                StackTraceElement[] element = e.getStackTrace();
                System.out.print(element);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openfile(String dDID) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        try {
            ctx.getExternalContext().redirect(getIDC_SERVER()+"?IdcService=VIEW_IN_AUTOVUE&dID=" +
                                              dDID);
            //  ctx.getExternalContext().redirect("http://kponwjis013.pertamina.com:16300/urm/idcplg?IdcService=VIEW_IN_AUTOVUE&dID=" +dDID);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void download(String dDID) {
        FacesContext ctx = FacesContext.getCurrentInstance();
        try { //ctx.getExternalContext().redirect("http://kponwjap005.pertamina.com:16300/urm/idcplg?IdcService=GET_FILE&dID=" +dDID);
            ctx.getExternalContext().redirect(getIDC_SERVER()+"?IdcService=GET_FILE&dID=" +
                                              dDID);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public oracle.jbo.domain.Date getNowDate(java.util.Date pJavaDate) {
        return new oracle.jbo.domain.Date(new Timestamp(pJavaDate.getTime()));
    }

    public void printDraft(ActionEvent actionEvent) {
        // Add event code here...
        printDraftCID(getWeblogicusername(), getWeblogicpassword());
    }

    public void cancelEdit(ActionEvent actionEvent) {
        // Add event code here...
        bindTabPage.setVisible(false);
        bindCreateInserDocList.setVisible(true);
        bindFinishDocList.setVisible(true);
        buttonDocListOn();
        refreshTableRequest();
    }

    public void bindFinishInputList(ActionEvent actionEvent) {
        // Add event code here...
        bindFormRequestEDMS.setVisible(false);
    }

    public void contractorInput(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        try {
            RemarksRewrite = bindRemarksRewrite.getValue().toString();
        } catch (Exception e) {
        }
    }

    public void remarksInput(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        try {
            contractorRewrite = bindContractorRewrite.getValue().toString();
        } catch (Exception e) {
        }
    }

    public void userContractorInput(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        try {
            contractorUser = bindUserContractor.getValue().toString();
        } catch (Exception e) {
        }
    }

    public void selectSV(ActionEvent actionEvent) {
        // Add event code here...
        userSelected = "SPV";
    }

    public void selectInternalUser(ActionEvent actionEvent) {
        // Add event code here...
        userSelected = "internalUser";
    }


    public void backRequest(ActionEvent actionEvent) {
        // Add event code here...
        refresh();
    }


    public void selectUserInternal(ActionEvent actionEvent) {
        // Add event code here...
        bindSVName.setValue(bindSelectUserFullName.getValue().toString());
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindSVName);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableInternalUser);
    }

    public void tabEDMS(DisclosureEvent disclosureEvent) {
        // Add event code here...
        bindDocSource.setValue("Non EDMS");
        bindDocSourceEDMS.setValue("EDMS");
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("btnAddStat", "");
        bindAddtoList.setDisabled(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindAddtoList);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindDocStatus);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindStatusDocRequestEDMS);
    }

    public void tabNonEDMS(DisclosureEvent disclosureEvent) {
        // Add event code here...
        bindDocNumNonEDMS.setValue("");
        bindDocTitleNonEDMS.setValue("");
        bindDocSource.setValue("Non EDMS");
        bindDocSourceEDMS.setValue("EDMS");
        bindAddtoList.setDisabled(false);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindAddtoList);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindDocNumNonEDMS);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindDocTitleNonEDMS);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindDocStatus);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindStatusDocRequestEDMS);
    }

    public String bindUpdateButton() {
        // Add event code here...
        bindSaveButton.setVisible(true);
        AdfFacesContext.getCurrentInstance().addPartialTarget(bindSaveButton);
        return null;
    }

    public void closePage(ReturnEvent returnEvent) {
        // Add event code here...
    }

    public void closePage(ActionEvent actionEvent) {
        // Add event code here...

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExtendedRenderKitService service =
            Service.getRenderKitService(facesContext,
                                        ExtendedRenderKitService.class);
        service.addScript(facesContext,
                          "window.open('', '_self', ''); window.close();self.close();");

    }
    
    public void selectApEvent(ValueChangeEvent vce) {
        String namaCompany="";
        String varnama=vce.getNewValue()==null?"":vce.getNewValue().toString();
//        System.out.println(varnama.replaceAll("\\s+",""));
        if(!varnama.equalsIgnoreCase("")){
            namaCompany= vce.getNewValue().toString();
            bindApNama.setValue(namaCompany);
            String tdclabel=getValueInDB("AppModuleDataControl", "PheConfigVO1", "UPPER(KEY_CONFIG) = UPPER('TDCLEAD_"+namaCompany.substring(4)+"')",
                                     "KeyDisplay");
            String tdcLead =
                        getValueInDB("AppModuleDataControl", "PheConfigVO1", "UPPER(KEY_CONFIG) = UPPER('TDCLEAD_"+namaCompany.substring(4)+"')",
                                     "KeyValue");
            //                System.out.print("\ntdcLead = " + tdcLead + '_'+namaCompany.substring(4));
                    bindTdcLead.setValue(tdcLead); 
                    bindTdcLead.setLabel(tdclabel); 
                    
            if(namaCompany.substring(4).replaceAll("\\s+","").equalsIgnoreCase("ONWJ")){
                setRoleTdcReview("TDC");
            }else{
                setRoleTdcReview(namaCompany.substring(4).replaceAll("\\s+",""));
            }
        }
//       bindApNamaCompany.setValue(namaCompany);
//       AdfFacesContext.getCurrentInstance().addPartialTarget(bindApNamaCompany);    
        
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindTdcLead);
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindApNama);
                AdfFacesContext.getCurrentInstance().addPartialTarget(bindTableRequest);
    }

    public void setBindCancelInputRequestCID(RichCommandButton bindCancelInputRequestCID) {
        this.bindCancelInputRequestCID = bindCancelInputRequestCID;
    }

    public RichCommandButton getBindCancelInputRequestCID() {
        return bindCancelInputRequestCID;
    }

    public void setBindCancelEditRequestCID(RichCommandButton bindCancelEditRequestCID) {
        this.bindCancelEditRequestCID = bindCancelEditRequestCID;
    }

    public RichCommandButton getBindCancelEditRequestCID() {
        return bindCancelEditRequestCID;
    }

    public void setBindLabelUSer(RichOutputText bindLabelUSer) {
        this.bindLabelUSer = bindLabelUSer;
    }

    public RichOutputText getBindLabelUSer() {
        return bindLabelUSer;
    }

    public void setBindLabelUSer2(RichOutputText bindLabelUSer2) {
        this.bindLabelUSer2 = bindLabelUSer2;
    }

    public RichOutputText getBindLabelUSer2() {
        return bindLabelUSer2;
    }

    public void setBindContractorRewrite(RichInputText bindContractorRewrite) {
        this.bindContractorRewrite = bindContractorRewrite;
    }

    public RichInputText getBindContractorRewrite() {
        return bindContractorRewrite;
    }

    public void setBindRemarksRewrite(RichInputText bindRemarksRewrite) {
        this.bindRemarksRewrite = bindRemarksRewrite;
    }

    public RichInputText getBindRemarksRewrite() {
        return bindRemarksRewrite;
    }
    public void setBindUserContractor(RichInputText bindUserContractor) {
        this.bindUserContractor = bindUserContractor;
    }

    public RichInputText getBindUserContractor() {
        return bindUserContractor;
    }

    public void setBindInternalUser(RichInputText bindInternalUser) {
        this.bindInternalUser = bindInternalUser;
    }

    public RichInputText getBindInternalUser() {
        return bindInternalUser;
    }
    public void setBindDeleteDocList(RichCommandButton bindDeleteDocList) {
        this.bindDeleteDocList = bindDeleteDocList;
    }

    public RichCommandButton getBindDeleteDocList() {
        return bindDeleteDocList;
    }

    public void setBindEditDocList(RichCommandButton bindEditDocList) {
        this.bindEditDocList = bindEditDocList;
    }

    public RichCommandButton getBindEditDocList() {
        return bindEditDocList;
    }

    public void setBindSendRequest(RichCommandButton bindSendRequest) {
        this.bindSendRequest = bindSendRequest;
    }

    public RichCommandButton getBindSendRequest() {
        return bindSendRequest;
    }

    public void setBindFinishDocList(RichCommandButton bindFinishDocList) {
        this.bindFinishDocList = bindFinishDocList;
    }

    public RichCommandButton getBindFinishDocList() {
        return bindFinishDocList;
    }

    public void setBindAddtoList(RichCommandButton bindAddtoList) {
        this.bindAddtoList = bindAddtoList;
    }

    public RichCommandButton getBindAddtoList() {
        return bindAddtoList;
    }

    public void setBindCreateRequestButton(RichCommandButton bindCreateRequestButton) {
        this.bindCreateRequestButton = bindCreateRequestButton;
    }

    public RichCommandButton getBindCreateRequestButton() {
        return bindCreateRequestButton;
    }

    public void setBindEditRequestButton(RichCommandButton bindEditRequestButton) {
        this.bindEditRequestButton = bindEditRequestButton;
    }

    public RichCommandButton getBindEditRequestButton() {
        return bindEditRequestButton;
    }

    public void setBindDeleteRequestButton(RichCommandButton bindDeleteRequestButton) {
        this.bindDeleteRequestButton = bindDeleteRequestButton;
    }

    public RichCommandButton getBindDeleteRequestButton() {
        return bindDeleteRequestButton;
    }

    public void setBindDocumentListButton(RichCommandButton bindDocumentListButton) {
        this.bindDocumentListButton = bindDocumentListButton;
    }

    public RichCommandButton getBindDocumentListButton() {
        return bindDocumentListButton;
    }

    public void setBindPopupAlreadyRequested(RichPopup bindPopupAlreadyRequested) {
        this.bindPopupAlreadyRequested = bindPopupAlreadyRequested;
    }

    public RichPopup getBindPopupAlreadyRequested() {
        return bindPopupAlreadyRequested;
    }

    public void setBindTableDocRequest(RichTable bindTableDocRequest) {
        this.bindTableDocRequest = bindTableDocRequest;
    }

    public RichTable getBindTableDocRequest() {
        return bindTableDocRequest;
    }

    public void setBindInputForm(RichPanelFormLayout bindInputForm) {
        this.bindInputForm = bindInputForm;
    }

    public RichPanelFormLayout getBindInputForm() {
        return bindInputForm;
    }

    public void setBindPopupValidationDocList(RichPopup bindPopupValidationDocList) {
        this.bindPopupValidationDocList = bindPopupValidationDocList;
    }

    public RichPopup getBindPopupValidationDocList() {
        return bindPopupValidationDocList;
    }
    public void setBindTabInputType(RichPanelTabbed bindTabInputType) {
        this.bindTabInputType = bindTabInputType;
    }

    public RichPanelTabbed getBindTabInputType() {
        return bindTabInputType;
    }

    public void setBindPopupValidationEdit(RichPopup bindPopupValidationEdit) {
        this.bindPopupValidationEdit = bindPopupValidationEdit;
    }

    public RichPopup getBindPopupValidationEdit() {
        return bindPopupValidationEdit;
    }

    public void setBindBackToReqList(RichCommandButton bindBackToReqList) {
        this.bindBackToReqList = bindBackToReqList;
    }

    public RichCommandButton getBindBackToReqList() {
        return bindBackToReqList;
    }

    public void setBindHome(RichCommandButton bindHome) {
        this.bindHome = bindHome;
    }

    public RichCommandButton getBindHome() {
        return bindHome;
    }
    public void setBindDocNumNonEDMS(RichInputText bindDocNumNonEDMS) {
        this.bindDocNumNonEDMS = bindDocNumNonEDMS;
    }

    public RichInputText getBindDocNumNonEDMS() {
        return bindDocNumNonEDMS;
    }

    public void setBindDocTitleNonEDMS(RichInputText bindDocTitleNonEDMS) {
        this.bindDocTitleNonEDMS = bindDocTitleNonEDMS;
    }

    public RichInputText getBindDocTitleNonEDMS() {
        return bindDocTitleNonEDMS;
    }
    public void setBindTableRequest(RichTable bindTableRequest) {
        this.bindTableRequest = bindTableRequest;
    }

    public RichTable getBindTableRequest() {
        return bindTableRequest;
    }

    public void setBindFormCreateRequest(RichPanelFormLayout bindFormCreateRequest) {
        this.bindFormCreateRequest = bindFormCreateRequest;
    }

    public RichPanelFormLayout getBindFormCreateRequest() {
        return bindFormCreateRequest;
    }

    public void setBindAttachUploadForm(RichInputText bindAttachUploadForm) {
        this.bindAttachUploadForm = bindAttachUploadForm;
    }

    public RichInputText getBindAttachUploadForm() {
        return bindAttachUploadForm;
    }

    public void setBindSelectDocument(RichCommandButton bindSelectDocument) {
        this.bindSelectDocument = bindSelectDocument;
    }

    public RichCommandButton getBindSelectDocument() {
        return bindSelectDocument;
    }

    public void setBindAddTolist(RichPanelGroupLayout bindAddTolist) {
        this.bindAddTolist = bindAddTolist;
    }

    public RichPanelGroupLayout getBindAddTolist() {
        return bindAddTolist;
    }

    public void setBindPopupNullSelect(RichPopup bindPopupNullSelect) {
        this.bindPopupNullSelect = bindPopupNullSelect;
    }

    public RichPopup getBindPopupNullSelect() {
        return bindPopupNullSelect;
    }
    public void setBindCountList(RichOutputText bindCountList) {
        this.bindCountList = bindCountList;
    }

    public RichOutputText getBindCountList() {
        return bindCountList;
    }

    public void setBindPopupValidationDelList(RichPopup bindPopupValidationDelList) {
        this.bindPopupValidationDelList = bindPopupValidationDelList;
    }

    public RichPopup getBindPopupValidationDelList() {
        return bindPopupValidationDelList;
    }

    public void setBindPopupValidationDel(RichPopup bindPopupValidationDel) {
        this.bindPopupValidationDel = bindPopupValidationDel;
    }

    public RichPopup getBindPopupValidationDel() {
        return bindPopupValidationDel;
    }

    public void setBindPopupValidationSend(RichPopup bindPopupValidationSend) {
        this.bindPopupValidationSend = bindPopupValidationSend;
    }

    public RichPopup getBindPopupValidationSend() {
        return bindPopupValidationSend;
    }

    public void setBindPopupValidationDelCreate(RichPopup bindPopupValidationDelCreate) {
        this.bindPopupValidationDelCreate = bindPopupValidationDelCreate;
    }

    public RichPopup getBindPopupValidationDelCreate() {
        return bindPopupValidationDelCreate;
    }

    public void setBindPopupSend(RichPopup bindPopupSend) {
        this.bindPopupSend = bindPopupSend;
    }

    public RichPopup getBindPopupSend() {
        return bindPopupSend;
    }

    public void setBindpopupDeleteRequestList(RichPopup bindpopupDeleteRequestList) {
        this.bindpopupDeleteRequestList = bindpopupDeleteRequestList;
    }

    public RichPopup getBindpopupDeleteRequestList() {
        return bindpopupDeleteRequestList;
    }

    public void setSampah(RichOutputText sampah) {
        this.sampah = sampah;
    }

    public RichOutputText getSampah() {
        return sampah;
    }

    public void setBindIteratorRowCount(RichOutputText bindIteratorRowCount) {
        this.bindIteratorRowCount = bindIteratorRowCount;
    }

    public RichOutputText getBindIteratorRowCount() {
        return bindIteratorRowCount;
    }

    public void setUserlogin(String Userlogin) {
        this.Userlogin = Userlogin;
    }

    public String getUserlogin() {
        return Userlogin;
    }

    public void setBindApNamaCompany(RichInputText bindApNamaCompany) {
        this.bindApNamaCompany = bindApNamaCompany;
    }

    public RichInputText getBindApNamaCompany() {
        return bindApNamaCompany;
    }



    public void setBindSocApName(RichSelectOneChoice bindSocApName) {
        this.bindSocApName = bindSocApName;
    }

    public RichSelectOneChoice getBindSocApName() {
        return bindSocApName;
    }

    public void setBindApNama(RichInputText bindApNama) {
        this.bindApNama = bindApNama;
    }

    public RichInputText getBindApNama() {
        return bindApNama;
    }

    public void setIDC_SERVER(String IDC_SERVER) {
        this.IDC_SERVER = IDC_SERVER;
    }

    public String getIDC_SERVER() {
        return IDC_SERVER;
    }

    public void setURL_CID(String URL_CID) {
        this.URL_CID = URL_CID;
    }

    public String getURL_CID() {
        return URL_CID;
    }

    public void setUrl_Database(String Url_Database) {
        this.Url_Database = Url_Database;
    }

    public String getUrl_Database() {
        return Url_Database;
    }

    public void setUser_Database(String User_Database) {
        this.User_Database = User_Database;
    }

    public String getUser_Database() {
        return User_Database;
    }

    public void setPass_Database(String Pass_Database) {
        this.Pass_Database = Pass_Database;
    }

    public String getPass_Database() {
        return Pass_Database;
    }

    public void setRoleTdcReview(String roleTdcReview) {
        this.roleTdcReview = roleTdcReview;
    }

    public String getRoleTdcReview() {
        return roleTdcReview;
    }
}
