<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page
	import="java.io.BufferedReader,java.io.ByteArrayOutputStream,java.io.IOException,java.io.InputStreamReader,java.io.OutputStreamWriter,java.net.*,java.util.logging.Level,java.util.logging.Logger,java.sql.*"%>
<%
	Class.forName("oracle.jdbc.driver.OracleDriver");
	Connection con = DriverManager.getConnection("jdbc:oracle:thin:@10.252.4.112:1521:OWCPROD", "DEV_URMSERVER", "owc14urm20prod");
	//Connection con = DriverManager.getConnection("jdbc:oracle:thin:@10.252.4.193:1521:OWCDEV", "DEV_URMSERVER","welcome1");
	Statement stmt = con.createStatement();
	String shareFolderLocation = "";
	String query = "SELECT KEY_VALUE FROM PHE_CONFIG WHERE KEY_CONFIG = 'SHARE_FOLDER_LOCATION'";
	ResultSet rset = stmt.executeQuery(query);
	while (rset.next()) {
		shareFolderLocation = rset.getString(1);
	}

	rset.close();
	stmt.close();
	con.close();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
<title>JSP Pages</title>
<script type="text/javascript" src="lib/jquery-1.8.2.js"></script>
<script src="lib/jqueryFileTree.js" type="text/javascript"></script>
<script src="lib/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link href="css/reset-fonts-grids.css" type="text/css" rel="stylesheet"></link>
<link href="css/container.css" type="text/css" rel="stylesheet"></link>
<link href="css/style.css" type="text/css" rel="stylesheet"></link>
<link href="css/treeview-core.css" type="text/css" rel="stylesheet"></link>
<link href="css/treeview-skin.css" type="text/css" rel="stylesheet"></link>
<link href="css/menu-core.css" type="text/css" rel="stylesheet"></link>
<link type="text/css" href="css/cupertino/jquery-ui.css"
	rel="stylesheet" />
<link href="css/jqueryFileTree.css" rel="stylesheet" type="text/css"
	media="screen" />
<style type="stylesheet">
            input[type="button"]{
                padding:0 4px;
            }
        </style>
<script type="text/javascript">
    var sfLocation = "<%=shareFolderLocation%>";

	function validateProcess() {
		var sFileName = document.getElementById("file").value;
		var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1];

		if (document.getElementById("file").value == "") {
			alert("Please fill attachment first.");
		} else if (sFileExtension != "xls") {
			alert("Extention type is .xls only");
		} else if ($('#txtFolder').val() == "") {
			alert("Please choose folder source");
		} else {
			document.fileuploadform.submit();
			$("#file").attr('disabled', 'true');
			$("#submitQueryForm").attr('disabled', 'true');
			$("#messageSubmit").css('display', 'block')
		}
	}

	var temp = "";
	$(document).ready(function() {
		$('#container_id').fileTree({
			root : sfLocation,
			//                    root : 'D://PEDOMAN PERUSAHAAN',
			script : 'connectors/jqueryFileTree.jsp'
		});
	});

	function openDialog() {
		$("#container_id").dialog({
			autoOpen : true,
			height : 350,
			width : 300,
			position : [ 'top', 0 ],
			modal : true,
			title : "Browse Folder .. ",
			resizable : false,
			buttons : {
				Ok : function() {
					$('#txtFolder').val(temp);
					$('#spanText').html(temp);
					$(this).dialog("close");
				},
				Cancel : function() {
					$(this).dialog("close");
				}
			}
		});
	}
</script>
</head>
<body>
	<div id="pageContent"
		style="display: block; width: 100%; height: auto; vertical-align: top;">
		<table cellspacing="0" cellpadding="0" summary=""
			style="width: 100%; height: 100%;">
			<tbody>
				<tr height="12px"></tr>
				<tr>
					<td width="10px"></td>
					<td align="center" style="vertical-align: top">
						  <!--<noscript>--> 
						  <!-- only shown if scripting is disabled --> 
						  <!--  <h1>You currently have JavaScript disabled for this web site.
			                    You must enable scripting in your browser for the content
			                    server to function properly.</h1>
			              </noscript>-->
						<form method="post" action="UploadServlet" name="fileuploadform"
							enctype="multipart/form-data" target="_parent">
							<div class="mainContent">
								<table
									style="width: 400px; border-collapse: separate; border-spacing: 5px;" cellpadding="5">
									<tbody>
										<tr>
											<td style="width: 35%; text-align: right;"><span
												class="searchLabel">Path File: </span></td>
											<td style="width: 65%; text-align: left;"><input
												type="file" name="file" id="file"></input>
											</td>
										</tr>
										<tr>
											<td style="width: 35%; text-align: right;"><span
												class="searchLabel">Folder Location:</span></td>
											<td style="width: 65%; text-align: left;"><input
												type="hidden" id="txtFolder" name="txtFolder"></input><span
												id="spanText"></span><input type="button"
												name="btn_BrowseFolder" id="btn_BrowseFolder"
												value="  Browse ..  " onclick="openDialog()" />
											</td>
										</tr>
									</tbody>
								</table>
								<hr></hr>								
								<table 
									style="width: 400px; border-collapse: separate; border-spacing: 5px; ">
									<tbody>
										<tr>
											<td style="width: 35%; text-align: right;">&nbsp;</td>
											<td style="width: 65%; text-align: left;">
												<input type="button" id="submitQueryForm" name="submitQueryForm" value="  Upload Data  " onclick="validateProcess()"></input>
												<input type="hidden" name="pheAp" value="<%=request.getParameter("ap")%>">
											</td>
										</tr>
									</tbody>
								</table>
								<br></br> 
								<span
									id="messageSubmit" style="display: none; color: red">
									<img src="images/Loading_Icon.gif" width="12"></img> Please wait.. Do not close your browser...
								</span>
							</div>
						</form>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="container_id" style="text-align: left; display: none"></div>
</body>
</html>