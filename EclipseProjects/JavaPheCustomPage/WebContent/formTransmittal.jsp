<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
<title>JSP Pages</title>
<script type="text/javascript" src="lib/jquery-1.8.2.js"></script>
<script src="lib/jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="lib/jquery.tokeninput.js"></script>
<script type="text/javascript" src="lib/colResizable-1.3.min.js"></script>
<link href="css/treeview-core.css" type="text/css" rel="stylesheet"></link>
<link href="css/treeview-skin.css" type="text/css" rel="stylesheet"></link>
<link href="css/container.css" type="text/css" rel="stylesheet"></link>
<link href="css/style.css" type="text/css" rel="stylesheet"></link>
<link href="css/menu-core.css" type="text/css" rel="stylesheet"></link>
<link type="text/css" href="css/jquery-ui.css" rel="stylesheet" />
<link rel="stylesheet" href="css/token-input.css" type="text/css" />
<link rel="stylesheet" href="css/token-input-facebook.css"
	type="text/css" />
<style type="stylesheet">
  .CRZ{table-layout:fixed;}.CRZ td,.CRZ th{padding-left:0px!important;padding-right:0px!important;overflow:hidden}.CRC{height:0px;position:relative;}.CRG{margin-left:-5px;position:absolute;z-index:5;}.CRG .CRZ{position:absolute;background-color:red;filter:alpha(opacity=1);opacity:0;width:10px;height:100%;top:0px}.CRL{position:absolute;width:1px}.CRD{ border-left:1px dotted black}
            input[type="button"]{
                padding:0 4px;
            }
        </style>
<script type="text/javascript">
	function validateProcess() {
		var sFileName = document.getElementById("file").value;
		var sFileExtension = sFileName.split('.')[sFileName.split('.').length - 1];

		if (document.getElementById("file").value == "") {
			alert("Please fill attachment first.");
		} else if (sFileExtension != "xls") {
			alert("Extention type is .xls only");
		} else if ($('#txtFolder').val() == "") {
			alert("Please choose folder source");
		} else {
			document.fileuploadform.submit();
			$("#file").attr('disabled', 'true');
			$("#submitQueryForm").attr('disabled', 'true');
			$("#messageSubmit").css('display', 'block')
		}
	}

	var temp = "";
	$(function() {
		$("#tabs").tabs({
			heightStyle : "auto"
		});
		$("#tabs").css("height", $(document).height() - 40);

		//nyisipkan autocompletenya ke form
		$('#xUser_value')
				.append(
						"<input type='text' id='tempName' style='border:inherit !important'>");

		//dapetin orang orang nya...
		JSONUrl = "http://localhost:16300/urm/idcplg?IdcService=PHE_GET_PARTICULAR_USER";

		//buat autocomplete nya..
		$('#tempName')
				.tokenInput(
						"http://localhost:16300/urm/idcplg?IdcService=PHE_GET_PARTICULAR_USER",
						{
							theme : "facebook"
						});
		$('.resizeable').colResizable();

		$('#optionDocument').on('change', function() {
			if (this.value == "EDMS") {
				openDialogEDMS();
			}
			$('#optionDocument').val("source");
		});
	});

	function openDialogEDMS() {
		$("#container_id").dialog({
			autoOpen : true,
			height : $(document).height() - 100,
			width : $(document).width() - 50,
			modal : true,
			title : "List Document EDMS ",
			resizable : false,
			buttons : {
				Ok : function() {
					$('#txtFolder').val(temp);
					$('#spanText').html(temp);
					$(this).dialog("close");
				},
				Cancel : function() {
					$(this).dialog("close");
				}
			}
		});
	}
</script>
</head>
<body style="font-size: 13px;">
	<div id="pageContent"
		style="display: block; width: 100%; vertical-align: top;">
		<div id="tabs" style="width: 99%;">
			<ul>
				<li><a href="#tabs-1">General Info</a></li>

				<li><a href="#tabs-2">List Documents</a></li>

				<li><a href="#tabs-3">Distribution/Circulation Team</a></li>
			</ul>
			<div id="tabs-1" style="height: 100%">
				<table>
					<tr>
						<td>Category</td>
						<td style="width: 10px;">:</td>
						<td nowrap="nowrap">&lt;$TransmittalType$&gt;</td>
					</tr>

					<tr>
						<td>Project Title</td>
						<td style="width: 10px;">:</td>
						<td nowrap="nowrap">&lt;$ProjectTitle$&gt;</td>
					</tr>

					<tr>
						<td>Subject</td>
						<td style="width: 10px;">:</td>
						<td nowrap="nowrap"><textarea cols="70" rows="3">
				</textarea></td>
					</tr>

					<tr>
						<td class="idcFieldCaption idcCheckinUpdateCaption">Due Date</td>
						<td style="width: 10px;">:</td>
						<td><input name="dueDate" id="dueDate" type="text" size="20"
							maxlength="40" value="" /></td>
					</tr>

					<tr>
						<td class="idcFieldCaption idcCheckinUpdateCaption">Contractor</td>
						<td style="width: 10px;">:</td>
					</tr>

					<tr>
						<td class="idcFieldCaption idcCheckinUpdateCaption">WO Number</td>
						<td style="width: 10px;">:</td>
						<td class="idcFieldEntry   idcCheckinUpdateEntry" nowrap="nowrap">
							<input type="text"></input>
						</td>
					</tr>
				</table>
			</div>
			<div id="tabs-2">
				<table>
					<tr>
						<td style="text-align: right; width: 50%"><span
							class="searchLabel">Add Document From</span></td>
						<td style="text-align: left" class="xuiSubheading"><select
							id="optionDocument">
								<option disabled="disabled" selected="selected" value="source">-
									Choose Source -</option>
								<option value="EDMS">EDMS</option>
								<option value="Electronic">Electronic</option>
								<option value="Physical">Physical</option>
						</select></td>
					</tr>

					<tr>
						<td colspan="2">
							<table id="resizeable" class="resizeable" border="0"
								style="width: 99%; border: 1px solid black">
								<tr>
									<td id="column_0_0" class="xuiListHeaderCell "
										style="width: 4%; word-wrap: break-word;">Document No</td>
									<td id="column_0_0" class="xuiListHeaderCell "
										style="width: 4%; word-wrap: break-word;">Document Title</td>
									<td id="column_0_0" class="xuiListHeaderCell "
										style="width: 4%; word-wrap: break-word;">Revision</td>
									<td id="column_0_0" class="xuiListHeaderCell "
										style="width: 4%; word-wrap: break-word;">Document Status</td>
									<td id="column_0_0" class="xuiListHeaderCell "
										style="width: 4%; word-wrap: break-word;">Pages</td>
									<td id="column_0_0" class="xuiListHeaderCell "
										style="width: 4%; word-wrap: break-word;">Remarks</td>
									<td id="column_0_0" class="xuiListHeaderCell "
										style="width: 4%; word-wrap: break-word;">Distribution
										Method</td>
									<td id="column_0_0" class="xuiListHeaderCell "
										style="width: 4%; word-wrap: break-word;">Action</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div id="tabs-3">
				<table>
					<tr>
						<td>Role</td>
						<td><input type="radio"></input> Reviewer <input type="radio"></input>
							Approver <input type="radio"></input> Originator <input
							type="radio"></input> Information Only</td>
					</tr>

					<tr>
						<td>User</td>
						<td><span id="xUser_value"> <input type="text"></input></span>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
	<div id="container_id" style="text-align: left; display: none">
		<form enctype="multipart/form-data">
			<table>
				<tr>
					<td>Get Document From:</td>
					<td><select id="optionEDMSDocument">
							<option value="createEDMS" selected="selected">Create
								New Document</option>
							<option value="existingEDMS">Existing Document</option>
					</select></td>
				</tr>
				<tr class="createEDMS" style="display: block">
					<td colspan="2">*Document Number *Document Title *Document
						Status IFE IFR IFA RIFA IFI Pages Remarks *Revisions</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>