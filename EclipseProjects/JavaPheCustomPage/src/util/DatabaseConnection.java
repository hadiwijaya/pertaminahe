package util;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection
{
  public static Connection getConnection()
    throws SQLException
  {
    String driver = "oracle.jdbc.driver.OracleDriver";
    
    String url = "jdbc:oracle:thin:@//10.252.4.193/OWCDEV";
    String username = "DEV_URMSERVER";
    String password = "welcome1";
    
    Connection conn = null;
    try
    {
      Class.forName(driver);
      conn = DriverManager.getConnection(url, username, password);
      System.out.print("Connection Success");
    }
    catch (ClassNotFoundException ex)
    {
      System.out.println("DatabaseConnectionError : " + ex);
    }
    return conn;
  }
}
