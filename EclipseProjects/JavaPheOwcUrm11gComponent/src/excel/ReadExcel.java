package excel;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradocservice.Folder;
import intradocservice.Util;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import util.DataCollection;
import util.Validator;

public class ReadExcel extends Service {
	private String uploadTempHeaderMasterlistDrawing = "TDC MASTER LIST DRAWING";
	private String uploadTempHeaderMasterlistNonDrawing = "TDC MASTER LIST NONDRAWING";
	private String uploadTempHeaderGeneral = "DCRMS GENERAL - GENERAL DOCUMENT";
	private String uploadTempHeaderPublic = "DCRMS PHE PUBLIC - PUBLIC DOCUMENT";
	private String uploadTempHeaderDrawing = "DCRMS GENERAL - DRAWING DOCUMENT";
	private String uploadTempHeaderNonDrawing = "DCRMS GENERAL - NONDRAWING DOCUMENT";
	private String uploadTempHeaderExternalResource = "EXTERNAL RESOURCE LIST";
	String rootApDest = "";
	
	public ArrayList getXlsFile(String filename, Workspace ws, String userLogin, String pheAp) throws DataException {
		
		ArrayList rowCollection = new ArrayList();
		try {
			DatabaseValue dValue = new DatabaseValue();
			String filePath = dValue.getPheConfig("PATH_TMP") + filename;

			String keyConfApCab = "CABINETS_" + pheAp;
			try {
				this.rootApDest = dValue.getPheConfig(keyConfApCab.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
			}
			
			FileInputStream file = new FileInputStream(new File(filePath));

			HSSFWorkbook workbook = new HSSFWorkbook(file);
			for (int sheetIndex = 0; sheetIndex < 1; sheetIndex++) {
				HSSFSheet sheet = workbook.getSheetAt(sheetIndex);
				String type = "";

				Iterator<Row> rowIterator = sheet.iterator();
				int rowIndex = 0;
				type = getTemplateType(rowIterator);
				while (rowIterator.hasNext()) {
					Row row = (Row) rowIterator.next();
					ArrayList oneRow = new ArrayList();
					oneRow.add(rowIndex);
					int lastColumn = 0;
					if (type.toUpperCase().equals(uploadTempHeaderMasterlistDrawing)) {
						lastColumn = Math.max(row.getLastCellNum(), 16);
					} else if (type.toUpperCase().equals(uploadTempHeaderMasterlistNonDrawing)) {
						lastColumn = Math.max(row.getLastCellNum(), 13);
					} else if (type.toUpperCase().equals(uploadTempHeaderGeneral)) {
						lastColumn = Math.max(row.getLastCellNum(), 12);
					} else if (type.toUpperCase().equals(uploadTempHeaderPublic)) {
						lastColumn = Math.max(row.getLastCellNum(), 12);
					} else if (type.toUpperCase().equals(uploadTempHeaderDrawing)) {
						lastColumn = Math.max(row.getLastCellNum(), 14);
					} else if (type.toUpperCase().equals(uploadTempHeaderNonDrawing)) {
						lastColumn = Math.max(row.getLastCellNum(), 15);
					} else if (type.toUpperCase().equals(uploadTempHeaderExternalResource)) {
						lastColumn = Math.max(row.getLastCellNum(), 15);
					}

					boolean insertRowCollection = false;
					for (int cn = 0; cn < lastColumn; cn++) {
						Cell cell = row.getCell(cn, Row.RETURN_BLANK_AS_NULL);
						if (cell == null) {
							oneRow.add("");
						} else {
							switch (cell.getCellType()) {
							case Cell.CELL_TYPE_BOOLEAN:
								oneRow.add(String.valueOf(cell.getBooleanCellValue()));
								insertRowCollection = true;
								break;
							case Cell.CELL_TYPE_NUMERIC:
								String numeric = new BigDecimal(cell.getNumericCellValue()).toPlainString();
								numeric = numeric.replace(".0", "");
								int numericTemp = Integer.parseInt(numeric);
								numeric = String.valueOf(numericTemp);
								oneRow.add(numeric);
								insertRowCollection = true;
								break;
							case Cell.CELL_TYPE_STRING:
								if (rowIndex == 0) {
									oneRow.add(type);
								} else {
									oneRow.add(cell.getStringCellValue());
								}
								insertRowCollection = true;
								break;
							}
						}
					}

					if (rowIndex >= 1) {
						if (type.toUpperCase().equals(uploadTempHeaderMasterlistDrawing)) {
							oneRow = checkMasterListDrawing(oneRow, type);
						} else if (type.toUpperCase().equals(uploadTempHeaderMasterlistNonDrawing)) {
							oneRow = checkMasterListNonDrawing(oneRow, type);
						} else if (type.toUpperCase().trim().equals(uploadTempHeaderDrawing)) {
							oneRow = checkEDMSDrawing(oneRow, type);
						} else if (type.toUpperCase().equals(uploadTempHeaderNonDrawing)) {
							oneRow = checkEDMSNonDrawing(oneRow, type);
						} else if (type.toUpperCase().equals(uploadTempHeaderGeneral)) {
							oneRow = checkEDMSGeneral(oneRow, type, userLogin, pheAp);
						} else if (type.toUpperCase().equals(uploadTempHeaderPublic)) {
							oneRow = checkEDMSPHEHPublic(oneRow, type, userLogin, pheAp);
						} else if (type.toUpperCase().equals(uploadTempHeaderExternalResource)) {
							oneRow = checkResourceList(oneRow, type, userLogin, rootApDest);
						} else {
							return rowCollection;
						}
					}
					rowIndex++;

					if (insertRowCollection) {
						rowCollection.add(oneRow);
					}
				}
			}
			file.close();
		} catch (FileNotFoundException e) {
			Log.error("PHE:ReadExcel.java:getXlsFile:129:FileNotFoundException: " + e.getLocalizedMessage());
		} catch (IOException e) {
			Log.error("PHE:ReadExcel.java:getXlsFile:131:IOException: " + e.getLocalizedMessage());
		}
		return rowCollection;
	}

	public ArrayList checkMasterListDrawing(ArrayList oneRow, String type) {
		String error = "";
		DatabaseValue dValue = new DatabaseValue();
		Validator validator = new Validator();
		if (oneRow.get(1).equals("")) {
			error = error + "area must be filled" + "<br>";
		} else {
			try {
				if (dValue.getLocation(oneRow.get(1).toString(), type).length() == 0) {
					error = error + "<br><mark>area is invalid</mark><br>";
				}
			} catch (DataException ex) {
				error = error + "<br><mark>area is invalid</mark><br>";
			}
		}
		if (oneRow.get(2).equals("")) {
			error = error + "<br><mark>platform must be filled</mark>" + "<br>";
		} else {
			try {
				if (dValue.getPlatform(oneRow.get(1).toString(), oneRow.get(2).toString()).length() == 0) {
					error = error + "<br><mark>platform is invalid</mark><br>";
				}
			} catch (DataException ex) {
				error = error + "<br><mark>platform is invalid</mark><br>";
			}
		}
		if (oneRow.get(3).equals("")) {
			error = error + "<br><mark>discipline_code must be filled</mark>" + "<br>";
		} else {
			try {
				if (dValue.getDisciplineCode(oneRow.get(3).toString(), type).length() == 0) {
					error = error + "<br><mark>discipline_code is invalid</mark><br>";
				}
			} catch (DataException ex) {
				error = error + "<br><mark>discipline_code is invalid</mark><br>";
			}
		}
		if (oneRow.get(4).equals("")) {
			error = error + "<br><mark>document_type must be filled</mark>" + "<br>";
		} else {
			try {
				if (dValue.getDocumentType(oneRow.get(4).toString(), type).length() == 0) {
					error = error + "<br><mark>document_type is invalid</mark><br>";
				}
			} catch (DataException ex) {
				error = error + "<br><mark>document_type is invalid</mark><br>";
			}
		}
		if (!validator.isNumeric(oneRow.get(11).toString()) && oneRow.get(11).toString().length() > 0) {
			error = error + "<br><mark>project_year must be numeric</mark>" + "<br>";
		}

		if (oneRow.get(12).equals("")) {
			error = error + "<br><mark>owner_organization must be filled</mark>" + "<br>";
		} else {
			try {
				String authorOrganizationList = dValue.getSpecifyOptionsList("AuthorOrganizationList",
						oneRow.get(12).toString());
				if (!authorOrganizationList.equals("0")) {
					oneRow.set(12, authorOrganizationList);
				} else {
					error = error + "<br><mark>owner_organization is not valid</mark>" + "<br>";
				}
			} catch (DataException ex) {
				Log.error("PHE:checkTDCDrawing:getSpecifyOptionList:" + ex.getMessage());
				error = error + ex.getMessage() + "<br>";
			}
		}

		if (!oneRow.get(5).equals("")) {
			String tempError = validator.validateDocumentNumberForTDCDrawing(oneRow.get(1).toString(),
					oneRow.get(2).toString(), oneRow.get(3).toString(), oneRow.get(4).toString(),
					oneRow.get(5).toString(), oneRow.get(6).toString(), type);
			if (tempError.length() > 0) {
				error = error + tempError + "<br>";
			}
		}

		oneRow.add(error);
		return oneRow;
	}

	public ArrayList checkMasterListNonDrawing(ArrayList oneRow, String type) {
		String error = "";
		DatabaseValue dValue = new DatabaseValue();
		Validator validator = new Validator();
		if (oneRow.get(1).equals("")) {
			error = error + "area must be filled" + "<br>";
		} else {
			try {
				if (dValue.getLocation(oneRow.get(1).toString(), type).length() == 0) {
					error = error + "area is invalid<br>";
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				error = error + "area is invalid<br>";
			}
		}
		if (oneRow.get(2).equals("")) {
			error = error + "discipline_code must be filled" + "<br>";
		} else {
			try {
				if (dValue.getDisciplineCode(oneRow.get(2).toString(), type).length() == 0) {
					error = error + "discipline_code is invalid<br>";
				}
			} catch (DataException ex) {
				Log.error("PHE:checkTDCNonDrawing:" + ex.getMessage());
				error = error + "discipline_code is invalid<br>";
			}
		}
		if (oneRow.get(3).equals("")) {
			error = error + "document_type must be filled" + "<br>";
		} else {
			try {
				if (dValue.getDocumentType(oneRow.get(3).toString(), type).length() == 0) {
					error = error + "document_type is invalid<br>";
				}
			} catch (DataException ex) {
				Log.error("PHE:checkTDCNonDrawing:" + ex.getMessage());
				error = error + "document_type is invalid<br>";
			}
		}
		if (!validator.isNumeric(oneRow.get(8).toString()) && oneRow.get(8).toString().length() > 0) {
			error = error + "project_year must be numeric <br>";
		}

		if (!oneRow.get(4).equals("")) {
			String tempError = validator.validateDocumentNumberForTDCNonDrawing(oneRow.get(1).toString(),
					oneRow.get(2).toString(), oneRow.get(3).toString(), oneRow.get(4).toString(),
					oneRow.get(6).toString(), type);
			if (tempError.length() > 0) {
				error = error + tempError + "<br>";
			}
		}

		if (oneRow.get(9).equals("")) {
			error = error + "<br><mark>author_organization must be filled</mark>" + "<br>";
		} else {
			try {
				String authorOrganizationList = dValue.getSpecifyOptionsList("AuthorOrganizationList",
						oneRow.get(9).toString());
				if (!authorOrganizationList.equals("0")) {
					oneRow.set(9, authorOrganizationList);
				} else {
					error = error + "<br><mark>owner_organization is not valid</mark>" + "<br>";
				}
			} catch (DataException ex) {
				Log.error("PHE:checkTDCNonDrawing:getSpecifyOptionList:" + ex.getMessage());
				error = error + ex.getMessage() + "<br>";
			}
		}

		oneRow.add(error);
		return oneRow;
	}

	public ArrayList checkEDMSGeneral(ArrayList oneRow, String type, String userLogin, String pheAp) {
		String error = "";
		DatabaseValue dValue = new DatabaseValue();
		Validator validator = new Validator();
		if (oneRow.get(1).equals("")) {
			error = error + "<br><mark>doc_name must be filled</mark>" + "<br>";
		}
		if (oneRow.get(2).equals("")) {
			error = error + "<br><mark>doc_title must be filled</mark>" + "<br>";
		}
		if (oneRow.get(5).equals("")) {
			error = error + "<br/><mark>--author_organization must be filled</mark>" + "<br>";
		} else {
			try {
				String authorOrganizationList = dValue.getSpecifyOptionsList("AuthorOrganizationList",
						oneRow.get(5).toString());
				if (!authorOrganizationList.equals("0")) {
					oneRow.set(5, authorOrganizationList);
				} else {
					error = error + "<br/><mark>--author_organization is not valid</mark>" + "<br>";
				}
			} catch (DataException ex) {
				Log.error("PHE:checkEDMS:getSpecifyOptionList:" + ex.getMessage());
				error = error + ex.getMessage() + "<br>";
			}
		}
		if (oneRow.get(7).equals("")) {
			error = error + "<br><mark>company must be filled</mark>" + "<br>";
		}
		if ((!oneRow.get(9).equals("")) && (!validator.isNumeric(oneRow.get(9).toString()))
				&& (!oneRow.get(9).toString().toUpperCase().equals("NOT APPLICABLE"))) {
			error = error + "<br><mark>retention_values are: Not applicable, 2, 5, 7, 10, 20, and 50</mark>" + "<br>";
		}
		if (oneRow.get(11).equals("")) {
			error = error + "<br><mark>destination_folder must be filled</mark>" + "<br>";
		} else {
			Folder folder = new Folder();
			DataBinder binder = new DataBinder();
			DataCollection dColl = new DataCollection();

			Workspace ws = dColl.getSystemWorkspace();
			Util util = new Util();
			util.setUserLogin(userLogin);

			// Find ROOT cabinet name based on AP, from PHE_CONFIG table
			String keyConfApCab = "CABINETS_" + pheAp;
			String targetCabinet = oneRow.get(11).toString();
			String targetApCabinet = "Cabinets";
			try {
				targetApCabinet = dValue.getPheConfig(keyConfApCab.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(ReadExcel.class.getName()).log(Level.SEVERE, null, ex);
			}

			// Replace ROOT cabinet according to AP
			targetCabinet = targetCabinet.replace("Cabinets", targetApCabinet);

			// Check target cabinet is exists or not, if not exists it will be created
			if (!folder.isFolderExist(targetCabinet, binder, ws, util)) {
				// Check or create folder
				String parentDir = "";
				Folder fld = new Folder();
				DataBinder binderFolder = new DataBinder();
				fld.DoCheckingFolder(parentDir, targetCabinet, binderFolder, util.getSystemWorkspace(), util);
			}

			if (!folder.isFolderExist(targetCabinet, binder, ws, util)) {
				error = error + "<br><mark>destination_folder is not found</mark>" + "<br>";
			} else {
				// Update existing cabinet path with a new target cabinet path
				oneRow.set(11, targetCabinet);
				ResultSet rs = folder.GetFolderInfo(targetCabinet, binder, ws, util);

				String documentNameDipotong = oneRow.get(1).toString();
				if (documentNameDipotong.indexOf(".") >= 0) {
					documentNameDipotong = documentNameDipotong.substring(0, documentNameDipotong.indexOf("."));
				}

				try {
					if (dValue.checkDocumentNameGeneral(documentNameDipotong, rs.getStringValueByName("fFolderGUID"))
							.length() > 0) {
						error = error + dValue.checkDocumentNameGeneral(documentNameDipotong,
								rs.getStringValueByName("fFolderGUID"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		oneRow.add(error);
		return oneRow;
	}

	public ArrayList checkEDMSPHEHPublic(ArrayList oneRow, String type, String userLogin, String pheAp) {
		String error = "";
		DatabaseValue dValue = new DatabaseValue();
		Validator validator = new Validator();
		// document name
		if (oneRow.get(1).equals("")) {
			error = error + "<br><mark>doc_name must be filled</mark>" + "<br>";
		}
		// document title
		if (oneRow.get(2).equals("")) {
			error = error + "<br><mark>doc_title must be filled</mark>" + "<br>";
		}
		// author organization
		if (oneRow.get(5).equals("")) {
			error = error + "<br/><mark>--author_organization must be filled</mark>" + "<br>";
		} else {
			try {
				String authorOrganizationList = dValue.getSpecifyOptionsList("AuthorOrganizationList",
						oneRow.get(5).toString());
				if (!authorOrganizationList.equals("0")) {
					oneRow.set(5, authorOrganizationList);
				} else {
					error = error + "<br/><mark>--author_organization is not valid</mark>" + "<br>";
				}
			} catch (DataException ex) {
				Log.error("PHE:checkEDMSPHEHPublic:getSpecifyOptionList:" + ex.getMessage());
				error = error + ex.getMessage() + "<br>";
			}
		}
		if (oneRow.get(7).equals("")) {
			error = error + "<br><mark>company must be filled</mark>" + "<br>";
		}
		if (oneRow.get(9).equals("")) {
		} else if (!validator.isNumeric(oneRow.get(9).toString())) {
			if (!oneRow.get(9).toString().toUpperCase().equals("NOT APPLICABLE")) {
				error = error + "<br><mark>retention_values are: Not applicable, 2, 5, 7, 10, 20, and 50</mark>"
						+ "<br>";
			}
		}
		if (oneRow.get(11).equals("")) {
			error = error + "<br><mark>destination_folder must be filled</mark>" + "<br>";
		} else {
			Folder folder = new Folder();
			DataBinder binder = new DataBinder();
			DataCollection dColl = new DataCollection();

			Workspace ws = dColl.getSystemWorkspace();
			Util util = new Util();
			util.setUserLogin(userLogin);

			// Find ROOT cabinet name based on AP, from PHE_CONFIG table
			String keyConfApCab = "CABINETS_PUBLIC_GENERAL_PHE";
			String targetCabinet = oneRow.get(11).toString();
			String targetApCabinet = "Cabinets";
			try {
				targetApCabinet = dValue.getPheConfig(keyConfApCab.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(ReadExcel.class.getName()).log(Level.SEVERE, null, ex);
			}

			// Replace ROOT cabinet according to AP
			targetCabinet = targetCabinet.replace("Cabinets", targetApCabinet);

			// Check target cabinet is exists or not, if not exists it will be created
			if (!folder.isFolderExist(targetCabinet, binder, ws, util)) {
				// Check or create folder
				String parentDir = "";
				Folder fld = new Folder();
				DataBinder binderFolder = new DataBinder();
				fld.DoCheckingFolder(parentDir, targetCabinet, binderFolder, util.getSystemWorkspace(), util);
			}

			if (!folder.isFolderExist(targetCabinet, binder, ws, util)) {
				error = error + "<br><mark>destination_folder is not found</mark>" + "<br>";
			} else {
				oneRow.set(11, targetCabinet);

				ResultSet rs = folder.GetFolderInfo(targetCabinet, binder, ws, util);

				Log.info("general folderinfo rs = " + rs);

				String documentNameDipotong = oneRow.get(1).toString();
				if (documentNameDipotong.indexOf(".") >= 0) {
					documentNameDipotong = documentNameDipotong.substring(0, documentNameDipotong.indexOf("."));
				}
				try {
					if (dValue.checkDocumentNameGeneral(documentNameDipotong, rs.getStringValueByName("fFolderGUID"))
							.length() > 0) {
						error = error + dValue.checkDocumentNameGeneral(documentNameDipotong,
								rs.getStringValueByName("fFolderGUID"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		oneRow.add(error);
		return oneRow;
	}

	public ArrayList checkEDMSDrawing(ArrayList oneRow, String type) {
		String error = "";
		DatabaseValue dValue = new DatabaseValue();
		Validator validator = new Validator();
		// document name
		if (oneRow.get(1).equals("")) {
			error = error + "<br><mark>doc_name must be filled</mark>" + "<br>";
		} else {
			try {
				if (oneRow.get(3).toString().equals("")) {
					error = error + "<br><mark>doc_purpose must be filled</mark>" + "<br>";
				} else {
					if ((!oneRow.get(3).toString().toUpperCase().equals("PUBLISHED"))
							&& (!oneRow.get(3).toString().toUpperCase().equals("NATIVE"))) {
						error = error + "<br><mark>doc_purpose must Published or Native</mark>" + "<br>";
					}
					error = error + validator.validateDocumentNumber(oneRow.get(1).toString(), oneRow.get(3).toString(),
							"DRAWING", dValue);
				}
			} catch (DataException ex) {
				Log.error("PHEDataException:" + ex.getMessage());
				error = error + "<br><mark>Cannot check to database</mark>" + "<br>";
			}
		}
		// document title
		if (oneRow.get(2).equals("")) {
			error = error + "<br><mark>doc_title must be filled</mark>" + "<br>";
		}

		// author organization
		if (oneRow.get(7).equals("")) {
			error = error + "<br><mark>author_organization must be filled</mark>" + "<br>";
		} else {
			try {
				String authorOrganizationList = dValue.getSpecifyOptionsList("AuthorOrganizationList",
						oneRow.get(7).toString());
				if (!authorOrganizationList.equals("0")) {
					oneRow.set(7, authorOrganizationList);
				} else {
					error = error + "<br><mark>--author_organization is not valid</mark>" + "<br>";
				}
			} catch (DataException ex) {
				Log.error("PHE:checkEDMS:getSpecifyOptionList:" + ex.getMessage());
				error = error + ex.getMessage() + "<br>";
			}
		}
		if (oneRow.get(9).equals("")) {
			error = error + "<br><mark>company must be filled</mark>" + "<br>";
		}
		if (oneRow.get(11).equals("")) {
			error = error + "<br><mark>contractor must be filled</mark>" + "<br>";
		}
		oneRow.add(error);
		return oneRow;
	}

	public ArrayList checkEDMSNonDrawing(ArrayList oneRow, String type) {
		String error = "";
		DatabaseValue dValue = new DatabaseValue();
		Validator validator = new Validator();
		// document name
		if (oneRow.get(1).equals("")) {
			error = error + "<mark>doc_name must be filled</mark>" + "<br>";
		} else {
			try {
				if (oneRow.get(3).toString().equals("")) {
					error = error + "<mark>doc_purpose must be filled</mark>" + "<br>";
				} else {
					if ((!oneRow.get(3).toString().toUpperCase().equals("PUBLISHED"))
							&& (!oneRow.get(3).toString().toUpperCase().equals("NATIVE"))) {
						error = error + "<mark>doc_purpose must Published or Native</mark>" + "<br>";
					}
					error = error + validator.validateDocumentNumber(oneRow.get(1).toString(), oneRow.get(3).toString(),
							"NONDRAWING", dValue);
				}
			} catch (DataException ex) {
				Log.error("PHEDataException:" + ex.getMessage());
				error = error + "<mark>Cannot check to database</mark>" + "<br>";
			}
		}
		// document title
		if (oneRow.get(2).equals("")) {
			error = error + "<mark>doc_title must be filled</mark>" + "<br>";
		}

		if (oneRow.get(4).equals("")) {
			error = error + "<mark>active_passive must be filled</mark>" + "<br>";
		} else if ((!oneRow.get(4).toString().toUpperCase().equals("ACTIVE"))
				&& (!oneRow.get(4).toString().toUpperCase().equals("PASSIVE"))) {
			error = error + "<mark>active_passive must Active or Passive</mark>" + "<br>";
		} else if (oneRow.get(4).toString().toUpperCase().equals("ACTIVE")) {
			if (oneRow.get(14).equals("")) {
				error = error + "<mark>retention_date must be filled</mark>" + "<br>";
			} else {
				if (oneRow.get(4).toString().toUpperCase().equals("ACTIVE")) {
					if (oneRow.get(14).equals("")) {
						error = error + "<mark>retention_date must be filled</mark>" + "<br>";
					} else {
						// update by gin69
						String dateDigantiPaksa = oneRow.get(14).toString();
						if (dateDigantiPaksa.length() < 10) {
							try {
								String separatorTanggal[] = dateDigantiPaksa.split("/");
								String tanggalKepaksa = separatorTanggal[0];
								String bulanKepaksa = separatorTanggal[1];
								String tahunKepaksa = separatorTanggal[2];

								if (Integer.valueOf(tanggalKepaksa) < 10) {
									tanggalKepaksa = "0" + String.valueOf(Integer.valueOf(tanggalKepaksa));
								}
								if (Integer.valueOf(bulanKepaksa) < 10) {
									bulanKepaksa = "0" + String.valueOf(Integer.valueOf(bulanKepaksa));
								}
								if (tahunKepaksa.length() < 3) {
									tahunKepaksa = "20" + tahunKepaksa;
								}

								oneRow.set(14, tanggalKepaksa + "/" + bulanKepaksa + "/" + tahunKepaksa);
							} catch (Exception ex) {
							}
						}
						if (!validator.dateChecker(oneRow.get(14).toString())) {
							error = error + "<mark>format retention_date not valid</mark>" + "<br>";
						}
					}
				} else {
					if (!oneRow.get(14).equals("")) {
						error = error + "<mark>retention_date must not be filled for passive document</mark>" + "<br>";
					}
				}
			}
		} else if (!oneRow.get(14).equals("")) {
			error = error + "<mark>retention_date must not be filled for passive document</mark>" + "<br>";
		}
		// author organization
		if (oneRow.get(7).equals("")) {
			error = error + "<br><mark>author_organization must be filled</mark>" + "<br>";
		} else {
			try {
				String authorOrganizationList = dValue.getSpecifyOptionsList("AuthorOrganizationList",
						oneRow.get(7).toString());

				if (!authorOrganizationList.equals("0")) {
					oneRow.set(7, authorOrganizationList);
				} else {
					error = error + "<br><mark>author_organization is not valid</mark>" + "<br>";
				}
			} catch (DataException ex) {
				Log.error("PHE:checkEDMS:getSpecifyOptionList:" + ex.getMessage());
				error = error + ex.getMessage() + "<br>";
			}
		}
		if (oneRow.get(9).equals("")) {
			error = error + "<mark>company must be filled</mark>" + "<br>";
		}
		if (oneRow.get(11).equals("")) {
			error = error + "<mark>contractor must be filled</mark>" + "<br>";
		}
		if (oneRow.get(13).equals("")) {
			oneRow.set(13, "Not applicable");
		} else if ((!validator.isNumeric(oneRow.get(13).toString()))
				&& (!oneRow.get(13).toString().toUpperCase().equals("NOT APPLICABLE"))) {
			error = error + "<mark>retention_values are: Not applicable, 2, 5, 7, 10, 20, and 50</mark>" + "<br>";
		}
		oneRow.add(error);

		return oneRow;
	}

	public String getTemplateType(Iterator<Row> rowIterator) {
		boolean hasNextRow = false;
		String type = "";
		if (rowIterator.hasNext()) {
			Row row = (Row) rowIterator.next();
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				Cell cell = (Cell) cellIterator.next();
				switch (cell.getCellType()) {
				case Cell.CELL_TYPE_BOOLEAN:
					if (String.valueOf(cell.getBooleanCellValue()).length() > 0) {
						type = String.valueOf(cell.getBooleanCellValue());
					}
					break;
				case Cell.CELL_TYPE_NUMERIC:
					if (String.valueOf(cell.getNumericCellValue()).length() > 0) {
						type = String.valueOf(cell.getNumericCellValue());
					}
					break;
				case Cell.CELL_TYPE_STRING:
					if (String.valueOf(cell.getStringCellValue()).length() > 0) {
						type = String.valueOf(cell.getStringCellValue());
					}
					break;
				}
			}
		}
		return type;
	}

	public ArrayList checkResourceList(ArrayList oneRow, String type, String userLogin, String rootApDest) {
		String error = "";
		DatabaseValue dValue = new DatabaseValue();
		Validator validator = new Validator();
		if (oneRow.get(1).equals("")) {
			error = error + "Title must be filled" + "<br>";
		}
		if (oneRow.get(2).equals("")) {
			error = error + "Number must be filled" + "<br>";
		}
		if (oneRow.get(3).equals("")) {
			error = error + "Division must be filled" + "<br>";
		}
		if ((!validator.isNumeric(oneRow.get(6).toString().trim())) || (oneRow.get(6).toString().trim().length() < 0)) {
			error = error + "Year must be filled and numeric <br>";
		}
		if (oneRow.get(10).equals("")) {
			error = error + "Status must be filled" + "<br>";
		}
		if (!oneRow.get(11).equals("")) {
			Folder folder = new Folder();
			DataBinder binder = new DataBinder();
			DataCollection dColl = new DataCollection();

			Workspace ws = dColl.getSystemWorkspace();
			Util util = new Util();
			util.setUserLogin(userLogin);

			String parentDir = "/" + rootApDest + "/External Resources List";
			String sFolder = oneRow.get(11).toString(); // Contoh penamaan folder pada kolom ini --> /Contoh/Penamaan/Folder
			String folderPath = "";
			if (sFolder != "") {
				folderPath = sFolder;
			}
			String targetCabinet = parentDir + folderPath;
			
			// Check target cabinet is exists or not, if not exists it will be created
			if (!folder.isFolderExist(targetCabinet, binder, ws, util)) {
				// Check or create folder
				Folder fld = new Folder();
				DataBinder binderFolder = new DataBinder();
				fld.DoCheckingFolder(parentDir, folderPath, binderFolder, util.getSystemWorkspace(), util);
			}
			
			if (!folder.isFolderExist(targetCabinet, binder, ws, util)) {
				Log.info("--destination_folder is not found ");
				error = error + "<br><mark>destination_folder is not found</mark>" + "<br>";
			} else {
				//ResultSet rs = folder.GetFolderInfo(parentDir, binder, ws, util);
				ResultSet rs = folder.GetFolderInfo(targetCabinet, binder, ws, util);

				//Log.info("PHE:ExtResource-getXlsFile checkResourceList rs = " + rs);

				String documentNameDipotong = oneRow.get(1).toString();
				if (documentNameDipotong.indexOf(".") >= 0) {
					documentNameDipotong = documentNameDipotong.substring(0, documentNameDipotong.indexOf("."));
				}
				try {
					if (dValue.checkDocumentNameGeneral(documentNameDipotong, rs.getStringValueByName("fFolderGUID"))
							.length() > 0) {
						error = error + dValue.checkDocumentNameGeneral(documentNameDipotong,
								rs.getStringValueByName("fFolderGUID"));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		if (oneRow.get(12).equals("")) {
			error = error + "Link document must be filled" + "<br>";
		}

		if (oneRow.get(13).equals("")) {
			error = error + "Owner PIC must be filled" + "<br>";
		}
		oneRow.add(error);
		return oneRow;
	}

	public static void getXlsxFile() throws FileNotFoundException, IOException {
		File excel = new File("C:\\test.xlsx");
		FileInputStream fis = new FileInputStream(excel);
		System.out.println("fis entered");
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		System.out.println("wb:" + wb);
		XSSFSheet ws = wb.getSheetAt(0);

		System.out.println("ws:" + ws);
		int rowNum = ws.getLastRowNum() + 1;
		int colNum = ws.getRow(0).getLastCellNum();
		String[][] data = new String[rowNum][colNum];

		for (int i = 0; i < rowNum; i++) {
			XSSFRow row = ws.getRow(i);
			for (int j = 0; j < colNum; j++) {
				XSSFCell cell = row.getCell(j);
				String value = cell.toString();
				data[i][j] = value;
				System.out.print(value + "\t\t");
			}
			System.out.println("");
		}
	}
}
