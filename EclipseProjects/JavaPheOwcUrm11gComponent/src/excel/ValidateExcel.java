package excel;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradocservice.Document;
import intradocservice.FilesDIS;
import intradocservice.Folder;
import intradocservice.Users;
import intradocservice.Util;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.DataCollection;
import util.ErrorHandler;
import util.Files;
import util.Validator;

public class ValidateExcel extends Service {
	private String uploadTempHeaderMasterlistDrawing = "TDC MASTER LIST DRAWING";
	private String uploadTempHeaderMasterlistNonDrawing = "TDC MASTER LIST NONDRAWING";
	private String uploadTempHeaderGeneral = "DCRMS GENERAL - GENERAL DOCUMENT";
	private String uploadTempHeaderPublic = "DCRMS PHE PUBLIC - PUBLIC DOCUMENT";
	private String uploadTempHeaderDrawing = "DCRMS GENERAL - DRAWING DOCUMENT";
	private String uploadTempHeaderNonDrawing = "DCRMS GENERAL - NONDRAWING DOCUMENT";
	private String uploadTempHeaderExternalResource = "EXTERNAL RESOURCE LIST";
	String securityGroupAp = "";
	String rootApDest = "";
	String rootApMainCabinet = "";	
	String allowedFileExt = ".XLS";

	public void validateExcel() throws DataException {
		ErrorHandler errorH = new ErrorHandler();
		Files files = new Files();
		DatabaseValue dValue = new DatabaseValue();
		String folderSource = "";
		String filename = this.m_binder.getLocal("xPath");
		String pheAp = this.m_binder.getLocal("pheAp");
		dValue.removeBatchTemp(this.m_binder.getLocal("dUser"));
		dValue.removeBatchTemp("sysadmin");

		Log.info("<== START VALIDATE EXCEL ==>\n"
				+ "PHE AP VALIDATE: " + pheAp);
		
		if (filename == null) {
			DataBinder binderError = errorH.errorHandler("No data found!!");
			this.m_binder.putLocal("StatusMessage", binderError.getLocal("StatusMessage"));
			this.m_binder.putLocal("isError", binderError.getLocal("isError"));
		} else {
			folderSource = this.m_binder.getLocal("folderSource");
			String fileExtension = filename.substring(filename.lastIndexOf("."), filename.length());
			if (fileExtension.toUpperCase().equals(this.allowedFileExt)) {
				Log.info("PHE:ExtResource read excel:" + fileExtension);
				ReadExcel rExcel = new ReadExcel();
				DataCollection dColl = new DataCollection();
				Workspace ws = dColl.getSystemWorkspace();
				ArrayList rowCollection = rExcel.getXlsFile(filename, ws, this.m_binder.getLocal("dUser"),
						this.m_binder.getLocal("pheAp"));

				boolean validTemplate = true;
				if (getTemplateType(rowCollection).length() == 0) {
					this.m_binder.putLocal("errorMessage",
							"Wrong template. Please download specific template from above");
					validTemplate = false;
				} else if ((!getTemplateType(rowCollection).equals(this.uploadTempHeaderMasterlistDrawing))
						&& (!getTemplateType(rowCollection).equals(this.uploadTempHeaderMasterlistNonDrawing))
						&& (!getTemplateType(rowCollection).equals(this.uploadTempHeaderGeneral))) {
					if ((!getTemplateType(rowCollection).equals(this.uploadTempHeaderPublic))
							&& (!getTemplateType(rowCollection).equals(this.uploadTempHeaderDrawing))
							&& (!getTemplateType(rowCollection).equals(this.uploadTempHeaderNonDrawing))
							&& (!getTemplateType(rowCollection).equals(this.uploadTempHeaderExternalResource))) {
						this.m_binder.putLocal("errorMessage",
								"Wrong template. Please download specific template from above");
						validTemplate = false;
					}
				}

				String hasError = executeXls(this.m_binder, rowCollection, getTemplateType(rowCollection), folderSource,
						pheAp);
				ResultSet rSet = dValue.getBatchTemp(this.m_binder.getLocal("dUser"));
				DataResultSet myDataResultSet = new DataResultSet();
				myDataResultSet.copy(rSet);
				this.m_binder.addResultSet("excelRS", myDataResultSet);
				if (validTemplate) {
					if (hasError.equals("hasError")) {
						Date date = new Date();

						doWhenHasError(this.m_binder.getLocal("dUser"), rowCollection, date, this.securityGroupAp);
					} else {
						this.m_binder.putLocal("indexSuccess", String.valueOf(rowCollection.size() - 1));
					}
				}
			}
			files.deleteFile(dValue.getPheConfig("PATH_TMP") + filename);
		}
	}

	public void doWhenHasError(String userLogin, ArrayList rowCollection, Date date, String secGroupAp) {
		DatabaseValue dValue = new DatabaseValue();
		dValue.insertBatchFailedLog(userLogin, date);
		WriteExcel wExcel = new WriteExcel();

		String filenameExcelLog = wExcel.writeXls(userLogin, rowCollection, getTemplateType(rowCollection), date);
		DateFormat dateFormat2 = new SimpleDateFormat("yy-MM-dd HH:mm:ss");

		String dDocTitle = "Upload Failed " + dateFormat2.format(date);

		Document document = new Document();
		Files files = new Files();
		try {
			document.executeCheckin(dValue.getPheConfig("PATH_TMP") + filenameExcelLog, "Document", userLogin,
					secGroupAp, dDocTitle, "Document", date);

			this.m_binder.putLocal("dateDetail", dateFormat2.format(date));
			ResultSet rs = dValue.getDocNameByDocTitle(dDocTitle);
			DataResultSet dRS = new DataResultSet();
			dRS.copy(rs);
			if (!rs.isEmpty()) {
				for (int i = 0; i < dRS.getNumRows(); i++) {
					List tempDocInfo = dRS.getCurrentRowAsList();

					this.m_binder.putLocal("dID", tempDocInfo.get(0).toString());
					this.m_binder.putLocal("dDocName", tempDocInfo.get(1).toString());
				}
			}
		} catch (DataException ex) {
			Log.error("PHE:doWhenHasError:executeCheckin-ex:" + ex.getMessage());
		}
		try {
			files.deleteFile(dValue.getPheConfig("PATH_TMP") + filenameExcelLog);
		} catch (DataException ex) {
			Log.error("PHE:doWhenHasError:deleteFile-ex:" + ex.getMessage());
		}
	}

	public String executeXls(DataBinder binder, ArrayList rowCollection, String type, String folderSource,
			String pheAp) {

		String hasError = "";
		if ((!type.equals("")) && (type.length() > 0)) {
			for (int j = 1; j < rowCollection.size(); j++) {
				ArrayList tempRow = (ArrayList) rowCollection.get(j);
				tempRow = manipulateData(binder, tempRow, type, folderSource, j, pheAp);
				rowCollection.set(j, tempRow);
				int lastIndex = tempRow.size() - 1;
				try {
					if (tempRow.get(lastIndex).toString().length() > 0) {
						hasError = "hasError";
					}
				} catch (Exception localException) {
				}
			}
		}
		return hasError;
	}

	public ArrayList manipulateData(DataBinder binder, ArrayList tempRow, String type, String folderSource, int countJ,
			String pheAp) {
		String typeDrawing = type;
		String indexRow = "";
		String area = "";
		String platform = "";
		String disciplineCode = "";
		String documentType = "";
		String runningNumber = "";
		String sheetNumber = "";
		String documentTitle = "";
		String oldDocNo = "";
		String projectName = "";
		String projectYear = "";
		String ownerOrganization = "";
		String ownerName = "";
		String author = "";
		String remarks = "";
		String userLogin = binder.getLocal("dUser");
		String docTitle = "";
		String securityGroup = "";
		String docClasification = "";
		String sheetName = "";
		String error = "";
		String documentName = "";
		String documentNumber = "";
		String authorOrganization = "";
		String company = "";
		String retentionPeriod = "";
		String buAssetFunction = "";
		String destinationPath = "";
		String documentPurpose = "";
		String MOCNo = "";
		String authorGroupName = "";
		String contractor = "";
		String contractNo = "";
		String WONo = "";
		String revision = "";
		String status = "";
		String dDocName = "";
		String dID = "";
		String retentionDate = "";
		String activePassive = "";
		String updatedDocNamePublish = "";
		String docModel = "";
		String description = "";
		String Type = this.m_binder.getLocal("type");
		String xDepartment = "";
		String xExternalOwner = "";
		String xSubmitDate = "";
		String xYear = "";
		String xStampDate = "";
		String xComments = "";
		String xDocName = "";
		String xStatus = "";
		String sFolder = "";
		String dDocAuthor = "";
		String xReferensi = "";
		String xAuthor = "";
		String docNumber = "";
		String tempStatus = "";
		String xTypeNumber = "";
		String xBusinessStage = "";
		boolean isUpdate = false;
		boolean hasPublish = false;
		boolean allHasPublish = false;
		boolean hasNative = false;
		boolean allHasNative = false;
		boolean hasRegistered = false;
		boolean isUpdateDocInfo = false;
		boolean isUpdatePublish = false;
		boolean isUpdateObjectID = false;
		boolean isCreatePartnerNonDrawing = false;
		boolean isUpdateFolderLocation = false;
		boolean isFolderLocationExt = false;
		DatabaseValue dValue = new DatabaseValue();
		Validator validator = new Validator();
		Util util = new Util();
		FilesDIS filesDIS = new FilesDIS();
		String dDocnameDIS = "";
		String dIDDIS = "";
		try {
			company = dValue.getPheXcompany(pheAp);
		} catch (DataException e) {
			Log.error("PHE:preparation:manipulateData:xcompany:gagal:" + e.getLocalizedMessage());
			error = error + "failed to get xcompany" + "<br>";
		}

		if (type.toUpperCase().equals(this.uploadTempHeaderMasterlistDrawing)) {
			indexRow = String.valueOf(Integer.parseInt(tempRow.get(0).toString()) + 2);
			if (tempRow.size() == 18) {
				if (tempRow.get(17).toString().length() > 0) {
					error = tempRow.get(17).toString();
					area = tempRow.get(1).toString();
					platform = tempRow.get(2).toString();
					disciplineCode = tempRow.get(3).toString();
					documentType = tempRow.get(4).toString();
					runningNumber = tempRow.get(5).toString();
					sheetNumber = tempRow.get(6).toString();
					documentTitle = specialCharacterHandling(tempRow.get(7).toString());
					MOCNo = tempRow.get(8).toString();
					oldDocNo = tempRow.get(9).toString();
					projectName = specialCharacterHandling(tempRow.get(10).toString());
					projectYear = tempRow.get(11).toString();
					ownerOrganization = tempRow.get(12).toString();
					ownerName = tempRow.get(13).toString();
					docTitle = documentTitle;
					author = tempRow.get(14).toString();
					remarks = tempRow.get(15).toString();
					description = tempRow.get(16).toString();
				} else {
					area = tempRow.get(1).toString();
					platform = tempRow.get(2).toString();
					disciplineCode = tempRow.get(3).toString();
					documentType = tempRow.get(4).toString();
					runningNumber = tempRow.get(5).toString();
					sheetNumber = tempRow.get(6).toString();
					documentTitle = specialCharacterHandling(tempRow.get(7).toString());
					MOCNo = tempRow.get(8).toString();
					oldDocNo = tempRow.get(9).toString();
					projectName = specialCharacterHandling(tempRow.get(10).toString());
					projectYear = tempRow.get(11).toString();
					ownerOrganization = tempRow.get(12).toString();
					ownerName = tempRow.get(13).toString();
					remarks = tempRow.get(14).toString();
					docTitle = documentTitle;
					sheetName = "";
					
					String keyConfApSGML = "SECGRP_MASTERLIST_" + pheAp;
					try {
						this.securityGroupAp = dValue.getPheConfig(keyConfApSGML.toUpperCase().trim());
					} catch (DataException ex) {
						Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
					}
					securityGroup = securityGroupAp;
					
					String keyConfApDTMLD = "DOCTYP_MSLIST_DRW_" + pheAp;
					String docClasificationAp = "";
					try {
						docClasificationAp = dValue.getPheConfig(keyConfApDTMLD.toUpperCase().trim());
					} catch (DataException ex) {
						Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
					}
					docClasification = docClasificationAp;
					
					Log.info("*** MASTERLIST DRAWING DEBUG INPUT***\n PHE AP: " + pheAp + "\n securityGroupAp: " + securityGroup + "\n docClasificationAp: " + docClasification);
					
					if (runningNumber.length() > 0) {
						if (Integer.parseInt(runningNumber) < 10) {
							runningNumber = "000" + Integer.parseInt(runningNumber);
						} else if (Integer.parseInt(runningNumber) < 100) {
							runningNumber = "00" + Integer.parseInt(runningNumber);
						} else if (Integer.parseInt(runningNumber) < 1000) {
							runningNumber = "0" + Integer.parseInt(runningNumber);
						}
						String tempError = validator.validateDocumentNumberForTDCDrawing(area, platform, disciplineCode,
								documentType, runningNumber, sheetNumber, type);
						if (tempError.length() > 0) {
							error = error + tempError + "<br>";
						}
					} else {
						try {
							runningNumber = dValue.generateRunningNumber("masterlistdrawing", disciplineCode,
									area + "-" + platform + "-" + disciplineCode + "-" + documentType);
						} catch (DataException ex) {
							Log.error("PHE:manipulateData:tdcdrawing:generategagal:" + ex.getMessage());
							error = error + "failed to generate running number" + "<br>";
						}
					}
					if (runningNumber.length() == 0) {
						error = error + "<mark>running_number is invalid</mark>" + "<br>";
					}
					documentNumber = area + "-" + platform + "-" + disciplineCode + "-" + documentType + "-"
							+ runningNumber;
					if (error.length() == 0) {
						try {
							if (dValue.getDIDDocumentNumberDeteledStatus(documentNumber, sheetNumber).length() > 0) {
								isUpdateDocInfo = true;
							}
							if (dValue.getDocumentNumberCombination(area, platform, disciplineCode, documentType,
									runningNumber, sheetNumber, type).length() > 0) {
								error = error + "<mark>running_number has already exists.</mark><br>";
							}
						} catch (DataException ex) {
							Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
				}
			}
		} else if (type.toUpperCase().equals(this.uploadTempHeaderMasterlistNonDrawing)) {
			indexRow = String.valueOf(Integer.parseInt(tempRow.get(0).toString()) + 2);
			if (tempRow.size() == 15) {
				if (tempRow.get(14).toString().length() > 0) {
					error = tempRow.get(14).toString();
					area = tempRow.get(1).toString();
					disciplineCode = tempRow.get(2).toString();
					documentType = tempRow.get(3).toString();
					runningNumber = tempRow.get(4).toString();
					documentTitle = specialCharacterHandling(tempRow.get(5).toString());
					docTitle = documentTitle;
					oldDocNo = tempRow.get(6).toString();
					projectName = specialCharacterHandling(tempRow.get(7).toString());
					projectYear = tempRow.get(8).toString();
					ownerOrganization = tempRow.get(9).toString();
					ownerName = tempRow.get(10).toString();
					author = tempRow.get(11).toString();
					remarks = tempRow.get(12).toString();
					description = tempRow.get(13).toString();
				} else {
					area = tempRow.get(1).toString();
					disciplineCode = tempRow.get(2).toString();
					documentType = tempRow.get(3).toString();
					runningNumber = tempRow.get(4).toString();
					documentTitle = specialCharacterHandling(tempRow.get(5).toString());
					oldDocNo = tempRow.get(6).toString();
					projectName = specialCharacterHandling(tempRow.get(7).toString());
					projectYear = tempRow.get(8).toString();
					ownerOrganization = tempRow.get(9).toString();
					ownerName = tempRow.get(10).toString();
					author = tempRow.get(11).toString();
					remarks = tempRow.get(12).toString();
					description = tempRow.get(13).toString();
					docTitle = documentTitle;
					sheetName = "";

					String keyConfApSGML = "SECGRP_MASTERLIST_" + pheAp;
					try {
						this.securityGroupAp = dValue.getPheConfig(keyConfApSGML.toUpperCase().trim());
					} catch (DataException ex) {
						Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
					}
					securityGroup = securityGroupAp;
					
					String keyConfApDTMLND = "DOCTYP_MSLIST_NONDRW_" + pheAp;
					String docClasificationAp = "";
					try {
						docClasificationAp = dValue.getPheConfig(keyConfApDTMLND.toUpperCase().trim());
					} catch (DataException ex) {
						Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
					}
					docClasification = docClasificationAp;
					
					Log.info("### MASTERLIST NON DRAWING DEBUG INPUT###\n PHE AP: " + pheAp + "\n securityGroupAp: " + securityGroup + "\n docClasificationAp: " + docClasification);
					
					if (runningNumber.length() > 0) {
						if (Integer.parseInt(runningNumber) < 10) {
							runningNumber = "000" + Integer.parseInt(runningNumber);
						} else if (Integer.parseInt(runningNumber) < 100) {
							runningNumber = "00" + Integer.parseInt(runningNumber);
						} else if (Integer.parseInt(runningNumber) < 1000) {
							runningNumber = "0" + Integer.parseInt(runningNumber);
						}
						String tempError = validator.validateDocumentNumberForTDCNonDrawing(area, disciplineCode,
								documentType, runningNumber, sheetNumber, type);
						if (tempError.length() > 0) {
							error = error + tempError + "<br>";
						}
					} else {
						try {
							runningNumber = dValue.generateRunningNumber("masterlistnondrawing", disciplineCode,
									area + "-" + disciplineCode + "-" + documentType);
						} catch (DataException ex) {
							Log.error("PHE:manipulateData:tdcdrawing:generategagal:" + ex.getMessage());
							error = error + "<mark>failed to generate running number</mark>" + "<br>";
						}
					}
					if (runningNumber.length() == 0) {
						error = error + "<mark>running_number is invalid</mark>" + "<br>";
					}
					documentNumber = area + "-" + disciplineCode + "-" + documentType + "-" + runningNumber;

					Log.info("--MasterlistNonDrawing error=" + error);
					if (error.length() == 0) {
						try {
							if (dValue.getDIDDocumentNumberDeteledStatus(documentNumber, sheetNumber).length() > 0) {
								isUpdateDocInfo = true;
							}
						} catch (DataException ex) {
							Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
						}
					}
				}
			}
		} else if (type.toUpperCase().equals(this.uploadTempHeaderNonDrawing)) {
			indexRow = String.valueOf(Integer.parseInt(tempRow.get(0).toString()) + 2);
			if (tempRow.size() == 17) {
				if (tempRow.get(16).toString().length() > 0) {
					error = tempRow.get(16).toString();
					documentName = tempRow.get(1).toString();
					documentTitle = tempRow.get(2).toString();
					documentPurpose = tempRow.get(3).toString();
					activePassive = tempRow.get(4).toString();
					oldDocNo = tempRow.get(5).toString();
					author = tempRow.get(6).toString();
					authorOrganization = specialCharacterHandling(tempRow.get(7).toString());
					authorGroupName = tempRow.get(8).toString();
					docTitle = documentTitle;
					//company = specialCharacterHandling(tempRow.get(9).toString());
					projectName = tempRow.get(10).toString();
					contractor = tempRow.get(11).toString();
					contractNo = tempRow.get(12).toString();
					retentionPeriod = tempRow.get(13).toString();
					retentionDate = tempRow.get(14).toString();
					WONo = tempRow.get(15).toString();
				} else {
					try {
						documentName = tempRow.get(1).toString();
						if ((!validator.isExistFile(folderSource + documentName)) && (!Type.equalsIgnoreCase("DIS"))) {
							error = error + "<mark>The primary file does not exist.</mark>" + "<br> Type=" + Type;
						} else {
							documentName = tempRow.get(1).toString();
							documentTitle = tempRow.get(2).toString();
							docTitle = documentTitle;
							if (tempRow.get(3).toString().toUpperCase().equals("PUBLISHED")) {
								documentPurpose = "Published";
							} else {
								documentPurpose = "Native";
							}
							if (tempRow.get(4).toString().toUpperCase().equals("ACTIVE")) {
								activePassive = "Active";
							} else {
								activePassive = "Passive";
							}
							oldDocNo = tempRow.get(5).toString();
							author = tempRow.get(6).toString();
							authorOrganization = specialCharacterHandling(tempRow.get(7).toString());
							authorGroupName = tempRow.get(8).toString();
							//company = specialCharacterHandling(tempRow.get(9).toString());
							projectName = tempRow.get(10).toString();
							contractor = tempRow.get(11).toString();
							contractNo = tempRow.get(12).toString();
							retentionPeriod = tempRow.get(13).toString();
							retentionDate = tempRow.get(14).toString();
							WONo = tempRow.get(15).toString();
							if (documentName.indexOf("~") >= 0) {
								String[] splitDocName = validator.splitDocName(documentName).split(";");
								documentNumber = splitDocName[0];
								revision = splitDocName[1];
								if (!splitDocName[2].toString().equalsIgnoreCase("-1")) {
									sheetNumber = splitDocName[2];
								}
							} else {
								try {
									try {
										if (documentNumber.indexOf(" ") >= 0) {
											documentNumber = documentNumber.substring(0, documentNumber.indexOf(" "));
										}
									} catch (Exception localException1) {
									}
									revision = String
											.valueOf(Integer.valueOf(dValue.getLatestRevisionDocument(documentNumber,
													sheetNumber, documentPurpose)).intValue() + 1);
								} catch (DataException ex) {
									Log.error("PHE:splitDocName:ex:" + ex.getMessage());
								}
							}
							String[] splitDocNumber = documentNumber.split("-");

							area = splitDocNumber[0];
							disciplineCode = splitDocNumber[1];
							documentType = splitDocNumber[2];
							runningNumber = splitDocNumber[3];
							//securityGroup = "PHE_TDC";
							String keyConfApSGNative = "SECGRP_TDC_" + pheAp;
							try {
								securityGroup = dValue.getPheConfig(keyConfApSGNative.toUpperCase().trim());
							} catch (DataException ex) {
								Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
							}
							
							ArrayList al = new ArrayList();
							try {
								try {
									al = dValue.getAllDocPurpose(documentName);
								} catch (DataException ex) {
									Log.error("PHE:getAllDocPurpose:errData:" + ex.getMessage());
								} catch (Exception ex) {
									Log.error("PHE:getAllDocPurpose:errServiceException:" + ex.getMessage());
								}
								if (al.size() == 0) {
									if (documentPurpose.toUpperCase().equals("PUBLISHED")) {
										status = "Published";
									}
									if (documentPurpose.toUpperCase().equals("NATIVE")) {
										status = "Native";
									}
								} else {
									for (int i = 0; i < al.size(); i++) {
										String[] aList = al.get(i).toString().split(";");
										if (aList[0] == null) {
											hasRegistered = true;
										} else {
											if (aList[0].toString().toUpperCase().equals("PUBLISHED")) {
												allHasPublish = true;
												if (documentPurpose.equalsIgnoreCase("Published")) {
													dDocName = aList[1];
													if (!aList[3].equalsIgnoreCase("Expired")) {
														tempStatus = aList[3];
													} else {
														tempStatus = "Published";
													}
												} else if (aList[5].toString().toUpperCase().equals(revision)) {
													hasPublish = true;
													dDocName = aList[1];
													tempStatus = "Implemented";
												}
												updatedDocNamePublish = dDocName;
											}
											if (aList[0].toString().toUpperCase().equals("NATIVE")) {
												allHasNative = true;
												if (aList[5].toString().toUpperCase().equals(revision)) {
													hasNative = true;
												}
												if (documentPurpose.equalsIgnoreCase("Native")) {
													dDocName = aList[1];
													if (!aList[3].equalsIgnoreCase("Expired")) {
														tempStatus = aList[3];
													} else {
														tempStatus = "Native";
													}
												}
											}
											if (aList[0].toString().toUpperCase().equals("IMPLEMENTED")) {
												allHasPublish = true;
												if (aList[5].toString().toUpperCase().equals(revision)) {
													hasPublish = true;
												}
												if (documentPurpose.equalsIgnoreCase("Published")) {
													dDocName = aList[1];
													if (!aList[3].equalsIgnoreCase("Expired")) {
														tempStatus = aList[3];
													} else {
														tempStatus = "Implemented";
													}
												}
											}
											if ((aList[0].toString().toUpperCase().equals("")) || (aList[0] == null)) {
												hasRegistered = true;
												dDocName = aList[1];
												dID = aList[2];
											}
										}
									}
									if ((hasPublish) && (!hasNative)
											&& (documentPurpose.toUpperCase().equals("NATIVE"))) {
										isUpdatePublish = true;
										status = "Native";
									}
									if ((!hasPublish) && (hasNative)
											&& (documentPurpose.toUpperCase().equals("PUBLISHED"))) {
										status = "Implemented";
									}
									if ((hasPublish) && (hasNative) && ((documentPurpose.toUpperCase().equals("NATIVE"))
											|| (documentPurpose.toUpperCase().equals("PUBLISHED")))) {
										isUpdate = true;
										status = "Implemented";
										if (documentPurpose.equalsIgnoreCase("Native")) {
											status = "Native";
										}
									}
									if ((!hasPublish) && (!hasNative)
											&& (documentPurpose.toUpperCase().equals("NATIVE"))) {
										status = "Native";
									}
									if ((!hasPublish) && (!hasNative)
											&& (documentPurpose.toUpperCase().equals("PUBLISHED"))) {
										status = "Published";
									}
								}
							} catch (Exception ex) {
								Log.error("PHE:DATAEXCEPTION:ex:" + ex.getMessage());
							}
							docTitle = documentTitle;

							String keyConfApSG = "SECGRP_NONDRAWING_" + pheAp;
							try {
								this.securityGroupAp = dValue.getPheConfig(keyConfApSG.toUpperCase().trim());
							} catch (DataException ex) {
								Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
							}
							String keyConfApDT = "DOCTYP_NONDRAWING_" + pheAp;
							String docClasificationAp = "";
							try {
								docClasificationAp = dValue.getPheConfig(keyConfApDT.toUpperCase().trim());
							} catch (DataException ex) {
								Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
							}
							
							if (activePassive.equals("Passive")) {
								docClasification = "NonDrawingPassive";
							} else {
								docClasification = docClasificationAp;
							}
							
							securityGroup = securityGroupAp;
						}
					} catch (Exception ex) {
						Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		} else if (type.toUpperCase().equals(this.uploadTempHeaderGeneral)) {
			indexRow = String.valueOf(Integer.parseInt(tempRow.get(0).toString()) + 2);
			documentName = tempRow.get(1).toString();
			documentTitle = tempRow.get(2).toString();
			documentNumber = tempRow.get(3).toString();
			author = tempRow.get(4).toString();
			authorOrganization = specialCharacterHandling(tempRow.get(5).toString());
			projectName = tempRow.get(6).toString();
			//company = specialCharacterHandling(tempRow.get(7).toString());
			oldDocNo = tempRow.get(8).toString();
			retentionPeriod = tempRow.get(9).toString();
			buAssetFunction = tempRow.get(10).toString();
			destinationPath = tempRow.get(11).toString();
			docModel = tempRow.get(12).toString();
			description = tempRow.get(13).toString();
			documentName = tempRow.get(1).toString();
			if (!validator.isExistFile(folderSource + documentName)) {
				error = error + "<mark>The primary file does not exist.</mark>" + "<br>";
			}
			docTitle = documentTitle;
			docClasification = "General";

			Users users = new Users();
			String roles = users.getRoleByUser(userLogin);
			String[] path = destinationPath.split("/");
			if (destinationPath.toUpperCase().indexOf("/CABINETS/OTHERS") >= 0) {
				//securityGroup = "PHE_TDC";
				String keyConfApSGGeneral = "SECGRP_TDC_" + pheAp;
				try {
					securityGroup = dValue.getPheConfig(keyConfApSGGeneral.toUpperCase().trim());
				} catch (DataException ex) {
					Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			String keyConfApCab = "CABINETS_" + pheAp;
			this.rootApDest = path[1];
			this.rootApMainCabinet = path[2];
			try {
				this.rootApDest = dValue.getPheConfig(keyConfApCab.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApSG = "SECGRP_PUBLIC_" + pheAp;
			try {
				this.securityGroupAp = dValue.getPheConfig(keyConfApSG.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApDT = "DOCTYP_GENERAL_" + pheAp;
			String docClasificationAp = "";
			try {
				docClasificationAp = dValue.getPheConfig(keyConfApDT.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
			}
			docClasification = docClasificationAp;
			if (destinationPath.toUpperCase()
					.indexOf("/" + this.rootApDest.toUpperCase() + "/" + this.rootApMainCabinet.toUpperCase()) >= 0) {
				securityGroup = this.securityGroupAp;
			}
			if (securityGroup.length() == 0) {
				int targetI = -1;
				for (int i = 0; i < path.length; i++) {
					if ((path[i].toUpperCase().equals("CABINETS"))
							|| (path[i].toUpperCase().equals(this.rootApDest.toUpperCase()))) {
						targetI = i + 1;
						break;
					}
				}
				if (targetI > -1) {
					securityGroup = path[targetI].substring(0, path[targetI].indexOf(" "));
					if (securityGroup.toUpperCase().indexOf(roles.toUpperCase()) >= 0) {
						securityGroup = this.securityGroupAp + "_Department";
					}
				}
			}
			if (tempRow.size() == 13) {
				error = tempRow.get(12).toString();
			}
			if (securityGroup.length() == 0) {
				error = error + "<mark>security group not valid </mark><br>";
			}
			status = "Published";
		} else if (type.toUpperCase().equals(this.uploadTempHeaderPublic)) {
			indexRow = String.valueOf(Integer.parseInt(tempRow.get(0).toString()) + 2);
			documentName = tempRow.get(1).toString();
			documentTitle = tempRow.get(2).toString();
			documentNumber = tempRow.get(3).toString();
			author = tempRow.get(4).toString();
			authorOrganization = specialCharacterHandling(tempRow.get(5).toString());
			projectName = tempRow.get(6).toString();
			//company = specialCharacterHandling(tempRow.get(7).toString());
			oldDocNo = tempRow.get(8).toString();
			retentionPeriod = tempRow.get(9).toString();
			buAssetFunction = tempRow.get(10).toString();
			destinationPath = tempRow.get(11).toString();
			docModel = tempRow.get(12).toString();
			description = tempRow.get(13).toString();
			documentName = tempRow.get(1).toString();
			if (!validator.isExistFile(folderSource + documentName)) {
				error = error + "<mark>The primary file does not exist.</mark>" + "<br>";
			}
			docTitle = documentTitle;
			docClasification = "General";
			Users users = new Users();
			String roles = users.getRoleByUser(userLogin);
			String[] path = destinationPath.split("/");

			String keyConfApCab = "CABINETS_PUBLIC_GENERAL_PHE";
			String rootApDest = path[0];
			try {
				rootApDest = dValue.getPheConfig(keyConfApCab.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApSG = "SECGRP_PUBLIC_GENERAL_PHE";
			try {
				securityGroupAp = dValue.getPheConfig(keyConfApSG.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApDT = "DOCTYP_PUBLIC_GENERAL_PHE";
			String docClasificationAp = "";
			try {
				docClasificationAp = dValue.getPheConfig(keyConfApDT.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
			}
			docClasification = docClasificationAp;
			if (destinationPath.toUpperCase()
					.indexOf("/" + rootApDest.toUpperCase() + "/PUBLIC GENERAL DOCUMENTS") >= 0) {
				securityGroup = securityGroupAp;
			}
			if (securityGroup.length() == 0) {
				int targetI = -1;
				for (int i = 0; i < path.length; i++) {
					if (path[i].toUpperCase().equals("CABINETS")) {
						targetI = i + 1;
						break;
					}
				}
				if (targetI > -1) {
					securityGroup = path[targetI].substring(0, path[targetI].indexOf(" "));
					if (securityGroup.toUpperCase().indexOf(roles.toUpperCase()) >= 0) {
						securityGroup = securityGroupAp + "_Department";
					}
				}
			}
			if (tempRow.size() == 13) {
				error = tempRow.get(12).toString();
			}
			if (securityGroup.length() == 0) {
				error = error + "<mark>destination_path not valid </mark><br>";
			}
			status = "Published";
		} else if (type.toUpperCase().equals(this.uploadTempHeaderDrawing)) {
			indexRow = String.valueOf(Integer.parseInt(tempRow.get(0).toString()) + 2);
			if (tempRow.size() == 17) {
				if (tempRow.get(16).toString().length() > 0) {
					error = tempRow.get(16).toString();
					documentName = tempRow.get(1).toString();
					documentTitle = tempRow.get(2).toString();
					documentPurpose = tempRow.get(3).toString();
					MOCNo = tempRow.get(4).toString();
					oldDocNo = tempRow.get(5).toString();
					author = tempRow.get(6).toString();
					authorOrganization = specialCharacterHandling(tempRow.get(7).toString());
					authorGroupName = tempRow.get(8).toString();
					docTitle = documentTitle;
					//company = specialCharacterHandling(tempRow.get(9).toString());
					projectName = tempRow.get(10).toString();
					contractor = tempRow.get(11).toString();
					contractNo = tempRow.get(12).toString();
					WONo = tempRow.get(13).toString();

					docModel = tempRow.get(14).toString();
					description = tempRow.get(15).toString();
				} else {
					try {
						docModel = tempRow.get(14).toString();
						description = tempRow.get(15).toString();
						if (description.isEmpty()) {
							description = " ";
						}
						documentName = tempRow.get(1).toString();
						if ((!validator.isExistFile(folderSource + documentName)) && (!Type.equalsIgnoreCase("DIS"))) {
							error = error + "<mark>The primary file does not exist.</mark>" + "<br>";
						} else if ((!validator.isMaxFile(folderSource + documentName))
								&& (!Type.equalsIgnoreCase("DIS"))) {
							error = error + "<mark>The primary file sholud be less or equal than 10 MB</mark>" + "<br>";
						} else {
							documentName = tempRow.get(1).toString();
							documentTitle = tempRow.get(2).toString();
							if (tempRow.get(3).toString().toUpperCase().equals("PUBLISHED")) {
								documentPurpose = "Published";
							} else {
								documentPurpose = "Native";
							}
							MOCNo = tempRow.get(4).toString();
							oldDocNo = tempRow.get(5).toString();
							author = tempRow.get(6).toString();
							authorOrganization = specialCharacterHandling(tempRow.get(7).toString());
							authorGroupName = tempRow.get(8).toString();
							//company = specialCharacterHandling(tempRow.get(9).toString());
							projectName = tempRow.get(10).toString();
							contractor = tempRow.get(11).toString();
							contractNo = tempRow.get(12).toString();
							WONo = tempRow.get(13).toString();
							if (documentName.indexOf("~") >= 0) {
								String[] splitDocName = validator.splitDocName(documentName).split(";");
								documentNumber = splitDocName[0];
								revision = splitDocName[1];
								if (!splitDocName[2].toString().equalsIgnoreCase("-1")) {
									sheetNumber = splitDocName[2];
								}
							} else {
								try {
									try {
										if (documentNumber.indexOf(" ") >= 0) {
											documentNumber = documentNumber.substring(0, documentNumber.indexOf(" "));
										}
									} catch (Exception localException2) {
									}
									revision = String
											.valueOf(Integer.valueOf(dValue.getLatestRevisionDocument(documentNumber,
													sheetNumber, documentPurpose)).intValue() + 1);
								} catch (DataException ex) {
									Log.error("PHE:splitDocName:ex:" + ex.getMessage());
								}
							}
							String checkDocument = dValue.checkDocumentName(documentNumber, Integer.valueOf(revision),
									sheetNumber.toString(), documentPurpose.toString());

							String[] splitDocNumber = documentNumber.split("-");
							area = splitDocNumber[0];
							platform = splitDocNumber[1];
							disciplineCode = splitDocNumber[2];
							documentType = splitDocNumber[3];
							runningNumber = splitDocNumber[4];
							if (!validator.IsValidDoc3D(documentType, docModel)) {
								error = error
										+ "<mark>Colaborate between Document Model and Discipline Code is invalid.</mark>"
										+ "<br>";
							} else if (!dValue.validDisciplineCode(disciplineCode, runningNumber, type) && pheAp == "PHEONWJ") {
								error = error
										+ "<mark>Colaborate between Running Number and Discipline Code is invalid.</mark>"
										+ "<br>";
							} else if (checkDocument.equals("revisionAlreadyExist")) {
								error = error + "<mark>Document name already exist</mark>" + "<br>";
							} else if (checkDocument.equalsIgnoreCase("revisionError")) {
								error = error + "<mark>Document name has higher revision</mark>" + "<br>";
							} else {
								ArrayList al = new ArrayList();
								try {
									try {
										al = dValue.getAllDocPurpose(documentName);
									} catch (DataException ex) {
										Log.error("PHE:getAllDocPurpose:errData:" + ex.getMessage());
									} catch (Exception ex) {
										Log.error("PHE:getAllDocPurpose:errServiceException:" + ex.getMessage());
									}
									if (al.size() == 0) {
										if (documentPurpose.toUpperCase().equals("PUBLISHED")) {
											status = "Published";
										}
										if (documentPurpose.toUpperCase().equals("NATIVE")) {
											status = "Native";
										}
									} else {
										for (int i = 0; i < al.size(); i++) {
											String[] aList = al.get(i).toString().split(";");
											if (aList[0] == null) {
												hasRegistered = true;
											} else {
												if (aList[0].toString().toUpperCase().equals("PUBLISHED")) {
													allHasPublish = true;
													if (documentPurpose.equalsIgnoreCase("Published")) {
														dDocName = aList[1];
														if (!aList[3].equalsIgnoreCase("Expired")) {
															tempStatus = aList[3];
														} else {
															tempStatus = "Published";
															isUpdateFolderLocation = true;
														}
													} else if (((!sheetNumber.equalsIgnoreCase(""))
															&& (sheetNumber != null)
															&& (!sheetNumber.equalsIgnoreCase("0")))
															|| (aList[6].length() == 0)
															|| (aList[6].equalsIgnoreCase("0"))) {
														if (aList[5].toString().toUpperCase().equals(revision)) {
															hasPublish = true;
															dDocName = aList[1];
															tempStatus = "Implemented";
														}
													} else if ((aList[5].toString().toUpperCase().equals(revision))
															&& (aList[6].toString().toUpperCase()
																	.equals(sheetNumber))) {
														hasPublish = true;
														dDocName = aList[1];
														tempStatus = "Implemented";
													}
													updatedDocNamePublish = dDocName;
												}
												if (aList[0].toString().toUpperCase().equals("IMPLEMENTED")) {
													allHasPublish = true;
													if (aList[5].toString().toUpperCase().equals(revision)) {
														hasPublish = true;
													}
													if (documentPurpose.equalsIgnoreCase("Published")) {
														dDocName = aList[1];
														tempStatus = "Published";
													}
												}
												if (aList[0].toString().toUpperCase().equals("NATIVE")) {
													allHasNative = true;
													if (((!sheetNumber.equalsIgnoreCase("")) && (sheetNumber != null)
															&& (!sheetNumber.equalsIgnoreCase("0")))
															|| (aList[6].length() == 0)
															|| (aList[6].equalsIgnoreCase("0"))) {
														if (aList[5].toString().toUpperCase().equals(revision)) {
															hasNative = true;
														}
													} else if ((aList[5].toString().toUpperCase().equals(revision))
															&& (aList[6].toString().toUpperCase()
																	.equals(sheetNumber))) {
														hasNative = true;
													}
													if (documentPurpose.equalsIgnoreCase("Native")) {
														dDocName = aList[1];

														tempStatus = "Native";
													}
												}
												if ((aList[0].toString().toUpperCase().equals(""))
														|| (aList[0] == null)) {
													hasRegistered = true;
													dDocName = aList[1];
													dID = aList[2];
												}
											}
										}
										if ((hasPublish) && (!hasNative)
												&& (documentPurpose.toUpperCase().equals("NATIVE"))) {
											isUpdatePublish = true;
											status = "Native";
											if (docModel.equalsIgnoreCase("3D")) {
												status = "Published";
												isUpdateFolderLocation = true;
											}
										}
										if ((!hasPublish) && (hasNative)
												&& (documentPurpose.toUpperCase().equals("PUBLISHED"))) {
											status = "Implemented";
										}
										if ((hasPublish) && (hasNative)
												&& ((documentPurpose.toUpperCase().equals("NATIVE"))
														|| (documentPurpose.toUpperCase().equals("PUBLISHED")))) {
											isUpdate = true;
											status = "Implemented";
											if (documentPurpose.equalsIgnoreCase("Native")) {
												status = "Native";
												isUpdatePublish = true;
											}
										}
										if ((!hasPublish) && (!hasNative)
												&& (documentPurpose.toUpperCase().equals("NATIVE"))) {
											status = "Native";
											if (docModel.equalsIgnoreCase("3D")) {
												status = "Published";
												isUpdateFolderLocation = true;
											}
										}
										if ((!hasPublish) && (!hasNative)
												&& (documentPurpose.toUpperCase().equals("PUBLISHED"))) {
											status = "Published";
										}
									}
								} catch (Exception ex) {
									Log.error("PHE:DATAEXCEPTION:ex:" + ex.getMessage());
								}
								docTitle = documentTitle;

								String keyConfApSG = "SECGRP_DRAWING_" + pheAp;
								try {
									this.securityGroupAp = dValue.getPheConfig(keyConfApSG.toUpperCase().trim());
								} catch (DataException ex) {
									Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
								}
								String keyConfApDT = "DOCTYP_DRAWING_" + pheAp;
								String docClasificationAp = "";
								try {
									docClasificationAp = dValue.getPheConfig(keyConfApDT.toUpperCase().trim());
								} catch (DataException ex) {
									Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
								}

								docClasification = docClasificationAp;
								securityGroup = securityGroupAp;
							}
						}
					} catch (Exception ex) {
						Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		} else if (type.toUpperCase().equals(this.uploadTempHeaderExternalResource)) {
			indexRow = String.valueOf(Integer.parseInt(tempRow.get(0).toString()) + 2);
			if (tempRow.size() <= 17) {
				error = tempRow.get(16).toString();
				documentTitle = specialCharacterHandling(tempRow.get(1).toString());
				documentNumber = tempRow.get(2).toString();
				xDepartment = tempRow.get(3).toString();
				xExternalOwner = tempRow.get(4).toString();
				xSubmitDate = tempRow.get(5).toString();
				xYear = tempRow.get(6).toString();
				xStampDate = tempRow.get(7).toString();
				xComments = tempRow.get(8).toString();
				xDocName = tempRow.get(9).toString();
				xStatus = tempRow.get(10).toString();
				sFolder = tempRow.get(11).toString();
				dDocAuthor = userLogin;
				xTypeNumber = tempRow.get(12).toString();
				xBusinessStage = tempRow.get(13).toString();
				xReferensi = tempRow.get(14).toString();
				xAuthor = tempRow.get(15).toString();
				docTitle = documentTitle;
				sheetName = "";
				countJ = countJ;
				docNumber = documentNumber;
				DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
				Date date = new Date();
				runningNumber = "EXT" + dateFormat.format(date) + countJ;
				
				String keyConfApSG = "SECGRP_PUBLIC_" + pheAp; //Security Group Ext Resources = Security Group Public AP
				try {
					this.securityGroupAp = dValue.getPheConfig(keyConfApSG.toUpperCase().trim());
				} catch (DataException ex) {
					Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
				}
				String keyConfApDT = "DOCTYP_EXTRES_" + pheAp;
				String docClasificationAp = "";
				try {
					docClasificationAp = dValue.getPheConfig(keyConfApDT.toUpperCase().trim());
				} catch (DataException ex) {
					Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
				}
				documentType = docClasificationAp;
				securityGroup = securityGroupAp;
				if (error.length() == 0) {
					try {
						if (dValue.getDIDDocumentNumberDeteledStatus(documentNumber, sheetNumber).length() > 0) {
							isUpdateDocInfo = true;
						}
					} catch (DataException ex) {
						Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}

		Document document = new Document();
		String executeResult = "";
		try {
			if (documentNumber.indexOf(" ") >= 0) {
				documentNumber = documentNumber.substring(0, documentNumber.indexOf(" "));
			}
		} catch (Exception localException3) {
		}
		
		if (error.length() < 1) {
			try {
				if ((type.equalsIgnoreCase(this.uploadTempHeaderMasterlistDrawing))
						|| (type.equalsIgnoreCase(this.uploadTempHeaderMasterlistNonDrawing))) {
					if (Type.equalsIgnoreCase("DIS")) {
						executeResult = document.executeUpdateDocInfoDIS(binder, typeDrawing, indexRow, area, platform,
								disciplineCode, documentType, runningNumber, sheetNumber, documentTitle, oldDocNo,
								projectName, projectYear, ownerOrganization, ownerName, author, remarks, userLogin,
								docTitle, securityGroup, docClasification, sheetName, error, documentName,
								documentNumber, authorOrganization, company, retentionPeriod, buAssetFunction,
								destinationPath, folderSource + documentName, MOCNo, authorGroupName, contractor,
								contractNo, WONo, revision, status, documentPurpose, activePassive, retentionDate,
								docModel, description, true);
					} else if (isUpdateDocInfo) {
						executeResult = document.executeUpdateDocInfo(binder, typeDrawing, indexRow, area, platform,
								disciplineCode, documentType, runningNumber, sheetNumber, documentTitle, oldDocNo,
								projectName, projectYear, ownerOrganization, ownerName, author, remarks, description,
								userLogin, docTitle, securityGroup, docClasification, sheetName, error, documentName,
								documentNumber, authorOrganization, company, retentionPeriod, buAssetFunction,
								destinationPath, folderSource + documentName, MOCNo, authorGroupName, contractor,
								contractNo, WONo, revision, status, documentPurpose, activePassive, retentionDate,
								true);
					} else {
						Log.info("<== START executeCheckinUniversal ATAS DRAWING ==> ");
						Log.info(binder+"\n"+typeDrawing+"\n"+indexRow+"\n"+area+"\n"+platform+"\n"+disciplineCode+"\n"+documentType+"\n"+runningNumber+"\n"+sheetNumber+"\n"+documentTitle+"\n"+oldDocNo+"\n"+projectName+"\n"+projectYear+"\n"+ownerOrganization+"\n"+ownerName+"\n"+author+"\n"+remarks+"\n"+userLogin+"\n"+docTitle+"\n"+securityGroup+"\n"+docClasification+"\n"+sheetName+"\n"+error+"\n"+documentName+"\n"+documentNumber+"\n"+authorOrganization+"\n"+company+"\n"+retentionPeriod+"\n"+buAssetFunction+"\n"+destinationPath+"\n"+folderSource+"\n"+documentName+"\n"+MOCNo+"\n"+authorGroupName+"\n"+contractor+"\n"+contractNo+"\n"+WONo+"\n"+revision+"\n"+status+"\n"+documentPurpose+"\n"+dDocName+"\n"+dID+"\n"+activePassive+"\n"+retentionDate+"\n"+docModel+"\n"+description+"\n"+true);
						executeResult = document.executeCheckinUniversal(binder, typeDrawing, indexRow, area, platform,
								disciplineCode, documentType, runningNumber, sheetNumber, documentTitle, oldDocNo,
								projectName, projectYear, ownerOrganization, ownerName, author, remarks, userLogin,
								docTitle, securityGroup, docClasification, sheetName, error, documentName,
								documentNumber, authorOrganization, company, retentionPeriod, buAssetFunction,
								destinationPath, folderSource + documentName, MOCNo, authorGroupName, contractor,
								contractNo, WONo, revision, status, documentPurpose, activePassive, retentionDate,
								docModel, description, true);
					}
					isUpdateObjectID = true;
					if ((executeResult.indexOf("executeResult") < 0)
							&& (type.equalsIgnoreCase(this.uploadTempHeaderMasterlistNonDrawing))) {
						isCreatePartnerNonDrawing = true;
					}
				} else if ((type.equalsIgnoreCase(this.uploadTempHeaderDrawing))
						|| (type.equalsIgnoreCase(this.uploadTempHeaderNonDrawing))) {

					if (!isUpdateDocInfo) {
						if (((!allHasNative) && (!allHasPublish) && (!hasRegistered))
								|| ((!allHasNative) && (documentPurpose.toUpperCase().equals("NATIVE")))
								|| ((!allHasPublish) && (!hasRegistered)
										&& (documentPurpose.toUpperCase().equals("PUBLISHED")))) {
							if ((type.equalsIgnoreCase(this.uploadTempHeaderDrawing))
									&& (docModel.equalsIgnoreCase("3D"))) {
								status = "Published";
								documentPurpose = "Published";
								isUpdateFolderLocation = true;
							}
							if (Type.equalsIgnoreCase("DIS")) {
								isUpdateFolderLocation = true;
								executeResult = document.executeUpdateDocInfoDIS(binder, typeDrawing, indexRow, area,
										platform, disciplineCode, documentType, runningNumber, sheetNumber,
										documentTitle, oldDocNo, projectName, projectYear, ownerOrganization, ownerName,
										author, remarks, userLogin, docTitle, securityGroup, docClasification,
										sheetName, error, documentName, documentNumber, authorOrganization, company,
										retentionPeriod, buAssetFunction, destinationPath, folderSource + documentName,
										MOCNo, authorGroupName, contractor, contractNo, WONo, revision, status,
										documentPurpose, activePassive, retentionDate, docModel, description, true);
							} else {
								Log.info("<== START executeCheckinUniversal DRAWING ==> ");
								Log.info(binder+"\n"+typeDrawing+"\n"+indexRow+"\n"+area+"\n"+platform+"\n"+disciplineCode+"\n"+documentType+"\n"+runningNumber+"\n"+sheetNumber+"\n"+documentTitle+"\n"+oldDocNo+"\n"+projectName+"\n"+projectYear+"\n"+ownerOrganization+"\n"+ownerName+"\n"+author+"\n"+remarks+"\n"+userLogin+"\n"+docTitle+"\n"+securityGroup+"\n"+docClasification+"\n"+sheetName+"\n"+error+"\n"+documentName+"\n"+documentNumber+"\n"+authorOrganization+"\n"+company+"\n"+retentionPeriod+"\n"+buAssetFunction+"\n"+destinationPath+"\n"+folderSource+"\n"+documentName+"\n"+MOCNo+"\n"+authorGroupName+"\n"+contractor+"\n"+contractNo+"\n"+WONo+"\n"+revision+"\n"+status+"\n"+documentPurpose+"\n"+dDocName+"\n"+dID+"\n"+activePassive+"\n"+retentionDate+"\n"+docModel+"\n"+description+"\n"+true);
								executeResult = document.executeCheckinUniversal(binder, typeDrawing, indexRow, area,
										platform, disciplineCode, documentType, runningNumber, sheetNumber,
										documentTitle, oldDocNo, projectName, projectYear, ownerOrganization, ownerName,
										author, remarks, userLogin, docTitle, securityGroup, docClasification,
										sheetName, error, documentName, documentNumber, authorOrganization, company,
										retentionPeriod, buAssetFunction, destinationPath, folderSource + documentName,
										MOCNo, authorGroupName, contractor, contractNo, WONo, revision, status,
										documentPurpose, activePassive, retentionDate, docModel, description, true);
							}
							isUpdateObjectID = true;
							if ((executeResult.indexOf("executeResult") < 0)
									&& (type.equalsIgnoreCase(this.uploadTempHeaderNonDrawing))
									&& (documentPurpose.equalsIgnoreCase("Published"))) {
								isCreatePartnerNonDrawing = true;
							}
						} else if ((!allHasPublish) && (hasRegistered)
								&& (documentPurpose.toUpperCase().equals("PUBLISHED"))) {
							if (Type.equalsIgnoreCase("DIS")) {
								isUpdateFolderLocation = true;
								executeResult = document.executeUpdateDocInfoDIS(binder, typeDrawing, indexRow, area,
										platform, disciplineCode, documentType, runningNumber, sheetNumber,
										documentTitle, oldDocNo, projectName, projectYear, ownerOrganization, ownerName,
										author, remarks, userLogin, docTitle, securityGroup, docClasification,
										sheetName, error, documentName, documentNumber, authorOrganization, company,
										retentionPeriod, buAssetFunction, destinationPath, folderSource + documentName,
										MOCNo, authorGroupName, contractor, contractNo, WONo, revision, status,
										documentPurpose, activePassive, retentionDate, docModel, description, true);
							} else {
								Log.info("<== START executeUpdateWithCheckin DRAWING 1 ==> ");
								Log.info(binder+"\n"+typeDrawing+"\n"+indexRow+"\n"+area+"\n"+platform+"\n"+disciplineCode+"\n"+documentType+"\n"+runningNumber+"\n"+sheetNumber+"\n"+documentTitle+"\n"+oldDocNo+"\n"+projectName+"\n"+projectYear+"\n"+ownerOrganization+"\n"+ownerName+"\n"+author+"\n"+remarks+"\n"+userLogin+"\n"+docTitle+"\n"+securityGroup+"\n"+docClasification+"\n"+sheetName+"\n"+error+"\n"+documentName+"\n"+documentNumber+"\n"+authorOrganization+"\n"+company+"\n"+retentionPeriod+"\n"+buAssetFunction+"\n"+destinationPath+"\n"+folderSource+"\n"+documentName+"\n"+MOCNo+"\n"+authorGroupName+"\n"+contractor+"\n"+contractNo+"\n"+WONo+"\n"+revision+"\n"+status+"\n"+documentPurpose+"\n"+dDocName+"\n"+dID+"\n"+activePassive+"\n"+retentionDate+"\n"+docModel+"\n"+description+"\n"+true);
								executeResult = document.executeUpdateWithCheckin(binder, typeDrawing, indexRow, area,
										platform, disciplineCode, documentType, runningNumber, sheetNumber,
										documentTitle, oldDocNo, projectName, projectYear, ownerOrganization, ownerName,
										author, remarks, userLogin, docTitle, securityGroup, docClasification,
										sheetName, error, documentName, documentNumber, authorOrganization, company,
										retentionPeriod, buAssetFunction, destinationPath, folderSource + documentName,
										MOCNo, authorGroupName, contractor, contractNo, WONo, revision, status,
										documentPurpose, dDocName, dID, activePassive, retentionDate, docModel,
										description, true);
							}
						} else if (((allHasPublish) && (documentPurpose.toUpperCase().equals("PUBLISHED")))
								|| ((allHasNative) && (documentPurpose.toUpperCase().equals("NATIVE")))) {
							if ((hasPublish) || (hasNative)) {
								if (status.length() == 0) {
									status = tempStatus;
								}
							} else if ((type.equalsIgnoreCase(this.uploadTempHeaderDrawing))
									&& (docModel.equalsIgnoreCase("3D"))) {
								status = "Published";
								documentPurpose = "Published";
								isUpdateFolderLocation = true;
							}
							if (Type.equalsIgnoreCase("DIS")) {
								dDocnameDIS = dValue.getDDocNameDocumentDIS(documentName, documentPurpose,
										docClasification, securityGroup);
								dIDDIS = dValue.getDIDDocumentDIS(documentName, documentPurpose, docClasification,
										securityGroup);
								if (filesDIS.downloadDocument(documentName, dIDDIS, dDocnameDIS, userLogin)
										.equalsIgnoreCase("Gagal")) {
									error = error + ":DIS Reivisi File Download Failed";
									executeResult = "executeFailed";
								} else {
									folderSource = "D:\\DIStmp\\";
									executeResult = document.executeUpdateRevision(binder, typeDrawing, indexRow, area,
											platform, disciplineCode, documentType, runningNumber, sheetNumber,
											documentTitle, oldDocNo, projectName, projectYear, ownerOrganization,
											ownerName, author, remarks, userLogin, docTitle, securityGroup,
											docClasification, sheetName, error, documentName, documentNumber,
											authorOrganization, company, retentionPeriod, buAssetFunction,
											destinationPath, folderSource + documentName, MOCNo, authorGroupName,
											contractor, contractNo, WONo, revision, status, documentPurpose, dDocName,
											dID, activePassive, retentionDate, docModel, description, true);
									Log.info("Bersih-bersih sisa file DIS");
									filesDIS.deleteFile(folderSource + documentName);
									document.deleteDoc(dDocnameDIS, userLogin, util.getSystemWorkspace());
								}
							} else {
								Log.info("<== START executeUpdateRevision DRAWING ==> ");
								Log.info(binder+"\n"+typeDrawing+"\n"+indexRow+"\n"+area+"\n"+platform+"\n"+disciplineCode+"\n"+documentType+"\n"+runningNumber+"\n"+sheetNumber+"\n"+documentTitle+"\n"+oldDocNo+"\n"+projectName+"\n"+projectYear+"\n"+ownerOrganization+"\n"+ownerName+"\n"+author+"\n"+remarks+"\n"+userLogin+"\n"+docTitle+"\n"+securityGroup+"\n"+docClasification+"\n"+sheetName+"\n"+error+"\n"+documentName+"\n"+documentNumber+"\n"+authorOrganization+"\n"+company+"\n"+retentionPeriod+"\n"+buAssetFunction+"\n"+destinationPath+"\n"+folderSource+"\n"+documentName+"\n"+MOCNo+"\n"+authorGroupName+"\n"+contractor+"\n"+contractNo+"\n"+WONo+"\n"+revision+"\n"+status+"\n"+documentPurpose+"\n"+dDocName+"\n"+dID+"\n"+activePassive+"\n"+retentionDate+"\n"+docModel+"\n"+description+"\n"+true);
								executeResult = document.executeUpdateRevision(binder, typeDrawing, indexRow, area,
										platform, disciplineCode, documentType, runningNumber, sheetNumber,
										documentTitle, oldDocNo, projectName, projectYear, ownerOrganization, ownerName,
										author, remarks, userLogin, docTitle, securityGroup, docClasification,
										sheetName, error, documentName, documentNumber, authorOrganization, company,
										retentionPeriod, buAssetFunction, destinationPath, folderSource + documentName,
										MOCNo, authorGroupName, contractor, contractNo, WONo, revision, status,
										documentPurpose, dDocName, dID, activePassive, retentionDate, docModel,
										description, true);
							}
							isUpdateObjectID = true;
							if (type.equalsIgnoreCase(this.uploadTempHeaderNonDrawing)) {
								isUpdateFolderLocation = true;
							}
						} else {
							if ((type.equalsIgnoreCase(this.uploadTempHeaderDrawing))
									&& (docModel.equalsIgnoreCase("3D"))) {
								status = "Published";
								documentPurpose = "Published";
								isUpdateFolderLocation = true;
							}
							if (Type.equalsIgnoreCase("DIS")) {
								isUpdateFolderLocation = true;
								executeResult = document.executeUpdateDocInfoDIS(binder, typeDrawing, indexRow, area,
										platform, disciplineCode, documentType, runningNumber, sheetNumber,
										documentTitle, oldDocNo, projectName, projectYear, ownerOrganization, ownerName,
										author, remarks, userLogin, docTitle, securityGroup, docClasification,
										sheetName, error, documentName, documentNumber, authorOrganization, company,
										retentionPeriod, buAssetFunction, destinationPath, folderSource + documentName,
										MOCNo, authorGroupName, contractor, contractNo, WONo, revision, status,
										documentPurpose, activePassive, retentionDate, docModel, description, true);
							} else {
								Log.info("<== START executeUpdateWithCheckin DRAWING 2 ==> ");
								Log.info(binder+"\n"+typeDrawing+"\n"+indexRow+"\n"+area+"\n"+platform+"\n"+disciplineCode+"\n"+documentType+"\n"+runningNumber+"\n"+sheetNumber+"\n"+documentTitle+"\n"+oldDocNo+"\n"+projectName+"\n"+projectYear+"\n"+ownerOrganization+"\n"+ownerName+"\n"+author+"\n"+remarks+"\n"+userLogin+"\n"+docTitle+"\n"+securityGroup+"\n"+docClasification+"\n"+sheetName+"\n"+error+"\n"+documentName+"\n"+documentNumber+"\n"+authorOrganization+"\n"+company+"\n"+retentionPeriod+"\n"+buAssetFunction+"\n"+destinationPath+"\n"+folderSource+"\n"+documentName+"\n"+MOCNo+"\n"+authorGroupName+"\n"+contractor+"\n"+contractNo+"\n"+WONo+"\n"+revision+"\n"+status+"\n"+documentPurpose+"\n"+dDocName+"\n"+dID+"\n"+activePassive+"\n"+retentionDate+"\n"+docModel+"\n"+description+"\n"+true);
								executeResult = document.executeUpdateWithCheckin(binder, typeDrawing, indexRow, area,
										platform, disciplineCode, documentType, runningNumber, sheetNumber,
										documentTitle, oldDocNo, projectName, projectYear, ownerOrganization, ownerName,
										author, remarks, userLogin, docTitle, securityGroup, docClasification,
										sheetName, error, documentName, documentNumber, authorOrganization, company,
										retentionPeriod, buAssetFunction, destinationPath, folderSource + documentName,
										MOCNo, authorGroupName, contractor, contractNo, WONo, revision, status,
										documentPurpose, dDocName, dID, activePassive, retentionDate, docModel,
										description, true);
							}
						}
					}
				} else if (type.equalsIgnoreCase(this.uploadTempHeaderExternalResource)) {
					executeResult = document.executeCheckinExtList(binder, documentType, documentTitle, docNumber,
							xDepartment, xExternalOwner, xSubmitDate, xYear, xStampDate, xComments, xDocName, xStatus,
							sFolder, dDocAuthor, xReferensi, xAuthor, securityGroup, true, runningNumber, countJ, xTypeNumber, xBusinessStage);
					isUpdateObjectID = true;
					isFolderLocationExt = true;
				} else {
					isUpdateObjectID = true;
					if (((type.equalsIgnoreCase(this.uploadTempHeaderDrawing))
							|| (type.toUpperCase().equals(this.uploadTempHeaderGeneral))
							|| (type.toUpperCase().equals(this.uploadTempHeaderPublic)))
							&& (docModel.equalsIgnoreCase("3D"))) {
						status = "Published";
						isUpdateFolderLocation = true;
					}
					if (Type.equalsIgnoreCase("DIS")) {
						Log.info("PHE:executeUpdateDocInfoDIS6:" + error);
						isUpdateFolderLocation = true;
						executeResult = document.executeUpdateDocInfoDIS(binder, typeDrawing, indexRow, area, platform,
								disciplineCode, documentType, runningNumber, sheetNumber, documentTitle, oldDocNo,
								projectName, projectYear, ownerOrganization, ownerName, author, remarks, userLogin,
								docTitle, securityGroup, docClasification, sheetName, error, documentName,
								documentNumber, authorOrganization, company, retentionPeriod, buAssetFunction,
								destinationPath, folderSource + documentName, MOCNo, authorGroupName, contractor,
								contractNo, WONo, revision, status, documentPurpose, activePassive, retentionDate,
								docModel, description, true);
					} else {
						executeResult = document.executeCheckinUniversal(binder, typeDrawing, indexRow, area, platform,
								disciplineCode, documentType, runningNumber, sheetNumber, documentTitle, oldDocNo,
								projectName, projectYear, ownerOrganization, ownerName, author, remarks, userLogin,
								docTitle, securityGroup, docClasification, sheetName, error, documentName,
								documentNumber, authorOrganization, company, retentionPeriod, buAssetFunction,
								destinationPath, folderSource + documentName, MOCNo, authorGroupName, contractor,
								contractNo, WONo, revision, status, documentPurpose, activePassive, retentionDate,
								docModel, description, true);
					}
					if ((executeResult.indexOf("executeResult") < 0)
							&& (type.equalsIgnoreCase(this.uploadTempHeaderNonDrawing))
							&& (documentPurpose.equalsIgnoreCase("Published"))) {
						isCreatePartnerNonDrawing = true;
					}
				}
				Workspace ws = getWorkspace();
				String idBuatUpdateFolderLocation = "";
				String docNameBuatUpdateFolderLocation = "";
				if (executeResult.indexOf("executeFailed") < 0) {
					String[] splitPerIDan = executeResult.split(";");
					String tempdDocName = splitPerIDan[1];
					String tempdID = splitPerIDan[0];
					if (isUpdateObjectID) {
						idBuatUpdateFolderLocation = tempdID;
						docNameBuatUpdateFolderLocation = tempdDocName;
						executeResult = dValue.updateXRObjectID(tempdDocName, tempdID, ws);
					}
					if (isUpdateFolderLocation) {
						if ((docNameBuatUpdateFolderLocation.length() > 0)
								&& (idBuatUpdateFolderLocation.length() > 0)) {
							DataBinder binderSiapDiExecuteChangeFolder = new DataBinder();

							Folder fld = new Folder();
							util.setUserLogin(userLogin);

							String parentDir = "";
							if (documentPurpose.toUpperCase().equals("PUBLISHED")) {
								parentDir = "/Cabinets/Technical Documents/";
							} else {
								parentDir = "/Cabinets/Native Documents/";
								if (docModel.equalsIgnoreCase("3D")) {
									parentDir = "/Cabinets/Technical Documents/";
								}
							}
							String folderPath = "";
							if (type.equalsIgnoreCase(this.uploadTempHeaderNonDrawing)) {
								parentDir = parentDir + "Non Drawings";
								folderPath = area + "/"
										+ fld.GetDisCodeDesc(type, disciplineCode, util.getSystemWorkspace()) + "/"
										+ documentType;
							} else {
								parentDir = parentDir + "Drawings";
								folderPath = area + "/" + platform + "/"
										+ fld.GetDisCodeDesc(type, disciplineCode, util.getSystemWorkspace()) + "/"
										+ documentType;
							}
							DataBinder binderFolder = new DataBinder();
							fld.DoCheckingFolder(parentDir, folderPath, binderFolder, util.getSystemWorkspace(), util);

							ResultSet rs = fld.GetFolderInfo(parentDir + "/" + folderPath, binder,
									util.getSystemWorkspace(), util);
							if (rs != null) {
								binderSiapDiExecuteChangeFolder.putLocal("fParentGUID",
										rs.getStringValueByName("fFolderGUID"));
								binderSiapDiExecuteChangeFolder.putLocal("dID", idBuatUpdateFolderLocation);
								binderSiapDiExecuteChangeFolder.putLocal("dDocName", docNameBuatUpdateFolderLocation);

								executeResult = document.executeUpdateDocInfoJustFParentGUID(
										binderSiapDiExecuteChangeFolder, indexRow, userLogin, true);
							}
						}
					}
					if (((type.toUpperCase().equals(this.uploadTempHeaderGeneral))
							|| (type.toUpperCase().equals(this.uploadTempHeaderPublic)))
							&& (!pheAp.equalsIgnoreCase("ONWJ"))) {
						String idToBeUpdateFolderLocation = tempdID;
						String docNameToBeUpdateFolderLocation = tempdDocName;

						DataBinder binderForChangeFolder = new DataBinder();

						Folder fld = new Folder();
						util.setUserLogin(userLogin);

						String folderPath = "";
						DataBinder binderFolder = new DataBinder();
						fld.DoCheckingFolder(destinationPath, folderPath, binderFolder, util.getSystemWorkspace(),
								util);

						ResultSet rs = fld.GetFolderInfo(destinationPath, binder, util.getSystemWorkspace(), util);
						if (rs != null) {
							binderForChangeFolder.putLocal("fParentGUID", rs.getStringValueByName("fFolderGUID"));
							binderForChangeFolder.putLocal("dID", idToBeUpdateFolderLocation);
							binderForChangeFolder.putLocal("dDocName", docNameToBeUpdateFolderLocation);
							executeResult = document.executeUpdateDocInfoJustFParentGUID(binderForChangeFolder,
									indexRow, userLogin, true);
						}
					}
					if (isUpdatePublish) {
						DataBinder binderDocInfo = new DataBinder();
						binderDocInfo.putLocal("dUser", userLogin);
						binderDocInfo.putLocal("dDocName", updatedDocNamePublish);

						util.setUserLogin(userLogin);

						ResultSet rs = document.GetDocInfoLatestRelease(binderDocInfo, ws, true, util);

						DataBinder binderUpdateDocInfo = new DataBinder();
						executeResult = document.executeUpdateDocInfoJustxStatus(binderUpdateDocInfo, indexRow,
								rs.getStringValueByName("dDocName"), rs.getStringValueByName("dID"), "Implemented",
								typeDrawing, userLogin, true);

					}
					if (isCreatePartnerNonDrawing) {
						documentPurpose = "";
						typeDrawing = this.uploadTempHeaderMasterlistNonDrawing;
						status = "Registered";
						docClasification = "MasterListNonDrawing";
						sheetName = "";
						securityGroup = "PHENonDrawing";
						documentPurpose = "";
						retentionDate = "";
						retentionPeriod = "";
						documentName = "";
						dID = "";
						dDocName = "";
						revision = "";
						sheetNumber = "";
						docModel = "";
						description = "";
						boolean jadiDiCekin = true;
						try {
							String tempNumeric = runningNumber.substring(1, runningNumber.length());
							if (Integer.valueOf(runningNumber.substring(0, 1)).intValue() < 6) {
								runningNumber = String
										.valueOf(Integer.valueOf(runningNumber.substring(0, 1)).intValue() + 5);
							} else {
								runningNumber = String
										.valueOf(Integer.valueOf(runningNumber.substring(0, 1)).intValue() - 5);
							}
							runningNumber = runningNumber + tempNumeric;
							Log.info("--3.3:runningNumber:" + runningNumber);

							String tempError = validator.validateDocumentNumberForTDCNonDrawing(area, disciplineCode,
									documentType, runningNumber, sheetNumber, type);
							Log.info("--tempError:" + tempError);
							if (tempError.length() > 0) {
								jadiDiCekin = false;
							}
						} catch (Exception ex) {
							jadiDiCekin = false;
						}
						if (jadiDiCekin) {
							executeResult = document.executeCheckinUniversal(binder, typeDrawing, indexRow, area,
									platform, disciplineCode, documentType, runningNumber, sheetNumber, documentTitle,
									oldDocNo, projectName, projectYear, ownerOrganization, ownerName, author, remarks,
									userLogin, docTitle, securityGroup, docClasification, sheetName, error,
									documentName, documentNumber, authorOrganization, company, retentionPeriod,
									buAssetFunction, destinationPath, folderSource + documentName, MOCNo,
									authorGroupName, contractor, contractNo, WONo, revision, status, documentPurpose,
									activePassive, retentionDate, docModel, description, true);
							if (executeResult.indexOf("executeFailed") < 0) {
								isUpdateObjectID = true;
							}
						}
						if (isUpdateObjectID) {
							DataBinder binderDocInfo = new DataBinder();
							binderDocInfo.putLocal("dUser", userLogin);

							DataBinder binderUpdateDocInfo = new DataBinder();

							executeResult = document.executeUpdateDocInfoJustObjectID(binderUpdateDocInfo, indexRow,
									tempdDocName, tempdID, typeDrawing, userLogin, true);
						}
					}
					
					String keyConfApCab = "CABINETS_" + pheAp;
					try {
						this.rootApDest = dValue.getPheConfig(keyConfApCab.toUpperCase().trim());
					} catch (DataException ex) {
						Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
					}
					
					if (isFolderLocationExt) {				
						Folder fld = new Folder();
						String parentDir = "/" + rootApDest + "/External Resources List";
						String folderPath = "";

						parentDir = parentDir + sFolder;

						DataBinder binderSiapDiExecuteChangeFolder = new DataBinder();

						util.setUserLogin(userLogin);

						DataBinder binderFolder = new DataBinder();
						fld.DoCheckingFolder(parentDir, folderPath, binderFolder, util.getSystemWorkspace(), util);
						Log.info("ext res docNumber = " + parentDir + "/" + folderPath);
						ResultSet rs = fld.GetFolderInfo(parentDir, binder, util.getSystemWorkspace(), util);
						Log.info("--rs ext res:" + rs);
						if (rs != null) {
							binderSiapDiExecuteChangeFolder.putLocal("fParentGUID",
									rs.getStringValueByName("fFolderGUID"));
							binderSiapDiExecuteChangeFolder.putLocal("dID", idBuatUpdateFolderLocation);
							binderSiapDiExecuteChangeFolder.putLocal("dDocName", docNameBuatUpdateFolderLocation);
							executeResult = document.executeUpdateDocInfoJustFParentGUID(
									binderSiapDiExecuteChangeFolder, indexRow, userLogin, true);
							Log.info("--ext res after execute binder");
						} else {
							parentDir = "/" + rootApDest + "/External Resources List";
							ResultSet rs1 = fld.GetFolderInfo(parentDir, binder, util.getSystemWorkspace(), util);

							error = error + "<br><mark>destination_folder is not found</mark>" + "<br>";
							binderSiapDiExecuteChangeFolder.putLocal("fParentGUID",
									rs1.getStringValueByName("fFolderGUID"));
							binderSiapDiExecuteChangeFolder.putLocal("dID", idBuatUpdateFolderLocation);
							binderSiapDiExecuteChangeFolder.putLocal("dDocName", docNameBuatUpdateFolderLocation);
							executeResult = document.executeUpdateDocInfoJustFParentGUID(
									binderSiapDiExecuteChangeFolder, indexRow, userLogin, true);
						}
					}
				}
			} catch (DataException ex) {
				Log.error(ex.getMessage());
			} catch (ServiceException ex) {
				Log.error(ex.getMessage());
			}
		}
		System.out.println("End checkin data.." + new Date());

		System.out.println("\nStart return error remarks.." + new Date());
		if (executeResult.indexOf("executeFailed") >= 0) {
			if (executeResult.indexOf("sufficient") >= 0) {
				error = error + "You does not have privilages to checkin." + "<br>";
			} else if (executeResult.indexOf("primaryFile") >= 0) {
				error = error + "The primary file does not exist." + "<br>";
			} else if (executeResult.indexOf("checkoutFailed") >= 0) {
				error = error + "Failed when checkout. Please try again." + "<br>";
			} else {
				error = error + "Document has failed to execute" + "<br>";
			}
		}
		System.out.println("End return error remarks.." + new Date());
		try {
			String returnExecute = "";
			if (type.equalsIgnoreCase(this.uploadTempHeaderExternalResource)) {
				if (sFolder.equalsIgnoreCase("")) {
					String keyConfApCab = "CABINETS_" + pheAp;
					try {
						this.rootApDest = dValue.getPheConfig(keyConfApCab.toUpperCase().trim());
					} catch (DataException ex) {
						Logger.getLogger(ValidateExcel.class.getName()).log(Level.SEVERE, null, ex);
					}
					sFolder = "/" + rootApDest + "/External Resources List";
				}
				dValue.insertBatchTempExtResource(indexRow, documentNumber, xDepartment, xExternalOwner, xSubmitDate,
						xYear, xStampDate, xComments, xDocName, xStatus, xReferensi, xAuthor, "1", runningNumber,
						documentTitle, securityGroup, dDocAuthor, docClasification, sheetName, typeDrawing, error,
						sFolder, userLogin);
			} else {
				dValue.insertBatchTemp(typeDrawing, indexRow, area, platform, disciplineCode, documentType,
						runningNumber, sheetNumber, documentTitle, oldDocNo, projectName, projectYear,
						ownerOrganization, ownerName, author, remarks, userLogin, docTitle, securityGroup,
						docClasification, sheetName, documentName, documentNumber, authorOrganization, company,
						retentionPeriod, buAssetFunction, destinationPath, documentPurpose, MOCNo, authorGroupName,
						contractor, contractNo, WONo, revision, status, activePassive, retentionDate, error, docModel,
						description);
			}
			returnExecute.equals("FAILED");
		} catch (Exception ex) {
			Log.error("PHE:manipulateBatch:ex:" + ex.getMessage());
		}
		if ((type.equals(this.uploadTempHeaderMasterlistDrawing)) && (error.length() > 0)) {
			if (tempRow.size() != 16) {
				tempRow.add(error);
			} else {
				tempRow.set(15, error);
			}
		} else if ((type.equals(this.uploadTempHeaderMasterlistNonDrawing)) && (error.length() > 0)) {
			if (tempRow.size() != 14) {
				tempRow.add(error);
			} else {
				tempRow.set(13, error);
			}
		} else if ((type.equals(this.uploadTempHeaderGeneral)) && (error.length() > 0)) {
			if (tempRow.size() != 14) {
				tempRow.add(error);
			} else {
				tempRow.set(13, error);
			}
		} else if ((type.equals(this.uploadTempHeaderPublic)) && (error.length() > 0)) {
			if (tempRow.size() != 14) {
				tempRow.add(error);
			} else {
				tempRow.set(13, error);
			}
		} else if ((type.equals(this.uploadTempHeaderDrawing)) && (error.length() > 0)) {
			if (tempRow.size() != 15) {
				tempRow.add(error);
			} else {
				tempRow.set(14, error);
			}
		} else if ((type.equals(this.uploadTempHeaderNonDrawing)) && (error.length() > 0)) {
			if (tempRow.size() != 17) {
				tempRow.add(error);
			} else {
				tempRow.set(16, error);
			}
		} else if ((type.equals(this.uploadTempHeaderExternalResource)) && (error.length() > 0)) {
			if (tempRow.size() != 15) {
				tempRow.add(error);
			} else {
				tempRow.set(14, error);
			}
		}
		return tempRow;
	}

	public String getTemplateType(ArrayList rowCollection) {
		String type = "";
		DatabaseValue dValue = new DatabaseValue();
		dValue.removeBatchTemp(this.m_binder.getLocal("dUser"));
		try {
			ArrayList row = (ArrayList) rowCollection.get(0);
			type = row.get(1).toString();
		} catch (Exception localException) {
		}
		return type;
	}

	public String specialCharacterHandling(String input) {
		String output = input.replaceAll("'", "''");
		return output;
	}

	public DataBinder setPartnerNonDrawing(DataBinder binder, String docType, String secGroup) {
		binder.putLocal("dDocAuthor", this.m_binder.getLocal("dUser"));
		binder.putLocal("dDocType", docType);
		binder.putLocal("dSecurityGroup", secGroup);
		binder.putLocal("dDocTitle", this.m_binder.getLocal("dDocTitle"));
		binder.putLocal("xDocPurpose", "");
		binder.putLocal("xStatus", "Registered");
		binder.putLocal("xSheetName", "");
		binder.putLocal("xSheetNumber", "");
		binder.putLocal("xRetentionDate", "");
		binder.putLocal("xRetentionPeriod", "");
		binder.putLocal("xDocName", "");
		binder.putLocal("dID", "");
		binder.putLocal("xRevision", "");
		binder.putLocal("createPrimaryMetaFile", "1");
		binder.putLocal("AutoNumberPrefix", "EDMS");
		String runningNumber = binder.getLocal("xRunningNumber");
		Validator validator = new Validator();
		String area = "";
		String disciplineCode = "";
		String documentType = "";
		String sheetNumber = "";
		String type = this.uploadTempHeaderMasterlistNonDrawing;
		if (!this.m_binder.getLocal("dDocName").isEmpty()) {
			binder.putLocal("dDocName", "");
		}
		String jadiDiCekin = "true";
		try {
			String tempNumeric = runningNumber.substring(1, runningNumber.length());
			if (Integer.valueOf(runningNumber.substring(0, 1)).intValue() < 5) {
				runningNumber = String.valueOf(Integer.valueOf(runningNumber.substring(0, 1)).intValue() + 5);
			} else {
				runningNumber = String.valueOf(Integer.valueOf(runningNumber.substring(0, 1)).intValue() - 5);
			}
			runningNumber = runningNumber + tempNumeric;

			String tempError = validator.validateDocumentNumberForTDCNonDrawing(area, disciplineCode, documentType,
					runningNumber, sheetNumber, type);
			if (tempError.length() > 0) {
				jadiDiCekin = "false";
			}
		} catch (Exception ex) {
			jadiDiCekin = "false";
		}
		binder.putLocal("xRunningNumber", runningNumber);
		binder.putLocal("jadiDiCekin", jadiDiCekin);
		return binder;
	}

	public void createPartnerTDCNonDrawing(String executeResult, Util util, boolean isCreatePartnerNonDrawing,
			Workspace ws, Document doc, String tempdID, DataBinder tempBinder, String tempdDocName, String type, String secGroup)
			throws DataException, ServiceException {
		if (isCreatePartnerNonDrawing) {
			DataBinder tempBinder2 = new DataBinder();
			tempBinder2 = setPartnerNonDrawing(tempBinder, type, secGroup);
			if (tempBinder2.getLocal("jadiDiCekin").toString().equalsIgnoreCase("true")) {
				executeResult = util.DoCheckin(tempBinder2, ws);
				if (executeResult.indexOf("executeFailed") < 0) {
					DataBinder binderDocInfoAgain = new DataBinder();

					binderDocInfoAgain.putLocal("dUser", this.m_binder.getLocal("dUser"));

					String[] splitPerIDanLagi = executeResult.split(";");
					String tempdDocNameLagi = splitPerIDanLagi[1];
					String tempdIDLagi = splitPerIDanLagi[0];
					DataBinder binderUpdateDocInfoAgain = new DataBinder();

					executeResult = doc.executeUpdateDocInfoJustObjectID(binderUpdateDocInfoAgain, "", tempdDocNameLagi,
							tempdIDLagi, type, this.m_binder.getLocal("dUser"), true);
					this.m_binder.putLocal("dDocTitle", tempBinder.getLocal("dDocTitle").toString());
					this.m_binder.putLocal("dID", tempdID);
					this.m_binder.putLocal("dDocName", tempdDocName);
				}
			}
		}
	}
}
