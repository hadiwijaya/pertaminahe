package excel;

import intradoc.common.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author Ginanjar
 */
public class WriteExcel {
	private String uploadTempHeaderMasterlistDrawing = "TDC MASTER LIST DRAWING";
	private String uploadTempHeaderMasterlistNonDrawing = "TDC MASTER LIST NONDRAWING";
	private String uploadTempHeaderGeneral = "DCRMS GENERAL - GENERAL DOCUMENT";
	private String uploadTempHeaderPublic = "DCRMS PHE PUBLIC - PUBLIC DOCUMENT";
	private String uploadTempHeaderDrawing = "DCRMS GENERAL - DRAWING DOCUMENT";
	private String uploadTempHeaderNonDrawing = "DCRMS GENERAL - NONDRAWING DOCUMENT";
	private String uploadTempHeaderExternalResource = "EXTERNAL RESOURCE LIST";

	public String copyFileUsingFileStreams(File source, File dest) throws IOException {
		InputStream input = null;
		OutputStream output = null;
		try {
			input = new FileInputStream(source);
			output = new FileOutputStream(dest);
			byte[] buf = new byte[102400];
			int bytesRead;
			while ((bytesRead = input.read(buf)) > 0) {
				output.write(buf, 0, bytesRead);
			}
			return "success";
		} finally {
			input.close();
			output.close();
		}
	}

	public String writeXls(String userLogin, ArrayList rowCollection, String type, Date date) {
		DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
		String filename = dateFormat.format(date) + "uploadfailed_" + userLogin;
		try {
			if (type.equals(uploadTempHeaderMasterlistDrawing)) {
				copyFileUsingFileStreams(new File("D:\\template\\masterlistdrawing.xls"),
						new File("D:\\tmp\\" + filename + ".xls"));
			} else if (type.equals(uploadTempHeaderMasterlistNonDrawing)) {
				copyFileUsingFileStreams(new File("D:\\template\\masterlistnondrawing.xls"),
						new File("D:\\tmp\\" + filename + ".xls"));
			} else if (type.equals(uploadTempHeaderGeneral)) {
				copyFileUsingFileStreams(new File("D:\\template\\edmsgeneral.xls"),
						new File("D:\\tmp\\" + filename + ".xls"));
			} else if (type.equals(uploadTempHeaderPublic)) {
				copyFileUsingFileStreams(new File("D:\\template\\edmspublicgeneral.xls"),
						new File("D:\\tmp\\" + filename + ".xls"));
			} else if (type.equals(uploadTempHeaderDrawing)) {
				copyFileUsingFileStreams(new File("D:\\template\\edmsdrawing.xls"),
						new File("D:\\tmp\\" + filename + ".xls"));
			} else if (type.equals(uploadTempHeaderNonDrawing)) {
				copyFileUsingFileStreams(new File("D:\\template\\edmsnondrawing.xls"),
						new File("D:\\tmp\\" + filename + ".xls"));
			} else if (type.equals(uploadTempHeaderExternalResource)) {
				copyFileUsingFileStreams(new File("D:\\template\\externalresourcelist.xls"),
						new File("D:\\tmp\\" + filename + ".xls"));
			}
		} catch (IOException ex) {
			Log.error("PHE:writeXls:ex:" + ex.getMessage());
		}

		FileInputStream file = null;
		try {
			file = new FileInputStream(new File("D:\\tmp\\" + filename + ".xls"));
		} catch (FileNotFoundException ex) {
			Log.error("PHE:writeFileInputStream:ex:" + ex.getMessage());
		}

		try {
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			Sheet sheet = workbook.getSheetAt(0);

			int indexRow = 2;
			ArrayList rowCollectionFailed = new ArrayList();
			for (int i = 1; i < rowCollection.size(); i++) {
				ArrayList columnCollection = (ArrayList) rowCollection.get(i);
				if (columnCollection.get(columnCollection.size() - 1).toString().length() > 0) {
					rowCollectionFailed.add(columnCollection);
				}
			}

			for (int i = 0; i < rowCollectionFailed.size(); i++) {
				ArrayList columnCollection = (ArrayList) rowCollectionFailed.get(i);
				Row row = sheet.createRow(indexRow);

				if (type.equals(uploadTempHeaderMasterlistDrawing)) {
					for (int j = 0; j < columnCollection.size(); j++) {
						if (j == 1 /* area */) {
							row.createCell(0).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 2 /* platform */) {
							row.createCell(1).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 3 /* discipline_code */) {
							row.createCell(2).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 4 /* ddoctype */) {
							row.createCell(3).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 5 /* running_number */) {
							row.createCell(4).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 6 /* sheet_number */) {
							row.createCell(5).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 7 /* doctitle */) {
							row.createCell(6).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 8 /* moc_no */) {
							row.createCell(7).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 9 /* old_number */) {
							row.createCell(8).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 10 /* project_name */) {
							row.createCell(9).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 11 /* project_year */) {
							row.createCell(10).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 12 /* owner_organization */) {
							row.createCell(11).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 13 /* owner_name */) {
							row.createCell(12).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 14/* remarks */) {
							row.createCell(13).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 15/* erroSr */) {
							row.createCell(14).setCellValue(columnCollection.get(j).toString());
						}
					}
				}
				if (type.equals(uploadTempHeaderMasterlistNonDrawing)) {
					for (int j = 0; j < columnCollection.size(); j++) {
						if (j == 1 /* area */) {
							row.createCell(0).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 2 /* discipline_code */) {
							row.createCell(1).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 3 /* ddoctype */) {
							row.createCell(2).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 4 /* running_number */) {
							row.createCell(3).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 5 /* doctitle */) {
							row.createCell(4).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 6 /* old_number */) {
							row.createCell(5).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 7 /* project_name */) {
							row.createCell(6).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 8 /* project_year */) {
							row.createCell(7).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 9 /* owner_organization */) {
							row.createCell(8).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 10 /* owner_name */) {
							row.createCell(9).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 11/* author */) {
							row.createCell(10).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 12/* remarks */) {
							row.createCell(11).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 13/* error */) {
							row.createCell(12).setCellValue(columnCollection.get(j).toString());
						}
					}
				}
				if (type.equals(uploadTempHeaderGeneral)) {

					for (int j = 0; j < columnCollection.size(); j++) {
						if (j == 1 /* doc_name */) {
							row.createCell(0).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 2 /* doc_title */) {
							row.createCell(1).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 3 /* doc_number */) {
							row.createCell(2).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 4 /* author_name */) {
							row.createCell(3).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 5 /* author_organization */) {
							row.createCell(4).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 6 /* project_name */) {
							row.createCell(5).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 7 /* company */) {
							row.createCell(6).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 8 /* old_doc_no */) {
							row.createCell(7).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 9 /* retention_period */) {
							row.createCell(8).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 10 /* bu_asset/function */) {
							row.createCell(9).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 11/* destination_path */) {
							row.createCell(10).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 12/* error */) {
							row.createCell(11).setCellValue(columnCollection.get(j).toString());
						}
					}
				}
				if (type.equals(uploadTempHeaderPublic)) {

					for (int j = 0; j < columnCollection.size(); j++) {
						if (j == 1 /* doc_name */) {
							row.createCell(0).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 2 /* doc_title */) {
							row.createCell(1).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 3 /* doc_number */) {
							row.createCell(2).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 4 /* author_name */) {
							row.createCell(3).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 5 /* author_organization */) {
							row.createCell(4).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 6 /* project_name */) {
							row.createCell(5).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 7 /* company */) {
							row.createCell(6).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 8 /* old_doc_no */) {
							row.createCell(7).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 9 /* retention_period */) {
							row.createCell(8).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 10 /* bu_asset/function */) {
							row.createCell(9).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 11/* destination_path */) {
							row.createCell(10).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 12/* error */) {
							row.createCell(11).setCellValue(columnCollection.get(j).toString());
						}
					}
				}
				if (type.equals(uploadTempHeaderDrawing)) {
					for (int j = 0; j < columnCollection.size(); j++) {
						if (j == 1 /* doc_name */) {
							row.createCell(0).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 2 /* doc_title */) {
							row.createCell(1).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 3 /* doc_purpose */) {
							row.createCell(2).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 4 /* moc_no */) {
							row.createCell(3).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 5 /* old_doc_no */) {
							row.createCell(4).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 6 /* author_name */) {
							row.createCell(5).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 7 /* author_organization */) {
							row.createCell(6).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 8 /* author_group_name */) {
							row.createCell(7).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 9 /* company */) {
							row.createCell(8).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 10 /* projectname */) {
							row.createCell(9).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 11/* contractor */) {
							row.createCell(10).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 12/* contract_no */) {
							row.createCell(11).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 13/* wo_no */) {
							row.createCell(12).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 14/* error */) {
							row.createCell(13).setCellValue(columnCollection.get(j).toString());
						}
					}
				}
				if (columnCollection.size() > 16 && type.equals(uploadTempHeaderNonDrawing)) {

					for (int j = 0; j < columnCollection.size(); j++) {
						if (j == 1 /* doc_name */) {
							row.createCell(0).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 2 /* doc_title */) {
							row.createCell(1).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 3 /* doc_purpose */) {
							row.createCell(2).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 4 /* active_passive */) {
							row.createCell(3).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 5 /* old_doc_no */) {
							row.createCell(4).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 6 /* author_name */) {
							row.createCell(5).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 7 /* author_organization */) {
							row.createCell(6).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 8 /* author_group_name */) {
							row.createCell(7).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 9 /* company */) {
							row.createCell(8).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 10 /* projectname */) {
							row.createCell(9).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 11/* contractor */) {
							row.createCell(10).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 12/* contract_no */) {
							row.createCell(11).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 13/* retention_period */) {
							row.createCell(12).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 14/* retention_date */) {
							row.createCell(13).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 15/* wo_no */) {
							row.createCell(14).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 16/* error */) {
							row.createCell(15).setCellValue(columnCollection.get(j).toString());
						}
					}
				}
				if (type.equals(uploadTempHeaderExternalResource)) {
					for (int j = 0; j < columnCollection.size(); j++) {
						if (j == 1) {
							row.createCell(0).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 2) {
							row.createCell(1).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 3) {
							row.createCell(2).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 4) {
							row.createCell(3).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 5) {
							row.createCell(4).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 6) {
							row.createCell(5).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 7) {
							row.createCell(6).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 8) {
							row.createCell(7).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 9) {
							row.createCell(8).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 10) {
							row.createCell(9).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 11) {
							row.createCell(10).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 12) {
							row.createCell(11).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 13) {
							row.createCell(12).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 14) {
							row.createCell(13).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 15) {
							row.createCell(14).setCellValue(columnCollection.get(j).toString());
						}
						if (j == 16) {
							row.createCell(15).setCellValue(columnCollection.get(j).toString());
						}
					}
				}
				indexRow++;
			}

			// Write the output to a file
			FileOutputStream fileOut;
			try {
				fileOut = new FileOutputStream("D:\\tmp\\" + filename + ".xls");
				workbook.write(fileOut);
				fileOut.close();
			} catch (FileNotFoundException ex) {
				Log.error("PHE:writeXls:workbook.write:FileNotFoundException:" + ex.getMessage());
				Logger.getLogger(WriteExcel.class.getName()).log(Level.SEVERE, null, ex);
			} catch (IOException ex) {
				Log.error("PHE:writeXls:workbook.write:IOException:" + ex.getMessage());
				Logger.getLogger(WriteExcel.class.getName()).log(Level.SEVERE, null, ex);
			}
		} catch (IOException ex) {
			Log.error("PHE:writeXls:Workbook:IoException:" + ex.getMessage());
		}
		return filename + ".xls";
	}
}
