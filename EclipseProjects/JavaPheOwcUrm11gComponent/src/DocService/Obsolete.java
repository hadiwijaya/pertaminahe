package DocService;

import data.DatabaseValue;
import intradoc.common.Log;

import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;

import intradoc.data.ResultSet;
import intradoc.data.Workspace;

import intradoc.server.DocService;

import intradocservice.Document;
import intradocservice.Folder;
import intradocservice.Util;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.ErrorHandler;

public class Obsolete extends DocService {

	public Obsolete() {
		super();
	}

	private ErrorHandler ErrHandler = new ErrorHandler();

	public void MoveToObsoleteFolder(DataBinder binder) {
		Util util = new Util();
		Document doc = new Document();
		String dID = binder.getLocal("dID");

		Log.info("PHE:MoveToObsoleteFolder:dID" + dID);
//        DataBinder binder = m_binder;
//        binder.putLocal("startDate", util.getCurrentTime());
//        binder.putLocal("endDate", util.getCurrentTime());
//        util.setUserLogin(binder.getLocal("dUser"));

		if (dID.length() > 0) {

			if (CreateObsoleteFolder(binder).equalsIgnoreCase("executeFailed")) {
				binder.putLocal("StatusMessage", "ERROR EXECUTE UPDATE");
			}

		}
//        m_binder.putLocal("dStatus", "EXPIRED");
//        Log.info("END_PHE:JOB_INFO");
	}

	public String CreateObsoleteFolder(DataBinder binder) {
		Folder fld = new Folder();
		Document doc = new Document();
		Content content = new Content();

		String parentDir;
		String folderPath;
		Workspace ws;
		ResultSet docRS;
		String type, xComments;
		Util util = new Util();
		ws = util.getSystemWorkspace();
		util.setUserLogin("weblogic");
		parentDir = "Cabinets/Obsolete Documents";
		String dID = binder.getLocal("dID");
		type = binder.getLocal("dDocType");
//        Log.info("PHE:cyat:type" + type);
		binder.putLocal("isObsolete", "true");
//        Log.info("PHE:JOB_INFO:dID: " + dID);
		DataBinder binders = new DataBinder();
		if (!dID.equalsIgnoreCase("0")) {
			binder.putLocal("dID", dID);
			docRS = doc.GetDocInfo(dID, binder, ws, false, util);
			binders.putLocal("dID", dID);
			if (docRS != null) {
				if (type.toUpperCase().contains("NONDRAWING")) {
					content.dID = docRS.getStringValueByName("dID");
					content.area = docRS.getStringValueByName("xNdLocation");
					content.disciplineCode = docRS.getStringValueByName("xNdDisciplineCode");
					content.docType = docRS.getStringValueByName("xNdDocTypeCode");
					content.xObsoleteReason = docRS.getStringValueByName("xNdObsoleteReason");// obsolete reason (rama
																								// 280116)
					parentDir = parentDir + "/Non Drawings";
					folderPath = content.area + "/" + fld.GetDisCodeDesc(type, content.disciplineCode, ws) + "/"
							+ content.docType;
				} else {
					content.dID = docRS.getStringValueByName("dID");
					content.area = docRS.getStringValueByName("xDLocation");
					content.platform = docRS.getStringValueByName("xDPlatform");
					content.disciplineCode = docRS.getStringValueByName("xDDisciplineCode");
					content.docType = docRS.getStringValueByName("xDDocTypeCode");
					content.xObsoleteReason = docRS.getStringValueByName("xDObsoleteReason");// obsolete reason (rama
																								// 280116)
					parentDir = parentDir + "/Drawings";
					folderPath = content.area + "/" + content.platform + "/"
							+ fld.GetDisCodeDesc(type, content.disciplineCode, ws) + "/" + content.docType;
				}

				content.parentGUID = docRS.getStringValueByName("fParentGUID");
				content.folderGUID = docRS.getStringValueByName("fFolderGUID");
				content.dDocName = docRS.getStringValueByName("dDocName");
				content.xObsoleteReason = docRS.getStringValueByName("xObsoleteReason");
				// String xObsoleteReasonAddObsoleteReason="";
				// if
				// (!content.xObsoleteReason.equals("")){xObsoleteReasonAddObsoleteReason=content.xObsoleteReason+".
				// ";};
				binders.putLocal("dDocName", binder.getLocal("dDocName"));
				binders.putLocal("xObsoleteReason", binder.getLocal("xObsoleteReason"));// obsolete reason (rama 280116)
				// binders.putLocal("xObsoleteReason",
				// xObsoleteReasonAddObsoleteReason+"Obsolete Reason:
				// "+binder.getLocal("xObsoleteReason"));//obsolete reason (rama 280116)
				binders.putLocal("dSecurityGroup", "PHE_TDC");
				binders.putLocal("fSecurityGroup", "PHE_TDC");
				binders.putLocal("xStatus", "Obsolete");
				binders.putLocal("dDocAuthor", docRS.getStringValueByName("dDocAuthor"));

				try {
					if (binders.getLocal("xRecordObsolateDate").isEmpty()) {
						Date date = new Date();
						DateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");
						try {
							binder.putLocal("xRecordObsolateDate", sdf.format(date));
							binder.setFieldType("xRecordObsolateDate", "Date");
						} catch (Exception ex) {
							Log.error("PHE:parsingDate:ex" + ex.getMessage());
						}
					}
				} catch (Exception ex) {
					Date date = new Date();
					DateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");
					try {
						binder.putLocal("xRecordObsolateDate", sdf.format(date));
						binder.setFieldType("xRecordObsolateDate", "Date");
					} catch (Exception exd) {
						Log.error("PHE:parsingDate:ex" + ex.getMessage());
					}
				}

//                Log.info("PHE:JOB_INFO:CreateObsoleteFolder: " + content.folderGUID);

				if (!fld.DoCheckingFolder(parentDir, folderPath, binder, ws, util)) {
					ErrHandler.ErrCheckin(binder);
					Log.error("PHE:JOB_ERROR: " + binder.getLocal("StatusMessage"));
				} else {
					ResultSet rs = fld.GetFolderInfo(parentDir + "/" + folderPath, binder, ws, util);
					if (!rs.isEmpty()) {
//                        Log.info("ParentGUID : " + binder.getLocal("fParentGUID"));
//                        Log.info("SWITCH with this - > FolderGUID : " + rs.getStringValueByName("fFolderGUID"));
						binders.putLocal("fParentGUID", rs.getStringValueByName("fFolderGUID"));
					}
				}
				if (util.UpdateCheckin_DocInfo(binders).equalsIgnoreCase("executeFailed")) {
					ErrHandler.ErrCheckin(binders);
					Log.error("PHE:JOB_ERROR: " + binders.getLocal("StatusMessage"));
					return "executeFailed";
				}
			}
		}
		return "executeSuccess";
	}

	public class Content {

		private String dID;
		private String docNumber;
		private String parentGUID;
		private String folderGUID;
		private String area;
		private String platform;
		private String disciplineCode;
		private String docType;
		private String dDocName;
		private String xObsoleteReason; // obsolete reason (rama 280116)

		public String getxObsoleteReason() {
			return xObsoleteReason;
		}

		public void setxObsoleteReason(String xObsoleteReason) {
			this.xObsoleteReason = xObsoleteReason;
		}

		public void setDID(String dID) {
			this.dID = dID;
		}

		public String getDID() {
			return dID;
		}

		public void setParentGUID(String parentGUID) {
			this.parentGUID = parentGUID;
		}

		public String getParentGUID() {
			return parentGUID;
		}

		public void setDocNumber(String docNumber) {
			this.docNumber = docNumber;
		}

		public String getDocNumber() {
			return docNumber;
		}

		public void setFolderGUID(String folderGUID) {
			this.folderGUID = folderGUID;
		}

		public String getFolderGUID() {
			return folderGUID;
		}

		public void setArea(String area) {
			this.area = area;
		}

		public String getArea() {
			return area;
		}

		public void setPlatform(String platform) {
			this.platform = platform;
		}

		public String getPlatform() {
			return platform;
		}

		public void setDisciplineCode(String disciplineCode) {
			this.disciplineCode = disciplineCode;
		}

		public String getDisciplineCode() {
			return disciplineCode;
		}

		public void setDocType(String docType) {
			this.docType = docType;
		}

		public String getDocType() {
			return docType;
		}

		public void setDDocName(String dDocName) {
			this.dDocName = dDocName;
		}

		public String getDDocName() {
			return dDocName;
		}
	}

	public void executeObsoleteDocument() {
		DatabaseValue dValue = new DatabaseValue();

		ResultSet rs;
		try {
			rs = dValue.getDefaultObsoleteDocument();
			DataResultSet myDataResultSet = new DataResultSet();
			myDataResultSet.copy(rs);
//            Log.info("Before While, rs:" + rs.isEmpty());
//            Log.info("Before While, myDataResultSet:" + myDataResultSet.isEmpty());
			if (!myDataResultSet.isEmpty()) {
				do {
//                    Log.info("Loop Result Set");
					DataBinder binder = new DataBinder();
					binder.putLocal("dID", myDataResultSet.getStringValueByName("dID"));
					// binder.putLocal("dID", rs.getStringValueByName("dID"));
//                    Log.info(myDataResultSet.getStringValueByName("dID"));
					binder.putLocal("dDocName", myDataResultSet.getStringValueByName("dDocName"));
					binder.putLocal("dDocType", myDataResultSet.getStringValueByName("dDocType"));
					binder.putLocal("xObsoleteReason", myDataResultSet.getStringValueByName("xObsoleteReason"));
					// binder.putLocal("dDocName", rs.getStringValueByName("dDocName"));
					// binder.putLocal("dDocType", rs.getStringValueByName("dDocType"));
					binder.putLocal("dUser", "weblogic");
					MoveToObsoleteFolder(binder);
				} while (rs.next());
			}
		} catch (DataException ex) {
			Log.error(ex.getMessage());
		}
	}

	public void executeObsoleteDocumentByDID() {

		DataBinder binder = new DataBinder();
		binder.putLocal("dID", m_binder.getLocal("dID"));
//        Log.info("PHE:executeObsoleteDocumentByDID:dID:" + m_binder.getLocal("dID"));
		binder.putLocal("dDocName", m_binder.getLocal("dDocName"));
		binder.putLocal("dDocType", m_binder.getLocal("dDocType"));
		// obsolete reason (rama 280116)
		binder.putLocal("xObsoleteReason", m_binder.getLocal("xObsoleteReason"));
		Log.info("PHE:executeObsoleteDocumentByDID:xObsoleteReason:" + m_binder.getLocal("xObsoleteReason"));
//        Log.info("PHE:executeObsoleteDocumentByDID:dDocName:" + m_binder.getLocal("dDocName"));
//        Log.info("PHE:executeObsoleteDocumentByDID:dDocType:" + m_binder.getLocal("dDocType"));

		binder.putLocal("dUser", "weblogic");
		MoveToObsoleteFolder(binder);

	}

	public void executeObsoleteDocumentScheduler(String did, String ddocname, String ddoctype) {
//add by nanda 310316 - obsolete scheduler
		DataBinder binder = new DataBinder();
		binder.putLocal("dID", did);
		binder.putLocal("dDocName", ddocname);
		binder.putLocal("dDocType", ddoctype);
		binder.putLocal("xObsoleteReason", "Obsoleted by scheduler");
		Log.info("PHE:ObsoleteByScheduler:" + ddocname);

		binder.putLocal("dUser", "weblogic");
		MoveToObsoleteFolder(binder);

	}
}
