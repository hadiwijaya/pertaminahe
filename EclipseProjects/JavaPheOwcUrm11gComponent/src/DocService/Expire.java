package DocService;

import ScheduledEvent.ScheduledEvt;
import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.common.ServiceException;

import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;

import intradoc.data.ResultSet;
import intradoc.data.Workspace;

import intradoc.server.DocService;

import intradocservice.Document;
import intradocservice.Folder;
import intradocservice.Util;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.DataCollection;
import util.ErrorHandler;
import util.UserAdmin;

public class Expire extends DocService {

	public Expire() {
		super();
	}

	private ErrorHandler ErrHandler = new ErrorHandler();

	public void MoveToExpireFolder(DataBinder binder) {
		Util util = new Util();
		Document doc = new Document();
		String dID = binder.getLocal("dID");
		Log.info("PHE:MoveToExpireFolder:" + dID);
//        DataBinder binder = m_binder;
//        binder.putLocal("startDate", util.getCurrentTime());
//        binder.putLocal("endDate", util.getCurrentTime());
		util.setUserLogin(binder.getLocal("dUser"));

		if (dID.length() > 0) {

			if (CreateExpiredFolder(binder).equalsIgnoreCase("executeFailed")) {
				binder.putLocal("StatusMessage", "ERROR EXECUTE UPDATE");
			}

		}
//        m_binder.putLocal("dStatus", "EXPIRED");
//        Log.info("END_PHE:JOB_INFO");
	}

	public String CreateExpiredFolder(DataBinder binder) {
		Folder fld = new Folder();
		Document doc = new Document();
		Content content = new Content();
		String parentDir;
		String folderPath;
		Workspace ws;
		ResultSet docRS;
		String type = "";

		UserAdmin uAdmin = new UserAdmin();
		ArrayList alUser = uAdmin.getUserAdmin();

		Util util = new Util();
		ws = util.getSystemWorkspace();
		util.setUserLogin(alUser.get(0).toString());
		parentDir = "Cabinets/Expired Documents";
		String dID = binder.getLocal("dID");

		binder.putLocal("isExpired", "true");
//        Log.info("PHE:JOB_INFO:dID: " + dID);

		if (!dID.equalsIgnoreCase("0")) {
			binder.putLocal("dID", dID);
			docRS = doc.GetDocInfo(dID, binder, ws, false, util);
			type = docRS.getStringValueByName("dDocType");
			if (docRS != null) {
				if (type.toUpperCase().contains("NONDRAWING")) {
					content.dID = docRS.getStringValueByName("dID");
					content.area = docRS.getStringValueByName("xNdLocation");
					content.disciplineCode = docRS.getStringValueByName("xNdDisciplineCode");
					content.docType = docRS.getStringValueByName("xNdDocTypeCode");
					parentDir = parentDir + "/Non Drawings";
					folderPath = content.area + "/" + fld.GetDisCodeDesc(type, content.disciplineCode, ws) + "/"
							+ content.docType;
				} else {
					content.dID = docRS.getStringValueByName("dID");
					content.area = docRS.getStringValueByName("xDLocation");
					content.platform = docRS.getStringValueByName("xDPlatform");
					content.disciplineCode = docRS.getStringValueByName("xDDisciplineCode");
					content.docType = docRS.getStringValueByName("xDDocTypeCode");
					parentDir = parentDir + "/Drawings";
					folderPath = content.area + "/" + content.platform + "/"
							+ fld.GetDisCodeDesc(type, content.disciplineCode, ws) + "/" + content.docType;
				}
				content.parentGUID = docRS.getStringValueByName("fParentGUID");
				content.folderGUID = docRS.getStringValueByName("fFolderGUID");
				content.dDocName = docRS.getStringValueByName("dDocName");
				binder.putLocal("dDocName", binder.getLocal("dDocName"));
				binder.putLocal("dSecurityGroup", "PHE_TDC");
				binder.putLocal("fSecurityGroup", "PHE_TDC");
				binder.putLocal("xStatus", "Expired");
				// binders.putLocal("dDocAuthor", docRS.getStringValueByName("dDocAuthor"));

//                Log.info("PHE:JOB_INFO:CreateExpiredFolder: " + content.folderGUID);

				if (!fld.DoCheckingFolder(parentDir, folderPath, binder, ws, util)) {
					ErrHandler.ErrCheckin(binder);
//                    Log.error("PHE:JOB_ERROR: " + binder.getLocal("StatusMessage"));
				} else {
					ResultSet rs = fld.GetFolderInfo(parentDir + "/" + folderPath, binder, ws, util);
					if (!rs.isEmpty()) {
//                        Log.info("ParentGUID : " + binder.getLocal("fParentGUID"));
//                        Log.info("SWITCH with this - > FolderGUID : " + rs.getStringValueByName("fFolderGUID"));
						binder.putLocal("fParentGUID", rs.getStringValueByName("fFolderGUID"));
					}
				}
				if (util.UpdateCheckin_DocInfo(binder).equalsIgnoreCase("executeFailed")) {
					ErrHandler.ErrCheckin(binder);
					Log.error("PHE:JOB_ERROR: " + binder.getLocal("StatusMessage"));
					return "executeFailed";
				} else {
					// kirim email kalo update berhasil..
//                    ScheduledEvt sendEmail = new ScheduledEvt();
					dID = binder.getLocal("dID");

					Log.info("PHE:pheReminderExpiredDocument:dID:" + dID);

//                    sendEmail.pheReminderExpiredDocument(dID);

				}
			}
		}
		return "executeSuccess";
	}

	public class Content {

		private String dID;
		private String docNumber;
		private String parentGUID;
		private String folderGUID;
		private String area;
		private String platform;
		private String disciplineCode;
		private String docType;
		private String dDocName;

		public void setDID(String dID) {
			this.dID = dID;
		}

		public String getDID() {
			return dID;
		}

		public void setParentGUID(String parentGUID) {
			this.parentGUID = parentGUID;
		}

		public String getParentGUID() {
			return parentGUID;
		}

		public void setDocNumber(String docNumber) {
			this.docNumber = docNumber;
		}

		public String getDocNumber() {
			return docNumber;
		}

		public void setFolderGUID(String folderGUID) {
			this.folderGUID = folderGUID;
		}

		public String getFolderGUID() {
			return folderGUID;
		}

		public void setArea(String area) {
			this.area = area;
		}

		public String getArea() {
			return area;
		}

		public void setPlatform(String platform) {
			this.platform = platform;
		}

		public String getPlatform() {
			return platform;
		}

		public void setDisciplineCode(String disciplineCode) {
			this.disciplineCode = disciplineCode;
		}

		public String getDisciplineCode() {
			return disciplineCode;
		}

		public void setDocType(String docType) {
			this.docType = docType;
		}

		public String getDocType() {
			return docType;
		}

		public void setDDocName(String dDocName) {
			this.dDocName = dDocName;
		}

		public String getDDocName() {
			return dDocName;
		}
	}

	public void executeExpiredDocument() {

		try {
			DatabaseValue dValue = new DatabaseValue();
			Util util = new Util();

			DataBinder binderHasExpired = new DataBinder();

			binderHasExpired.putLocal("IdcService", "PHE_HAS_EXPIRED_NO_FOLDER");
			DataCollection dColl = new DataCollection();

			UserAdmin uAdmin = new UserAdmin();
			ArrayList alUser = uAdmin.getUserAdmin();

			util.executeService(binderHasExpired, alUser.get(0).toString(), false, dColl.getSystemWorkspace());

			ResultSet rs = binderHasExpired.getResultSet("HAS_EXPIRED_RS");

			DataResultSet myDataResultSet = new DataResultSet();
			myDataResultSet.copy(rs);

			if (!myDataResultSet.isEmpty()) {
				do {
					DataBinder binder = m_binder;
					binder.putLocal("dID", myDataResultSet.getStringValueByName("dID"));
					binder.putLocal("dDocName", myDataResultSet.getStringValueByName("dDocName"));
					binder.putLocal("dDocType", myDataResultSet.getStringValueByName("dDocType"));

					binder.putLocal("dUser", alUser.get(0).toString());
					MoveToExpireFolder(binder);
				} while (myDataResultSet.next());
			}
		} catch (DataException ex) {
			Log.info("PHE:executeExpiredDocument:exDataException:" + ex.getMessage());
		} catch (ServiceException ex) {
			Log.info("PHE:executeExpiredDocument:exServiceException:" + ex.getMessage());
		}
	}

	public void executeSetExpiredDocument() {
		// kalo ini dirubah, executeSetExpiredDocumentEmail dirubah juga -
		// PHE_GET_EXPIRED_DOCUMENT_MIDDLE_REVISION
		DatabaseValue dValue = new DatabaseValue();
		try {
			Util util = new Util();
			Log.info("PHE:executeSetExpiredDocument:enter");
			DataBinder binderHasExpired = new DataBinder();

			binderHasExpired.putLocal("IdcService", "PHE_GET_EXPIRED_DOCUMENT");
			DataCollection dColl = new DataCollection();
			UserAdmin uAdmin = new UserAdmin();
			ArrayList alUser = uAdmin.getUserAdmin();

			util.executeService(binderHasExpired, alUser.get(0).toString(), false, dColl.getSystemWorkspace());

			ResultSet rs = binderHasExpired.getResultSet("GET_EXPIRED_RS");
			DataResultSet dRS = new DataResultSet();
			dRS.copy(rs);

			String strList = "";
			String strListMessage = "";
			String author = "";
			int lengthDRS = dRS.getNumRows();
			for (int i = 0; i < lengthDRS; i++) {
				List<String> listData = dRS.getRowAsList(i);
//                    Log.info("PHE:listData:" + listData);
				DataBinder binder = new DataBinder();
				binder.putLocal("dID", listData.get(0).toString());
				binder.putLocal("dDocName", listData.get(1).toString());
				binder.putLocal("dDocType", listData.get(2).toString());
				binder.putLocal("dUser", alUser.get(0).toString());
				binder.putLocal("sendEmail", "false");

				// int numCols=dRS.getNumFields();

				// Listing Message Expired Reminder - add by Nanda 190814

				strList = "<td>" + listData.get(4).toString() + "</td>" + "<td>" + listData.get(5).toString() + "</td>"
						+ "<td>" + listData.get(6).toString() + "</td>" + "<td>" + listData.get(7).toString() + "</td>"
						+ "<td>" + listData.get(8).toString() + "</td>";

				strList = "<tr>" + strList + "</tr>";
				strListMessage = strListMessage + strList;
				strList = "";
				Log.info("PHE:executeSetExpiredDocument:author:" + author);
				Log.info("PHE:executeSetExpiredDocument:listData.get(8):" + listData.get(8).toString());
				if (!author.contains(listData.get(8).toString())) {
					author = listData.get(8).toString() + "," + author;
				}

				String xDocPurpose = "";
				try {
					xDocPurpose = listData.get(3).toString();
				} catch (Exception ex) {
					xDocPurpose = "";
				}
				if (!xDocPurpose.equalsIgnoreCase("Native")) {
					MoveToExpireFolder(binder);
				} else {
					Document document = new Document();
					document.executeUpdateDocInfoJustxStatus(binder, "-", listData.get(1).toString(),
							listData.get(0).toString(), "Expired", listData.get(2).toString(), alUser.get(0).toString(),
							true);
				}
			}
			Log.info("PHE:executeSetExpiredDocument:author:" + author);
			ScheduledEvt sendEmail = new ScheduledEvt();

			// add by nanda 190814 - untuk mengirim email reminder expired dlm list
			sendEmail.pheReminderExpiredDocument2(strListMessage, author);

			// ini update buat yang di tengah tengah revisi - Di comment 200814 by nanda
//            binderHasExpired = new DataBinder();
//
//            binderHasExpired.putLocal("IdcService", "PHE_GET_EXPIRED_DOCUMENT_MIDDLE_REVISION");
//            util.executeService(binderHasExpired, alUser.get(0).toString(), false, dColl.getSystemWorkspace());
//
//            rs = binderHasExpired.getResultSet("GET_EXPIRED_MIDDLE_REVISION_RS");
//            dRS = new DataResultSet();
//            dRS.copy(rs);
//
//            lengthDRS = dRS.getNumRows();
//            for (int i = 0; i < lengthDRS; i++) {
//                List<String> listData = dRS.getRowAsList(i);
////                    Log.info("PHE:listData:" + listData);
//                DataBinder binder = new DataBinder();
//                binder.putLocal("dID", listData.get(0).toString());
//                binder.putLocal("dDocName", listData.get(1).toString());
//                binder.putLocal("dDocType", listData.get(2).toString());
//                binder.putLocal("dUser", alUser.get(0).toString());
//                binder.putLocal("sendEmail", "false");
//                
//                strList = "<td>" + listData.get(4).toString() + "</td>"
//                                 + "<td>" + listData.get(5).toString() + "</td>"
//                                 + "<td>" + listData.get(6).toString() + "</td>"
//                                 + "<td>" + listData.get(7).toString() + "</td>"
//                                 + "<td>" + listData.get(8).toString() + "</td>";
////                    }
//                    strList = "<tr>" + strList + "</tr>";
//                    strListMessage = strListMessage + strList;
//                    strList = "";
//                     author = listData.get(8).toString()+","+author;
//
//                Document document = new Document();
//                document.executeUpdateDocInfoJustxStatus(binder, "-", listData.get(1).toString(), listData.get(0).toString(), "Expired", listData.get(2).toString(), alUser.get(0).toString(), true);
//                
//            }
//            
//            //update by gin69..
//            //tambahin log pas email scheduler..
//            dValue.insertScheduleLog("EXPIRED_DOCUMENT", "Success", "PHE_CLICK_SCHEDULE_SET_EXPIRED");

//             sendEmail.pheReminderExpiredDocument2(strListMessage,author);
		} catch (DataException ex) {
			dValue.insertScheduleLog("EXPIRED_DOCUMENT", "Failed", "PHE_CLICK_SCHEDULE_SET_EXPIRED");
			Log.info("PHE:executeExpiredDocument:exDataException:" + ex.getMessage());
		} catch (ServiceException ex) {
			dValue.insertScheduleLog("EXPIRED_DOCUMENT", "Failed", "PHE_CLICK_SCHEDULE_SET_EXPIRED");
			Log.info("PHE:executeExpiredDocument:exServiceException:" + ex.getMessage());
		}
	}

	public void executeSetExpiredDocumentEmail() {
		// kalo ini dirubah, executeSetExpiredDocument dirubah juga
		try {
			UserAdmin uAdmin = new UserAdmin();
			ArrayList alUser = uAdmin.getUserAdmin();

			DatabaseValue dValue = new DatabaseValue();
			Util util = new Util();
//            Log.info("PHE:executeSetExpiredDocument:enter");
			DataBinder binderHasExpired = new DataBinder();

			binderHasExpired.putLocal("IdcService", "PHE_GET_EXPIRED_DOCUMENT");
			DataCollection dColl = new DataCollection();
			util.executeService(binderHasExpired, alUser.get(0).toString(), false, dColl.getSystemWorkspace());

			ResultSet rs = binderHasExpired.getResultSet("GET_EXPIRED_RS");
			DataResultSet dRS = new DataResultSet();
			dRS.copy(rs);

			int lengthDRS = dRS.getNumRows();
			for (int i = 0; i < lengthDRS; i++) {
				try {
					List<String> listData = dRS.getRowAsList(i);
//                    Log.info("PHE:listData:" + listData);
					DataBinder binder = new DataBinder();
					binder.putLocal("dID", listData.get(0).toString());
					binder.putLocal("dDocName", listData.get(1).toString());
					binder.putLocal("dDocType", listData.get(2).toString());
					binder.putLocal("dUser", alUser.get(0).toString());
					binder.putLocal("sendEmail", "true");
//                    Log.info("PHE:executeSetExpiredDocument:binder:" + binder);
					MoveToExpireFolder(binder);
				} catch (Exception ex) {
					Log.error("for ketiga:ex:" + ex.getMessage());
				}
			}

		} catch (DataException ex) {
			Log.info("PHE:executeExpiredDocument:exDataException:" + ex.getMessage());
		} catch (ServiceException ex) {
			Log.info("PHE:executeExpiredDocument:exServiceException:" + ex.getMessage());
		}
	}

	public void executeSetExpiredDocumentByDID() {
		DataBinder binder = new DataBinder();
		binder.putLocal("dID", m_binder.getLocal("dID"));

		binder.putLocal("xStatus", "Expired");

		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm a");
		try {
			binder.putLocal("xRetentionDate", sdf.format(date));
			binder.setFieldType("xRetentionDate", "Date");
		} catch (Exception ex) {
			Log.error("PHE:parsingDate:ex" + ex.getMessage());
		}
		Util util = new Util();
		UserAdmin uAdmin = new UserAdmin();
		ArrayList alUser = uAdmin.getUserAdmin();
		util.setUserLogin(alUser.get(0).toString());
		Workspace ws = getWorkspace();
		if (util.UpdateCheckin_DocInfo(binder).equalsIgnoreCase("executeFailed")) {
			ErrHandler.ErrCheckin(binder);
			Log.error("PHE:JOB_ERROR: " + binder.getLocal("StatusMessage"));

		}

		MoveToExpireFolder(binder);

	}

	public void MoveToExpireFolderByDIDAndDDocType() {
		Util util = new Util();

		String dID = m_binder.getLocal("dID");
		DataBinder binder = new DataBinder();
		binder.putLocal("dID", dID);
		binder.putLocal("dDocType", m_binder.getLocal("dDocType"));
		Log.info("PHE:MoveToExpireFolderByDIDAndDDocType:" + dID);

		util.setUserLogin(binder.getLocal("dUser"));

		if (dID.length() > 0) {

			if (CreateExpiredFolder(binder).equalsIgnoreCase("executeFailed")) {
				binder.putLocal("StatusMessage", "ERROR EXECUTE UPDATE");
			}

		}
//        m_binder.putLocal("dStatus", "EXPIRED");
//        Log.info("END_PHE:JOB_INFO");
	}
}
