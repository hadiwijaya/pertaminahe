/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ScheduledEvent;

import data.DatabaseValue;
import intradoc.data.DataException;
import intradoc.server.Service;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ginanjar
 */
public class ScheduleManually extends Service{
    public void executeReminderPreparing(){
        ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderPreparing();
        } catch (DataException ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeReminderReturnToTDC(){
        ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderReturnToTDC();
        } catch (DataException ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeReminderReturnToRequestor(){
        ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderReturnToRequestor();
        } catch (DataException ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeReminderReturnToRequestorHplus(){
        ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderReturnToRequestorHplus();
        } catch (DataException ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeReminderInternalTransmittalDueDate(){
         ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderInternalTransmittalDueDate();
        } catch (DataException ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeReminderExternalTransmittalDueDate(){
         ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderExternalTransmittalDueDate();
        } catch (DataException ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void executeReminderPublishInternalTransmittalDueDate(){
        ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderPublishInternalTransmittalDueDate();
        } catch (DataException ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void executeReminderPublishExternalTransmittalDueDate(){
        ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderPublishExternalTransmittalDueDate();
        } catch (DataException ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void executeReminderExpiredDocument(){
        ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderExpiredDocument();
        } catch (DataException ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeDeleteMarkupBulk() throws DataException{
        ScheduledEvt evt = new ScheduledEvt();
        DatabaseValue dValue = new DatabaseValue();
        String scheduleDelInterval = dValue.getPheConfig("SCHEDULE_DELINTERVAL");
        try {
            evt. pheDeleteMarkup(scheduleDelInterval);;
        } catch (Exception ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     public void executeObsoleteDocument(){
        ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheObsoleteScheduler();
        } catch (Exception ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void executeReminderInternalTransmittalBeforeDueDate(){
        ScheduledEvt evt = new ScheduledEvt();
        try {
            evt.pheReminderInternalTransmittalBeforeDueDate();
        } catch (Exception ex) {
            Logger.getLogger(ScheduleManually.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
