/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ScheduledEvent;

import DocService.Expire;
import DocService.Obsolete;
import data.DatabaseValue;
import intradoc.server.*;

import intradoc.shared.*;

import intradoc.common.*;

import intradoc.data.*;

import intradoc.common.Log;

import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradocservice.Document;
import intradocservice.Util;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.internet.InternetAddress;
import oracle.stellent.ridc.model.DataObject;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

import util.DataCollection;
import util.UserAdmin;
import Markup.DeleteMarkup;

/**
 *
 * @author Bijak
 */
public class ScheduledEvt implements FilterImplementor {
 DatabaseValue dValue = new DatabaseValue();
    public void UpdateOverDueStatus() throws DataException {
        String query =
                "UPDATE DEV_URMSERVER.RESERVATIONITEMMAP SET DREQUESTSTATUS = 'wwOverDue' where drequestduedate < sysdate and DREQUESTSTATUS='wwCheckedOut'";
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        ws.executeSQL(query);
    }

    public void pheReminderPreparing() throws DataException {
        DataResultSet ticketRS = null;
        DataResultSet emailRS = null;
        String strListMessage = "";
        String requestName = "";
        String strTo = "";
        String strSubject;
        String strMessage;
        int i;
//        DatabaseValue dValue = new DatabaseValue();

        String query = "select a.drequestname,e.dbarcode from DEV_URMSERVER.reservations a,DEV_URMSERVER.reservationitemmap b,dev_urmserver.extitems e where e.did = b.did and b.drequestid = a.drequestid and b.dpherequestitemstatus='wwWaiting' and b.dpherequestitemstatustmp='wwApproveTDC' and sysdate > (b.dLastModifiedDate+interval '1' day)";
        // Log.info("pheReminderPreparing: query:" + query);

        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        //ws.executeSQL(query);
        try {
            dValue.insertScheduleLog("LATE_PREPARATION_DLS", "Success", "PHE_CLICK_SCHEDULE_REMINDER_PREPARING");
            ResultSet rs = ws.createResultSetSQL(query);
            ticketRS = new DataResultSet();
            ticketRS.copy(rs);
            //Log.info("pheReminderPreparing: ticketRS:" + ticketRS);                

            //edit by nanda - 230614 - validasi if ticket is not empty
            if (!ticketRS.isEmpty()) {
                int numCols = ticketRS.getNumFields();
                // Log.info("pheReminderPreparing: numCols:"+numCols);
                do {
                    for (i = 0; i < numCols; i++) {
                        strListMessage = strListMessage + "<td>" + ticketRS.getStringValue(i) + "</td>";
                    }
                    strListMessage = "<tr>" + strListMessage + "</tr>";
                    requestName = requestName + strListMessage;
                    strListMessage = "";
                } while (ticketRS.next());
                System.out.println("pheReminderPreparing: listTicket:" + requestName);
                Log.info("pheReminderPreparing: listTicket:" + requestName);

                //update by nanda 100914 - add the query 'and dfullname is not null'
                String query2 = "select demail from dev_urmserver.users a, dev_urmserver.usersecurityattributes b where a.dname = b.dusername and dattributetype='role' and dattributename='tdcadmin' and demail is not null";
                Log.info("pheReminderPreparing:query:lateprep:" + query2);

                DataCollection dColl2 = new DataCollection();
                Workspace ws2 = dColl2.getSystemWorkspace();
                //ws.executeSQL(query);

                try {
                    ResultSet rs2 = ws2.createResultSetSQL(query2);
                    emailRS = new DataResultSet();
                    emailRS.copy(rs2);
                    Log.info("pheReminderPreparing: emailRS:" + emailRS);

                    if (!emailRS.isEmpty()) {
                        //emailRS.first();
//                        Log.info("pheReminderReturnToTDC:emailRS.getNumRows():" + emailRS.getNumRows());
                        do {
                            strTo = emailRS.getStringValue(0) + "," + strTo;

                        } while (emailRS.next());
                        Log.info("pheReminderPreparing: strTo: sebelumDiPotong" + strTo);

                        if (strTo.length() > 0) {

                            if (strTo.lastIndexOf(",") == strTo.length() - 1) {
                                Log.info("--strToDalemIfSblmSamadengan:" + strTo);
                                strTo = strTo.substring(0, strTo.length() - 1);
                                Log.info("--strToDalemIfStlhSamadengan:" + strTo);
                            }

                            strSubject = "[DLS] Late Preparation Alert";

                            strMessage = "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ;  background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>";
                            strMessage = strMessage + "<span style='font-family:Arial'><p>Dear Mr/Mrs,<br>This e-mail is sent to notify you that there is/are ticket/tickets hasn't been prepared by warehouse admin.</p><br>List of tickets and requested documents :";
                            strMessage = strMessage + "<table><tr><th><h3>Ticket No</h3></th><th><h3>Document Tagging</h3></th></tr>" + requestName + "</table>";
                            strMessage = strMessage + "<p>Thank you for your attention.</p></span>";

                            //Log.info("pheReminderPreparing: strMessage:" + strMessage);
                            pheSendMail(strTo, strMessage, strSubject, "");
                            Log.info("send reminder return to tdc succesfully");
                        }

//                        for (i = 0; i < emailRS.getNumRows(); i++) {
//                            List list = emailRS.getRowAsList(i);
//                            strTo = list.get(0).toString();
//                            strTo = emailRS.getStringValue(i);
//                            Log.info("pheReminderReturnToTDC:strTo.length():" + strTo.length());


//                        }
                    }
                } catch (DataException ex) {
                    Log.error("pheReminderPreparing: DataException: getEmail:" + ex.getMessage());
                }
            }


        } catch (DataException ex) {
            dValue.insertScheduleLog("LATE_PREPARATION_DLS", "Failed", "PHE_CLICK_SCHEDULE_REMINDER_PREPARING");
            Log.error("pheReminderPreparing: DataException: getTicket:" + ex.getMessage());
        }

    }

    public void pheReminderReturnToTDC() throws DataException {
        DataResultSet ticketRS = null;
        DataResultSet emailRS = null;
        String strListMessage = "";
        String requestName = "";
        String strTo = "";
        String strSubject;
        String strMessage;
        int i;

//        DatabaseValue dValue = new DatabaseValue();
        String query = "select a.drequestor,a.drequestname,e.dbarcode from DEV_URMSERVER.reservations a,DEV_URMSERVER.reservationitemmap b,dev_urmserver.extitems e where e.did = b.did and b.drequestid = a.drequestid and b.dpherequestitemstatus='wwBorrowed' and to_char(sysdate,'dd-MON-yy') = to_char(drequestduedate,'dd-MON-yy')";

        //Log.info("pheReminderReturnToTDC: guery:" + query);
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        //ws.executeSQL(query);
        try {
            dValue.insertScheduleLog("DOCUMENT_RETURN_ALERT_TO_TDC", "Success", "PHE_CLICK_SCHEDULE_DOCUMENT_ALERT_TO_TDC");
            ResultSet rsreturn = ws.createResultSetSQL(query);
            ticketRS = new DataResultSet();
            ticketRS.copy(rsreturn);

            //Log.info("pheReminderReturnToTDC: ticketRS:" + ticketRS);
            int numCols = ticketRS.getNumFields();
            //Log.info("pheReminderReturnToTDC: numCols:"+numCols);

            //edit by nanda - 230614 - validasi if ticket is not empty
            if (!ticketRS.isEmpty()) {
                do {
                    for (i = 0; i < numCols; i++) {
                        strListMessage = strListMessage + "<td>" + ticketRS.getStringValue(i) + "</td>";
                    }
                    strListMessage = "<tr>" + strListMessage + "</tr>";
                    requestName = requestName + strListMessage;
                    strListMessage = "";
                } while (ticketRS.next());
                //System.out.println("return to tdc list="+requestName);
                Log.info("pheReminderReturnToTDC: listTicket:" + requestName);

                String query2 = "select demail from dev_urmserver.users a, dev_urmserver.usersecurityattributes b where a.dname = b.dusername and dattributetype='role' and dattributename='tdcadmin' and demail is not null";
                //Log.info("pheReminderReturnToTDC: queryEmail:" + query2);
                DataCollection dColl2 = new DataCollection();
                Workspace ws2 = dColl2.getSystemWorkspace();
                //ws.executeSQL(query);
                try {
                    ResultSet rs2 = ws2.createResultSetSQL(query2);
                    emailRS = new DataResultSet();
                    emailRS.copy(rs2);

                    //Log.info("pheReminderReturnToTDC: emailRS:" + emailRS); 

                    if (!emailRS.isEmpty()) {
                        emailRS.first();
                        do {
                            strTo = emailRS.getStringValue(0) + "," + strTo;
                        } while (emailRS.next());

                        Log.info("pheReminderReturnToTDC:strTo: sebelum dipotong" + strTo);
                        if (strTo.length() > 0) {

                            if (strTo.lastIndexOf(",") == strTo.length() - 1) {
                                Log.info("--strToDalemIfSblmSamadengan:" + strTo);
                                strTo = strTo.substring(0, strTo.length() - 1);
                                Log.info("--strToDalemIfStlhSamadengan:" + strTo);
                            }

                            strSubject = "[DLS] Document Return Alert to TDC";
                            strMessage = "<span style='font-family:Arial'><style>table,th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>";
                            strMessage = strMessage + "<p>Dear Mr/Mrs,<br>This e-mail is sent to notify you that today is duedate for some documents to return</p><br>List of tickets and requested documents :";
                            strMessage = strMessage + "<table><tr><th><h3>Requestor</h3></th><th><h3>Ticket No</h3></th><th><h3>Document Tagging</h3></th></tr>" + requestName + "</table>";
                            strMessage = strMessage + "<p>Thank you for your attention.</p></span>";

                            //Log.info("pheReminderReturnToTDC: strMessage:" + strMessage);
                            pheSendMail(strTo, strMessage, strSubject, "");
                            //Log.info("send reminder return to tdc succesfully");
                        }

//                        for (i = 0; i < emailRS.getNumRows(); i++) {

//                            List list = emailRS.getRowAsList(i);
//                            strTo = list.get(0).toString();

//                        }

                    }
                    //Log.info("pheReminderReturnToTDC: strTo:" + strTo);

                } catch (DataException ex) {
                    Log.error("pheReminderReturnToTDC: DataException: getEmail:" + ex.getMessage());
                }
            }


        } catch (DataException ex) {
            dValue.insertScheduleLog("DOCUMENT_RETURN_ALERT_TO_TDC", "Failed", "PHE_CLICK_SCHEDULE_DOCUMENT_ALERT_TO_TDC");
            Log.error("pheReminderReturnToTDC: DataException: getTicket:" + ex.getMessage());
        }
    }

    public void pheReminderReturnToRequestor() throws DataException {
        DataResultSet ticketRS = null;
        DataResultSet emailRS = null;
        String strListMessage = "";
        String requestName = "";
        String strTo = "";
        String strSubject;
        String strMessage;
        int i;
        String query;
        String param = "";

//        DatabaseValue dValue = new DatabaseValue();
        for (int a = 0; a <= 3; a++) {
            query = "select a.drequestor,a.drequestname,e.dbarcode from DEV_URMSERVER.reservations a,DEV_URMSERVER.reservationitemmap b,dev_urmserver.extitems e where e.did = b.did and b.drequestid = a.drequestid and b.dpherequestitemstatus='wwBorrowed' and to_char(sysdate,'dd-MON-yy') = to_char((drequestduedate-" + a + "),'dd-MON-yy')";

            Log.info("pheReminderReturnToRequestor: query h-" + a + "=" + query);

            DataCollection dColl = new DataCollection();
            Workspace ws = dColl.getSystemWorkspace();
            try {
                dValue.insertScheduleLog("DOCUMENT_RETURN_ALERT", "Success", "PHE_CLICK_SCHEDULE_DOCUMENT_RETURN_ALERT_H");
                ResultSet rs = ws.createResultSetSQL(query);
                ticketRS = new DataResultSet();
                ticketRS.copy(rs);

                Log.info("pheReminderReturnToRequestor: ticketRS h-" + a + "=" + ticketRS);

                DataCollection dColl2 = new DataCollection();
                Workspace ws2 = dColl2.getSystemWorkspace();

                int numCols = ticketRS.getNumFields();
                Log.info("pheReminderReturnToRequestor: numColsReturn:" + numCols);

                //edit by nanda - 230614 - validasi if ticket is not empty
                if (!ticketRS.isEmpty()) {
                    do {
                        for (i = 0; i < numCols; i++) {
                            strListMessage = strListMessage + "<td style='background-color:#DCDCDC'>" + ticketRS.getStringValue(i) + "</td>";
                        }
                        strListMessage = "<tr >" + strListMessage + "</tr>";
                        requestName = requestName + strListMessage;
                        strListMessage = "";
                        param = ticketRS.getStringValue(0);
                        //System.out.println("param="+param);
//                    Log.info("pheReminderReturnToRequestor: param:"+param);

                        //edit by nanda - 180614
                        // add UPPER
                        String query2 = "select demail from dev_urmserver.users where UPPER(dname)=UPPER('" + param + "')";
//                    Log.info("pheReminderReturnToRequestor: queryEmail:" + query2);
                        try {
                            ResultSet rs2 = ws2.createResultSetSQL(query2);
                            emailRS = new DataResultSet();
                            emailRS.copy(rs2);

//                        Log.info("pheReminderReturnToRequestor: emailRS :" + emailRS);

                            for (int k = 0; k < emailRS.getNumRows(); k++) {
                                List list = emailRS.getRowAsList(k);

                                strTo = list.get(0).toString();
                                Log.info("pheReminderReturnToRequestor: strTo:" + strTo);
                                if (strTo.length() > 0) {
                                    strSubject = "[DLS] Document Return Alert H-" + a;
                                    strMessage = "<style>td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>";
                                    strMessage = strMessage + "<span style='font-family:Arial'><p>Dear Mr/Mrs,<br>This e-mail is sent to notify you that today is H-" + a + " duedate for some documents to return</p><br>List of tickets and requested documents :";
                                    strMessage = strMessage + "<table border='0'><tr><th style='background-color:#F0E68C;text-align:center;'><h3>Requestor</h3></th><th style='background-color:#F0E68C;text-align:center;'><h3>Ticket No</h3></th><th style='background-color:#F0E68C;text-align:center;'><h3>Document Tagging</h3></th></tr>" + requestName + "</table>";
                                    strMessage = strMessage + "<p>Thank you for your attention.</p></span>";

//                                Log.info("pheReminderReturnToRequestor: strMessage"+strMessage);
                                    pheSendMail(strTo, strMessage, strSubject, "");
                                    // Log.info("send reminder return to reqestor succesfully");
                                }


                            }

                        } catch (DataException ex) {
                            Log.error("pheReminderReturnToRequestor: DataException: getEmail:" + ex.getMessage());
                        }
                    } while (ticketRS.next());
                }


            } catch (DataException ex) {
                dValue.insertScheduleLog("DOCUMENT_RETURN_ALERT", "Failed", "PHE_CLICK_SCHEDULE_DOCUMENT_RETURN_ALERT_H");
                Log.error("pheReminderReturnToRequestor: DataException: getTicket:" + ex.getMessage());
            }
        }
    }

    public void pheReminderReturnToRequestorHplus() throws DataException {
        DataResultSet ticketRS = null;
        DataResultSet emailRS = null;
        String strListMessage = "";
        String requestName = "";
        String strTo = "";
        String strSubject;
        String strMessage;
        int i;
        String query;
        String param = "";

//        DatabaseValue dValue = new DatabaseValue();

        query = "select a.drequestor,a.drequestname,e.dbarcode,b.drequestduedate,extract(day from(sysdate-drequestduedate)) as past from DEV_URMSERVER.reservations a,DEV_URMSERVER.reservationitemmap b,dev_urmserver.extitems e where e.did = b.did and b.drequestid = a.drequestid and b.dpherequestitemstatus='wwBorrowed' and extract(day from(sysdate-drequestduedate)) > 0";
        //Log.info("pheReminderReturnToRequestorHplus: query:" + query);

        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        try {
            dValue.insertScheduleLog("DOCUMENT_OVERDUE_ALERT", "Success", "PHE_CLICK_SCHEDULE_DOCUMENT_OVERDUE_ALERT");
            ResultSet rs = ws.createResultSetSQL(query);
            ticketRS = new DataResultSet();
            ticketRS.copy(rs);

            //Log.info("pheReminderReturnToRequestorHplus: ticketRS hPlus:" + ticketRS);           
            DataCollection dColl2 = new DataCollection();
            Workspace ws2 = dColl2.getSystemWorkspace();

            int numCols = ticketRS.getNumFields();
            //Log.info("pheReminderReturnToRequestorHplus: numCols hPlus:"+numCols);

            //edit by nanda - 230614 - validasi if ticket is not empty
            if (!ticketRS.isEmpty()) {
                do {
                    for (i = 0; i < numCols; i++) {
                        strListMessage = strListMessage + "<td>" + ticketRS.getStringValue(i) + "</td>";
                    }
                    strListMessage = "<tr>" + strListMessage + "</tr>";
                    requestName = requestName + strListMessage;
                    strListMessage = "";
                    param = ticketRS.getStringValue(0);
                    Log.info("pheReminderReturnToRequestorHplus: param:" + param);

                    //edit by nanda - 180614
                    // add UPPER
                    String query2 = "select demail from dev_urmserver.users where UPPER(dname)=UPPER('" + param + "')";
                    // Log.info("pheReminderReturnToRequestorHplus: query email hPlus:" + query2);
                    try {
                        ResultSet rs2 = ws2.createResultSetSQL(query2);
                        emailRS = new DataResultSet();
                        emailRS.copy(rs2);
                        // Log.info("pheReminderReturnToRequestorHplus: emailRS hPlus=" + emailRS);
                        do {
                            strTo = emailRS.getStringValue(0);
                            Log.info("pheReminderReturnToRequestorHplus: strTo hPlus =" + strTo);
                            if (strTo.length() > 0) {
                                strSubject = "[DLS] Document Overdue Return Alert";
                                strMessage = "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>";
                                strMessage = strMessage + "<span style='font-family:Arial'><p>Dear Mr/Mrs,<br>This e-mail is sent to notify you that duedate for some documents to return have past</p><br>List of tickets and requested documents :";
                                strMessage = strMessage + "<table><tr><th><h3>Requestor</h3></th><th><h3>Ticket No</h3></th><th><h3>Document Tagging</h3></th><th><h3>Ticket Due Date</h3></th><th><h3>Past Day(s)</h3></th></tr>" + requestName + "</table>";
                                strMessage = strMessage + "<p>Thank you for your attention.</p></span>";
                                pheSendMail(strTo, strMessage, strSubject, "");
                                //   Log.info("send reminder overdue return to reqestor successfully");   
                            }
                        } while (emailRS.next());
                    } catch (DataException ex) {
                        Log.error("pheReminderReturnToRequestor: DataException: getEmail:" + ex.getMessage());
                    }
                } while (ticketRS.next());
            }


        } catch (DataException ex) {
            dValue.insertScheduleLog("DOCUMENT_OVERDUE_ALERT", "Failed", "PHE_CLICK_SCHEDULE_DOCUMENT_OVERDUE_ALERT");
            Log.error("pheReminderReturnToRequestor: DataException: getTicket:" + ex.getMessage());
        }
    }

    public void pheReminderInternalTransmittalDueDate() throws DataException {
        DataResultSet transRS = null;
        String strTo = "";
        String strSubject;
        String strMessage = "";


        String tempTransId = "";
        String transmittal_duedate = "";
        String transmittal_subject = "";
        String projectorganization_name = "";
        String demail = "";
//        update Fajar
        String duser = "";
//        ------- Fajar

//        DatabaseValue dValue = new DatabaseValue();

        //add by nanda - 010814 - to get idc server address
        String idcserver = dValue.getPheConfig("IDC_SERVER");

        String query = "SELECT DISTINCT pm.transmittal_id,\n"
                + "  pm.transmittal_subject,\n"
                + "  TO_CHAR(pm.transmittal_duedate,'dd-Mon-YYYY') AS duedate,\n"
                + "  pm.projectorganization_name,\n"
                + "  pp.receiver,\n"
                + "  u.dfullname,\n"
                + "  u.demail,\n"
                + "  trunc(sysdate)-trunc(pm.transmittal_duedate)\n"
                + "  FROM phe_dswf_master pm,\n"
                + "  phe_dswf_participant pp,\n"
                + "  users u,\n"
                + "  phe_dswf_doc pd\n"
                + "WHERE pm.transmittal_id = pp.transmittal_id\n"
                + "AND pd.transmittal_id   = pm.transmittal_id\n"
                + "AND pm.transmittal_type = 'Internal'\n"
                // edit by Nanda - 03092014
                //edit flag biar hanya terkirim ke reviewer yang belum submit (ga terkirim ke reviewer yang sudah submit) - sebelumnya asal masih in progress, email terkirim ke semua reviewer
                + "and pp.enddate is null\n"
                //                
                + "AND pm.transmittal_status = 'In Progress'\n"
                + "AND u.dname                        = pp.receiver\n"
                + "AND (pd.publish_status            <> '2'\n"
                + "OR pd.publish_status              IS NULL)\n"
                + "AND pd.doc_type                    = 'Supporting'\n"
                + "AND (TRUNC(pm.transmittal_duedate) = TRUNC(sysdate)\n"
                + "OR TRUNC(pm.transmittal_duedate-1) =TRUNC(sysdate)\n"
                + "OR TRUNC(pm.transmittal_duedate+1) =TRUNC(sysdate)\n"
                + "OR trunc(sysdate)-trunc(pm.transmittal_duedate)>1)\n"
                + "ORDER BY pm.transmittal_id";
        Log.info("pheReminderInternalTransmittalDueDate: query:" + query);

        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        try {
            dValue.insertScheduleLog("REMINDER_INTERNAL_TRANSMITTAL_DUEDATE", "Success", "PHE_CLICK_REMINDER_INTERNAL_TRANSMITTAL_DUEDATE");
            ResultSet rs = ws.createResultSetSQL(query);
            transRS = new DataResultSet();
            transRS.copy(rs);

            String HtmlText = "";
//            int lastRow = 0;
//            while (transRS.next()) {
//                lastRow++;
//            }
            Log.info("pheReminderInternalTransmittalDueDate:transRS:" + transRS);
            Log.info("pheReminderInternalTransmittalDueDate:transRSfirst()" + transRS.first());
            if (transRS.first()) {
                do {

                    if (tempTransId.equals("")) {
                        tempTransId = transRS.getStringValue(0);
                        transmittal_duedate = transRS.getStringValue(2);
                        transmittal_subject = transRS.getStringValue(1);
                        projectorganization_name = transRS.getStringValue(3);
//                        update Fajar
                        duser =  transRS.getStringValue(4);
//                        demail = transRS.getStringValue(6) + ",";
                        demail = transRS.getStringValue(6);
//                        ------- Fajar

                        Log.info("pheReminderInternalTransmittalDueDate: tempTransId:" + tempTransId);
                        Log.info("pheReminderInternalTransmittalDueDate: transmittal_duedate:" + transmittal_duedate);
                        Log.info("pheReminderInternalTransmittalDueDate: transmittal_subject:" + transmittal_subject);
                        Log.info("pheReminderInternalTransmittalDueDate: projectorganization_name:" + projectorganization_name);
//                        update Fajar
                        Log.info("pheReminderInternalTransmittalDueDate: duser:" + duser);
//                        ------- Fajar
                        Log.info("pheReminderInternalTransmittalDueDate: demail:" + demail);
                        Log.info("pheReminderInternalTransmittalDueDate: transRS.getCurrentRow():" + transRS.getCurrentRow());
                        Log.info("pheReminderInternalTransmittalDueDate: transRS.getNumRows():" + transRS.getNumRows());
//                    System.out.println(rs.getString("demail"));

                        HtmlText = "<html>\n"
                                + "<head></head>\n"
                                + "<body style=\"font-family:arial\">\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th></tr>\n"
                                + "<tr><td>" + tempTransId + "</td>"
                                + "<td>" + transmittal_subject + "</td>"
                                + "<td>" + transmittal_duedate + "</td>"
                                + "<td>" + projectorganization_name + "</td></tr>"
                                + "</table><br/><br/>"
//                        update Fajar
                                + "Please see the transmittal in Inbox DSWF menu or click this <a href=\"" + idcserver + "?IdcService=PHE_GET_TRANSMITTAL_INBOX&fromEmail=1&dUser="+duser+"&dateType=&month=&year=&status=&category=&startRow=1&endRow=10&transmittalNo="+tempTransId+"\">link</a></body></html>";
//                        Log.info("kirim email ke " + demail.substring(0, demail.lastIndexOf(",")));
                        Log.info("kirim email ke " + demail);
                        Log.info(HtmlText);
//                        strTo = demail.substring(0, demail.lastIndexOf(","));
                        strTo = demail;
//                        ------- Fajar
                        strMessage = HtmlText;
//                            Log.info("send email berhasil\n\n");

                    } else if (tempTransId.trim().equalsIgnoreCase(transRS.getStringValue(0).trim())) {

//                        update Fajar
                        tempTransId = transRS.getStringValue(0);
                        transmittal_duedate = transRS.getStringValue(2);
                        transmittal_subject = transRS.getStringValue(1);
                        projectorganization_name = transRS.getStringValue(3);
                        duser =  transRS.getStringValue(4);
                        demail = transRS.getStringValue(6);
                        
                        Log.info("pheReminderInternalTransmittalDueDate: tempTransId:" + tempTransId);
                        Log.info("pheReminderInternalTransmittalDueDate: transmittal_duedate:" + transmittal_duedate);
                        Log.info("pheReminderInternalTransmittalDueDate: transmittal_subject:" + transmittal_subject);
                        Log.info("pheReminderInternalTransmittalDueDate: projectorganization_name:" + projectorganization_name);
                        Log.info("pheReminderInternalTransmittalDueDate: duser:" + duser);
                        Log.info("pheReminderInternalTransmittalDueDate: demail:" + demail);
                        Log.info("pheReminderInternalTransmittalDueDate: transRS.getCurrentRow():" + transRS.getCurrentRow());
                        Log.info("pheReminderInternalTransmittalDueDate: transRS.getNumRows():" + transRS.getNumRows());
                        
                        HtmlText = "<html>\n"
                                + "<head></head>\n"
                                + "<body style=\"font-family:arial\">\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th></tr>\n"
                                + "<tr><td>" + tempTransId + "</td>"
                                + "<td>" + transmittal_subject + "</td>"
                                + "<td>" + transmittal_duedate + "</td>"
                                + "<td>" + projectorganization_name + "</td></tr>"
                                + "</table><br/><br/>"
                                + "Please see the transmittal in Inbox DSWF menu or click this <a href=\"" + idcserver + "?IdcService=PHE_GET_TRANSMITTAL_INBOX&fromEmail=1&dUser="+duser+"&dateType=&month=&year=&status=&category=&startRow=1&endRow=10&transmittalNo="+tempTransId+"\">link</a></body></html>";
                        
                        Log.info("kirim email ke " + demail);
                        Log.info(HtmlText);
                        strTo = demail;
                        strMessage = HtmlText;
                        
//                        demail = demail + transRS.getStringValue(6) + ",";
//                        Log.info("pheInternalTransmittal: dEmail:" + demail);
//                        Log.info("pheInternalTransmittal: dEmail:" + demail);

//                        HtmlText = "<html>\n"
//                                + "<head></head>\n"
//                                + "<body style=\"font-family:arial\">Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
//                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
//                                + "Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
//                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th></tr>\n"
//                                + "<tr><td>" + tempTransId + "</td>"
//                                + "<td>" + transmittal_subject + "</td>"
//                                + "<td>" + transmittal_duedate + "</td>"
//                                + "<td>" + projectorganization_name + "</td></tr>"
//                                + "</table><br/><br/>"
//                                + "Please see the transmittal in Inbox DSWF menu or click this<a href=\"" + idcserver + "?IdcService=PHE_GET_TRANSMITTAL_DETAIL&transmittalID=" + tempTransId + "\">link</a></body></html>";
//                        update Fajar
//                        Log.info("kirim email ke " + demail.substring(0, demail.lastIndexOf(",")));
//                        Log.info("kirim email ke " + demail);
//                        Log.info(HtmlText);
//                        strTo = demail.substring(0, demail.lastIndexOf(","));
//                        strTo = demail;
//                        strMessage = HtmlText;
//                            Log.info("send email berhasil\n\n");
//                        ------- Fajar
                    } else if (!tempTransId.trim().equalsIgnoreCase(transRS.getStringValue(0).trim())) {
//                      Log.info("pheReminderTransmittalInternal: strTo=" + strTo);
//                        if (strTo.length() > 0) {
//                            strSubject = projectorganization_name + ":" + transmittal_subject;
//                            pheSendMail(strTo, strMessage, strSubject, "");
//                            Log.info("send pheReminderTransmittalInternal successfully");
//                        }  
//                    System.out.println("send email berhasil\n\n");


//                    if (!tempTransId.trim().equalsIgnoreCase(transRS.getStringValue(0).trim())) {

                        tempTransId = transRS.getStringValue(0);
                        transmittal_duedate = transRS.getStringValue(2);
                        transmittal_subject = transRS.getStringValue(1);
                        projectorganization_name = transRS.getStringValue(3);
//                        update Fajar
                        duser =  transRS.getStringValue(4);
//                        demail = transRS.getStringValue(6) + ",";
                        demail = transRS.getStringValue(6);
//                        ------ Fajar

                        Log.info("pheReminderInternalTransmittalDueDate: tempTransId:" + tempTransId);
                        Log.info("pheReminderInternalTransmittalDueDate: transmittal_duedate:" + transmittal_duedate);
                        Log.info("pheReminderInternalTransmittalDueDate: transmittal_subject:" + transmittal_subject);
                        Log.info("pheReminderInternalTransmittalDueDate: projectorganization_name:" + projectorganization_name);
//                        update Fajar
                        Log.info("pheReminderInternalTransmittalDueDate: duser:" + duser);
//                        ------ Fajar
                        
                        Log.info("pheReminderInternalTransmittalDueDate: demail:" + demail);
                        Log.info("pheReminderInternalTransmittalDueDate: transRS.getCurrentRow():" + transRS.getCurrentRow());
                        Log.info("pheReminderInternalTransmittalDueDate: transRS.getNumRows():" + transRS.getNumRows());
//                       
                        HtmlText = "<html>\n"
                                + "<head></head>\n"
                                + "<body style=\"font-family:arial\">\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th></tr>\n"
                                + "<tr><td>" + tempTransId + "</td>"
                                + "<td>" + transmittal_subject + "</td>"
                                + "<td>" + transmittal_duedate + "</td>"
                                + "<td>" + projectorganization_name + "</td></tr>"
                                + "</table><br/><br/>"
//                      update Fajar
                                + "Please see the transmittal in Inbox DSWF menu or click this <a href=\"" + idcserver + "?IdcService=PHE_GET_TRANSMITTAL_INBOX&fromEmail=1&dUser="+duser+"&dateType=&month=&year=&status=&category=&startRow=1&endRow=10&transmittalNo="+tempTransId+"\">link</a></body></html>";

//                        Log.info("kirim email ke " + demail.substring(0, demail.lastIndexOf(",")));
                        Log.info("kirim email ke " + demail);
                        Log.info(HtmlText);
//                        strTo = demail.substring(0, demail.lastIndexOf(","));
                        strTo = demail;
//                        ------- Fajar
                        strMessage = HtmlText;
                    }
//                    update Fajar
                    Log.info("pheReminderTransmittalInternal: strTo=" + strTo);
                    if (strTo.length() > 0) {
                        strSubject = projectorganization_name + ":" + transmittal_subject;
                        pheSendMail(strTo, strMessage, strSubject, "");
                        Log.info("send pheReminderTransmittalInternal successfully");
                    }
//                    ------- Fajar
                } while (transRS.next());
            }

            Log.info("pheReminderTransmittalInternal: strTo=" + strTo);
            if (strTo.length() > 0) {
                strSubject = projectorganization_name + ":" + transmittal_subject;
                pheSendMail(strTo, strMessage, strSubject, "");
                //   Log.info("send reminder overdue return to reqestor successfully");   
            }
        } catch (DataException ex) {
            dValue.insertScheduleLog("REMINDER_INTERNAL_TRANSMITTAL_DUEDATE", "Failed", "PHE_CLICK_REMINDER_INTERNAL_TRANSMITTAL_DUEDATE");
            Log.error("pheReminderTransmittalInternal: DataException: getTicket:" + ex.getMessage());
        }

    }
    
    //add by nanda 040416 - reminder transmittal before due date
   public void pheReminderInternalTransmittalBeforeDueDate() throws DataException {
        DataResultSet transRS = null;
        String strTo = "";
        String strSubject;
        String strMessage = "";


        String tempTransId = "";
        String transmittal_duedate = "";
        String transmittal_subject = "";
        String projectorganization_name = "";
        String demail = "";
        String sender = "";

        String duser = "";

        //add by nanda - 010814 - to get idc server address
        String idcserver = dValue.getPheConfig("IDC_SERVER");

        String query = "SELECT DISTINCT pm.transmittal_id,\n"
                + "  pm.transmittal_subject,\n"
                + "  TO_CHAR(pm.transmittal_duedate,'dd-Mon-YYYY') AS duedate,\n"
                + "  pm.projectorganization_name,\n"
                + "  pp.receiver,\n"
                + "  u.dfullname,\n"
                + "  u.demail,\n"
                + "  trunc(sysdate)-trunc(pm.transmittal_duedate),\n"
                + "  transmittal_creator"
                + "  FROM phe_dswf_master pm,\n"
                + "  phe_dswf_participant pp,\n"
                + "  users u,\n"
                + "  phe_dswf_doc pd\n"
                + "WHERE pm.transmittal_id = pp.transmittal_id\n"
                + "AND pd.transmittal_id   = pm.transmittal_id\n"
                + "AND pm.transmittal_type = 'Internal'\n"
                // edit by Nanda - 03092014
                //edit flag biar hanya terkirim ke reviewer yang belum submit (ga terkirim ke reviewer yang sudah submit) - sebelumnya asal masih in progress, email terkirim ke semua reviewer
                + "and pp.enddate is null\n"
                //                
                + "AND pm.transmittal_status = 'In Progress'\n"
                + "AND u.dname                        = pp.receiver\n"
                + "AND (pd.publish_status            <> '2'\n"
                + "OR pd.publish_status              IS NULL)\n"
                + "AND pd.doc_type                    = 'Supporting'\n"
                + "AND (TRUNC(pm.transmittal_duedate-1) = TRUNC(sysdate)\n"
                + "OR TRUNC(pm.transmittal_duedate-2) =TRUNC(sysdate))\n"
                + "ORDER BY pm.transmittal_id";
        Log.info("pheReminderInternalTransmittalBeforeDueDate: query:" + query);

        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        try {
            dValue.insertScheduleLog("REMINDER_INTERNAL_TRANSMITTAL_BEFORE_DUEDATE", "Success", "PHE_CLICK_REMINDER_INTERNAL_TRANSMITTAL_BEFORE_DUEDATE");
            ResultSet rs = ws.createResultSetSQL(query);
            transRS = new DataResultSet();
            transRS.copy(rs);

            String HtmlText = "";
//            int lastRow = 0;
//            while (transRS.next()) {
//                lastRow++;
//            }
            Log.info("pheReminderInternalTransmittalBeforeDueDate:transRS:" + transRS);
            Log.info("pheReminderInternalTransmittalBeforeDueDate:transRSfirst()" + transRS.first());
            if (transRS.first()) {
                do {

                    if (tempTransId.equals("")) {
                        tempTransId = transRS.getStringValue(0);
                        transmittal_duedate = transRS.getStringValue(2);
                        transmittal_subject = transRS.getStringValue(1);
                        projectorganization_name = transRS.getStringValue(3);
                        sender = transRS.getStringValue(8).toString();
//                        update Fajar
                        duser =  transRS.getStringValue(4);
//                        demail = transRS.getStringValue(6) + ",";
                        demail = transRS.getStringValue(6);
//                        ------- Fajar

                        Log.info("pheReminderInternalTransmittalBeforeDueDate: tempTransId:" + tempTransId);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transmittal_duedate:" + transmittal_duedate);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transmittal_subject:" + transmittal_subject);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: projectorganization_name:" + projectorganization_name);
//                        update Fajar
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: duser:" + duser);
//                        ------- Fajar
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: demail:" + demail);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transRS.getCurrentRow():" + transRS.getCurrentRow());
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transRS.getNumRows():" + transRS.getNumRows());
//                    System.out.println(rs.getString("demail"));

                        HtmlText = "<html>\n"
                                + "<head></head>\n"
                                + "<body style=\"font-family:arial\">\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th><th><h3>Sender</h3></th></tr>\n"
                                + "<tr><td>" + tempTransId + "</td>"
                                + "<td>" + transmittal_subject + "</td>"
                                + "<td>" + transmittal_duedate + "</td>"
                                + "<td>" + projectorganization_name + "</td>"
                                + "<td>" + sender + "</td></tr>"
                                + "</table><br/><br/>"
//                        update Fajar
                                + "Please see the transmittal in Inbox DSWF menu or click this <a href=\"" + idcserver + "?IdcService=PHE_GET_TRANSMITTAL_INBOX&fromEmail=1&dUser="+duser+"&dateType=&month=&year=&status=&category=&startRow=1&endRow=10&transmittalNo="+tempTransId+"\">link</a></body></html>";
//                        Log.info("kirim email ke " + demail.substring(0, demail.lastIndexOf(",")));
                        Log.info("kirim email ke " + demail);
                        Log.info(HtmlText);
//                        strTo = demail.substring(0, demail.lastIndexOf(","));
                        strTo = demail;
//                        ------- Fajar
                        strMessage = HtmlText;
//                            Log.info("send email berhasil\n\n");

                    } else if (tempTransId.trim().equalsIgnoreCase(transRS.getStringValue(0).trim())) {

//                        update Fajar
                        tempTransId = transRS.getStringValue(0);
                        transmittal_duedate = transRS.getStringValue(2);
                        transmittal_subject = transRS.getStringValue(1);
                        projectorganization_name = transRS.getStringValue(3);
                        sender = transRS.getStringValue(8).toString();
                        duser =  transRS.getStringValue(4);
                        demail = transRS.getStringValue(6);
                        
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: tempTransId:" + tempTransId);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transmittal_duedate:" + transmittal_duedate);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transmittal_subject:" + transmittal_subject);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: projectorganization_name:" + projectorganization_name);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: duser:" + duser);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: demail:" + demail);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transRS.getCurrentRow():" + transRS.getCurrentRow());
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transRS.getNumRows():" + transRS.getNumRows());
                        
                        HtmlText = "<html>\n"
                                + "<head></head>\n"
                                + "<body style=\"font-family:arial\">\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th><th><h3>Sender</h3></th></tr>\n"
                                + "<tr><td>" + tempTransId + "</td>"
                                + "<td>" + transmittal_subject + "</td>"
                                + "<td>" + transmittal_duedate + "</td>"
                                + "<td>" + projectorganization_name + "</td>"
                                + "<td>" + sender + "</td></tr>"
                                + "</table><br/><br/>"
                                + "Please see the transmittal in Inbox DSWF menu or click this <a href=\"" + idcserver + "?IdcService=PHE_GET_TRANSMITTAL_INBOX&fromEmail=1&dUser="+duser+"&dateType=&month=&year=&status=&category=&startRow=1&endRow=10&transmittalNo="+tempTransId+"\">link</a></body></html>";
                        
                        Log.info("kirim email ke " + demail);
                        Log.info(HtmlText);
                        strTo = demail;
                        strMessage = HtmlText;
                        
//                        demail = demail + transRS.getStringValue(6) + ",";
//                        Log.info("pheInternalTransmittal: dEmail:" + demail);
//                        Log.info("pheInternalTransmittal: dEmail:" + demail);

//                        HtmlText = "<html>\n"
//                                + "<head></head>\n"
//                                + "<body style=\"font-family:arial\">Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
//                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
//                                + "Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
//                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th></tr>\n"
//                                + "<tr><td>" + tempTransId + "</td>"
//                                + "<td>" + transmittal_subject + "</td>"
//                                + "<td>" + transmittal_duedate + "</td>"
//                                + "<td>" + projectorganization_name + "</td></tr>"
//                                + "</table><br/><br/>"
//                                + "Please see the transmittal in Inbox DSWF menu or click this<a href=\"" + idcserver + "?IdcService=PHE_GET_TRANSMITTAL_DETAIL&transmittalID=" + tempTransId + "\">link</a></body></html>";
//                        update Fajar
//                        Log.info("kirim email ke " + demail.substring(0, demail.lastIndexOf(",")));
//                        Log.info("kirim email ke " + demail);
//                        Log.info(HtmlText);
//                        strTo = demail.substring(0, demail.lastIndexOf(","));
//                        strTo = demail;
//                        strMessage = HtmlText;
//                            Log.info("send email berhasil\n\n");
//                        ------- Fajar
                    } else if (!tempTransId.trim().equalsIgnoreCase(transRS.getStringValue(0).trim())) {
//                      Log.info("pheReminderTransmittalInternal: strTo=" + strTo);
//                        if (strTo.length() > 0) {
//                            strSubject = projectorganization_name + ":" + transmittal_subject;
//                            pheSendMail(strTo, strMessage, strSubject, "");
//                            Log.info("send pheReminderTransmittalInternal successfully");
//                        }  
//                    System.out.println("send email berhasil\n\n");


//                    if (!tempTransId.trim().equalsIgnoreCase(transRS.getStringValue(0).trim())) {

                        tempTransId = transRS.getStringValue(0);
                        transmittal_duedate = transRS.getStringValue(2);
                        transmittal_subject = transRS.getStringValue(1);
                        projectorganization_name = transRS.getStringValue(3);
                        sender = transRS.getStringValue(8);
//                        update Fajar
                        duser =  transRS.getStringValue(4);
//                        demail = transRS.getStringValue(6) + ",";
                        demail = transRS.getStringValue(6);
//                        ------ Fajar

                        Log.info("pheReminderInternalTransmittalBeforeDueDate: tempTransId:" + tempTransId);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transmittal_duedate:" + transmittal_duedate);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transmittal_subject:" + transmittal_subject);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: projectorganization_name:" + projectorganization_name);
//                        update Fajar
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: duser:" + duser);
//                        ------ Fajar
                        
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: demail:" + demail);
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transRS.getCurrentRow():" + transRS.getCurrentRow());
                        Log.info("pheReminderInternalTransmittalBeforeDueDate: transRS.getNumRows():" + transRS.getNumRows());
//                       
                        HtmlText = "<html>\n"
                                + "<head></head>\n"
                                + "<body style=\"font-family:arial\">\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th><th><h3>Sender</h3></th></tr>\n"
                                + "<tr><td>" + tempTransId + "</td>"
                                + "<td>" + transmittal_subject + "</td>"
                                + "<td>" + transmittal_duedate + "</td>"
                                + "<td>" + projectorganization_name + "</td>"
                                + "<td>" + projectorganization_name + "</td></tr>"
                                + "</table><br/><br/>"
//                      update Fajar
                                + "Please see the transmittal in Inbox DSWF menu or click this <a href=\"" + idcserver + "?IdcService=PHE_GET_TRANSMITTAL_INBOX&fromEmail=1&dUser="+duser+"&dateType=&month=&year=&status=&category=&startRow=1&endRow=10&transmittalNo="+tempTransId+"\">link</a></body></html>";

//                        Log.info("kirim email ke " + demail.substring(0, demail.lastIndexOf(",")));
                        Log.info("kirim email ke " + demail);
                        Log.info(HtmlText);
//                        strTo = demail.substring(0, demail.lastIndexOf(","));
                        strTo = demail;
//                        ------- Fajar
                        strMessage = HtmlText;
                    }
//                    update Fajar
                    Log.info("pheReminderTransmittalInternal: strTo=" + strTo);
                    if (strTo.length() > 0) {
                        strSubject = "Reminder H"+transRS.getStringValue(7).toString()+" for :"+projectorganization_name + ":" + transmittal_subject;
                        pheSendMail(strTo, strMessage, strSubject, "");
                        Log.info("send pheReminderTransmittalInternal successfully");
                    }
//                    ------- Fajar
                } while (transRS.next());
            }

//            Log.info("pheReminderTransmittalInternal: strTo=" + strTo);
//            if (strTo.length() > 0) {
//                strSubject = projectorganization_name + ":" + transmittal_subject;
//                pheSendMail(strTo, strMessage, strSubject, "");
//                //   Log.info("send reminder overdue return to reqestor successfully");   
//            }
        } catch (DataException ex) {
            dValue.insertScheduleLog("REMINDER_INTERNAL_TRANSMITTAL_BEFORE_DUEDATE", "Failed", "PHE_CLICK_REMINDER_INTERNAL_TRANSMITTAL_BEFORE_DUEDATE");
            Log.error("pheReminderInternalTransmittalBeforeDueDate: DataException: getTicket:" + ex.getMessage());
        }

    }

    public void pheReminderExternalTransmittalDueDate() throws DataException {
        DataResultSet transRS = null;
        String strTo = "";
        String strSubject;
        String strMessage = "";

        String tempTransId = "";
        String transmittal_duedate = "";
        String transmittal_subject = "";
        String projectorganization_name = "";
        String demail = "";

        DatabaseValue dValue = new DatabaseValue();
        //add by nanda - 010814 - to get external dswf address
        String externalLink = dValue.getPheConfig("EXTERNAL_DSWF");
        String query = "SELECT DISTINCT DECODE(pm.transmittal_rnr_code,'Non Routine','Projects','Organization') linkexternal,\n"
                + " pm.transmittal_id,\n"
                + " pm.transmittal_subject,\n"
                + " TO_CHAR(pm.transmittal_duedate,'dd-Mon-YYYY') AS duedate,\n"
                + " pm.projectorganization_name,\n"
                + " pp.receiver,\n"
                + " u.dfullname,\n"
                + " u.demail\n"
                + " FROM phe_dswf_master pm,\n"
                + " phe_dswf_participant pp,\n"
                + " users u,\n"
                + " phe_dswf_doc pd\n"
                + " WHERE pm.transmittal_id            = pp.transmittal_id\n"
                + " AND pd.transmittal_id              = pm.transmittal_id\n"
                + " AND pm.transmittal_type            = 'External'\n"
                // edit by Nanda - 03092014
                //edit flag biar hanya terkirim ke reviewer yang belum submit (ga terkirim ke reviewer yang sudah submit) - sebelumnya asal masih in progress, email terkirim ke semua reviewer
                + "and pp.enddate is null\n"
                + " AND (TRUNC(pm.transmittal_duedate)                           = TRUNC(sysdate)\n"
                + " OR TRUNC(pm.transmittal_duedate-1)                           =TRUNC(sysdate)\n"
                + " OR TRUNC(pm.transmittal_duedate+1)                           =TRUNC(sysdate)\n"
                + " OR TRUNC(sysdate)-TRUNC(pm.transmittal_duedate)>1)\n"
                + " AND pm.transmittal_status = 'In Progress'\n"
                + " AND u.dname                        = pp.receiver\n"
                + " AND (pd.publish_status            <> '2'\n"
                + " OR pd.publish_status              IS NULL)\n"
                + " AND pd.doc_type                    = 'Supporting'\n"
                + " ORDER BY pm.transmittal_id ";
        Log.info("pheReminderExternalTransmittalDueDate: query:" + query);

        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        try {
            dValue.insertScheduleLog("REMINDER_EXTERNAL_TRANSMITTAL_DUEDATE", "Success", "PHE_CLICK_REMINDER_EXTERNAL_TRANSMITTAL_DUEDATE");
            ResultSet rs = ws.createResultSetSQL(query);
            transRS = new DataResultSet();
            transRS.copy(rs);

            Log.info("pheExternalTransmittal:transRS:" + transRS);
            Log.info("pheExternalTransmittal:transRSfirst()" + transRS.first());


            String HtmlText = "";
//            int lastRow = 0;
//            while (transRS.next()) {
//                lastRow++;
//            }
            if (transRS.first()) {

                do {

                    if (tempTransId.equals("")) {
                        tempTransId = transRS.getStringValue(1);
                        transmittal_duedate = transRS.getStringValue(3);
                        transmittal_subject = transRS.getStringValue(2);
                        projectorganization_name = transRS.getStringValue(4);
                        demail = transRS.getStringValue(7) + ",";

                        Log.info("pheExternalTransmittal: tempTransId:" + tempTransId);
                        Log.info("pheExternalTransmittal: transmittal_duedate:" + transmittal_duedate);
                        Log.info("pheExternalTransmittal: transmittal_subject:" + transmittal_subject);
                        Log.info("pheExternalTransmittal: projectorganization_name:" + projectorganization_name);
                        Log.info("pheExternalTransmittal: demail:" + demail);
                        Log.info("pheExternalTransmittal: transRS.getNumRows():" + transRS.getNumRows());

//                    if (lastRow == transRS.getNumRows()) {
                        HtmlText = "<html>\n"
                                + "<head></head>\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "<body style=\"font-family:arial\">Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th></tr>\n"
                                + "<tr><td>" + tempTransId + "</td>\n"
                                + "<td>" + transmittal_subject + "</td>"
                                + "<td>" + transmittal_duedate + "</td>"
                                + "<td>" + projectorganization_name + "</td>"
                                + "</table><br/><br/>"
                                + "Please see the transmittal in Inbox DSWF menu or click this <a href=\"" + externalLink + "?tPath=" + transRS.getStringValue(0) + "/" + projectorganization_name + "/" + tempTransId + "\">here</a> to open the transmittal.<br/> Thank you for your attention. </body></html>";
                        Log.info("kirim email ke " + demail.substring(0, demail.lastIndexOf(",")));
                        Log.info(HtmlText);
                        strTo = demail.substring(0, demail.lastIndexOf(","));
                        strMessage = HtmlText;
                    } else if (tempTransId.trim().equalsIgnoreCase(transRS.getStringValue(1).trim())) {

                        demail = demail + transRS.getStringValue(7) + ",";
                        Log.info("pheExternalTransmittal: dEmail:" + demail);

//                        HtmlText = "<html>\n"
//                                + "<head></head>\n"
//                                + "<body style=\"font-family:arial\">Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
//                                + "<table><tr><td>Transmittal No</td><td>:</td><td style=\"text-align:left\">" + tempTransId + "</td></tr>\n"
//                                + "<tr><td>Transmittal Subject</td><td>:</td><td>" + transmittal_subject + "</td></tr>"
//                                + "<tr><td>Due Date</td><td>:</td><td>" + transmittal_duedate + "</td></tr>"
//                                + "<tr><td>Project/Organization</td><td>:</td><td>" + projectorganization_name + "</td></tr>"
//                                + "</table><br/><br/>"
//                                + "Please see the transmittal in Inbox DSWF menu or click this <a href=\""+externalLink+"?tPath="+transRS.getStringValue(0)+"/"+projectorganization_name+"/"+tempTransId+"\">here</a> to open the transmittal.<br/> Thank you for your attention. </body></html>";
                        Log.info("kirim email ke " + demail.substring(0, demail.lastIndexOf(",")));
                        Log.info(HtmlText);
                        strTo = demail.substring(0, demail.lastIndexOf(","));
                        strMessage = HtmlText;
//                            Log.info("send email berhasil\n\n");

                    } else if (!tempTransId.trim().equalsIgnoreCase(transRS.getStringValue(1).trim())) {
                        Log.info("pheReminderTransmittalExternal: strTo=" + strTo);
                        if (strTo.length() > 0) {
                            strSubject = projectorganization_name + ":" + transmittal_subject;
                            pheSendMail(strTo, strMessage, strSubject, "");
                            //   Log.info("send reminder overdue return to reqestor successfully");   
                        }

                        tempTransId = transRS.getStringValue(1);
                        transmittal_duedate = transRS.getStringValue(3);
                        transmittal_subject = transRS.getStringValue(2);
                        projectorganization_name = transRS.getStringValue(4);
                        demail = transRS.getStringValue(7) + ",";

                        HtmlText = "<html>\n"
                                + "<head></head>\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "<body style=\"font-family:arial\">Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following transmittal has not been review and will be reached its due date. The details are:<br/><br/>\n"
                                + "<table><tr><th><h3>Transmittal No</h3></th><th><h3>Transmittal Subject</h3></th><th><h3>Due Date</h3></th><th><h3>Project/Organization</h3></th></tr>\n"
                                + "<tr><td>" + tempTransId + "</td>\n"
                                + "<td>" + transmittal_subject + "</td>"
                                + "<td>" + transmittal_duedate + "</td>"
                                + "<td>" + projectorganization_name + "</td>"
                                + "</table><br/><br/>"
                                + "Please see the transmittal in Inbox DSWF menu or click this <a href=\"" + externalLink + "?tPath=" + transRS.getStringValue(0) + "/" + projectorganization_name + "/" + tempTransId + "\">here</a> to open the transmittal.<br/> Thank you for your attention. </body></html>";
                        Log.info("kirim email ke " + demail.substring(0, demail.lastIndexOf(",")));
                        Log.info(HtmlText);
                        strTo = demail.substring(0, demail.lastIndexOf(","));
                        strMessage = HtmlText;
//                            
                    } else {
//                        Log.info("pheReminderTransmittalExternal: ga masuk");
                    }
//                    Log.info("pheReminderTransmittalExternal: at least nyampe siniih..");

                } while (transRS.next());
            }


            Log.info("pheReminderTransmittalExternal: strTo=" + strTo);
            if (strTo.length() > 0) {
                strSubject = projectorganization_name + ":" + transmittal_subject;
                pheSendMail(strTo, strMessage, strSubject, "");
                //   Log.info("send reminder overdue return to reqestor successfully");   
            }
        } catch (DataException ex) {
            dValue.insertScheduleLog("REMINDER_EXTERNAL_TRANSMITTAL_DUEDATE", "Failed", "PHE_CLICK_REMINDER_EXTERNAL_TRANSMITTAL_DUEDATE");
            Log.error("pheReminderTransmittalExternal: DataException: getTicket:" + ex.getMessage());
        }

    }

    public void pheReminderPublishInternalTransmittalDueDate() throws DataException {
        DataResultSet transRS = null;
        DataResultSet transDocRS = null;
        String strTo = "";
        String strSubject;
        String strMessage = "";

        DatabaseValue dValue = new DatabaseValue();
        String query = "select distinct pm.transmittal_id,pm.transmittal_duedate, pp.receiver,u.dfullname,u.demail from phe_dswf_master pm,phe_dswf_participant pp,users u,phe_dswf_doc pd where pm.transmittal_id = pp.transmittal_id and pd.transmittal_id= pm.transmittal_id and pm.transmittal_type = 'Internal' and pp.enddate is null and pm.transmittal_status = 'In Progress' and (trunc(pm.transmittal_duedate) = trunc(sysdate) or trunc(pm.transmittal_duedate-1)=trunc(sysdate) or trunc(pm.transmittal_duedate+1)=trunc(sysdate) or TRUNC(sysdate)-TRUNC(pm.transmittal_duedate)>1) and pp.enddate is null and u.dname = pp.receiver and pd.publish_status = 2";
        Log.info("pheReminderPublishInternalTransmittalDueDate: query:" + query);

        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        try {
            dValue.insertScheduleLog("REMINDER_PUBLISH_INTERNAL_TRANSMITTAL_DUEDATE", "Success", "PHE_CLICK_REMINDER_PUBLISH_INTERNAL_TRANSMITTAL_DUEDATE");
            ResultSet rs = ws.createResultSetSQL(query);
            transRS = new DataResultSet();
            transRS.copy(rs);

            Log.info("pheReminderPublishInternalTransmittalDueDate:transRS:" + transRS);

            Log.info("pheReminderPublishInternalTransmittalDueDate:transRSfirst()" + transRS.first());
            Log.info("pheReminderPublishInternalTransmittalDueDate:transRSfirst()" + transRS.next());

            if (transRS.first()) {

                do {
                    String query2 = "select pd.doc_number,pd.doc_title from phe_dswf_doc pd where pd.doc_type = 'Supporting' and pd.transmittal_id = '" + transRS.getStringValue(0) + "'";
                    Log.info("pheReminderPublishInternalTransmittalDueDate: query2:" + query2);
                    ResultSet rs2 = ws.createResultSetSQL(query2);
                    transDocRS = new DataResultSet();
                    transDocRS.copy(rs2);

                    Log.info("pheReminderPublishInternalTransmittalDueDate:transDocRS:" + transDocRS);

                    String DocString = "<table style=\"font-family:arial\">\n"
                            + "<tr style=\"background-color:#f0e68c\">\n"
                            + "<th><h3>Document No</h3></th>\n"
                            + "<th><h3>Document Title<h3></th>\n"
                            + "</tr>";
                    strTo = transRS.getStringValue(4);
                    Log.info("pheReminderPublishInternalTransmittalDueDate:transDocRSfirst()" + transDocRS.first());
                    if (!transDocRS.isEmpty()) {
                        do {
                            String a = transDocRS.getStringValue(0);
                            String b = transDocRS.getStringValue(1);
                            DocString = DocString + "<tr style=\"background-color:#dcdcdc\">"
                                    + "<td>" + a + "</td>"
                                    + "<td>" + b + "</td></tr>";
                        } while (transDocRS.next());


                        DocString = DocString + "</table>";

                        strMessage = "<html>\n"
                                + "<head></head>\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "<body style=\"font-family:arial\">Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following documents has not been sent to TDC:<br/><br/>\n"
                                + DocString + "<br/><br/>"
                                + "Please see click this <a href=\"http://kponwjap005.pheonwj.com:16300/urm/idcplg?IdcService=PHE_GET_TRANSMITTAL_DETAIL&transmittalID=" + transRS.getStringValue(0) + "\">link</a>"
                                + " to open the transmittal sheet and upload the native file into the system.<br/>Thank you for your attention.</body></html>";
//                    sendMail.SendEmailWithCC(rs.getString("demail"),"Reminder Request Native File - "+rs.getString("transmittal_id"),
//                                             HtmlText,tdcEmail);

                        Log.info("pheReminderTransmittalExternal: strTo=" + strTo);
                        if (strTo.length() > 0) {
                            strSubject = "Reminder Request Native File - " + transRS.getStringValue(0);
                            pheSendMail(strTo, strMessage, strSubject, "");
                            //   Log.info("send reminder overdue return to reqestor successfully");   
                        }
                    }
                } while (transRS.next());
            }

        } catch (DataException ex) {
            dValue.insertScheduleLog("REMINDER_PUBLISH_INTERNAL_TRANSMITTAL_DUEDATE", "Failed", "PHE_CLICK_REMINDER_PUBLISH_INTERNAL_TRANSMITTAL_DUEDATE");
            Log.error("pheReminderPublishInternalTransmittalDueDate: DataException:" + ex.getMessage());
        }

    }

    public void pheReminderPublishExternalTransmittalDueDate() throws DataException {
        DataResultSet transRS = null;
        DataResultSet transDocRS = null;
        String strTo = "";
        String strSubject;
        String strMessage = "";

        DatabaseValue dValue = new DatabaseValue();
        String query = "select distinct decode(pm.transmittal_rnr_code,'Non Routine','Projects','Organization') linkexternal,pm.transmittal_id, pm.transmittal_subject,pm.transmittal_duedate,pm.projectorganization_name,pp.receiver,u.dfullname,u.demail from phe_dswf_master pm,phe_dswf_participant pp,users u,phe_dswf_doc pd where pm.transmittal_id = pp.transmittal_id and pd.transmittal_id= pm.transmittal_id and pm.transmittal_type = 'External' and pp.enddate is null and pm.transmittal_status= 'In Progress' and (trunc(pm.transmittal_duedate) = trunc(sysdate) or trunc(pm.transmittal_duedate-1)=trunc(sysdate) or trunc(pm.transmittal_duedate+1)=trunc(sysdate) or TRUNC(sysdate)-TRUNC(pm.transmittal_duedate)>1) and pp.enddate is null and u.dname = pp.receiver and pd.publish_status = 2";
        Log.info("pheReminderPublishExternalTransmittalDueDate: query:" + query);
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        try {
            dValue.insertScheduleLog("REMINDER_PUBLISH_EXTERNAL_TRANSMITTAL_DUEDATE", "Success", "PHE_CLICK_REMINDER_PUBLISH_EXTERNAL_TRANSMITTAL_DUEDATE");
            ResultSet rs = ws.createResultSetSQL(query);
            transRS = new DataResultSet();
            transRS.copy(rs);

            Log.info("pheReminderPublishExternalTransmittalDueDate:transRSfirst()" + transRS.first());
            Log.info("pheReminderPublishExternalTransmittalDueDate:transRSfirst()" + transRS.next());
            if (transRS.first()) {
                do {
                    String query2 = "select pd.doc_number,pd.doc_title from phe_dswf_doc pd where pd.doc_type = 'Supporting' and pd.transmittal_id = '" + transRS.getStringValue(1) + "'";
                    Log.info("pheReminderPublishExternalTransmittalDueDate: query2:" + query2);
                    ResultSet rs2 = ws.createResultSetSQL(query2);
                    transDocRS = new DataResultSet();
                    transDocRS.copy(rs2);

                    String DocString = "<table style=\"font-family:arial\">\n"
                            + "<tr style=\"background-color:#f0e68c\">\n"
                            + "<th><h3>Document No</h3></td>\n"
                            + "<th><h3>Document Title<h3></td>\n"
                            + "</tr>";
                    strTo = transRS.getStringValue(7);
                    Log.info("pheReminderPublishExternalTransmittalDueDate:transDocRSfirst()" + transDocRS.first());
                    if (transDocRS.first()) {
                        do {
                            String a = transDocRS.getStringValue(0);
                            String b = transDocRS.getStringValue(1);
                            DocString = DocString + "<tr style=\"background-color:#dcdcdc\">"
                                    + "<td>" + a + "</td>"
                                    + "<td>" + b + "</td></tr>";
                        } while (transDocRS.next());


                        DocString = DocString + "</table>";

                        strMessage = "<html>\n"
                                + "<head></head>\n"
                                + "<style>table,th,td{border:1px solid black;}th{padding: 8px 10px ;background-color:#F0E68C;text-align:center;}td{padding: 8px 10px ; background-color:#DCDCDC; text-align:center;}p{font-size:1em;}</style>\n"
                                + "<body style=\"font-family:arial\">Dear Sir/Madam<br/><br/>This notification e-mail is sent to remind you that the following documents has not been sent to TDC:<br/><br/>\n"
                                + DocString + "<br/><br/>"
                                + "Please see click this <a href=\"http://kponwjap005.pheonwj.com:16300/urm/idcplg?IdcService=PHE_GET_TRANSMITTAL_DETAIL&transmittalID=" + transRS.getStringValue(0) + "\">link</a>"
                                + " to open the transmittal sheet and upload the native file into the system.<br/>Thank you for your attention.</body></html>";
//                    sendMail.SendEmailWithCC(rs.getString("demail"),"Reminder Request Native File - "+rs.getString("transmittal_id"),
//                                             HtmlText,tdcEmail);

                        Log.info("pheReminderTransmittalPublishExternal: strTo=" + strTo);
                        if (strTo.length() > 0) {
                            strSubject = "Reminder Request Native File - " + transRS.getStringValue(1);
                            pheSendMail(strTo, strMessage, strSubject, "");
                            //   Log.info("send reminder overdue return to reqestor successfully");   
                        }
                    }
                } while (transRS.next());
            }

        } catch (DataException ex) {
            dValue.insertScheduleLog("REMINDER_PUBLISH_EXTERNAL_TRANSMITTAL_DUEDATE", "Failed", "PHE_CLICK_REMINDER_PUBLISH_EXTERNAL_TRANSMITTAL_DUEDATE");
            Log.error("pheReminderPublishInternalTransmittalDueDate: DataException:" + ex.getMessage());
        }

    }

    public int doFilter(Workspace ws, DataBinder eventData,
            ExecutionContext cxt) throws DataException,
            ServiceException {

        // get the action, and be sure to only execute your code if the 'action'
        // matches the value for action in the 'CustomScheduledEvents' table
        String action = eventData.getLocal("action");
        Log.info("ScheduledEvt called successfully:action:" + action);
        // execute the daily event, or the hourly event
        if (action.equals("CustomDailyEvent")) {
            doCustomDailyEvent(ws, eventData, cxt);
            return FINISHED;
        } else if (action.equals("CustomHourlyEvent")) {
            doCustomHourlyEvent(ws, eventData, cxt);
            return FINISHED;
        }

        // Return CONTINUE so other filters have a chance at it.
        return CONTINUE;
    }

    /**
     * Execute the custom daily event
     *
     * @return an error string, or null if no error
     */
    protected void doCustomDailyEvent(Workspace ws, DataBinder eventData,
            ExecutionContext cxt) throws DataException,
            ServiceException {
        // you MUST perform at least one update
        update("CustomDailyEvent", "ScheduledEvt event starting...", ws);
        trace(" ScheduledEvt doing custom daily event... should be run around midnight");

        Log.info("ScheduledEvt doing custom daily event... should be run around midnight");

        DatabaseValue dValue = new DatabaseValue();
        String scheduleEmail = dValue.getPheConfig("SCHEDULE_EMAIL");
        Log.info("ScheduleHourly:" + scheduleEmail);
        if (scheduleEmail.equalsIgnoreCase("daily")) {
            UpdateOverDueStatus();
            pheReminderPreparing();////
            pheReminderReturnToTDC();////
            pheReminderReturnToRequestor();
            pheReminderReturnToRequestorHplus();
            pheReminderExpiredDocument();

            pheReminderInternalTransmittalDueDate();
            pheReminderExternalTransmittalDueDate();
            pheReminderPublishInternalTransmittalDueDate();
            pheReminderPublishExternalTransmittalDueDate();

            Expire expire = new Expire();
            expire.executeSetExpiredDocument();
        }


        update("CustomDailyEvent", "ScheduledEvt event finished successfully",
                ws);
    }

    /**
     * Execute the custom hourly event
     *
     * @return an error string, or null if no error
     */
    protected void doCustomHourlyEvent(Workspace ws, DataBinder eventData,
            ExecutionContext cxt) throws DataException,
            ServiceException {
        // you MUST perform at least one update
        update("CustomHourlyEvent", "ScheduledEvt event starting...", ws);

        trace("ScheduledEvt doing custom hourly event");
        Util util = new Util();
        

        DatabaseValue dValue = new DatabaseValue();
        String scheduleEmail = dValue.getPheConfig("SCHEDULE_EMAIL");
        String scheduleDelmarkup = dValue.getPheConfig("SCHEDULE_DELMARKUP");
        String scheduleDelInterval = dValue.getPheConfig("SCHEDULE_DELINTERVAL");

        //add by nanda - 220714 - ngasi flag untuk masing2 reminder agar bisa di config dr DB
        String latePreparation = dValue.getPheConfig("REMINDER_DLS_LATE_PREPARATION");
        String returnToTDC = dValue.getPheConfig("REMINDER_DLS_RETURN_TO_TDC");
        String returnToRequestor = dValue.getPheConfig("REMINDER_DLS_RETURN_TO_REQUESTOR");
        String returnToRequestorHPlus = dValue.getPheConfig("REMINDER_DLS_RETURN_TO_REQUESTOR_HPLUS");
        String reminderWillExpired = dValue.getPheConfig("REMINDER_WILLBE_EXPIRED_DOCUMENT");
        String reminderExpired = dValue.getPheConfig("REMINDER_EXECUTE_EXPIRED_DOCUMENT");
        String internalTransmittal = dValue.getPheConfig("REMINDER_INTERNAL_TRANSMITTAL_DUEDATE");
        String externalTransmittal = dValue.getPheConfig("REMINDER_EXTERNAL_TRANSMITTAL_DUEDATE");
        String publishInternalTransmittal = dValue.getPheConfig("REMINDER_PUBLISH_INTERNAL_TRANSMITTAL_DUEDATE");
        String publishExternalTransmittal = dValue.getPheConfig("REMINDER_PUBLISH_EXTERNAL_TRANSMITTAL_DUEDATE");
        
        //add by Haries: Registration reminder
        String registrationReminder = dValue.getPheConfig("REMINDER_REGISTRATION");
        
        Log.info("ScheduleHourly:" + scheduleEmail);
        Log.info("ScheduleLatePreparation:" + latePreparation);
        Log.info("ScheduleReturnToTDC:" + returnToTDC);
        Log.info("ScheduleReturnToRequestor:" + returnToRequestor);
        Log.info("ScheduleReturnToRequestorHPlus:" + returnToRequestorHPlus);
        Log.info("ScheduleReminderWillExpired:" + reminderWillExpired);
        Log.info("ScheduleReminderExpired:" + reminderExpired);
        Log.info("ScheduleInternalTransmittal:" + internalTransmittal);
        Log.info("ScheduleExternalTransmittal:" + externalTransmittal);
        Log.info("SchedulePublishInternalTransmittal:" + publishInternalTransmittal);
        Log.info("SchedulePublishExternalTransmittal:" + publishExternalTransmittal);
        Log.info("RegistrationReminder:" + registrationReminder);
        
        
        if(registrationReminder.equalsIgnoreCase("active")){
        //	pheReminderRegistration();
        //	pheReminderRegistrationAfter();
        }
        if (util.getCurrentHour().equalsIgnoreCase(scheduleEmail)) {////JLNNYA JM 2 PAGI AJA nanda
//          
            UpdateOverDueStatus();
            //add by nanda - 220714 - ngasi flag untuk masing2 reminder agar bisa di config dr DB    
            if (latePreparation.equalsIgnoreCase("active")) {
                pheReminderPreparing();
            }
            if (returnToTDC.equalsIgnoreCase("active")) {
                pheReminderReturnToTDC();
            }
            if (returnToRequestor.equalsIgnoreCase("active")) {
                pheReminderReturnToRequestor();
            }
            if (returnToRequestorHPlus.equalsIgnoreCase("active")) {
                pheReminderReturnToRequestorHplus();
            }
            if (reminderWillExpired.equalsIgnoreCase("active")) {
                pheReminderExpiredDocument();
            }
            if (internalTransmittal.equalsIgnoreCase("active")) {
                pheReminderInternalTransmittalDueDate();
            }
            if (externalTransmittal.equalsIgnoreCase("active")) {
                pheReminderExternalTransmittalDueDate();
            }
            if (publishInternalTransmittal.equalsIgnoreCase("active")) {
                pheReminderPublishInternalTransmittalDueDate();
            }
            if (publishExternalTransmittal.equalsIgnoreCase("active")) {
                pheReminderPublishExternalTransmittalDueDate();
            }
            if (reminderExpired.equalsIgnoreCase("active")) {
                Expire expire = new Expire();
                expire.executeSetExpiredDocument();
            }
            
            //add by Haries: Registration reminder
            if(registrationReminder.equalsIgnoreCase("active")){
            	pheReminderRegistration();
            	pheReminderRegistrationAfter();
            }
            pheDeleteDownloadTempZip();
            pheObsoleteScheduler();
            pheReminderInternalTransmittalBeforeDueDate();
            
            //nanda 080715 230715
        Log.info("ScheduleDel_dd:" +util.getCurrentDate());
//        Log.info("ScheduleDel_MM:" +util.getCurrentMonth());
        if (util.getCurrentDate().equalsIgnoreCase(scheduleDelmarkup)){
            pheDeleteMarkup(scheduleDelInterval);
        }

        }
//        }
        // event has finished!
        update("CustomHourlyEvent", "ScheduledEvt event finished successfully",
                ws);
    }

    public void pheReminderTransmittal() {
        //update by gin69 simpen data username
        UserAdmin uAdmin = new UserAdmin();
        ArrayList alUser = uAdmin.getUserAdmin();

        //set userlogin
        Util util = new Util();
        String userLogin = alUser.get(0).toString();

        DataBinder bin = new DataBinder();
        bin.putLocal("case", "1");
        bin.putLocal("IdcService", "PHE_CALL_EMAILJSP");
        DataCollection dc = new DataCollection();
        try {
            dc.executeService(bin, userLogin, true);
        } catch (DataException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        }

        DataBinder bin2 = new DataBinder();
        //edit by nanda - 110714 - edit nama binder bin menjadi bin2
        bin2.putLocal("case", "2");
        bin2.putLocal("IdcService", "PHE_CALL_EMAILJSP");
        try {
            dc.executeService(bin2, userLogin, true);
        } catch (DataException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        }

        DataBinder bin3 = new DataBinder();
        //edit by nanda - 110714 - edit nama binder bin menjadi bin3
        bin3.putLocal("case", "3");
        bin3.putLocal("IdcService", "PHE_CALL_EMAILJSP");
        try {
            dc.executeService(bin3, userLogin, true);
        } catch (DataException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        }

        DataBinder bin4 = new DataBinder();
        //edit by nanda - 110714 - sebelumnya case 3, menjadi case 4
        //edit by nanda - 110714 - edit nama binder bin menjadi bin4
        bin4.putLocal("case", "4");
        bin4.putLocal("IdcService", "PHE_CALL_EMAILJSP");
        try {
            dc.executeService(bin4, userLogin, true);
        } catch (DataException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Update the state of the event. Must be done at least once to tell the
     * content server when the scehduled event is finished.
     */
    protected void update(String action, String msg,
            Workspace workspace) throws ServiceException,
            DataException {
        long curTime = System.currentTimeMillis();
        ScheduledSystemEvents sse =
                IdcSystemLoader.getOrCreateScheduledSystemEvents(workspace);
        sse.updateEventState(action, msg, curTime);
    }

    /**
     * Log a trace message to the 'scheduledevents' section
     */
    protected void trace(String str) {
        SystemUtils.trace("scheduledevents", "- custom - " + str);
    }

    public static void pheSendMail(String to, String message, String subject, String cc) {
        DatabaseValue dValue = new DatabaseValue();
        String emailFrom = "";
        try {

            String usernameAuthenticator = dValue.getPheConfig("TDC_EMAIL_USERNAME");
            String passwordAuthenticator = dValue.getPheConfig("TDC_EMAIL_PASSWORD");
            String smtpHostName = dValue.getPheConfig("SMTP_EMAIL");
            
            emailFrom = dValue.getPheConfig("TDC_EMAIL");
            HtmlEmail email = new HtmlEmail();
            
//            email.setHostName("10.252.4.100");
//            email.setHostName("10.252.89.10");
            email.setHostName(smtpHostName);
            email.setSmtpPort(25);
            email.setAuthenticator(new DefaultAuthenticator(usernameAuthenticator, passwordAuthenticator));
            email.setTLS(false);
            //email.setSSLOnConnect(true);
            email.setFrom(emailFrom);
            email.setSubject(subject);
            email.setHtmlMsg(message);

            if (cc.length() > 0) {
            	String ccSplit[] = cc.split(",");
    			for (int i = 0; i < ccSplit.length; i++) {
    				if (ccSplit[i].length() > 0) {
    					email.addCc(ccSplit[i]);
    				}
    			}
            }


            email.setMsg(message);
            email.setTextMsg(message);
            String toSplit[] = to.split(",");
//            Log.info("PHE:--a:sendMail:to:" + to);
//            Log.info("PHE:--a:sendMail:toSplit.length:" + toSplit.length);

            for (int i = 0; i < toSplit.length; i++) {
                //added by nanda - 180614
                //cek dapat alamat email ato ngga
                if (toSplit[i].length() > 0) {
                    email.addTo(toSplit[i]);
                }
            }
            email.send();
            dValue.insertEmailLog(emailFrom, to, subject, "Success");
        } catch (Exception e) {
            Log.error("PHE:--a:sendMail:ex:" + e.getMessage());
            dValue.insertEmailLog(emailFrom, to, subject, "Failed");
            e.printStackTrace();
        }
    }
    
     

    /**
     * @param args
     * @throws SQLException
     */
    //static dbWrapper myDB = new dbWrapper();
    public static void main(String args[]) throws EmailException {

//        HtmlEmail email = new HtmlEmail();
//        email.setHostName("mail.pheonwj.pertamina.com");
//        email.setSmtpPort(587);
//        email.setAuthenticator(new DefaultAuthenticator("owc.support", "pheonwj2014"));
//        email.setTLS(false);
//        //email.setSSLOnConnect(true);
//        email.setFrom("owc.support@pheonwj.pertamina.com");
//        email.setSubject("SUBJEEEEEECT!!!!");
//        email.setHtmlMsg("<p style='font-size:3em'>CIUNG WANARA</p>");
//
//        email.setMsg("CIUNGWANARA");
//        email.setTextMsg("CIUNG WANARA");
////        email.addTo("bijakas@gmail.com");
////        email.addTo("davidtohea88@gmail.com");
////        email.addTo("sumantry.ginanjarseptian@gmail.com");
//        InternetAddress iAddress = new InternetAddress();
//        iAddress.setAddress("sumantry.ginanjarseptian@gmail.com");
//
//        ArrayList ia = new ArrayList();
//        ia.add(iAddress);
//        email.setTo(ia);
//        email.send();
        String path = "X:\\\\To Be Upload Documents\\\\Non Drawings\\Testing Development Server\\Performance Test\\";
        File f = new File(path);
        System.out.println("f.getAbsolutePath:" + f.getAbsolutePath()); //  

        File files[] = f.listFiles();
            for (int index = 0; index < files.length; index++) {

               System.out.println("list"+files[index].toString());
//                Log.info("Delete temporary zipdownload" + wasDeleted);

            }

        System.out.println("f.exists():" + f.exists());
    }

    public void pheObsoleteScheduler(){
       DataResultSet transRS = null;
       DataResultSet transDocRS = null;
       String strTo = "";
       String strSubject;
       String strMessage = "";
       ArrayList alUser = new ArrayList();
        
       DatabaseValue dValue = new DatabaseValue();
       Obsolete obs = new Obsolete();
       String query = "select revisions.dID,dDocName,dDocType,xdocnumber,ddoctitle,xrevision,xretentiondate,xauthor,xdocname from docmeta, revisions where docmeta.did=revisions.did and xdocpurpose='Published' and xstatus='Expired' and ddoctype like '%Drawing' and drevrank=0 and sysdate > (xretentiondate + interval '2' year)";
       Log.info("pheObsoleteScheduler: query:" + query);
       
       DataCollection dColl = new DataCollection();
       Workspace ws = dColl.getSystemWorkspace();
       try{
           dValue.updateScheduleLog("OBSOLETE", "Success");
            ResultSet rs = ws.createResultSetSQL(query);
            transRS = new DataResultSet();
            transRS.copy(rs);

            Log.info("pheObsoleteScheduler:transRSfirst()" + transRS.first());
            Log.info("pheObsoleteScheduler:transRSfirst()" + transRS.next());
            Log.info("PHE:pheReminderExpiredDocument:resReturn.getNumRows():" + transRS.getNumRows());
            
            strSubject = "[PHEONWJ]Obsolete Document Notification";
                     strMessage = "<span style='font-family:Arial'><p>Dear Sir/Madam, <br><br>"
                        + "This notification e-mail is sent to inform you that <b>Your Document(s)</b> already <b>Obsolete</b> : ";

                strMessage = strMessage + "<table border='1' style='width:100%'>";
                strMessage = strMessage + "<tr>";
                strMessage = strMessage + "<td style='padding: 8px 10px; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Document No</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Document Title</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Rev. No.</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Expired Date</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C'>";
                strMessage = strMessage + "<h3>Document Owner</h3>";
                strMessage = strMessage + "</td>";
                
             for (int i = 0; i < transRS.getNumRows(); i++) {
                 strMessage = strMessage + "<tr>";
                    List list = transRS.getRowAsList(i);

                    String backgroundColor = "#DCDCDC";
                    String color = "black";

                    String columnDetailStart = "<td style='background-color:backgroundColor; color:colorText; padding: 8px 10px ;'>";
                    String columnDetailEnd = "</td>";
                    //document No
                    strMessage = strMessage + columnDetailStart;
                    strMessage = strMessage + list.get(3).toString();
                    strMessage = strMessage + columnDetailEnd;

                    //document Title
                    strMessage = strMessage + columnDetailStart;
                    strMessage = strMessage + list.get(4).toString();
                    strMessage = strMessage + columnDetailEnd;

                    //revisi
                    strMessage = strMessage + columnDetailStart;
                    strMessage = strMessage + list.get(5).toString();
                    strMessage = strMessage + columnDetailEnd;

                    //retention_date
                    strMessage = strMessage + columnDetailStart;
                    strMessage = strMessage + list.get(6).toString();
                    strMessage = strMessage + columnDetailEnd;

                    //document owner
                    strMessage = strMessage + columnDetailStart;
                    String author = list.get(7).toString();
                    Log.info("PHE:obsoleteScheduler:author:" + author);
                    String getNameCollection[] = author.split(",");

                    String allEmail = "";
                    for (int l = 0; l < getNameCollection.length; l++) {
                        String email = dValue.getEmailFromName(getNameCollection[l]);

                        try {
                            if (email.length() > 0) {
                                if (l + 1 == getNameCollection.length) {
                                    allEmail = allEmail + email;
                                } else {
                                    allEmail = allEmail + email + ", ";
                                }
                            }
                        } catch (Exception ex) {
                            allEmail = allEmail + "owc.support@pertamina.com";
                        }
                    }
                    Log.info("--allEmail.length():" + allEmail.length());
                    Log.info("--allEmail:" + allEmail);
                    if (allEmail.length() > 0) {
                        strMessage = strMessage.replaceAll("backgroundColor", "#DCDCDC");
                        strMessage = strMessage.replaceAll("colorText", "black");
                    } else {
                        strMessage = strMessage.replaceAll("backgroundColor", "red");
                        strMessage = strMessage.replaceAll("colorText", "white");
                    }
                    strMessage = strMessage + author;
                    strMessage = strMessage + columnDetailEnd;


                    strMessage = strMessage + "</tr>";
                    strTo=allEmail;
                    
                    obs.executeObsoleteDocumentScheduler(list.get(0).toString(), list.get(1).toString(), list.get(2).toString());
                    
                     strMessage = strMessage + "</table>";
                strMessage = strMessage + "<br>";
                strMessage = strMessage + "<p><b>Notes: </b>";
                strMessage = strMessage + "<br><b>Red Background : </b> Document Owner(s) was not found or Document Owner(s) does not have e-mail address. </p>";
                strMessage = strMessage + "<p>Please revalidate the Document(s) based on required conditions: <br>"
                        + "<br>1. Extension/ Prolongation without changes"
                        + "<br>2. Extension / Prolongation with revision changes"
                        + "<br>3. Outdated/ No longer need (Obsolete)"
                        + "<br>4. Replace with another document</p>";
                strMessage = strMessage + "<p>After due date, the expired document will no longer available to be viewed in EDMS.</p>";
                strMessage = strMessage + "<p>If you need any assistance how to revalidate, please contact Technical Document Control <br>"
                        + "onwj.tdc@pheonwj.pertamina.com or dial 3132 for the extension number. </p>";
                strMessage = strMessage + "<p>Thank you for your attention.<br></p>";
                strMessage = strMessage + "<p><b>Technical Document Control</b><br></p></span>";
                String cc = "";   
//                try {
//                        cc = dValue.getPheConfig("TDC_EMAIL_CC");
//                    } catch (Exception ex) {
//                        cc = "";
//                    }
                    //cc diapus
                    pheSendMail(strTo, strMessage, strSubject, cc); 
             }
            
                
                
       } catch(Exception e){
           
       }
    }
    
    public void pheDeleteMarkup(String interval){
        DeleteMarkup delmarkup = new DeleteMarkup();
        try {
            String result = delmarkup.updateMarkupDocInfoBulk(interval);
            Log.info("pheDeleteMarkup:result:"+result);
            dValue.insertScheduleLog("BATCH_DELETE_MARKUP", "Success", "PHE_CLICK_SCHEDULE_BATCH_DELETE_MARKUP");
        } catch (DataException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServiceException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void pheDeleteDownloadTempZip() {

        String path = "D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\app\\examples.war\\download";
        
        try {
            File f = new File(path);


            File files[] = f.listFiles();
            for (int index = 0; index < files.length; index++) {

                boolean wasDeleted = files[index].delete();
                Log.info("Delete temporary zipdownload" + wasDeleted);

            }

        } catch (Exception e) {
        }
    }

    public void pheReminderExpiredDocument2(String listMessage, String listAuthor) {
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        DatabaseValue dValue = new DatabaseValue();

        String emailSplit[];

        String strTo = "";
        String strSubject = "";
        String strMessage = "";
        boolean isDistinct = false;
        int i, j;

        if (listAuthor.length() > 0) {
            emailSplit = listAuthor.split(",");

            if (emailSplit.length > 0) {
                for (i = 0; i < emailSplit.length; i++) {
                    Log.info("PHE:emailSplit[" + i + "].trim():" + emailSplit[i].trim());
                    for (j = 0; j < i; j++) {
                        Log.info("PHE:emailSplit[" + j + "].trim()" + emailSplit[j].trim());
                        if (j == emailSplit.length - 1) {
                            isDistinct = true;
                            break;
                        }
                        if (emailSplit[i].trim().equalsIgnoreCase(emailSplit[j].trim())) {
                            isDistinct = true;
                            break;
                        }
                    }
                    if (!isDistinct) {
//                        strTo = strTo + emailSplit[i] + ",";

                        String query = "select distinct dEmail from users where (UPPER(dName) = UPPER('" + emailSplit[j].trim() + "') or UPPER(dFullName) = UPPER('" + emailSplit[j].trim() + "'))";

                        ResultSet rsreturn = null;
                        try {
                            rsreturn = ws.createResultSetSQL(query);

                            if (!rsreturn.isEmpty()) {
                                rsreturn.first();

                                strTo = strTo + rsreturn.getStringValue(0) + ",";


                            }
                        } catch (DataException ex) {
                            Log.error("PHE:pheReminderExpiredDocument:DataException:" + ex.getMessage());
                        }
                    }
                }
//                 strTo = strTo + emailSplit[emailSplit.length-1];
                Log.info("PHE:ExpiredDocument:strTo:" + strTo);
            }
//             Log.info("PHE:ExpiredDocument:strTo belum dipottong:" + strTo);

        }

        if (strTo.length() > 0) {
            strSubject = "[PHEONWJ] Expired Document Alert";


            try {
                strMessage = "<span style='font-family:Arial'><p>Dear Sir/Madam, <br><br>"
                        + "This notification e-mail is sent to remind you that <b>Your Document(s)</b> has been <b>Expired</b> : ";

                strMessage = strMessage + "<table border='1' style='width:100%'>";
                strMessage = strMessage + "<tr>";
                strMessage = strMessage + "<td style='padding: 8px 10px; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Document No</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Document Title</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Rev. No.</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Expired Date</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C'>";
                strMessage = strMessage + "<h3>Document Owner</h3>";
                strMessage = strMessage + "</td>";


//                strMessage = strMessage + "<tr>";

                String backgroundColor = "#DCDCDC";
                String color = "black";

                strMessage = strMessage + listMessage;


//                strMessage = strMessage + "</tr>";

                strMessage = strMessage + "</table>";
                strMessage = strMessage + "<br>";
                strMessage = strMessage + "<p><b>Notes: </b>";
                strMessage = strMessage + "<br><b>Red Background : </b> Document Owner(s) was not found or Document Owner(s) does not have e-mail address. </p>";
                strMessage = strMessage + "<p>Please revalidate the Document(s) based on required conditions: <br>"
                        + "<br>1. Extension/ Prolongation without changes"
                        + "<br>2. Extension / Prolongation with revision changes"
                        + "<br>3. Outdated/ No longer need (Obsolete)"
                        + "<br>4. Replace with another document</p>";
                strMessage = strMessage + "<p>After due date, the expired document will no longer available to be viewed in EDMS.</p>";
                strMessage = strMessage + "<p>If you need any assistance how to revalidate, please contact Technical Document Control <br>"
                        + "onwj.tdc@pheonwj.pertamina.com or dial 3132 for the extension number. </p>";
                strMessage = strMessage + "<p>Thank you for your attention.<br></p>";
                strMessage = strMessage + "<p><b>Technical Document Control</b><br></p></span>";


                Log.info("PHE:ExpiredDocument:strMessage:" + strMessage);

                if (strTo.length() > 0) {
                    if (strTo.lastIndexOf(",") == strTo.length() - 1) {
//                        Log.info("--strToDalemIfSblmSamadengan:" + strTo);
                        strTo = strTo.substring(0, strTo.length() - 1);
                    }
                    Log.info("PHE:ExpiredDocument:strTo sudah dipotong:" + strTo);
                }

                //edited by nanda 180614
                //ambil alamat email cc dari DB
                String cc = "";
                try {
                    cc = dValue.getPheConfig("TDC_EMAIL_CC");
                } catch (Exception ex) {
                    cc = "";
                }
                //cc diapus
                pheSendMail(strTo, strMessage, strSubject, cc);
//                pheSendMail(strTo, strMessage, strSubject, "");
            } catch (Exception ex) {
                Log.error("PHE:pheReminderExpiredDocument:DataException:" + ex.getMessage());
            }
        }
    }

    public void pheReminderExpiredDocument(String dID) {
        Document doc = new Document();
        DataBinder binder = new DataBinder();
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();

        //update by gin69 simpen data username
        UserAdmin uAdmin = new UserAdmin();
        ArrayList alUser = uAdmin.getUserAdmin();

        //set userlogin
        Util util = new Util();
        util.setUserLogin(alUser.get(0).toString());
        ResultSet docRS;

        docRS = doc.GetDocInfo(dID, binder, ws, false, util);
//        Log.info("PHE:--a:xAuthor:" + docRS.getStringValueByName("xAuthor"));
        String emailCompilation = docRS.getStringValueByName("xAuthor");

//        Log.info("PHE:--a:emailCompilation:" + emailCompilation);
        String emailSplit[];

        String strTo = "";
        if (emailCompilation.length() > 0) {
            emailSplit = emailCompilation.split(",");

            if (emailSplit.length > 0) {
                for (int i = 0; i < emailSplit.length; i++) {
                    //edit by nanda - 180614
                    //add trim() to delete whitespace in split
                    String query = "select distinct dEmail from users where (UPPER(dName) = UPPER('" + emailSplit[i].trim() + "') or UPPER(dFullName) = UPPER('" + emailSplit[i].trim() + "'))";

                    ResultSet rsreturn = null;
                    try {
                        rsreturn = ws.createResultSetSQL(query);

                        if (!rsreturn.isEmpty()) {
                            rsreturn.first();

                            if (i == emailSplit.length - 1) {
                                strTo = strTo + rsreturn.getStringValue(0);
                            } else {
                                strTo = strTo + rsreturn.getStringValue(0) + ",";
                            }

                        }
                    } catch (DataException ex) {
                        Log.error("PHE:pheReminderExpiredDocument:DataException:" + ex.getMessage());
                    }
                }

            }

        }

        //update by gin69
        String revisionExpired = docRS.getStringValueByName("xRevision");
        String documentNoExpired = docRS.getStringValueByName("xDocNumber");
        String expiredDateExpired = docRS.getStringValueByName("xRetentionDate");

        String strSubject = "";
        String strMessage = "";

        DatabaseValue dValue = new DatabaseValue();

        //Log.info("PHE:--a:strTo.length():" + strTo.length());
        if (strTo.length() > 0) {
            strSubject = "[PHEONWJ] Expired Document Alert";


            try {
                strMessage = "<span style='font-family:Arial'><p>Dear Sir/Madam, <br><br>"
                        + "This notification e-mail is sent to remind you that <b>Your Document(s)</b> has been <b>Expired</b> : ";

                strMessage = strMessage + "<table border='0' style='width:100%'>";
                strMessage = strMessage + "<tr>";
                strMessage = strMessage + "<td style='padding: 8px 10px; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Document No</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Document Title</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Rev. No.</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Expired Date</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C'>";
                strMessage = strMessage + "<h3>Document Owner</h3>";
                strMessage = strMessage + "</td>";


                strMessage = strMessage + "<tr>";

                String backgroundColor = "#DCDCDC";
                String color = "black";

                String columnDetailStart = "<td style='background-color:backgroundColor; color:colorText; padding: 8px 10px ;'>";
                String columnDetailEnd = "</td>";

                //document No
                strMessage = strMessage + columnDetailStart;
                strMessage = strMessage + documentNoExpired;
                strMessage = strMessage + columnDetailEnd;

                //document Title
                strMessage = strMessage + columnDetailStart;
                strMessage = strMessage + docRS.getStringValueByName("dDocTitle");
                strMessage = strMessage + columnDetailEnd;

                //revisi
                strMessage = strMessage + columnDetailStart;
                strMessage = strMessage + revisionExpired;
                strMessage = strMessage + columnDetailEnd;

                //retention_date
                strMessage = strMessage + columnDetailStart;
                strMessage = strMessage + expiredDateExpired;
                strMessage = strMessage + columnDetailEnd;

                //document owner
                strMessage = strMessage + columnDetailStart;
                String author = docRS.getStringValueByName("xAuthor");
                //            Log.info("PHE:author:" + author);
                String getNameCollection[] = author.split(",");

                String allFullName = "";
                for (int l = 0; l < getNameCollection.length; l++) {
                    String fullName = dValue.getFullNameByName(getNameCollection[l]);
                    //                    Log.info("--fullName:" + fullName);
                    try {
                        if (fullName.length() > 0) {
                            if (l + 1 == getNameCollection.length) {
                                allFullName = allFullName + fullName;
                            } else {
                                allFullName = allFullName + fullName + ", ";
                            }
                        }
                    } catch (Exception ex) {
                        allFullName = allFullName + "";
                    }
                }

                //                Log.info("--allFullName.ength():" + allFullName.length());
                if (allFullName.length() > 0) {
                    strMessage = strMessage.replaceAll("backgroundColor", "#DCDCDC");
                    strMessage = strMessage.replaceAll("colorText", "black");
                } else {
                    strMessage = strMessage.replaceAll("backgroundColor", "red");
                    strMessage = strMessage.replaceAll("colorText", "white");
                }
                strMessage = strMessage + allFullName;
                strMessage = strMessage + columnDetailEnd;


                strMessage = strMessage + "</tr>";

                strMessage = strMessage + "</table>";
                strMessage = strMessage + "<br>";
                strMessage = strMessage + "<p><b>Notes: </b>";
                strMessage = strMessage + "<br><b>Red Background : </b> Document Owner(s) was not found or Document Owner(s) does not have e-mail address. </p>";
                strMessage = strMessage + "<p>Please revalidate the Document(s) based on required conditions: <br>"
                        + "<br>1. Extension/ Prolongation without changes"
                        + "<br>2. Extension / Prolongation with revision changes"
                        + "<br>3. Outdated/ No longer need (Obsolete)"
                        + "<br>4. Replace with another document</p>";
                strMessage = strMessage + "<p>After due date, the expired document will no longer available to be viewed in EDMS.</p>";
                strMessage = strMessage + "<p>If you need any assistance how to revalidate, please contact Technical Document Control <br>"
                        + "onwj.tdc@pheonwj.pertamina.com or dial 3132 for the extension number. </p>";
                strMessage = strMessage + "<p>Thank you for your attention.<br></p>";
                strMessage = strMessage + "<p><b>Technical Document Control</b><br></p></span>";


                Log.info("PHE:ExpiredDocument:strMessage:" + strMessage);

                //edited by nanda 180614
                //ambil alamat email cc dari DB
                String cc = "";
                try {
                    cc = dValue.getPheConfig("TDC_EMAIL_CC");
                } catch (Exception ex) {
                    cc = "";
                }
                //cc diapus
                pheSendMail(strTo, strMessage, strSubject, cc);
//                pheSendMail(strTo, strMessage, strSubject, "");
            } catch (DataException ex) {
            }
        }

    }

    public void pheReminderExpiredDocument() throws DataException {
        DataResultSet resReturn = null;
        DatabaseValue dValue = new DatabaseValue();
        String scheduleReminder = dValue.getPheConfig("SCHEDULE_REMINDER_WILL_EXPIRED");
        dValue.insertScheduleLog("WILL_BE_EXPIRED_DOCUMENT", "Success", "PHE_CLICK_SCHEDULE_WILL_BE_EXPIRED");
        try {
            if (scheduleReminder.length() == 0) {
                scheduleReminder = "30,60,90";
            }
        } catch (Exception ex) {
            scheduleReminder = "30,60,90";
        }
        try {
            String queryreturn = "SELECT\n"
                    + " XRETENTIONDATE,\n"
                    + " DOCMETA.DID AS DID,\n"
                    + " ROUND(TO_DATE(TO_CHAR(xretentiondate, 'DD-MON-YY'),'DD-MON-YY')- SYSDATE) AS SISADATE,\n"
                    + " XDOCNUMBER,\n"
                    + " XREVISION,\n"
                    + " TO_CHAR(XRETENTIONDATE, 'DD-MON-YY') AS EXPIREDDATE,\n"
                    + " XAUTHOR,\n"
                    + " DDOCNAME,\n"
                    + " DDOCTITLE\n"
                    + " FROM DOCMETA, \n"
                    + " REVISIONS\n"
                    + " WHERE DOCMETA.DID = REVISIONS.DID AND UPPER(xdocpurpose)<>UPPER('Native')\n"
                    + " AND drevlabel = (SELECT MAX(drevlabel) FROM revisions ri,docmeta di WHERE ri.did = di.did AND ri.ddocname = REVISIONS.ddocname GROUP BY ddocname)"
                    + " AND ROUND(TO_DATE(TO_CHAR(xretentiondate, 'DD-MON-YY'),'DD-MON-YY')- SYSDATE) IN (" + scheduleReminder + " )";


            Log.info("PHE:pheReminderExpiredDocument:query return:" + queryreturn);
            DataCollection dColl = new DataCollection();
            Workspace ws = dColl.getSystemWorkspace();
            //ws.executeSQL(query);
            ResultSet rsreturn = ws.createResultSetSQL(queryreturn);
            resReturn = new DataResultSet();
            resReturn.copy(rsreturn);
//        Log.info("PHE:pheReminderExpiredDocument:rsReturn.isEmpty:" + rsreturn.isEmpty());
//        Log.info("PHE:pheReminderExpiredDocument:resReturn.getNumRows():" + resReturn.getNumRows());

            //update by nanda - 020714 - cek apa resultset ada isinya
            if (!rsreturn.isEmpty()) {
                ArrayList alUser = new ArrayList();

                String strListMessage = "";
                String requestName = "";
                String strTo = "";
                String strSubject = "[PHEONWJ]Document Expiry Reminder";
                String strMessage = "<span style='font-family:Arial'><p>Dear Sir/Madam, <br><br>"
                        + "This notification e-mail is sent to remind you that <b>Your Document(s)</b> will be <b>Expired</b> : ";

                strMessage = strMessage + "<table border='1' style='width:100%'>";
                strMessage = strMessage + "<tr>";
                strMessage = strMessage + "<td style='padding: 8px 10px; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Document No</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Document Title</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Rev. No.</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
                strMessage = strMessage + "<h3>Expired Date</h3>";
                strMessage = strMessage + "</td>";

                strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C'>";
                strMessage = strMessage + "<h3>Document Owner</h3>";
                strMessage = strMessage + "</td>";

                boolean sendEmail = false;
                for (int i = 0; i < resReturn.getNumRows(); i++) {
                    sendEmail = true;
                    strMessage = strMessage + "<tr>";
                    List list = resReturn.getRowAsList(i);

                    String backgroundColor = "#DCDCDC";
                    String color = "black";

                    String columnDetailStart = "<td style='background-color:backgroundColor; color:colorText; padding: 8px 10px ;'>";
                    String columnDetailEnd = "</td>";

                    //document No
                    strMessage = strMessage + columnDetailStart;
                    strMessage = strMessage + list.get(3).toString();
                    strMessage = strMessage + columnDetailEnd;

                    //document Title
                    strMessage = strMessage + columnDetailStart;
                    strMessage = strMessage + list.get(8).toString();
                    strMessage = strMessage + columnDetailEnd;

                    //revisi
                    strMessage = strMessage + columnDetailStart;
                    strMessage = strMessage + list.get(4).toString();
                    strMessage = strMessage + columnDetailEnd;

                    //retention_date
                    strMessage = strMessage + columnDetailStart;
                    strMessage = strMessage + list.get(5).toString();
                    strMessage = strMessage + columnDetailEnd;

                    //document owner
                    strMessage = strMessage + columnDetailStart;
                    String author = list.get(6).toString();
//            Log.info("PHE:author:" + author);
                    String getNameCollection[] = author.split(",");

                    String allFullName = "";
                    for (int l = 0; l < getNameCollection.length; l++) {
                        String fullName = dValue.getFullNameByName(getNameCollection[l]);
//                    Log.info("--fullName:" + fullName);
                        try {
                            if (fullName.length() > 0) {
                                if (l + 1 == getNameCollection.length) {
                                    allFullName = allFullName + fullName;
                                } else {
                                    allFullName = allFullName + fullName + ", ";
                                }
                            }
                        } catch (Exception ex) {
                            allFullName = allFullName + "";
                        }
                    }

//                Log.info("--allFullName.ength():" + allFullName.length());
                    if (allFullName.length() > 0) {
                        strMessage = strMessage.replaceAll("backgroundColor", "#DCDCDC");
                        strMessage = strMessage.replaceAll("colorText", "black");
                    } else {
                        strMessage = strMessage.replaceAll("backgroundColor", "red");
                        strMessage = strMessage.replaceAll("colorText", "white");
                    }
                    strMessage = strMessage + allFullName;
                    strMessage = strMessage + columnDetailEnd;


                    strMessage = strMessage + "</tr>";

                    renderToExpiredDocument(alUser, list.get(6).toString(), dValue);
                }
                strMessage = strMessage + "</table>";
                strMessage = strMessage + "<br>";
                strMessage = strMessage + "<p><b>Notes: </b>";
                strMessage = strMessage + "<br><b>Red Background : </b> Document Owner(s) was not found or Document Owner(s) does not have e-mail address. </p>";
                strMessage = strMessage + "<p>Please revalidate the Document(s) based on required conditions: <br>"
                        + "<br>1. Extension/ Prolongation without changes"
                        + "<br>2. Extension / Prolongation with revision changes"
                        + "<br>3. Outdated/ No longer need (Obsolete)"
                        + "<br>4. Replace with another document</p>";
                strMessage = strMessage + "<p>After due date, the expired document will no longer available to be viewed in EDMS.</p>";
                strMessage = strMessage + "<p>If you need any assistance how to revalidate, please contact Technical Document Control <br>"
                        + "onwj.tdc@pheonwj.pertamina.com or dial 3132 for the extension number. </p>";
                strMessage = strMessage + "<p>Thank you for your attention.<br></p>";
                strMessage = strMessage + "<p><b>Technical Document Control</b><br></p></span>";

//            Log.info("--alUser.size():" + alUser.size());
                for (int i = 0; i < alUser.size(); i++) {
//                Log.info("--i:" + i);
                    if (i + 1 == alUser.size()) {
//                    Log.info("--if:i:" + i);
                        strTo = strTo + alUser.get(i).toString();
                    } else {
//                    Log.info("--else:i:" + i);
                        strTo = strTo + alUser.get(i).toString() + ",";
                    }
                }
//            Log.info("--alUser.afterFor:");
                if (sendEmail) {
//                Log.info("--strTo:" + strTo);

                    if (strTo.length() > 0) {
                        if (strTo.lastIndexOf(",") == strTo.length() - 1) {
//                        Log.info("--strToDalemIfSblmSamadengan:" + strTo);
                            strTo = strTo.substring(0, strTo.length() - 1);
//                        Log.info("--strToDalemIfStlhSamadengan:" + strTo);
                        }
                    }
//            Log.info("PHE:reminderExpired:strTo inside getnumrows:" + strTo);

                    //edited by nanda 180614
                    //ambil alamat email cc dari DB
                    String cc = "";
                    try {
                        cc = dValue.getPheConfig("TDC_EMAIL_CC");
                    } catch (Exception ex) {
                        cc = "";
                    }
                    //cc diapus
                    pheSendMail(strTo, strMessage, strSubject, cc);
//                pheSendMail(strTo, strMessage, strSubject, "");
                }//Log ngejalanin scheduler 
            }



        } catch (Exception ex) {
            Log.error("WILL_BE_EXPIRED_DOCUMENT:ex:" + ex.getMessage());
            dValue.insertScheduleLog("WILL_BE_EXPIRED_DOCUMENT", "Failed", "PHE_CLICK_SCHEDULE_WILL_BE_EXPIRED");
        }
    }

    public void renderToExpiredDocument(ArrayList alUser, String getTempAuthor, DatabaseValue dValue) {
        //validasi buat To di emailnya...
        String tempAuthor = getTempAuthor;

        String email = "";
        //buat ambil ada koma koma nya.. ato orang nya banyak..
        String tempAuthorList[] = null;
        try {
            tempAuthorList = tempAuthor.split(",");
        } catch (Exception ex) {
        }
        //cek looping..
//        Log.info("--alUser.size:" + alUser.size());
        if (alUser.size() > 0) {
            boolean doesnthave = true;
//            Log.info("--tempAuthorList:" + tempAuthorList);
            if (tempAuthorList.length > 0) {
//                Log.info("--tempAuthorList.length():" + tempAuthorList.length);
                for (int k = 0; k < tempAuthorList.length; k++) {

                    try {
                        email = dValue.getEmailFromName(tempAuthorList[k]);
                    } catch (Exception ex) {
                    }
                    for (int j = 0; j < alUser.size(); j++) {
                        if (alUser.get(j).toString().equalsIgnoreCase(email)) {
                            doesnthave = false;
                        }
                    }

                    if (doesnthave) {
                        alUser.add(email);
                    }
                }
            }

        } else {
//            Log.info("--else:tempAuthorList:" + tempAuthorList);
//            Log.info("--else:tempAuthorList.length:" + tempAuthorList.length);
            if (tempAuthorList.length > 0) {
                for (int k = 0; k < tempAuthorList.length; k++) {
                    try {
                        email = dValue.getEmailFromName(tempAuthorList[k]);
                    } catch (DataException ex) {
                        Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    alUser.add(email);
                }
            }
        }
        //end Validasi
    }
    
    //add by Haries: Registration reminder
    public void pheReminderRegistration() throws DataException {
        DataResultSet ticketRS = null;
        DataResultSet emailRS = null;
        String strListMessage = "";
        String requestName = "";
        String strTo = "";
        String strSubject;
        String strMessage = "";
        int i;

//        DatabaseValue dValue = new DatabaseValue();
        String query = " select revisions.did, ddocname, ddoctitle, xdocnumber, ddocauthor, xApprover, xauthor, to_char(xstampdate,'DD-MON-YYYY') xstampdate,xstatus,(select dfullname from users where dname=ddocauthor)docauthorname,xdocname, " ;  
        		query = query + " CASE ";
        		query = query + " WHEN to_char(xstampdate-30,'dd/mm/yyyy') = to_char(sysdate,'dd/mm/yyyy')";
        		query = query + " THEN (select demail from users where dname=ddocauthor)";
        		query = query + " WHEN to_char(xstampdate-15,'dd/mm/yyyy') = to_char(sysdate,'dd/mm/yyyy')";
        		query = query + " THEN CONCAT(CONCAT((select demail from users where dname=ddocauthor),','), (select key_value from phe_config where key_config='REGISTRATION_TDC_EMAIL'))";
        		query = query + " WHEN to_char(xstampdate-1,'dd/mm/yyyy') = to_char(sysdate,'dd/mm/yyyy')";
        		query = query + " THEN  (select demail from users where dname=ddocauthor)";
        		query = query + " WHEN to_char(xstampdate,'dd/mm/yyyy') = to_char(sysdate,'dd/mm/yyyy')";
        		query = query + " THEN CONCAT(CONCAT((select demail from users where dname=ddocauthor),','), (select key_value from phe_config where key_config='REGISTRATION_TDC_EMAIL'))";
        		query = query + " END sendemailto";
        		query = query + " from revisions, docmeta where docmeta.did=revisions.did";
        		query = query + " and (to_char(xstampdate-30,'dd/mm/yyyy') = to_char(sysdate,'dd/mm/yyyy') or to_char(xstampdate-15,'dd/mm/yyyy') = to_char(sysdate,'dd/mm/yyyy')";
        		query = query + " or to_char(xstampdate-1,'dd/mm/yyyy') = to_char(sysdate,'dd/mm/yyyy')";
        		query = query + " or to_char(xstampdate,'dd/mm/yyyy') = to_char(sysdate,'dd/mm/yyyy'))";
        		query = query + " and XSTATUS ='Registered'";
        		query = query + " and DDOCTYPE='Registration'";
        //Log.info("pheReminderReturnToTDC: guery:" + query);
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        //ws.executeSQL(query);
        try {
            dValue.insertScheduleLog("REGISTRATION_REMINDER", "Success", "PHE_CLICK_SCHEDULE_REGISTRATION_REMINDER");
            ResultSet rsreturn = ws.createResultSetSQL(query);
            ticketRS = new DataResultSet();
            ticketRS.copy(rsreturn);

            //Log.info("pheReminderReturnToTDC: ticketRS:" + ticketRS);
            int numCols = ticketRS.getNumFields();
            //Log.info("pheReminderReturnToTDC: numCols:"+numCols);

            //edit by nanda - 230614 - validasi if ticket is not empty
            if (!ticketRS.isEmpty()) {
            	while (ticketRS.next()){
                	strTo = ticketRS.getStringValue(11);
                	strSubject = "Registration Reminder "+ticketRS.getStringValue(10);
                	strMessage = "";
                	strMessage = strMessage + "<html>";
                	strMessage = strMessage + "<body style='font-family: Arial, Verdana, san-serif;'>";
                	strMessage = strMessage + "<p>Dear Mr/Mrs "+ticketRS.getStringValue(9)+",</p><br />";
                	strMessage = strMessage + "<p>Your New Document Registration will be due in near future. The Detail are:</p>";
                	strMessage = strMessage + "<table width='750px'  style='border-color: #666; border-collapse:collapse;' cellpadding='10' border='1px'>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <th width='24%' scope='col' style='background: #eee;'><div align='left'>Document Title </div></th>";
                	strMessage = strMessage + "    <th width='76%' scope='col'><div align='left'>"+ticketRS.getStringValue(2)+"</div></th>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <th width='24%' scope='col' style='background: #eee;'><div align='left'>Document Name</div></th>";
                	strMessage = strMessage + "    <th width='76%' scope='col'><div align='left'>"+ticketRS.getStringValue(10)+"</div></th>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <th width='24%' scope='col' style='background: #eee;'><div align='left'>Document Number</div></th>";
                	strMessage = strMessage + "    <th width='76%' scope='col'><div align='left'>"+ticketRS.getStringValue(3)+"</div></th>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Uploader</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(4)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Initiator</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(5)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Supervisor / Manager</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(6)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Due Date</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(7)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Status</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(8)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "</table>";
                	strMessage = strMessage + "<p>Please submit the final approved document or extend the registration in Registration Inbox menu or click this ";
                	strMessage = strMessage + " <a href='http://kponwjis013.pertamina.com:16300/urm/idcplg?IdcService=DOC_INFO&dID="+ticketRS.getStringValue(0)+"&dDocName="+ticketRS.getStringValue(1)+"'>link</a></p><br/><br/>";
                	strMessage = strMessage + "<p>Thank  you for your attention</p>";
                	strMessage = strMessage + "</body>";
                	strMessage = strMessage + "</html>";
                	
                	
                	 pheSendMail(strTo, strMessage, strSubject, "");
                } ;
                //System.out.println("return to tdc list="+requestName);
            }


        } catch (DataException ex) {
            dValue.insertScheduleLog("REGISTRATION_REMINDER", "Failed", "PHE_CLICK_SCHEDULE_REGISTRATION_REMINDER");
            Log.error("pheReminderReturnToTDC: DataException: registrationReminder:" + ex.getMessage());
        }
    }
    
    //add by Mita: Registration reminder
    public void pheReminderRegistrationAfter() throws DataException {
        DataResultSet ticketRS = null;
        DataResultSet emailRS = null;
        String strListMessage = "";
        String requestName = "";
        String strTo = "";
        String strSubject;
        String strMessage = "";
        int i;

//        DatabaseValue dValue = new DatabaseValue();
        String query = " select revisions.did, ddocname, ddoctitle, xdocnumber, ddocauthor, xApprover, xauthor, to_char(xstampdate,'DD-MON-YYYY') xstampdate,xstatus,(select dfullname from users where dname=ddocauthor)docauthorname,xdocname, " ;  
        		query = query + " CASE ";
        		query = query + " WHEN (sysdate >= xstampdate AND sysdate < xstampdate+7   ) ";
        		query = query + " THEN (select demail from users where dname=ddocauthor) ";
        		query = query + " WHEN  (sysdate >=  xstampdate+7 AND sysdate < xstampdate+8   ) ";
        		query = query + " THEN CONCAT( ";
        		query = query + " CONCAT( ";
        		query = query + " CONCAT( ";
        		query = query + " CONCAT( ";
        		query = query + " CONCAT( ";
        		query = query + " CONCAT((select demail from users where dname=ddocauthor),','), ";
        		query = query + " (select demail from users where dfullname=xauthor)),','), ";
        		query = query + " (select demail from users where dfullname=XAPPROVER)),','), ";
        		query = query + " (select key_value from phe_config where key_config='REGISTRATION_TDC_EMAIL')) ";
        		query = query + " WHEN  (sysdate >=  xstampdate+8 ) ";
        		query = query + " THEN CONCAT(CONCAT(  ";
        		query = query + " CONCAT(CONCAT((select demail from users where dname=ddocauthor),','),  ";
        		query = query + " (select demail from users where dfullname=xauthor)),','),  ";
        		query = query + " (select demail from users where dfullname=XAPPROVER)) ";
        		query = query + " END toemail ";
        		query = query + " from revisions, docmeta  ";
        		query = query + " where docmeta.did=revisions.did ";
        		query = query + " and( (sysdate >= xstampdate AND sysdate < xstampdate+7   ) ";
        		query = query + " OR (sysdate >=  xstampdate+7 AND sysdate < xstampdate+8   ) ";
        		query = query + " OR (sysdate >=  xstampdate+8 ) )";
        		query = query + " and XSTATUS ='Registered'";
        		query = query + " and DDOCTYPE='Registration'";
        //Log.info("pheReminderReturnToTDC: guery:" + query);
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        //ws.executeSQL(query);
        try {
            dValue.insertScheduleLog("REGISTRATION_REMINDER_AFTER", "Success", "PHE_CLICK_SCHEDULE_REGISTRATION_REMINDER_AFTER");
            ResultSet rsreturn = ws.createResultSetSQL(query);
            ticketRS = new DataResultSet();
            ticketRS.copy(rsreturn);

            //Log.info("pheReminderReturnToTDC: ticketRS:" + ticketRS);
            int numCols = ticketRS.getNumFields();
            //Log.info("pheReminderReturnToTDC: numCols:"+numCols);

            //edit by nanda - 230614 - validasi if ticket is not empty
            if (!ticketRS.isEmpty()) {
            	while (ticketRS.next()){
                	strTo = ticketRS.getStringValue(11);
                	strSubject = "Registration Reminder "+ticketRS.getStringValue(10);
                	strMessage = "";
                	strMessage = strMessage + "<html>";
                	strMessage = strMessage + "<body style='font-family: Arial, Verdana, san-serif;'>";
                	strMessage = strMessage + "<p>Dear Mr/Mrs "+ticketRS.getStringValue(9)+",</p><br />";
                	strMessage = strMessage + "<p>Your New Document Registration is already past the due date. The Detail are:</p>";
                	strMessage = strMessage + "<table width='750px'  style='border-color: #666; border-collapse:collapse;' cellpadding='10' border='1px'>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <th width='24%' scope='col' style='background: #eee;'><div align='left'>Document Title </div></th>";
                	strMessage = strMessage + "    <th width='76%' scope='col'><div align='left'>"+ticketRS.getStringValue(2)+"</div></th>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <th width='24%' scope='col' style='background: #eee;'><div align='left'>Document Name</div></th>";
                	strMessage = strMessage + "    <th width='76%' scope='col'><div align='left'>"+ticketRS.getStringValue(10)+"</div></th>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <th width='24%' scope='col' style='background: #eee;'><div align='left'>Document Number</div></th>";
                	strMessage = strMessage + "    <th width='76%' scope='col'><div align='left'>"+ticketRS.getStringValue(3)+"</div></th>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Uploader</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(4)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Initiator</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(5)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Supervisor / Manager</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(6)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Due Date</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(7)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "  <tr>";
                	strMessage = strMessage + "    <td style='background: #eee;'><div align='left'>Status</div></td>";
                	strMessage = strMessage + "    <td><div align='left'>"+ticketRS.getStringValue(8)+"</div></td>";
                	strMessage = strMessage + "  </tr>";
                	strMessage = strMessage + "</table>";
                	strMessage = strMessage + "<p>Please submit the final approved document or extend the registration in Registration Inbox menu or click this ";
                	strMessage = strMessage + " <a href='http://kponwjis013.pertamina.com:16300/urm/idcplg?IdcService=DOC_INFO&dID="+ticketRS.getStringValue(0)+"&dDocName="+ticketRS.getStringValue(1)+"'>link</a></p><br/><br/>";
                	strMessage = strMessage + "<p>Thank  you for your attention</p>";
                	strMessage = strMessage + "</body>";
                	strMessage = strMessage + "</html>";
                	
                	
                	 pheSendMail(strTo, strMessage, strSubject, "");
                } ;
                //System.out.println("return to tdc list="+requestName);
            }


        } catch (DataException ex) {
            dValue.insertScheduleLog("REGISTRATION_REMINDER_AFTER", "Failed", "PHE_CLICK_SCHEDULE_REGISTRATION_REMINDER_AFTER");
            Log.error("pheReminderReturnToTDC: DataException: registrationReminder:" + ex.getMessage());
        }
    }
}
