/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.data.DataException;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    private Pattern pattern;
    private Matcher matcher;
    private static final String DATE_PATTERN =
            "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";

    public Validator() {
        pattern = Pattern.compile(DATE_PATTERN);
    }

    /**
     * Validate date format with regular expression
     *
     * @param date date address for validation
     * @return true valid date format, false invalid date format
     */
    public boolean isNumeric(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean dateChecker(final String date) {

        matcher = pattern.matcher(date);

        if (matcher.matches()) {

            matcher.reset();

            if (matcher.find()) {

                String day = matcher.group(1);
                String month = matcher.group(2);
                int year = Integer.parseInt(matcher.group(3));

                if (day.equals("31")
                        && (month.equals("4") || month.equals("6") || month.equals("9")
                        || month.equals("11") || month.equals("04") || month.equals("06")
                        || month.equals("09"))) {
                    return false; // only 1,3,5,7,8,10,12 has 31 days
                } else if (month.equals("2") || month.equals("02")) {
                    //leap year
                    if (year % 4 == 0) {
                        if (day.equals("30") || day.equals("31")) {
                            return false;
                        } else {
                            return true;
                        }
                    } else {
                        if (day.equals("29") || day.equals("30") || day.equals("31")) {
                            return false;
                        } else {
                            return true;
                        }
                    }
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public String splitDocName(String documentName) {
        String documentNumber = "";
        String revision = "";
        String sheetNumber = "";

        //cek dokumen ada judulnya ato ngga.. terus di seperate
        documentName = documentName.trim();
        if (documentName.indexOf(" ") >= 0) {
            String tempDocumentName = "";
            String extension = "";
            tempDocumentName = documentName.substring(0, documentName.indexOf(" "));
            Log.info("PHE:splitDocName:tempDocumentName:" + tempDocumentName);
            extension = documentName.substring(documentName.lastIndexOf(".", documentName.length()));
            Log.info("PHE:splitDocName:extension:" + extension);
            documentName = tempDocumentName + extension;
            Log.info("PHE:splitDocName:documentName:" + documentName);

        }

        //cek revisi sama sheet number
        if (documentName.indexOf(".") >= 0) {
            if (documentName.indexOf("~") >= 0) {
                documentNumber = documentName.substring(0, documentName.indexOf("~"));
                revision = documentName.substring(documentName.indexOf("~") + 1, documentName.lastIndexOf("."));
            } else {
                documentNumber = documentName.substring(0, documentName.indexOf("."));
            }

            if (revision.indexOf("-") >= 0) {
                sheetNumber = revision.substring(revision.indexOf("-") + 1, revision.length());
                revision = revision.substring(0, revision.indexOf("-"));
            } else {
                sheetNumber = "";
            }
        } else {
            revision = documentName.substring(documentName.indexOf("~") + 1, documentName.length());
            if (revision.indexOf("-") >= 0) {
                sheetNumber = revision.substring(revision.indexOf("-") + 1, revision.length());
                revision = revision.substring(0, revision.indexOf("-"));
            }
        }
        
        //modified by nanda - 300115 - commented untuk menolak dokumen jika tidak punya caching
//        if (revision.length() == 0) {
//            revision = "0";
//        }
        //end modified
        if (sheetNumber.length() == 0) {
            sheetNumber = "-1";
        }
        //Modified Haries 270716: Menambah pengecekan jika document Name tidak ada title 
        if(documentNumber==""){
        	 if (documentName.indexOf("~") >= 0) {
                 documentNumber = documentName.substring(0, documentName.indexOf("~"));
        	 }
        }
        Log.info("PHE:splitDocName:" + documentNumber + ";" + revision + ";" + sheetNumber);
        return documentNumber + ";" + revision + ";" + sheetNumber;
    }

    public String validateDocumentNumber(String documentName, String docPurpose, String type, DatabaseValue dValue) throws DataException {
        String error = "";
        String docNumber = "";
        String sheetNumber = "";
        int revision=0;
//        DatabaseValue dValue = new DatabaseValue();
        
        //add by nanda - 2310 --pindahan dari checkdocumentname biar ga initialize class validator lagi
         String splitDocName[] = splitDocName(documentName).split(";");
        docNumber = splitDocName[0];

        Log.info("PHE:checkDocumentName:splitSize:" + splitDocName.length);
        if (splitDocName[2].equals("-1")) {
            sheetNumber = "";
        } else {
            sheetNumber = splitDocName[2];
        }
        //modified by nanda - 300115 - commented untuk menolak dokumen jika tidak punya caching
        if (splitDocName[1].length() == 0) {
            return "<mark>invalidFormat:Doc_Name have no Revision<mark><br>";
        } else {
            try {
                revision = Integer.parseInt(splitDocName[1]);
            } catch (Exception ex) {
//                revision = 0;
                Log.error("PHE:checkDocumentName:invalidParse");
                return "<mark>invalidFormat:Revision not Numeric<mark><br>";
            }
        }
        //end modified

        Log.info("PHE:checkDocumentName:docNumber:" + docNumber + ";revision:" + revision + ";sheetNumber:" + sheetNumber);
        if (sheetNumber.length() > 0) {
            Log.info("isNumeric(sheetNumber)="+isNumeric(sheetNumber));
            Log.info("isNumeric(1)="+isNumeric("1"));
            if (!isNumeric(sheetNumber)) {
                return "<mark>invalidFormat:sheetNumber not Numeric<mark><br>";
            }
        }
        //end
        
        String checkDocument = dValue.checkDocumentName(docNumber.toString(), revision, sheetNumber.toString(), docPurpose.toString());
        Log.info("PHE:checkDocument:" + checkDocument);
        if (checkDocument.equals("invalidFormat")) {
            error = error + "<mark>doc_name is invalid format</mark>" + "<br>";
        } else if (checkDocument.equals("revisionError")) {
            error = error + "<mark>doc_name has higher revision</mark>" + "<br>";
            //add by nanda 0909 - message error already exist
        } else if (checkDocument.equals("revisionAlreadyExist")) {
            error = error + "<mark>doc_name already exist</mark>" + "<br>";
        } else if (checkDocument.equals("invalidFormat:sheetNumberNotNumeric")) {
            error = error + "<mark>sheet_number not numeric</mark>" + "<br>";
        } else if (checkDocument.equals("")) {
//            String docNumber = "";
            if (documentName.toString().indexOf("~") >= 0) {
                docNumber = documentName.toString().substring(0, documentName.toString().indexOf("~"));
            } else if (documentName.toString().indexOf(".") >= 0) {
                docNumber = documentName.toString().substring(0, documentName.toString().indexOf("."));
            }
            Log.info("PHE:docNumber:" + docNumber);
            String splitArea[] = docNumber.split("-");

            try {
                if (type.equals("DRAWING")) {
                    if (dValue.getLocation(splitArea[0], "DRAWING").length() == 0) {
                        error = error + "<mark>area from docname is invalid</mark>" + "<br>";
                    }
                    if (dValue.getPlatform(splitArea[0], splitArea[1]).length() == 0) {
                        error = error + "<mark>platform from docname is invalid</mark>" + "<br>";
                    }
                    if (dValue.getDisciplineCode(splitArea[2], "DRAWING").length() == 0) {
                        error = error + "<mark>discipline from docname is invalid</mark>" + "<br>";
                    }
                    if (dValue.getDocumentType(splitArea[3], "DRAWING").length() == 0) {
                        error = error + "<mark>document_type from docname is invalid</mark>" + "<br>";
                    }
                } else {
                    if (dValue.getLocation(splitArea[0], "NONDRAWING").length() == 0) {
                        error = error + "<mark>area from docname is invalid</mark>" + "<br>";
                    }
                    if (dValue.getDisciplineCode(splitArea[1], "NONDRAWING").length() == 0) {
                        error = error + "<mark>discipline from docname is invalid</mark>" + "<br>";
                    }
                    if (dValue.getDocumentType(splitArea[2], "NONDRAWING").length() == 0) {
                        error = error + "<mark>document_type from docname is invalid</mark>" + "<br>";
                    }
                }
            } catch (Exception ex) {
                error = error + "<mark>docname format is invalid</mark>" + "<br>";
            }
        }
        return error;
    }

    public boolean isExistFile(String path) {

        File f = new File(path);
        Log.info(f.getAbsolutePath()); //  
         
        if(f.isFile()){
            Log.info("PHE:f.isFile():" + path);
        };
        
        System.out.println(f.exists());
        if (f.exists()) {
            Log.info("PHE:isExistFile:path:" + path);
            return true;
        } else {
            Log.info("PHE:isNotExistFile:path:" + path);
            return false;
        }

    }
    
    public boolean isMaxFile(String path){
        File f = new File(path);
        Log.info(f.getAbsolutePath());
        
        long fileSizeInBytes = f.length();
        long fileSizeInKB = fileSizeInBytes /1024;
        long fileSizeInMB = fileSizeInKB / 1024;
        Log.info("fileSizeInBytes"+fileSizeInBytes);
        Log.info("fileSizeInMB"+fileSizeInMB);
        if (fileSizeInMB>30){
            return false;
        } else {
        return true;
        }
    }
//nanda - 2110
    public String validateDocumentNumberForTDCDrawing(String area, String platform, String disciplineCode, String docType, String runningNumber, String sheetNumber, String type) {
        String error = "";
        DatabaseValue dValue = new DatabaseValue();

        if (!isNumeric(runningNumber)) {
            error = error + "<mark>running_number must be numeric</mark>" + "<br>";
        } else if (runningNumber.length() > 4) {
            //edited by nanda 191115 - sebelumnya > 5
            error = error + "<mark>running_number must 4 characters</mark>" + "<br>";
        } else {

            if (area.length() > 0 && platform.length() > 0 && disciplineCode.toString().length() > 0 && docType.length() > 0 && runningNumber.length() > 0) {
                try {
                    if (dValue.getDocumentNumberCombination(area, platform, disciplineCode, docType, runningNumber, sheetNumber, type).length() > 0) {
                        error = error + "<mark>running_number has already exists.</mark><br>";
                    }
                } catch (DataException ex) {
                    Log.error("PHE:checkTDCDrawing:getDocumentNumberCombination:DataException:ex:" + ex.getMessage());
                    error = error + "<mark>combination document number has already exists.</mark><br>";
                }
            }
            try {
                if (!dValue.validDisciplineCode(disciplineCode, runningNumber, type)) {
                    error = error + "<mark>colaborate between discipline_code and running_number is invalid.</mark><br>";
                }
            } catch (Exception ex) {
                error = error + "<mark>colaborate between discipline_code and running_number is invalid.</mark><br>";
            }
        }
        return error;
    }

    public String validateDocumentNumberForTDCNonDrawing(String area, String disciplineCode, String docType, String runningNumber, String sheetNumber, String type) {
        String error = "";
        DatabaseValue dValue = new DatabaseValue();

        Log.info("--validateDocumentNumberForTDCNonDrawing:runningNumber:"+runningNumber+";runningNumber.length():"+runningNumber.length());

        //edited by nanda 191115 - sebelumnya > 5
        if (runningNumber.length() > 4) {
            error = error + "<mark>running_number must 4 characters</mark>" + "<br>";
        }
        if (!isNumeric(runningNumber)) {
            error = error + "<mark>running_number must be numeric </mark><br>";
        }

        if (area.length() > 0 && disciplineCode.length() > 0 && docType.length() > 0) {
            try {
                if (dValue.getDocumentNumberCombination(area, "", disciplineCode, docType, runningNumber, "", type).length() > 0) {
                    error = error + "<mark>running_number has already exists.</mark><br>";
                }
            } catch (DataException ex) {
                Log.error("PHE:validateDocumentNumberForTDCNonDrawing:ex:" + ex.getMessage());
                error = error + "";
            }
        }
        
        Log.info("--validateDocumentNumberForTDCNonDrawing:error:"+error);
        return error;
    }
    
    //add by nanda 280515
    public boolean IsValidDoc3D(String docType, String model) {
//        String[] names = docNumber.split("-");
         Log.info("Start validating doc 3d");
        Log.info(docType);
        if(docType.equalsIgnoreCase("Drawing")){
             if(docType.equalsIgnoreCase("Drawing")&&docType.equalsIgnoreCase("D3D")&& model.equalsIgnoreCase("3D")){
             return true;
            } else if(model.equalsIgnoreCase("2D")&&!docType.equalsIgnoreCase("D3D")){
             return true;
            } else if(model.equalsIgnoreCase("")&&!docType.equalsIgnoreCase("D3D")){
             return true;
            }else{
             return false;
            }
        } else {
            return true;
        }
         
    }

	public String verifyApPrefix(String xCompany) {
		String apSuffix = "";
		if ((xCompany.toUpperCase().replaceAll(" ", "").equalsIgnoreCase("PHE") && xCompany.length() == 3) ||
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEH") ||
			xCompany.toUpperCase().replaceAll(" ", "").contains("HOLDING")) {
			apSuffix = "PHE";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("ONWJ") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEONWJ")) {
			apSuffix = "PHEONWJ";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("ABAR") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEABAR")) {
			apSuffix = "PHEABAR";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("ANGGURSI") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEANGGURSI")) {
			apSuffix = "PHEANGGURSI";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("NUNUKAN") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHENUNUKAN")) {
			apSuffix = "PHENUNUKAN";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("WMO") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEWMO")) {
			apSuffix = "PHEWMO";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("NSO") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHENSO")) {
			apSuffix = "PHENSO";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("NSB") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHENSB")) {
			apSuffix = "PHENSB";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("SIAK") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHESIAK")) {
			apSuffix = "PHESIAK";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("UH") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEUH")) {
			apSuffix = "PHEUH";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("TUBANEASTJAVA") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHETUBANEASTJAVA")) {
			apSuffix = "PHETUBANEASTJAVA";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("KAMPAR") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEKAMPAR")) {
			apSuffix = "PHEKAMPAR";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("RANDUGUNTING")|| 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHERANDUGUNTING")) {
			apSuffix = "PHERANDUGUNTING";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("OGANKOMERING")|| 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEOGANKOMERING")) {
			apSuffix = "PHEOGANKOMERING";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("OSES") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEOSES")) {
			apSuffix = "PHEOSES";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("JAMBIMERANG") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHEJAMBIMERANG")) {
			apSuffix = "PHEJAMBIMERANG";
		} 
		if (xCompany.toUpperCase().replaceAll(" ", "").contains("RAJATEMPIRAI") || 
			xCompany.toUpperCase().replaceAll(" ", "").contains("PHERAJATEMPIRAI")) {
			apSuffix = "PHERAJATEMPIRAI";
		} 

		return apSuffix;
	}
}