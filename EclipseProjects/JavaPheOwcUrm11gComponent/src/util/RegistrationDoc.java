/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import data.DatabaseValue;
import intradocservice.Document;
import intradocservice.Folder;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradoc.server.ServiceManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author raditlr
 */
public class RegistrationDoc extends Service {
	private String dateFormat = "mm/dd/yy";
	
    public static void main(String[] args) {
            // TODO Auto-generated method stub

    }
	   
    public void qResultRegistration() throws DataException, ServiceException{
        //Workspace ws = getSystemWorkspace();
        String idcToken = null;
        idcToken = this.m_binder.getLocal("idcToken");
        
        String query = "";
        query = "select * from" +
                " (select rownum as rownumber," +
                "  xApprover, a.did as did, xAuthorOrganization, xComments, xstatus, xAuthor, TO_CHAR(xStampDate, 'dd-Mon-yyyy') as xStampDate, dDocType, ddoctitle, XDOCNAME, dDocAuthor, B.DLASTMODIFIEDDATE, xCompany " +
                " ,(select DD.AUTHORORGANIZATIONDESC  from phe_author_organization dd  where DD.AUTHOR_ORGANIZATION= a.XAUTHORORGANIZATION) as XAUTHORORGANIZATIONDESC "+
                " ,(select c.KEY_DISPLAY from PHE_CONFIG c where c.KEY_VALUE = a.xCompany) as XCOMPANYDISPLAY"+
                " from docmeta a, revisions b " +
        		"  where a.did=b.did and dDocType = 'Registration' and b.DREVRANK=0";        
//        query = query + "select * from" +
//        	"  select xApprover, xAuthorOrganization,  xComments, xstatus, xStampDate, dDocType, ddoctitle, XDOCNAME, dDocAuthor " +
//        	"  from docmeta a, revisions b " +
//        	"  where a.did=b.did and dDocType = 'Registration' ";
        	
//            " (select rownum as rownumber," +
//            "   r.did, r.ddocname, r.ddoctype, R.DDOCAUTHOR, R.DSECURITYGROUP, R.DDOCTITLE, R.DREVRANK, R.DCREATEDATE," +
//            "   D.XAPPROVER, D.XKATEGORI, d.xsubkategori, D.XKRITERIA, d.xjenisdokumen, d.xdocname, d.xdocnumber, d.xstatus, D.XCOMMENTS, d.xinternalowner, d.xexternalowner, d.xreferensi" +
//            " from revisions r, docmeta d where r.did=d.did and r.drevrank=0 and r.dDocType='Komet' and d.xStatus='Published' ";
        
 
        // date Stamp
        String xDateFrom = null;
        String xDateTo = null;
        xDateFrom = this.m_binder.getLocal("iDateFrom");
        xDateTo = this.m_binder.getLocal("iDateTo");
        
        if (xDateFrom!= null && !xDateFrom.isEmpty() && xDateTo!= null && !xDateTo.isEmpty()) {
        	xDateTo = this.addOneDay(xDateTo);
            query = query + "and (xStampDate between TO_DATE('"+ xDateFrom +"', '"+dateFormat+"') and TO_DATE('"+xDateTo+"', '"+dateFormat+"'))";
        }
        
        String xStatus = null;
        xStatus = this.m_binder.getLocal("iStatus");
        if (xStatus!= null && !xStatus.isEmpty()) {
            query = query + "and lower(xStatus) like '%" + xStatus.toLowerCase() + "%' ";
        }
        
        String dDocAuthor = null;
        dDocAuthor = this.m_binder.getLocal("dUser");
        query = query + "and lower(dDocAuthor) = '" + dDocAuthor.toLowerCase() + "' ";
        
        DataBinder contentBinder = new DataBinder();
        DatabaseValue dValue = new DatabaseValue();
        
        String startRow = this.m_binder.getLocal("startRow");
        String endRow = this.m_binder.getLocal("endRow");
        query = query + ") where rownumber between "+ startRow +" and "+ endRow;
		query = query + " order by DLASTMODIFIEDDATE desc";
        Log.info("PHE:REGISTRATION qResultRegistration:" + query);
        
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        ResultSet resultSet = ws.createResultSetSQL(query);
        
        /*if ((resultSet == null) || (resultSet.isEmpty())) {return resultSet; }*/
        
        //ResultSet rSet = dValue.getTicketByAuthor(ItemStatus1, ItemStatus2, User, Role);
        DataResultSet myDataResultSet = new DataResultSet();
        myDataResultSet.copy(resultSet);
        this.m_binder.addResultSet("pheRegistrationRS", myDataResultSet);
    }
    
	   
    public void qTotalResultRegistration() throws DataException, ServiceException{
        //Workspace ws = getSystemWorkspace();
        String idcToken = null;
        idcToken = this.m_binder.getLocal("idcToken");
        
        String query = "";
        query = " select count(a.did) as TOTALROW" +
        	"  from docmeta a, revisions b " +
        	"  where a.did=b.did and dDocType = 'Registration' and b.DREVRANK=0";        
//        query = query + "select * from" +
//        	"  select xApprover, xAuthorOrganization,  xComments, xstatus, xStampDate, dDocType, ddoctitle, XDOCNAME, dDocAuthor " +
//        	"  from docmeta a, revisions b " +
//        	"  where a.did=b.did and dDocType = 'Registration' ";
        	
//            " (select rownum as rownumber," +
//            "   r.did, r.ddocname, r.ddoctype, R.DDOCAUTHOR, R.DSECURITYGROUP, R.DDOCTITLE, R.DREVRANK, R.DCREATEDATE," +
//            "   D.XAPPROVER, D.XKATEGORI, d.xsubkategori, D.XKRITERIA, d.xjenisdokumen, d.xdocname, d.xdocnumber, d.xstatus, D.XCOMMENTS, d.xinternalowner, d.xexternalowner, d.xreferensi" +
//            " from revisions r, docmeta d where r.did=d.did and r.drevrank=0 and r.dDocType='Komet' and d.xStatus='Published' ";
        
        // date Stamp
        String xDateFrom = null;
        String xDateTo = null;
        xDateFrom = this.m_binder.getLocal("iDateFrom");
        xDateTo = this.m_binder.getLocal("iDateTo");
        
        if (xDateFrom!= null && !xDateFrom.isEmpty() && xDateTo!= null && !xDateTo.isEmpty()) {
        	xDateTo = this.addOneDay(xDateTo);
            query = query + "and (xStampDate between TO_DATE('"+ xDateFrom +"', '"+dateFormat+"') and TO_DATE('"+xDateTo+"', '"+dateFormat+"'))";
        }
        
        String xStatus = null;
        xStatus = this.m_binder.getLocal("iStatus");
        if (xStatus!= null && !xStatus.isEmpty()) {
            query = query + "and lower(xStatus) like '%" + xStatus.toLowerCase() + "%' ";
        }
        
        String dDocAuthor = null;
        dDocAuthor = this.m_binder.getLocal("dUser");
        Log.info("PHE:REGISTRATION dDocAuthor:" + dDocAuthor);
//        if (dDocAuthor!= null && !dDocAuthor.isEmpty()) {
            query = query + "and lower(dDocAuthor) = '" + dDocAuthor.toLowerCase() + "' ";
//        }
        Log.info("PHE:REGISTRATION query:" + query);
        DataBinder contentBinder = new DataBinder();
        DatabaseValue dValue = new DatabaseValue();
        
//        String startRow = this.m_binder.getLocal("startRow");
//        String endRow = this.m_binder.getLocal("endRow");
//        query = query + ") where rownumber between "+ startRow +" and "+ endRow;
//        Log.info("PHE:query REGISTRATION:" + query);
        
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        ResultSet resultSet = ws.createResultSetSQL(query);
        Log.info("PHE:REGISTRATION qTotalResultRegistration:" + query);
        
        /*if ((resultSet == null) || (resultSet.isEmpty())) {return resultSet; }*/
        
        //ResultSet rSet = dValue.getTicketByAuthor(ItemStatus1, ItemStatus2, User, Role);
        DataResultSet myDataResultSet = new DataResultSet();
        myDataResultSet.copy(resultSet);
        this.m_binder.addResultSet("pheRegistrationTotalRS", myDataResultSet);
    }

    public String addOneDay(String StringDate){
//    	String date = "06/27/2007";
        DateFormat df = new SimpleDateFormat(dateFormat); 
        Date startDate;
        try {
        	startDate = df.parse(StringDate);
        	
        	Calendar cal = Calendar.getInstance();
        	cal.setTime(startDate);
        	cal.add(Calendar.DATE, 1);
        	
        	StringDate = df.format(cal.getTime());
            System.out.println(StringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return StringDate;
    }
    
    public void DoCheckin() throws DataException, ServiceException
    {
        try
        {
            String returnFunction = "";
            Validator validator = new Validator();
            DataBinder binder = new DataBinder();
            DataCollection dc = new DataCollection();            
            
            // String folderSource = "D:/PHE_TEMP_NEWDOC/F-PHEONWJ-NEWDOC-20161118-00006.pdf";
            String folderSource = this.m_binder.getLocal("xFolderSource");
            
            binder.putLocal("dDocName",             this.m_binder.getLocal("dDocName"));
            
            binder.putLocal("IdcService",           "CHECKIN_NEW");
            binder.putLocal("idcToken",             this.m_binder.getLocal("idcToken"));
            
            binder.putLocal("xAuthor",              this.m_binder.getLocal("xAuthor"));
            binder.putLocal("xAuthorOrganization",  this.m_binder.getLocal("xAuthorOrganization"));
            binder.putLocal("xApprover",            this.m_binder.getLocal("xApprover"));
            binder.putLocal("xDepartment",          this.m_binder.getLocal("xDepartment"));
            binder.putLocal("xComments",            this.m_binder.getLocal("xComments"));
            binder.putLocal("dDocTitle",            this.m_binder.getLocal("dDocTitle"));
            
            binder.putLocal("xDocName",             this.m_binder.getLocal("xDocName"));
            binder.putLocal("dDocAuthor",           this.m_binder.getLocal("dUser"));
            binder.putLocal("dDocType",             this.m_binder.getLocal("dDocType"));
            binder.putLocal("dSecurityGroup",       this.m_binder.getLocal("dSecurityGroup"));
            binder.putLocal("xStatus",              this.m_binder.getLocal("xStatus"));
            
            binder.putLocal("isJavaMethod",         "true");
            binder.putLocal("isCustom",             "true");

            binder.putLocal("createPrimaryMetaFile", "0");
            binder.putLocal("primaryFile",          folderSource);
            binder.putLocal("primaryFile:path",     folderSource);
            if (!validator.isExistFile(folderSource)) {
                Log.info("PHE:RegistrationDoc:docheckin: file not found");
            }			 
           
            returnFunction = dc.executeService(binder, this.m_binder.getLocal("dUser"), true);
            if (returnFunction.length() == 0) {
                returnFunction = " Gagal Checkin: " + binder.getLocal("dID") + ";" + binder.getLocal("dDocName");
            }
			
            Log.info("PHE:RegistrationDoc:docheckin:" +  returnFunction);
        }
        catch(Exception ex)
        {
            Log.error("PHE:RegistrationDoc:docheckin: failed :ex: " + ex.getMessage().toString());	
        }
    }        
}
