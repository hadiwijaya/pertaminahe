/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import intradoc.common.ExecutionContext;
import intradoc.common.LocaleUtils;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.provider.Provider;
import intradoc.provider.Providers;
import intradoc.server.Service;
import intradoc.server.ServiceData;
import intradoc.server.ServiceManager;
import intradoc.server.UserStorage;
import intradoc.shared.UserData;
import java.util.ArrayList;

/**
 *
 * @author Ginanjar
 */
public class DataCollection {

    public Workspace getSystemWorkspace() {
        Workspace workspace = null;
        Provider wsProvider = Providers.getProvider("SystemDatabase");
        if (wsProvider != null) {
            workspace = (Workspace) wsProvider.getProvider();
        }
        return workspace;
    }

    public UserData getFullUserData(String userName, ExecutionContext cxt, Workspace ws) throws DataException, ServiceException {
        if (ws == null) {
            ws = getSystemWorkspace();
        }
        UserData userData = UserStorage.retrieveUserDatabaseProfileDataFull(userName, ws, null, cxt, true, true);
//        ws.releaseConnection();
        return userData;
    }

    public String executeService(DataBinder binder, String userName, boolean suppressServiceError)
            throws DataException, ServiceException {
        ServiceException error;
        Workspace workspace = getSystemWorkspace();
        String cmd = binder.getLocal("IdcService");
        
        if (cmd == null) {
            throw new DataException("!csIdcServiceMissing");
        }
        ServiceData serviceData = ServiceManager.getFullService(cmd);
        if (serviceData == null) {
            throw new DataException(LocaleUtils.encodeMessage("!csNoServiceDefined", null, cmd));
        }
        Service service = ServiceManager.createService(serviceData.m_classID, workspace, null, binder, serviceData);
        Log.info("PHE:executeService:userName:" + userName);
        UserData fullUserData = getFullUserData(userName, service, workspace);
        Log.info("PHE:executeService:fullUserData:" + fullUserData);
        service.setUserData(fullUserData);
        binder.m_environment.put("REMOTE_USER", userName);
        error = null;
        try {
            service.setSendFlags(true, true);
            service.initDelegatedObjects();
            service.globalSecurityCheck();
            service.preActions();
            service.doActions();
            service.postActions();
            service.updateSubjectInformation(true);
            service.updateTopicInformation(binder);
        } catch (ServiceException e) {
            Log.error("PHE:execute:error:" + e.getMessage());
            Log.error("PHE:execute:error:Binder:" + binder);
            Log.error("PHE:execute:error.toString:" + e.toString());
            String errorEX = binder.getLocal("StatusMessage").toString();

            if(errorEX.indexOf("sufficient")>=0){
                return "executeFailed:sufficient privilege";
            }else if(errorEX.matches(".*The primary file.*")){
                return "executeFailed:primaryFile";
            } else {
                return "executeFailed";
            }
        } finally {
            //update by nanda - 071014 - 151014 - untuk ngilangin csJdbcCommitCalledInAutoCommitMode
//            service.cleanUp(true);
//            service.clear();
//            workspace.releaseConnection();
        }
        
        if (error != null) {
            if (suppressServiceError) {
                error.printStackTrace();
                if (binder.getLocal("StatusCode") == null) {
                    binder.putLocal("StatusCode", String.valueOf(error.m_errorCode));
                    binder.putLocal("StatusMessage", error.getMessage());
                }
            } else {
                throw new ServiceException(error.m_errorCode, error.getMessage());
            }
        }
        //nanda - 0311
        service.cleanUp(true);
        return binder.getLocal("dID") + ";" + binder.getLocal("dDocName");
    }
    
    public void executeService(DataBinder binders, String userName, boolean suppressServiceError,String temp)
            throws DataException, ServiceException {
        ServiceException error;
        Workspace workspace = getSystemWorkspace();
        String cmd = binders.getLocal("IdcService");
        
        if (cmd == null) {
            throw new DataException("!csIdcServiceMissing");
        }
        ServiceData serviceData = ServiceManager.getFullService(cmd);
        if (serviceData == null) {
            throw new DataException(LocaleUtils.encodeMessage("!csNoServiceDefined", null, cmd));
        }
        Service service = ServiceManager.createService(serviceData.m_classID, workspace, null, binders, serviceData);
//        Log.info("PHE:executeCheckin:userName:" + userName);
        UserData fullUserData = getFullUserData(userName, service, workspace);
//        Log.info("PHE:executeCheckin:fullUserData:" + fullUserData);
        service.setUserData(fullUserData);
        binders.m_environment.put("REMOTE_USER", userName);
        error = null;
        try {
            service.setSendFlags(true, true);
            service.initDelegatedObjects();
            service.globalSecurityCheck();
            service.preActions();
            service.doActions();
            service.postActions();
            service.updateSubjectInformation(true);
            service.updateTopicInformation(binders);          
        } catch (ServiceException e) {
            Log.error("PHE:execute:error:" + e.getMessage());
            e.printStackTrace();
        } finally {
            //update by nanda 151014
//            service.cleanUp(true);
        }
        if (error != null) {
            if (suppressServiceError) {
                error.printStackTrace();
                if (binders.getLocal("StatusCode") == null) {
                    binders.putLocal("StatusCode", String.valueOf(error.m_errorCode));
                    binders.putLocal("StatusMessage", error.getMessage());
                }
            } else {
                throw new ServiceException(error.m_errorCode, error.getMessage());
            }
        }
        
        //nanda - 0311
//        service.cleanUp(true);
    }
    
    public ArrayList convertRSAL(ResultSet rs){
        ArrayList rowCollection = new ArrayList();
        if(rs != null){
            int totalColumn = rs.getNumFields();
            while(rs.next()){
                ArrayList row = new ArrayList();
                for(int i = 0 ; i < totalColumn ; i++){
                    row.add(rs.getStringValue(i));
                }
                rowCollection.add(row);
            }
        }
        return rowCollection;
    }
}
