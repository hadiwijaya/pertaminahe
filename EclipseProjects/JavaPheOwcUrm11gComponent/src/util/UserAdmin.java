/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import ScheduledEvent.ScheduledEvt;
import data.DatabaseValue;
import intradoc.data.DataException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ginanjar
 */
public class UserAdmin {
    public ArrayList getUserAdmin(){
        //update by gin69
        DatabaseValue dValue = new DatabaseValue();

        String usernamePassword = "";
        try {
            usernamePassword = dValue.getPheConfig("USERNAME_ADMIN");
        } catch (DataException ex) {
            Logger.getLogger(ScheduledEvt.class.getName()).log(Level.SEVERE, null, ex);
        }

        String username = "";
        String password = "";

        if (usernamePassword.length() == 0) {
            username = "weblogic";
        }else{
            String splitUsernamePassword[] = usernamePassword.split(";");
            try{
                username = splitUsernamePassword[0];
                password = splitUsernamePassword[1];
            }catch(Exception ex){
            
            }
        }
        
        if(username.length() == 0){
            username = "weblogic";
        }
        
        ArrayList alUser = new ArrayList();
        alUser.add(username);
        alUser.add(password);
        
        return alUser;
    }
}
