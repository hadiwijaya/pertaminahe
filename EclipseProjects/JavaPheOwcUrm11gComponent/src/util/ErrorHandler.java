/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import data.DatabaseValue;

import intradoc.data.DataBinder;
import intradoc.data.Workspace;

/**
 *
 * @author Ginanjar
 */
public class ErrorHandler {
    public DataBinder errorHandler(String text){
        DataBinder binder = new DataBinder();
        binder.putLocal("StatusMessage", text);
        binder.putLocal("isError", "true");
        return binder;
    }
    
    public void ErrHandler(DataBinder binder, String text){
        binder.putLocal("StatusMessage", text);
        binder.putLocal("isError", "true");
    }
    
    public void ErrCheckin(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errCheckinFail";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
    
    public void ErrCheckout(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errCheckoutFail";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
    
    public void ErrDocNumberExists(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errExistsDocNumber";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
    
    public void ErrInvalidDocNumber(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errInvalidDocNumber";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
    
    //add by nanda 300115 - add exception for revision not numeric and no caching doc
    public void ErrInvalidDocName(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errInvalidDocName";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
    
    public void ErrRevisionsNotNumeric(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errRevisionsNotNumeric";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
    // end modified
    public void ErrLessDocRevision(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errLessDocRevision";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
    
    public void ErrColaborateDisciplineCode(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errColaborateDisciplineCode";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
            
    public void errColaborate3DDisciplineCode(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errColaborate3DDisciplineCode";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
    
    public void ErrSheetNumberNotNumeric(DataBinder binder)
    {
        String errMessage;
        DatabaseValue db = new DatabaseValue();
        DataCollection dc = new DataCollection();
        Workspace ws = dc.getSystemWorkspace();
        String code;
        code = "errSheetNumberNotNumeric";
        errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }
    
    public void ErrFailedUpload(DataBinder binder)
    {
        String errMessage = "Upload File Failed";
        //DatabaseValue db = new DatabaseValue();
        //DataCollection dc = new DataCollection();
        //Workspace ws = dc.getSystemWorkspace();
        //String code;
        //code = "errLessDocRevision";
        //errMessage = db.GetConfigValue(code, ws);
        ErrHandler(binder, errMessage);
    }           

}
