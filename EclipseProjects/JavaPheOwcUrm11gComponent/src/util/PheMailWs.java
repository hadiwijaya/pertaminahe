/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import intradoc.common.Log;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Bijak
 */
public class PheMailWs {
    static String wsURL = "http://10.252.4.191:16300/EDMSWebservice-Project1-context-root/EDMSServiceSoap12HttpPort";
    /**
     * @param args the command line arguments
     */
//   public static void main(String[] args) {
//        try {
//            callEmail("bijakas@gmail.com", "Halo Aku Sendiri", "Halo dahh");
//        } catch (IOException ex) {
//            Logger.getLogger(PheMailWs.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
   public  void callEmail (String email, String subject, String message) {
    String urlString = "http://10.252.4.191:16300/EDMSWebservice-Project1-context-root/EDMSServiceSoap12HttpPort";
        URL urlForInfWebSvc;
        try {
            urlForInfWebSvc = new URL(urlString);
        
        URLConnection UrlConnInfWebSvc = urlForInfWebSvc.openConnection();
        HttpURLConnection httpUrlConnInfWebSvc = (HttpURLConnection) UrlConnInfWebSvc;
        httpUrlConnInfWebSvc.setDoOutput(true);
        httpUrlConnInfWebSvc.setDoInput(true);
        httpUrlConnInfWebSvc.setAllowUserInteraction(true);
        httpUrlConnInfWebSvc.setRequestMethod("POST");
        httpUrlConnInfWebSvc.setRequestProperty("Host", "localhost");
        httpUrlConnInfWebSvc.setRequestProperty("Content-Type","application/soap+xml; charset=utf-8");
        OutputStreamWriter infWebSvcReqWriter = new OutputStreamWriter(httpUrlConnInfWebSvc.getOutputStream());
        String infWebSvcRequestMessage = 
        "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:tran=\"http://transmittalExternal/\">\n" +
        "   <soap:Header/>\n" +
        "   <soap:Body>\n" +
        "      <tran:SendEmail>\n" +
        "         <!--Optional:-->\n" +
        "         <EmailTo>"+email+"</EmailTo>\n" +
        "         <!--Optional:-->\n" +
        "         <Subject>"+subject+"</Subject>\n" +
        "         <!--Optional:-->\n" +
        "         <HtmlText>"+message+"</HtmlText>\n" +
        "      </tran:SendEmail>\n" +
        "   </soap:Body>\n" +
        "</soap:Envelope>";
        
        infWebSvcReqWriter.write(infWebSvcRequestMessage);
        infWebSvcReqWriter.flush();
        BufferedReader infWebSvcReplyReader = new BufferedReader(new InputStreamReader(httpUrlConnInfWebSvc.getInputStream()));
        String line;
        String infWebSvcReplyString = "";
        while ((line = infWebSvcReplyReader.readLine()) != null) {
            infWebSvcReplyString = infWebSvcReplyString.concat(line);
            }
        infWebSvcReqWriter.close();
        infWebSvcReplyReader.close();
        httpUrlConnInfWebSvc.disconnect();
        System.out.println(infWebSvcReplyString);
        } catch (MalformedURLException ex) {
            Log.error("PHE:callEmail:MalformedError:" + ex.getMessage());
        } catch (IOException ex) {
            Log.error("PHE:callEmail:IOException:" + ex.getMessage());
        }
   }
}


