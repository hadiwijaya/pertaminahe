/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.data.DataException;
import intradoc.server.Service;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Properties;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.modelmbean.ModelMBeanInfo;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import static util.UserManagement.conn;

/**
 *
 * @author Ginanjar
 */
public class UserManagementWLS extends Service {

//    public static void main(String[] args) {
//        createWeblogicUser("bapuk again");
//    }
    Connection conn;

    public void createWeblogicUser(String username, String fullName, String password, String email, String group,String authType) {
        try {
            DatabaseValue dValue = new DatabaseValue();
            String domainUserPasswordWeblogic = "";
            String hostnameUrm = "";
            String portConsole = "";
            try {
                domainUserPasswordWeblogic = dValue.getPheConfig("DOMAIN_USER_PASSWORD_WEBLOGIC_BARU");
                hostnameUrm = dValue.getPheConfig("HOSTNAME_URM");
                portConsole = dValue.getPheConfig("PORT_CONSOLE");
            } catch (DataException ex) {
                Log.info("PHE:connect:error:" + ex.getMessage());
            }

            String splitConfigWeblogic[] = domainUserPasswordWeblogic.split(";");
            String usernameWeblogic = splitConfigWeblogic[1];
            String passwordWeblogic = splitConfigWeblogic[2];
            
            Hashtable<String, String> env = new Hashtable<String, String>();
            env.put(Context.SECURITY_PRINCIPAL, usernameWeblogic);
            env.put(Context.SECURITY_CREDENTIALS, passwordWeblogic);
            env.put(Context.PROVIDER_URL, "t3://"+hostnameUrm+":"+ portConsole);
            int port = Integer.valueOf(portConsole);
            String protocol = "rmi";
            String url = new String("/jndi/iiop://"+hostnameUrm+":"+portConsole+"/weblogic.management.mbeanservers.domainruntime");

            JMXServiceURL serviceURL = new JMXServiceURL(protocol, hostnameUrm, port, url);
            JMXConnector connector = JMXConnectorFactory.connect(serviceURL, env);
            MBeanServerConnection connection = connector.getMBeanServerConnection();

            ObjectName userEditor = null;
            ObjectName mBeanTypeService = new ObjectName("com.bea:Name=MBeanTypeService,Type=weblogic.management.mbeanservers.MBeanTypeService");
            ObjectName rs = new ObjectName("com.bea:Name=DomainRuntimeService,Type=weblogic.management.mbeanservers.domainruntime.DomainRuntimeServiceMBean");
            ObjectName domainMBean = (ObjectName) connection.getAttribute(rs, "DomainConfiguration");
            ObjectName securityConfig = (ObjectName) connection.getAttribute(domainMBean, "SecurityConfiguration");
            ObjectName defaultRealm = (ObjectName) connection.getAttribute(securityConfig, "DefaultRealm");
            ObjectName[] authProviders = (ObjectName[]) connection.getAttribute(defaultRealm, "AuthenticationProviders");

            for (ObjectName providerName : authProviders) {
                Log.info("Auth provider is: " + providerName);

                if (userEditor == null) {
                    ModelMBeanInfo info = (ModelMBeanInfo) connection.getMBeanInfo(providerName);
                    String className = (String) info.getMBeanDescriptor().getFieldValue("interfaceClassName");
                    Log.info("className is: " + className);

                    if (className != null) {
                        String[] mba = (String[]) connection.invoke(mBeanTypeService, "getSubtypes", new Object[]{"weblogic.management.security.authentication.UserEditorMBean"}, new String[]{"java.lang.String"});
                        for (String mb : mba) {
                            Log.info("Model Bean is: " + mb);
                            if (className.equals(mb)) {
                                Log.info("Found a macth for the model bean and class name!");
                                userEditor = providerName;
                            }
                        }
                    }
                }
            }

            if (userEditor == null) {
                throw new RuntimeException("Could not retrieve user editor");
            }

            try {
                connection.invoke(userEditor, "createUser", new Object[]{username, password, "User created programmatically via java method (Business Contact)."}, new String[]{"java.lang.String", "java.lang.String", "java.lang.String"});
                Log.info("PHE:createWeblogicUser:createUser:" + username + " success");
//                connection.invoke(userEditor, "addMemberToGroup", new Object[]{group, username}, new String[]{"java.lang.String", "java.lang.String"});
//                Log.info("PHE:createWeblogicUser:addMemberToGroup:" + username + " success");

                // Create User in ECM
                dValue.insertUser(username, fullName, email, password, authType);
                Log.info("PHE:createWeblogicUser:insertUserToUCM:" + username + " success." );

                // Assign in ECM
                dValue.insertUserSecurityAttributes(username, "#none", "account", "15");
                Log.info("PHE:createWeblogicUser:insertUserSecurityAccess:" + "#none " + " success." );
                dValue.insertUserSecurityAttributes(username, group, "role", "15");
                Log.info("PHE:createWeblogicUser:insertUserSecurityAccess:" + group + " success." );


                Log.info("User created successfully");
                
            } catch (Exception e) {
                Log.error("PHE:createWeblogicUser:makeEverything:error:" + e.getMessage());
            }

            connector.close();

        } catch (Exception ex) {
            Log.error("PHE:createWeblogicUser:ex:"+ ex.getMessage());
        }
    }
}