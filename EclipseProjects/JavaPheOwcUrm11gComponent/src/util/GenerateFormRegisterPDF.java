/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import intradoc.common.Log;
import intradoc.server.Service;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.ElementHandlerPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;


public class GenerateFormRegisterPDF extends Service{
//private static Font catFont = new Font(Font.FontFamily.HELVETICA, 16,Font.BOLD);
    public static final String CSS ="td { margin:30px;padding-left:10px;}";
    public static final String CSSE ="td { margin:30px;padding-left:10px;}table.listdoc { margin:30px;float:center;font-family: Arial, Verdana, san-serif;vertical-align: middle;   border-spacing: 0;    width: 100%;    }"+
			"td.listdoc {margin:30px; vertical-align: middle;   border-top: 0.5px solid #000000;border-left: 0.5px solid #000000;border-right: 0.5px solid #000000;    border-bottom: 0.5px solid #000000;    padding: 8px;	font-size:12px;    }";//+

	public void generateDoc() {
    	 Log.info("GenerateFormRegisterPDF filepath:"+m_binder.getLocal("pheMailAttachName"));
    	 String gDocName    = m_binder.getLocal("pheGenPdfDocName");
    	 String gInitiator= m_binder.getLocal("pheGenPdfInitiator");
    	 String gOwner    = m_binder.getLocal("pheGenPdfOwner");
    	 String gDivision= m_binder.getLocal("pheGenPdfDivision");
    	 String gDepartment    = m_binder.getLocal("pheGenPdfDepartment");
    	 String gTitle= m_binder.getLocal("pheGenPdfTitle");
    	 String gReason    = m_binder.getLocal("pheGenPdfReason");
    	 String gCompany= m_binder.getLocal("pheGenPdfCompany");
    	 String gDocNumber    = m_binder.getLocal("pheGenPdfDocNumber");
    	 String gAccept= m_binder.getLocal("pheGenPdfAccept");
    	 String gRemark    = m_binder.getLocal("pheGenPdfRemark");
    	 String gAppDueDate    = m_binder.getLocal("pheGenPdfAppDueDate");
    	 String curDateInitiator    = m_binder.getLocal("pheGenPdfcurDateInitiator");
    	 String curDateOwner    = m_binder.getLocal("pheGenPdfcurDateOwner");
    	 String curDateTdc    = m_binder.getLocal("pheGenPdfcurDateTdc");
    	 String gExtendDate    = m_binder.getLocal("pheGenPdfExtendDate");
    	 String gExtendReason    = m_binder.getLocal("pheGenPdfExtendReason");
    	 String extDateInitiator    = m_binder.getLocal("pheGenPdfextDateInitiator");
    	 String extDateOwner    = m_binder.getLocal("pheGenPdfextDateOwner");
    	 String extDateTdc    = m_binder.getLocal("pheGenPdfextDateTdc");
    	 String approve="uncheck";
    	 String reject="uncheck";
    	 String ttdInitiator = "whitespace";
    	 String ttdOwner = "whitespace";
    	 String ttdTdc = "whitespace";
    	 String ttdInitiator1 = "whitespace";
    	 String ttdOwner1 = "whitespace";
    	 String ttdTdc1 = "whitespace";
    	 DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    	 Date date = new Date();
    	// String curDateInitiator = "";
    	// String curDateOwner = "";
    	// String curDateTdc = "";
			if(gAccept.contains("Initiator Approved")){
				approve="check";
				reject="uncheck";
				ttdInitiator = "approved";
				ttdOwner = "whitespace";
				ttdTdc = "whitespace";
			//	curDateInitiator = dateFormat.format(date);
//		    	curDateOwner = curDateOwner;
//		    	curDateTdc = "";
			}else if(gAccept.contains("Supervisor Approved")){
				approve="check";
				reject="uncheck";
				ttdInitiator = "approved";
				ttdOwner = "approved";
				ttdTdc = "whitespace";
//				curDateInitiator = "";
			//	curDateInitiator;
		//    	curDateOwner = dateFormat.format(date);
//		    	curDateTdc = "";
			}else if(gAccept.contains("Registered")){
				approve="check";
				reject="uncheck";
				ttdInitiator = "approved";
				ttdOwner = "approved";
				ttdTdc = "approved";
//				curDateInitiator = "";
//		    	curDateOwner = "";
		//    	curDateTdc = dateFormat.format(date);
			}else if(gAccept.contains("Reject")){
				approve="uncheck";
				reject="check";
				ttdInitiator = "whitespace";
				ttdOwner = "whitespace";
				ttdTdc = "whitespace";
			}else if(gAccept.contains("Cancelled")){
				approve="uncheck";
				reject="check";
				ttdInitiator = "whitespace";
				ttdOwner = "whitespace";
				ttdTdc = "whitespace";
			}else if(gAccept.equalsIgnoreCase("Extended")){
				approve="check";
				reject="uncheck";
				ttdInitiator = "approved";
				ttdOwner = "approved";
				ttdTdc = "approved";				
			}else if(gAccept.equalsIgnoreCase("Extended Approved by Initiator")){
				approve="check";
				reject="uncheck";
				ttdInitiator = "approved";
				ttdOwner = "approved";
				ttdTdc = "approved";
				ttdInitiator1 = "approved";
			}else if(gAccept.equalsIgnoreCase("Extended Approved by Supervisor")){
				approve="check";
				reject="uncheck";
				ttdInitiator = "approved";
				ttdOwner = "approved";
				ttdTdc = "approved";	
				ttdInitiator1 = "approved";
				ttdOwner1 = "approved";	
			}else if(gAccept.equalsIgnoreCase("Extended Approved by TDC")){
				approve="check";
				reject="uncheck";
				ttdInitiator = "approved";
				ttdOwner = "approved";
				ttdTdc = "approved";	
				ttdInitiator1 = "approved";
				ttdOwner1 = "approved";	
				ttdTdc1 = "approved";
			}
			Log.info("GenerateFormRegisterPDF pheGenPdfcurDateInitiator:"+m_binder.getLocal("pheGenPdfcurDateInitiator"));
			Log.info("GenerateFormRegisterPDF pheGenPdfcurDateOwner:"+m_binder.getLocal("pheGenPdfcurDateOwner"));
			Log.info("GenerateFormRegisterPDF pheGenPdfcurDateTdc:"+m_binder.getLocal("pheGenPdfcurDateTdc"));
			gReason = gReason.replaceAll("\n", "<br/>");
			gRemark = gRemark.replaceAll("\n", "<br/>");
			gReason = gReason.replaceAll("<br>", "<br/>");
			gRemark = gRemark.replaceAll("<br>", "<br/>");
   			String file = "D:\\PHE_TEMP_NEWDOC\\"+gDocName+".pdf";
    	// step 1
        Document document = new Document(PageSize.A4, 25, 0, 25, 25);
        // step 2
        try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
			 // step 3
	        document.open();
	        // step 4
	      
	        document.add(Chunk.NEWLINE);
	        PdfPTable table = getTable(gDocName,gInitiator,gOwner,gDivision,gDepartment,gTitle,gReason,gCompany,gDocNumber,gAccept,gRemark,gAppDueDate,approve,reject,ttdInitiator,ttdOwner,ttdTdc,curDateInitiator,curDateOwner,curDateTdc);
	        document.add(table);
	        PdfPTable tableExtend = getTableExtend(gInitiator,gCompany,gOwner,gReason,gExtendReason, gExtendDate,ttdInitiator1, ttdOwner1, ttdTdc1, extDateInitiator, extDateOwner, extDateTdc,gDocName);
	        if(gAccept.contains("Extended")){				
				document.newPage();
		        document.add(tableExtend);
			}
	        // step 5
	        document.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
    }
 
    public PdfPTable getTable(String gDocName, String gInitiator, String gOwner, String gDivision, String gDepartment, String gTitle, String gReason, String gCompany, String gDocNumber, String gAccept, String gRemark, String gAppDueDate, String approve, String reject, String ttdInitiator,String ttdOwner,String ttdTdc,String curDateInitiator,String curDateOwner,String curDateTdc) throws IOException {
 
    	String 	htmlTemp = "";
 	    htmlTemp = htmlTemp + "<html>";
 	    htmlTemp = htmlTemp + "<body style='margin:30px;font-family: Arial Narrow, Verdana, san-serif;'>";
 	    htmlTemp = htmlTemp + "<table style='margin:20px;font-family: Arial Narrow;border: 1px solid #000000;' width='95%'>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>";
 	    htmlTemp = htmlTemp + "<table>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td width='20%'><img src='D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PHE_NEWDOC_REGISTRATION\\img\\logo.jpg' style='weigth: 221px; height: 71px' /></td>";
 	    htmlTemp = htmlTemp + "</tr>";
            htmlTemp = htmlTemp + "<tr>";
            htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
            htmlTemp = htmlTemp + "</tr>";
            htmlTemp = htmlTemp + "<tr>";
            htmlTemp = htmlTemp + "<td style='text-align: center; font-size: 20px; font-weight: bold'>Pengajuan Dokumen Baru <br /><i>New Document Registration Form</i><br/><p style='font-weight: normal; font-size: 14px !important;margin-top:10px !important;'>Nomor : "+gDocName+" </p></td>";
	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: center; font-size: 12px; font-weight: bold'>";
 	    htmlTemp = htmlTemp + "<td colspan='2'></td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "</table> <br /> <br />";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>";
 	    htmlTemp = htmlTemp + "<table style='border-color: #000000;'1px'>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td colspan='3' style='font-weight: bold'>A. Diisi olehPengaju Dokumen / Filled by Document Initiator* </td> <br />";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='24%' style='vertical-align: middle;'>Nama yang mengajukan /<br/><i>Initiator Name</i></td>";
 	    htmlTemp = htmlTemp + "<td width='3%'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gInitiator+" <br /></td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='24%' style='vertical-align: middle;margin:50px'>Nama Penanggung Jawab /<br/><i>Owner Name</i></td>";
 	    htmlTemp = htmlTemp + "<td width='3%' style='vertical-align: middle;'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gOwner+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='24%' style='vertical-align: middle;'>Divisi / <i>Division</i></td>";
 	    htmlTemp = htmlTemp + "<td width='3%' style='vertical-align: middle;'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gDivision+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='24%' style='vertical-align: middle;'>Departemen / <i>Department</i></td>";
 	    htmlTemp = htmlTemp + "<td width='3%' style='vertical-align: middle;'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gDepartment+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='24%' style='vertical-align: middle;'>Judul / <i>Title</i></td>";
 	    htmlTemp = htmlTemp + "<td width='3%' style='vertical-align: middle;'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gTitle+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='24%' style='vertical-align: middle;'>Alasan Pengajuan / <br /> <i>Reason of Initiate</i></td>";
 	    htmlTemp = htmlTemp + "<td width='3%' style='vertical-align: middle;'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gReason+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='24%' style='vertical-align: middle;'>Anak Perusahaan / <i>Company</i></td>";
 	    htmlTemp = htmlTemp + "<td width='3%' style='vertical-align: middle;'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gCompany+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td colspan='3' style='font-weight: bold'>B. Pendaftaran Diisi oleh / <i>Registration Filled by Document Controller Non Drawing</i></td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: center; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='10%'></td>";
 	    htmlTemp = htmlTemp + "<td width='80%' colspan='2' style='text-align: left !important; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<table style='margin-right:30px !important'>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td width='30%' style='text-align: center !important; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<img src='D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PHE_NEWDOC_REGISTRATION\\img\\"+approve+".png' /> Diterima / <i>Acceptable</i></td>";
 	    htmlTemp = htmlTemp + "<td width='30%' style='text-align: left !important; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<img src='D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PHE_NEWDOC_REGISTRATION\\img\\"+reject+".png' /> Tidak Diterima / <i>Not Acceptable</i></td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "</table> <br />";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px ;vertical-align: middle;''>";
 	    htmlTemp = htmlTemp + "<td width='24%'>Nomor Dokumen /<br/>Document Number</td>";
 	    htmlTemp = htmlTemp + "<td width='3%'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gDocNumber+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='24%'>Keterangan / <i>Remark</i><br /> (jika tidak diterima / <br/><i>if not acceptable)</i></td>";
 	    htmlTemp = htmlTemp + "<td width='3%'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gRemark+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td width='24%'>Batas Waktu Pengesahan / Approval Due Date</td>";
 	    htmlTemp = htmlTemp + "<td width='3%'>:</td>";
 	    htmlTemp = htmlTemp + "<td width='73%' style='vertical-align: middle;border-bottom: 1px solid #000000; padding-bottom:5px !important'>"+gAppDueDate+" (tidak lebih dari 6 bulan setelah pendaftaran / <i>not more than 6 months after registered</i>)**</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "</table>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td><br />";
 	    htmlTemp = htmlTemp + "<p style='text-align: left; font-size: 12px; font-weight: bold'>Catatan / <i>Notes: </i><br />";
 	    htmlTemp = htmlTemp + "</p>";
 	    htmlTemp = htmlTemp + "<p style='text-align: left; font-size: 12px'>* Rancangan Dokumen harus dilampirkan / <i>Draft Document shall be Attached</i><br />";
 	    htmlTemp = htmlTemp + " ** Jika batas waktu pengesahan dokumen melebihi 6 bulan, maka mengisi form Perpanjangan Waktu Pengesahan /<br/><i>If approval due date is more than 6 months, fill the form of Approval Date Extension</i><br />";
 	    htmlTemp = htmlTemp + "</p> <br /> <br /></td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>";
 	    htmlTemp = htmlTemp + "<table>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td>Pengaju / <i>Initiator</i></td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;</td>";
 	    htmlTemp = htmlTemp + "<td>Penanggung Jawab / <i>Owner</i></td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;</td>";
 	    htmlTemp = htmlTemp + "<td>Document Control "+gCompany+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td>Tanggal / <i>Date: </i>"+curDateInitiator+"</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;</td>";
 	    htmlTemp = htmlTemp + "<td>Tanggal / <i>Date: </i>"+curDateOwner+"</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;</td>";
 	    htmlTemp = htmlTemp + "<td>Tanggal / <i>Date: </i>"+curDateTdc+"</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td style='text-align: left; font-size: 12px'><img src='D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PHE_NEWDOC_REGISTRATION\\img\\"+ttdInitiator+".png' /></td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;&nbsp;&nbsp;</td>";
 	    htmlTemp = htmlTemp + "<td style='text-align: left; font-size: 12px'><img src='D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PHE_NEWDOC_REGISTRATION\\img\\"+ttdOwner+".png' /></td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;&nbsp;&nbsp;</td>";
 	    htmlTemp = htmlTemp + "<td style='text-align: left; font-size: 12px'><img src='D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PHE_NEWDOC_REGISTRATION\\img\\"+ttdTdc+".png' /></td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
 	    htmlTemp = htmlTemp + "<td style='border-top: 1px solid #000000;'>Nama / <i>Name</i> : "+gInitiator+"</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;&nbsp;&nbsp;</td>";
 	    htmlTemp = htmlTemp + "<td style='border-top: 1px solid #000000;'>Nama / <i>Name</i> : "+gOwner+"</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;&nbsp;&nbsp;</td>";
 	    htmlTemp = htmlTemp + "<td style='border-top: 1px solid #000000;'>Nama / <i>Name</i> : </td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "<tr>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "</table>";
 	    htmlTemp = htmlTemp + "</td>";
 	    htmlTemp = htmlTemp + "</tr>";
 	    htmlTemp = htmlTemp + "</table>";
 	    htmlTemp = htmlTemp + "</body>";
 	    htmlTemp = htmlTemp + "</html>";
		
		StringBuilder sb = new StringBuilder(htmlTemp);
        CSSResolver cssResolver = new StyleAttrCSSResolver();
        CssFile cssFile = XMLWorkerHelper.getCSS(new ByteArrayInputStream(CSS.getBytes()));
        cssResolver.addCss(cssFile);
 
        // HTML
        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
 
        // Pipelines
        ElementList elements = new ElementList();
        ElementHandlerPipeline pdf = new ElementHandlerPipeline(elements, null);
        HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
        CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
 
        // XML Worker
        XMLWorker worker = new XMLWorker(css, true);
        XMLParser p = new XMLParser(worker);
        p.parse(new ByteArrayInputStream(sb.toString().getBytes()));
        return (PdfPTable)elements.get(0);
		
		
       
    }
    
    public PdfPTable getTableExtend(String gInitiator, String gCompany, String gOwner,String gReason,String gRemark, String gAppDueDate,String ttdInitiator, String ttdOwner, String ttdTdc, String curDateInitiator, String curDateOwner, String curDateTdc, String gDocName) throws IOException {
      	 
	    String 	htmlTemp = "";
	    htmlTemp = htmlTemp + "<html>";
	    htmlTemp = htmlTemp + "<body style='margin:30px;font-family: Arial, Verdana, san-serif;'>";
	    htmlTemp = htmlTemp + "<table style='margin:20px;font-family: Arial;border: 1px solid #000000;' width='95%'>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>";
	    htmlTemp = htmlTemp + "<table>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td width='70%' style='text-align: center; font-size: 18px; font-weight: bold'>Perpanjangan Waktu Pengesahan / <i>Approval Date Extension</i><br/>(jika diperlukan / <i>if necessary</i>)";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr style='text-align: justify; font-size: 12px; color:red'>";
	    htmlTemp = htmlTemp + "<td >Catatan / Note: <br/>";
	    htmlTemp = htmlTemp + "Permintaan Perpanjangan Waktu Pengesahan hanya bisa dilakukan sebanyak 1 x 6 bulan, jika lebih, maka dokumen harus diajukan kembali dengan mengisi form Pengajuan Dokumen Baru. Form Pengajuan yang sudah dilakukan, akan dibatalkan / <i>Request of Approval Date Extension can be done as much as 1 time for 6 months, if more than 1 time, shall fill in the new form. The existing form will be cancelled.</i>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td> &nbsp; ";	    
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr style='text-align: Center; font-size: 12px;'>";
	    htmlTemp = htmlTemp + "<td> Nomor : "+gDocName;	    
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "</table>"; 	    
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>";
	    htmlTemp = htmlTemp + "<table class='listdoc'>";
	    htmlTemp = htmlTemp + "<tr class='listdoc'>	";
	    htmlTemp = htmlTemp + "<td width='30%' class='listdoc'><br/>Alasan / <i>Reason</i><br/>&nbsp;";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td width='70%' class='listdoc'>"+gRemark;
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr class='listdoc'>";
	    htmlTemp = htmlTemp + "<td width='30%' class='listdoc'>Batas Waktu Pengesahan Baru / <br/><i>New Approval Due Date</i>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td width='70%' class='listdoc'>"+gAppDueDate;
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "</table>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>";
	    htmlTemp = htmlTemp + "<table>";
	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
	    htmlTemp = htmlTemp + "<td>Pengaju / <i>Initiator</i></td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;</td>";
	    htmlTemp = htmlTemp + "<td>Penanggung Jawab / <i>Owner</i></td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;</td>";
	    htmlTemp = htmlTemp + "<td>Document Control "+gCompany+"</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
	    htmlTemp = htmlTemp + "<td>Tanggal / <i>Date: </i>"+curDateInitiator+"</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;</td>";
	    htmlTemp = htmlTemp + "<td>Tanggal / <i>Date: </i>"+curDateOwner+"</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;</td>";
	    htmlTemp = htmlTemp + "<td>Tanggal / <i>Date: </i>"+curDateTdc+"</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
	    htmlTemp = htmlTemp + "<td style='border-bottom: 1px solid #000000;text-align: left; font-size: 12px'><img src='D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PHE_NEWDOC_REGISTRATION\\img\\"+ttdInitiator+".png' /></td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;&nbsp;&nbsp;</td>";
	    htmlTemp = htmlTemp + "<td style='border-bottom: 1px solid #000000;text-align: left; font-size: 12px'><img src='D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PHE_NEWDOC_REGISTRATION\\img\\"+ttdOwner+".png' /></td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;&nbsp;&nbsp;</td>";
	    htmlTemp = htmlTemp + "<td style='border-bottom: 1px solid #000000;text-align: left; font-size: 12px'><img src='D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PHE_NEWDOC_REGISTRATION\\img\\"+ttdTdc+".png' /></td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr style='text-align: left; font-size: 12px'>";
	    htmlTemp = htmlTemp + "<td style='vertical-align: middle;'>Nama / <i>Name</i> : "+gInitiator+"</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;&nbsp;&nbsp;</td>";
	    htmlTemp = htmlTemp + "<td style='vertical-align: middle;'>Nama / <i>Name</i> : "+gOwner+"</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;&nbsp;&nbsp;</td>";
	    htmlTemp = htmlTemp + "<td style='vertical-align: middle;'>Nama / <i>Name</i> : </td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "<tr>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "<td>&nbsp;<br/>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "</table>";
	    htmlTemp = htmlTemp + "</td>";
	    htmlTemp = htmlTemp + "</tr>";
	    htmlTemp = htmlTemp + "</table>";
	    htmlTemp = htmlTemp + "</body>";
	    htmlTemp = htmlTemp + "</html>";
	
	StringBuilder sb = new StringBuilder(htmlTemp);
    CSSResolver cssResolver = new StyleAttrCSSResolver();
    CssFile cssFile = XMLWorkerHelper.getCSS(new ByteArrayInputStream(CSSE.getBytes()));
    cssResolver.addCss(cssFile);

    // HTML
    HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
    htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());

    // Pipelines
    ElementList elements = new ElementList();
    ElementHandlerPipeline pdf = new ElementHandlerPipeline(elements, null);
    HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
    CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);

    // XML Worker
    XMLWorker worker = new XMLWorker(css, true);
    XMLParser p = new XMLParser(worker);
    p.parse(new ByteArrayInputStream(sb.toString().getBytes()));
    return (PdfPTable)elements.get(0);
	
	
   
}
    
     
}
