package util;

import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.server.Service;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FormDoCheckinSystem
  extends Service
{
  public void DoCheckin()
    throws DataException, ServiceException
  {
    try
    {
      String returnFunction = "";
      Validator validator = new Validator();
      DataBinder binder = new DataBinder();
      DataCollection dc = new DataCollection();
      
      String folderSource = this.m_binder.getLocal("xFolderSource");
      
      binder.putLocal("dDocName", this.m_binder.getLocal("dDocName"));
      
      binder.putLocal("IdcService", "CHECKIN_SEL");
      binder.putLocal("idcToken", this.m_binder.getLocal("idcToken"));
      binder.putLocal("dDocTitle", this.m_binder.getLocal("dDocTitle"));
      binder.putLocal("xDocName", this.m_binder.getLocal("xDocName"));
      binder.putLocal("xCompany", this.m_binder.getLocal("xCompany"));
      
      binder.putLocal("xDocNumber", this.m_binder.getLocal("xDocNumber"));
      binder.putLocal("dDocAuthor", this.m_binder.getLocal("dUser"));
      binder.putLocal("dDocType", this.m_binder.getLocal("dDocType"));
      
      binder.putLocal("dSecurityGroup", this.m_binder.getLocal("dSecurityGroup"));
      binder.putLocal("dRevLabel", "1");
      binder.putLocal("doFileCopy", "1");
      
      binder.putLocal("dID", this.m_binder.getLocal("dID"));
      binder.putLocal("xRevision", this.m_binder.getLocal("xRevision"));
      binder.putLocal("isFinished", this.m_binder.getLocal("isFinished"));
      binder.putLocal("dDocAccount", "");
      binder.putLocal("xStatus", this.m_binder.getLocal("xStatus"));
      binder.putLocal("xAuthor", this.m_binder.getLocal("xAuthor"));
      binder.putLocal("xApprover", this.m_binder.getLocal("xApprover"));
      binder.putLocal("isJavaMethod", "true");
      binder.putLocal("isCustom", "true");
      
      binder.putLocal("createPrimaryMetaFile", "0");
      binder.putLocal("primaryFile", folderSource);
      binder.putLocal("primaryFile:path", folderSource);
      if (!validator.isExistFile(folderSource)) {
        Log.info("PHE:RegistrationDoc:docheckin: file not found");
      }
      returnFunction = dc.executeService(binder, this.m_binder.getLocal("dUser"), true);
      if (returnFunction.length() == 0) {
        returnFunction = " Gagal Checkin: " + binder.getLocal("dID") + ";" + binder.getLocal("dDocName");
      }
      Log.info("PHE:RegistrationDoc:docheckin sel:" + returnFunction);
    }
    catch (Exception ex)
    {
      Log.error("PHE:RegistrationDoc:docheckin sel: failed :ex: " + ex.getMessage().toString());
    }
  }
  
  public void formRegCheckin()
    throws DataException, ServiceException
  {
    String returnFunction = "";
    Validator validator = new Validator();
    DataBinder binder = new DataBinder();
    DataCollection dc = new DataCollection();
    
    String folderSource = this.m_binder.getLocal("primaryFile");
    Log.info("PHE:RegistrationDoc:docheckin:" + folderSource);
    
    binder.putLocal("IdcService", this.m_binder.getLocal("serviceIdc"));
    binder.putLocal("idcToken", this.m_binder.getLocal("idcToken"));
    binder.putLocal("dDocTitle", this.m_binder.getLocal("dDocTitle"));
    binder.putLocal("xDocName", this.m_binder.getLocal("xDocName"));
    binder.putLocal("xCompany", this.m_binder.getLocal("xCompany"));
    
    binder.putLocal("dDocAuthor", this.m_binder.getLocal("dUser"));
    binder.putLocal("dDocType", this.m_binder.getLocal("dDocType"));
    binder.putLocal("dSecurityGroup", this.m_binder.getLocal("dSecurityGroup"));
    binder.putLocal("dRevLabel", "1");
    binder.putLocal("doFileCopy", "1");
    
    DateFormat df = new SimpleDateFormat("MMddyyyyHHmmss");
    Date today = Calendar.getInstance().getTime();
    String reportDate = df.format(today);
    
    binder.putLocal("dDocName", "RF" + reportDate);
    
    binder.putLocal("xRevision", this.m_binder.getLocal("xRevision"));
    binder.putLocal("isFinished", this.m_binder.getLocal("isFinished"));
    binder.putLocal("dDocAccount", "");
    binder.putLocal("xStatus", this.m_binder.getLocal("xStatus"));
    binder.putLocal("xAuthor", this.m_binder.getLocal("xAuthor"));
    binder.putLocal("xApprover", this.m_binder.getLocal("xApprover"));
    binder.putLocal("isJavaMethod", "true");
    binder.putLocal("isCustom", "true");
    
    binder.putLocal("createPrimaryMetaFile", "0");
    binder.putLocal("primaryFile", folderSource);
    binder.putLocal("primaryFile:path", folderSource);
    Log.info("PHE:RegistrationDoc:Checkin Form:Before Try...");
    try
    {
      returnFunction = dc.executeService(binder, this.m_binder.getLocal("dUser"), true);
      if (returnFunction.length() == 0) {
        returnFunction = "Gagal Checkin: " + binder.getLocal("dID") + ";" + binder.getLocal("dDocName");
      }
      Log.info("PHE:RegistrationDoc:Checkin Form:" + returnFunction);
    }
    catch (Exception ex)
    {
      Log.error("PHE:RegistrationDoc:Checkin Form: failed :ex: " + ex.getMessage().toString());
    }
  }
  
  public String newRegCheckIn()
    throws DataException, ServiceException
  {
    Log.info("PHE:RegistrationDoc:Checkin:Start");
    String returnFunction = "";
    
    DataBinder binder = new DataBinder();
    DataCollection dc = new DataCollection();
    
    String folderSource = this.m_binder.getLocal("xFolderSource");
    Log.info("PHE:RegistrationDoc:Checkin:folderSource" + folderSource);
    
    String xAuthor = this.m_binder.getLocal("xAuthor");
    binder.putLocal("xAuthor", xAuthor);
    Log.info("PHE:RegistrationDoc:Checkin:xAuthor:" + xAuthor);
    
    String xApprover = this.m_binder.getLocal("xApprover");
    binder.putLocal("xApprover", this.m_binder.getLocal("xApprover"));
    Log.info("PHE:RegistrationDoc:Checkin:xApprover:" + xApprover);
    
    String xAuthorOrganization = this.m_binder.getLocal("xAuthorOrganization");
    binder.putLocal("xAuthorOrganization", this.m_binder.getLocal("xAuthorOrganization"));
    Log.info("PHE:RegistrationDoc:Checkin:xAuthorOrganization:" + xAuthorOrganization);
    
    String xDepartment = this.m_binder.getLocal("xDepartment");
    binder.putLocal("xDepartment", this.m_binder.getLocal("xDepartment"));
    Log.info("PHE:RegistrationDoc:Checkin:xDepartment:" + xDepartment);
    
    String dDocTitle = this.m_binder.getLocal("dDocTitle");
    binder.putLocal("dDocTitle", this.m_binder.getLocal("dDocTitle"));
    Log.info("PHE:RegistrationDoc:Checkin:dDocTitle:" + dDocTitle);
    
    String xComments = this.m_binder.getLocal("xComments");
    binder.putLocal("xComments", this.m_binder.getLocal("xComments"));
    Log.info("PHE:RegistrationDoc:Checkin:xComments:" + xComments);
    
    String xKategori = this.m_binder.getLocal("xApprover");
    binder.putLocal("xKategori", this.m_binder.getLocal("xKategori"));
    Log.info("PHE:RegistrationDoc:Checkin:xApprover:" + xApprover);
    
    String xDocName = this.m_binder.getLocal("xDocName");
    binder.putLocal("xDocName", this.m_binder.getLocal("xDocName"));
    Log.info("PHE:RegistrationDoc:Checkin:xDocName:" + xDocName);
    
    String dUser = this.m_binder.getLocal("dUser");
    binder.putLocal("dDocAuthor", this.m_binder.getLocal("dUser"));
    Log.info("PHE:RegistrationDoc:Checkin:dUser:" + dUser);
    
    String dDocType = this.m_binder.getLocal("dDocType");
    binder.putLocal("dDocType", this.m_binder.getLocal("dDocType"));
    Log.info("PHE:RegistrationDoc:Checkin:dDocType:" + dDocType);
    
    String dSecurityGroup = this.m_binder.getLocal("dSecurityGroup");
    binder.putLocal("dSecurityGroup", this.m_binder.getLocal("dSecurityGroup"));
    Log.info("PHE:RegistrationDoc:Checkin:dSecurityGroup:" + dSecurityGroup);
    
    String xStatus = this.m_binder.getLocal("xStatus");
    binder.putLocal("xStatus", this.m_binder.getLocal("xStatus"));
    Log.info("PHE:RegistrationDoc:Checkin:xStatus:" + xStatus);
    
    DateFormat df = new SimpleDateFormat("MMddyyyyHHmmss");
    Date today = Calendar.getInstance().getTime();
    String reportDate = df.format(today);
    
    binder.putLocal("dDocName", "NR" + reportDate);
    binder.putLocal("IdcService", "CHECKIN_NEW");
    binder.putLocal("idcToken", this.m_binder.getLocal("idcToken"));
    
    binder.putLocal("dRevLabel", "1");
    
    String xRevision = this.m_binder.getLocal("xRevision");
    binder.putLocal("xRevision", this.m_binder.getLocal("xRevision"));
    Log.info("PHE:RegistrationDoc:Checkin:xRevision:" + xRevision);
    
    binder.putLocal("dDocAccount", "");
    
    binder.putLocal("isJavaMethod", "true");
    binder.putLocal("isCustom", "true");
    
    String primaryFile = binder.getLocal("primaryFile");
    if ((binder.getLocal("GetFileBack") != null) && 
      (binder.getLocal("GetFileBack").equals("true")))
    {
      binder.putLocal("primaryFile:path", binder.getLocal("primaryFile"));
      binder.putLocal("IsJava", "1");
      binder.addTempFile(binder.getLocal("primaryFile:path"));
      File f = new File(binder.getLocal("primaryFile"));
      binder.putLocal("primaryFile", f.getName());
    }
    Log.info("PHE:RegistrationDoc:Checkin:Binder:Before try :");
    Log.info("PHE:RegistrationDoc:Checkin:Binder:" + binder);
    try
    {
      returnFunction = dc.executeService(binder, this.m_binder.getLocal("dUser"), true);
      if (returnFunction.length() == 0) {
        returnFunction = "PHE:RegistrationDoc:CheckIn Registration:Failed:" + binder.getLocal("dID") + ";" + binder.getLocal("dDocName");
      }
      Log.info("PHE:RegistrationDoc:CheckIn Registration:" + returnFunction);
    }
    catch (Exception ex)
    {
      Log.error("PHE:RegistrationDoc:CheckIn Registration: failed :ex: " + ex.getMessage().toString());
    }
    return returnFunction;
  }
}
