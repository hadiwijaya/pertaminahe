/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.data.DataException;
import java.util.*;
import weblogic.management.scripting.utils.WLSTInterpreter;

import org.python.util.InteractiveInterpreter;
import java.sql.*;

/**
 *
 * @author Ginanjar
 */
public class UserManagement {

    static InteractiveInterpreter interpreter = null;
    static Connection conn;

    public UserManagement() {
        Log.info("ABAH:OPEN");
        interpreter = new WLSTInterpreter();
        Log.info("ABAH:CLOSED");
    }

    public static void connect() throws SQLException {
        // connect to WLS
        DatabaseValue dValue = new DatabaseValue();
        String domainUserPasswordWeblogic = "";
        try {
            domainUserPasswordWeblogic = dValue.getPheConfig("DOMAIN_USER_PASSWORD_WEBLOGIC");
        } catch (DataException ex) {
            Log.info("PHE:connect:error:" + ex.getMessage());
        }

        String splitConfigWeblogic[] = domainUserPasswordWeblogic.split(";");
//        String domainWeblogic = splitConfigWeblogic[0];
//        String usernameWeblogic = splitConfigWeblogic[1];
//        String passwordWeblogic = splitConfigWeblogic[2];
//        Log.info("PHE:connect:usernameWeblogic:" + usernameWeblogic);
//        Log.info("PHE:connect:passwordWeblogic:" + passwordWeblogic);
       
        String usernameWeblogic = "weblogic";
        String passwordWeblogic = "welcome1";
        StringBuffer buffer = new StringBuffer();
        
        buffer.append("connect('"+usernameWeblogic+"','"+passwordWeblogic+"', 't3://localhost:7001')");
        
        interpreter.exec(buffer.toString());

        // connect to DB
        String databaseConnection = "";
        try{
            databaseConnection = dValue.getPheConfig("DATABASE_HOSTNAME_PORT_USERNAME_PASSWORD");
        }catch(DataException ex){
        
        }
        String splitDatabaseConnection[] = databaseConnection.split(";");
//        String addressDatabase = splitDatabaseConnection[0];
//        String usernameDatabase = splitDatabaseConnection[1];
//        String passwordDatabase = splitDatabaseConnection[2];
        String addressDatabase = "localhost";
        String usernameDatabase = "PHE2_URMSERVER";
        String passwordDatabase = "oracle";
        Log.info("PHE:connect:user:"+usernameDatabase);
        Log.info("PHE:connect:password:"+passwordDatabase);
        Properties connectionProps = new Properties();
        connectionProps.put("user", usernameDatabase);
        connectionProps.put("password", passwordDatabase);

        conn = DriverManager.getConnection("jdbc:oracle:thin://"+addressDatabase+"/orcl" + "/", connectionProps);
        Log.info("PHE:connect:addressDatabase:" + addressDatabase);
        conn.setCatalog(usernameDatabase);
    }

    public static void createUser(String username, String password, String email, String fullName, String groupName) throws SQLException {
        // Create User in WLS
        StringBuffer buf = new StringBuffer();
        buf.append("serverConfig()\n");
        buf.append("username = '" + username + "' \n");
        buf.append("password = '" + password + "'\n");
        buf.append("description = '" + fullName + "'\n");
        buf.append("atnr=cmo.getSecurityConfiguration().getDefaultRealm().lookupAuthenticationProvider('DefaultAuthenticator')\n");
        buf.append("atnr.createUser(username,password,description)\n");
        buf.append("print 'Add user complete' \n");
        interpreter.exec(buf.toString());

        // Create User in ECM
        Statement myStatement = conn.createStatement();
        String q1 = "insert into Users(dName,dFullName,dEmail,dPasswordEncoding,dPassword,dUserAuthType,dUserArriveDate,dUserChangeDate,dUserLocale,dUserTimezone,UClassifiedMarkings,UBarcode) values ('" + username + "','" + fullName + "','" + email + "','SHA1-CB','" + password + "','LOCAL',SYSDATE,SYSDATE,'English-US','Asia/Jakarta','NoMarkings',UPPER('" + username + "'))";
        System.out.println(q1);
        myStatement.executeQuery(q1);

        // Assign user ke Kantah
        
        assignUserToGroupAndRole(username, groupName);
    }

    public static void removeUser(String username) throws SQLException {
        // Remove User in WLS
        StringBuffer buf = new StringBuffer();
        buf.append("serverConfig()\n");
        buf.append("username = '" + username + "' \n");
        buf.append("atnr=cmo.getSecurityConfiguration().getDefaultRealm().lookupAuthenticationProvider('DefaultAuthenticator')\n");
        buf.append("atnr.removeUser(username)\n");
        buf.append("print 'Remove user complete' \n");
        interpreter.exec(buf.toString());

        // Remove User in ECM
        Statement myStatement = conn.createStatement();
        String q = "DELETE FROM USERS WHERE DNAME ='" + username + "'";
        System.out.println(q);
        myStatement.executeQuery(q);
        myStatement.close();

        Statement myStatement2 = conn.createStatement();
        String q2 = "DELETE FROM UserSecurityAttributes WHERE dUserName='" + username + "'";
        System.out.println(q2);
        myStatement2.executeQuery(q2);
        myStatement2.close();
    }

    public static void createKantah(String kode_kantah, String nama_kantah) throws SQLException {
        // Create Kantah in WLS
        StringBuffer buf = new StringBuffer();
        buf.append("serverConfig()\n");
        buf.append("group = '" + kode_kantah + "' \n");
        buf.append("description ='" + nama_kantah + "'\n");
        buf.append("atnr=cmo.getSecurityConfiguration().getDefaultRealm().lookupAuthenticationProvider('DefaultAuthenticator')\n");
        buf.append("atnr.createGroup(group,description)\n");
        buf.append("print 'Add kantah complete' \n");
        interpreter.exec(buf.toString());

        // Create Security Group in ECM
        Statement myStatement = conn.createStatement();
        String q = "INSERT INTO SECURITYGROUPS (DGROUPNAME,DDESCRIPTION) VALUES ('" + kode_kantah + "','" + nama_kantah + "')";
        System.out.println(q);
        myStatement.executeQuery(q);
        Log.info("============Security Group Added==============");
        myStatement.close();

        // Create Role    
        Statement myStatement2 = conn.createStatement();
        ResultSet myRS = myStatement2.executeQuery("SELECT distinct(DROLENAME) FROM ROLEDEFINITION");
        while (myRS.next()) {
            Statement mySt = conn.createStatement();
            String role = myRS.getString("DROLENAME");
            String q1 = "INSERT INTO ROLEDEFINITION (DROLENAME,DGROUPNAME,DPRIVILEGE) VALUES('" + role + "','" + kode_kantah + "',0)";
            System.out.println(q1);
            mySt.executeQuery(q1);
            mySt.close();
        }
        myRS.close();
        myStatement2.close();
        Log.info("============Role Added 1============");

        Statement myStatement3 = conn.createStatement();
        ResultSet myResultSet = myStatement3.executeQuery("SELECT DISTINCT(DGROUPNAME) FROM SECURITYGROUPS");
        while (myResultSet.next()) {
            Statement mySt = conn.createStatement();
            String q1;
            if (myResultSet.getString("DGROUPNAME").equalsIgnoreCase("Public")) {
                q1 = "INSERT INTO ROLEDEFINITION(DROLENAME,DGROUPNAME,DPRIVILEGE) VALUES('" + kode_kantah + "','Public',1)";
            } else if (myResultSet.getString("DGROUPNAME").equalsIgnoreCase(kode_kantah)) {
                q1 = "INSERT INTO ROLEDEFINITION(DROLENAME,DGROUPNAME,DPRIVILEGE) VALUES('" + kode_kantah + "','" + kode_kantah + "',3)";
                Log.info("masuk");
            } else {
                String temp = myResultSet.getString("DGROUPNAME");
                q1 = "INSERT INTO ROLEDEFINITION(DROLENAME,DGROUPNAME,DPRIVILEGE) VALUES('" + kode_kantah + "','" + temp + "',0)";
            }
            System.out.println(q1);
            mySt.executeQuery(q1);
            mySt.close();
        }
        myResultSet.close();
        Log.info("============Role Added 2============");

        myStatement3.close();
        Log.info("Complete");
    }

    public static void removeKantah(String kode_kantah) throws SQLException {
        // remove Kantah from WLS
        StringBuffer buf = new StringBuffer();
        buf.append("serverConfig()\n");
        buf.append("group = '" + kode_kantah + "' \n");
        buf.append("atnr=cmo.getSecurityConfiguration().getDefaultRealm().lookupAuthenticationProvider('DefaultAuthenticator')\n");
        buf.append("atnr.removeGroup(group)\n");
        buf.append("print 'Remove kantah complete' \n");
        interpreter.exec(buf.toString());

        // remove Kantah from ECM

        // removing Security Group
        Statement myStatement = conn.createStatement();
        String q1 = "DELETE FROM SECURITYGROUPS WHERE DGROUPNAME ='" + kode_kantah + "'";
        System.out.println(q1);
        myStatement.executeQuery(q1);
        myStatement.close();

        // removing Role Definition
        Statement myStatement2 = conn.createStatement();
        String q2 = "DELETE FROM ROLEDEFINITION WHERE DGROUPNAME ='" + kode_kantah + "'";
        System.out.println(q2);
        myStatement2.executeQuery(q2);
        myStatement2.close();

        Statement myStatement3 = conn.createStatement();
        String q3 = "DELETE FROM ROLEDEFINITION WHERE DROLENAME ='" + kode_kantah + "'";
        System.out.println(q3);
        myStatement3.executeQuery(q3);
        myStatement3.close();
    }

    public static void assignUserToGroupAndRole(String username, String groupName) throws SQLException {
        // Assign in WLS
        StringBuffer buf = new StringBuffer();
        buf.append("serverConfig()\n");
        buf.append("atnr=cmo.getSecurityConfiguration().getDefaultRealm().lookupAuthenticationProvider('DefaultAuthenticator')\n");
        buf.append("atnr.addMemberToGroup('" + groupName + "','" + username + "')    \n");
        buf.append("print 'Assign user complete' \n");
        interpreter.exec(buf.toString());

        // Assign in ECM
        //contonh kalo misalkan masukin 2 data...
//        Statement myStatement = conn.createStatement();
//        String q1 = "insert into UserSecurityAttributes(dUserName,dAttributeName,dAttributeType,dAttributePrivilege) values('" + username + "','#none','account',3)";
//        System.out.println(q1);
//        myStatement.executeQuery(q1);
//        myStatement.close();

        Statement myStatement2 = conn.createStatement();
        String q2 = "insert into UserSecurityAttributes(dUserName,dAttributeName,dAttributeType,dAttributePrivilege) values('" + username + "','" + groupName + "','role',3)";
        System.out.println(q2);
        myStatement2.executeQuery(q2);
        myStatement2.close();
    }

    
    //remove user dari group ..
//    private static void removeUserFromKantah(String username, String kode_kantah) throws SQLException {
//        StringBuffer buf = new StringBuffer();
//        buf.append("serverConfig()\n");
//        buf.append("atnr=cmo.getSecurityConfiguration().getDefaultRealm().lookupAuthenticationProvider('DefaultAuthenticator')\n");
//        buf.append("atnr.removeMemberFromGroup('" + kode_kantah + "','" + username + "')    \n");
//        buf.append("print 'Remove user from kantah complete' \n");
//        interpreter.exec(buf.toString());
//
//        Statement myStatement2 = conn.createStatement();
//        String q2 = "DELETE FROM UserSecurityAttributes WHERE dUserName='" + username + "' AND dAttributeName='" + kode_kantah + "'";
//        System.out.println(q2);
//        myStatement2.executeQuery(q2);
//
//    }

    public static void main(String[] args) {
        new UserManagement();
        try {
            connect();
            //createKantah("121212","Kantah 121212");
            createUser("jimmy", "welcome1", "dias@dias.com", "Jimmy Ramadhan", "121212");
            //removeUser("diazz");
            //removeUser("diazzz");
            //removeUser("diazzzz");
            //removeKantah("121212");
            //removeUserFromKantah("siska","2803");
            //getAllUser();
            //getAllGroup();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
