/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.server.Service;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;

//import sun.misc.BASE64Decoder;
import utils.system;

import java.io.File;
import java.io.FileOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.binary.Base64;
/**
 *
 * @author NandaY
 */
public class PHESendMailPublish extends Service{
     public void pheSendMailPublish() {
    	 Log.info("yuhu filepath");
    	 Log.info("yuhu2 filepath:"+m_binder.getLocal("pheMailAttachName"));
    	 String attcName= m_binder.getLocal("pheMailAttachName");
    	 String attcen    = m_binder.getLocal("pheMailAttach");
	     String to      = m_binder.getLocal("pheMailTo");
	     String message = m_binder.getLocal("pheMailMessage");
	     String subject = m_binder.getLocal("pheMailSubject");
	     String sender 	= m_binder.getLocal("pheMailSender");
	     String locRecipient 	= m_binder.getLocal("pheMailLocRecipient");
        DatabaseValue dValue = new DatabaseValue();
        String emailFrom = "";
        try {

            String usernameAuthenticator = dValue.getPheConfig("ANNOUNCE_EMAIL_USERNAME");
            String passwordAuthenticator = dValue.getPheConfig("ANNOUNCE_EMAIL_PASSWORD");
            String smtpHostName = dValue.getPheConfig("SMTP_EMAIL");
            Log.info("sethostname:"+smtpHostName);

            emailFrom = dValue.getPheConfig("TDC_EMAIL");
            HtmlEmail email = new HtmlEmail();
            email.setHostName(smtpHostName);
            email.setSmtpPort(25);
            email.setAuthenticator(new DefaultAuthenticator(usernameAuthenticator, passwordAuthenticator));
            email.setTLS(false);
            //email.setSSLOnConnect(true);
            email.setFrom(emailFrom);
            email.setSubject(subject);
       //     email.setHtmlMsg(message);
            String toSplit[] = to.split(",");
            for (int i = 0; i < toSplit.length; i++) {
                //added by nanda - 180614
                //cek dapat alamat email ato ngga
                if (toSplit[i].length() > 0) {
                    email.addTo(toSplit[i]);
                }
            }

           // email.setHtmlMsg(message);
            email.setTextMsg(message);
            if(attcen.equalsIgnoreCase("")||attcen==null||attcName.equalsIgnoreCase("")||attcName==null){            	
            }else{
            	Base64 decoder2 = new Base64();
	            byte[] decoded2 = decoder2.decode(attcen.getBytes());
	            String attc = new String(decoded2);
	            Log.info("yuhu decoded2 done");
            	String firstSplit[] = attc.split("@");
            	String filenameSplit[] = attcName.split("\\|");
	            for(int i=0;i<firstSplit.length;i++){
	            	String attcSplit[] = firstSplit[i].split(",");
	    	            Base64 decoder = new Base64();
	    	            byte[] decoded = decoder.decode(attcSplit[1].getBytes());
	    	            Log.info("yuhu decoded3 done");
	    	            File fileAttch = new File("D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PublishAttachment\\"+filenameSplit[i]);
	    	            FileOutputStream fos = null;
	    	            fos = new FileOutputStream(fileAttch);
	    	            fos.write(decoded);
	    	            fos.close();
	    	            Log.info("yuhu file created : "+filenameSplit[i]);
	    	            email.attach(fileAttch);
	            
	            }  
            }
           // String abc="arewrwer@ewettrwt.rwerwer;erwer@eroiuoiu.ooii";
           // String subj = subject.replace(" ","-");
        //    subj =subject.replace("\n","");
            File recipientFile = new File("D:\\Oracle\\Middleware\\user_projects\\domains\\base_domain\\ucm\\urm\\weblayout\\PublishAttachment\\recipient\\"+locRecipient+".txt");
            byte[] byteRecipient = to.getBytes();
            FileOutputStream fo = new FileOutputStream(recipientFile);
            fo.write(byteRecipient);
            fo.close();
            
            email.send();
            dValue.insertEmailLog(emailFrom, to, subject, "Success");
        } catch (Exception e) {
            Log.error("PHE:--a:sendMail:ex:" + e.getMessage());
            dValue.insertEmailLog(emailFrom, to, subject, "Failed");
            e.printStackTrace();
        }
    }
    
     
}
