/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import data.DatabaseValue;
import intradocservice.Document;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradoc.server.ServiceManager;
import java.io.PrintStream;
import org.apache.commons.lang.ArrayUtils;

/**
 *
 * @author AkhmadH
 */
public class DeleteFolder extends Service {

    public String folderGUID(String folderPath)
            throws DataException, ServiceException {
        Workspace workspace = getWorkspace();
        DataBinder requestData = new DataBinder();
        requestData.putLocal("IdcService", "FLD_INFO");
        requestData.putLocal("path", folderPath);

        String folderGUID = null;

        executeService(requestData, workspace);
        ResultSet result = requestData.getResultSet("FolderInfo");

        folderGUID = result.getStringValueByName("fFolderGUID");
        if (folderGUID.isEmpty()) {
            System.out.println("Folderpath " + folderPath + " not found");
        } else {
            System.out.println("Folder " + folderPath + " has GUID: "
                    + folderGUID);
        }
        return folderGUID;
    }

    public void deleteFolder()
            throws DataException, ServiceException {
        Workspace workspace = getWorkspace();
        Log.info("DSWF: Starting Delete Folder");
        String isRoutine = null;
        isRoutine = this.m_binder.getLocal("isRoutine");
        String idcToken = null;
        idcToken = this.m_binder.getLocal("idcToken");
        String dswfCategory = null;
        dswfCategory = this.m_binder.getLocal("dswfCategory");

        String poTitle = null;
        String Path = null;

        if (dswfCategory.equals("NonRoutine")) {
            poTitle = this.m_binder.getLocal("PROJECT_TITLE");
            if (poTitle != null) {
                Path = "/Document Secure Workflow/Non Routine/" + poTitle;
            }
        } else if (dswfCategory.equals("Routine")) {
            poTitle = this.m_binder.getLocal("ORGANIZATION_NAME");
            if (poTitle != null) {
                Path = "/Document Secure Workflow/Routine/" + poTitle;
            }
        }

        Log.info("idcToken =..." + idcToken);
        Log.info("Project/Organization Title =..." + poTitle);
        Log.info("Path =  " + Path);

        Log.info("DSWF:Path =... " + Path);
        System.out.println(Path);
        DataBinder requestData = new DataBinder();
        requestData.putLocal("IdcService", "FLD_DELETE");
        requestData.putLocal("item1", "fFolderGUID:" + folderGUID(Path));

        if ((Path != "/Document Secure Workflow/Routine/") || (Path != "/Document Secure Workflow/Non Routine/")) {
            executeService(requestData, workspace);
        }
    }

    //nanda 200516
    public void deleteDocTransmittal() throws DataException, ServiceException {
        DataResultSet docRS = null;
        Workspace workspace = getWorkspace();
        Document document = new Document();
        Log.info("DSWF: Starting Delete Document Transmittal");

        String transmittalID = null;
        transmittalID = this.m_binder.getLocal("TRANSMITTAL_ID");

        DatabaseValue dValue = new DatabaseValue();
        ResultSet rs = dValue.getTransmittalDocName(transmittalID);
        docRS = new DataResultSet();
            docRS.copy(rs);
            Log.info("deleteDocTransmittal: emailRS:" + docRS);
//null pointer whyyy?lin 106
        if (!docRS.isEmpty()) {
            do {                       
                document.deleteDoc(docRS.getStringValue(0), m_binder.getLocal("dUser"), workspace);
                Log.info("delete doc transmittal :"+docRS.getStringValue(0));
            } while (docRS.next());
        }

    }

    public void deleteTransmittal()
            throws DataException, ServiceException {
        Workspace workspace = getWorkspace();
        Log.info("DSWF: Starting Delete Folder");

        String idcToken = null;
        idcToken = this.m_binder.getLocal("idcToken");
        //String dswfCategory = null;
        //dswfCategory = this.m_binder.getLocal("dswfCategory");
        String isRoutine = null;
        isRoutine = this.m_binder.getLocal("isRoutine");
        String poTitle = null;
        poTitle = this.m_binder.getLocal("project");
        String transmittalID = null;
        transmittalID = this.m_binder.getLocal("TRANSMITTAL_ID");

        String poPath = null;
        if (poTitle != null) {
            if (isRoutine.equals("false")) {
                poPath = "/Document Secure Workflow/Non Routine/" + poTitle + "/" + transmittalID;
            } else if (isRoutine.equals("true")) {
                poPath = "/Document Secure Workflow/Routine/" + poTitle + "/" + transmittalID;
            }
        }
        Log.info("idcToken =..." + idcToken);
        Log.info("Project/Organization Title =..." + poTitle);
        Log.info("Transmittal ID =..." + transmittalID);

        Log.info("DSWF:Path =... " + poPath);
        System.out.println(poPath);
        DataBinder requestData = new DataBinder();
        requestData.putLocal("IdcService", "FLD_DELETE");
        requestData.putLocal("item1", "fFolderGUID:" + folderGUID(poPath));

        if ((poPath != "/Document Secure Workflow/Routine/") || (poPath != "/Document Secure Workflow/Non Routine/")) {
            executeService(requestData, workspace);
        }
    }

    public static void executeService(DataBinder serviceBinder, Workspace workspace)
            throws ServiceException {
        try {
            serviceBinder.setEnvironmentValue("REMOTE_USER", "sysadmin");
            ServiceManager smg = new ServiceManager();
            smg.init(serviceBinder, workspace);
            smg.processCommand();
            smg.clear();
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }
    
    public static void executeServiceByUser(DataBinder serviceBinder, Workspace workspace, String dUser)
            throws ServiceException {
        try {
            serviceBinder.setEnvironmentValue("REMOTE_USER", dUser);
            ServiceManager smg = new ServiceManager();
            smg.init(serviceBinder, workspace);
            smg.processCommand();
            smg.clear();
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }
    
    public void updateDocInfoMultiple()
            throws DataException, ServiceException {
        Workspace workspace = getWorkspace();
        Log.info("EDMS: Starting Update Doc Multiple");

        String idcToken = null;
        idcToken = this.m_binder.getLocal("idcToken");
        String dDocName = null;
        dDocName = this.m_binder.getLocal("dDocName");
        String dID = null;
        dID = this.m_binder.getLocal("dID");
        String xOldDocNumberData = null;
        xOldDocNumberData = this.m_binder.getLocal("xOldDocNumberData");
        String xAuthorData = null;
        xAuthorData = this.m_binder.getLocal("xAuthorData");
        String xAuthorOrganizationData = null;
        xAuthorOrganizationData = this.m_binder.getLocal("xAuthorOrganizationData");
        String xAuthorGroupNameData = null;
        xAuthorGroupNameData = this.m_binder.getLocal("xAuthorGroupNameData");
        String xProjectNameData = null;
        xProjectNameData = this.m_binder.getLocal("xProjectNameData");
        String xContractorData = null;
        xContractorData = this.m_binder.getLocal("xContractorData");
        String xWONumberData = null;
        xWONumberData = this.m_binder.getLocal("xWONumberData");
        String xCommentsData = null;
        xCommentsData = this.m_binder.getLocal("xCommentsData");
        String xRetentionPeriodData = null;
        xRetentionPeriodData = this.m_binder.getLocal("xRetentionPeriodData");
        String xRetentionDateData = null;
        xRetentionDateData = this.m_binder.getLocal("xRetentionDateData");
        String dDocAccountData= null;
        dDocAccountData = this.m_binder.getLocal("dDocAccountData");
        String dSecurityGroupData = null;
        dSecurityGroupData = this.m_binder.getLocal("dSecurityGroupData");
        String xRemarksData = null;
        xRemarksData = this.m_binder.getLocal("xRemarksData");
        String xContractNumberData = null;
        xContractNumberData = this.m_binder.getLocal("xContractNumberData");
        String metadataUpd = null;
        metadataUpd = this.m_binder.getLocal("metadataUpd");
        String countData = null;
        int countDataInt = 0;
        countData = this.m_binder.getLocal("countData");
        countDataInt = Integer.parseInt(countData);
        String userLogin = null;
        userLogin = this.m_binder.getLocal("userLogin");
        
        Log.info("UpdateMultiple:userLogin =... " + userLogin);
        
        Log.info("idcToken =..." + idcToken);
        Log.info("dDocName =..." + dDocName);
        Log.info("dID =..." + dID);
        Log.info("Metadata to Update =..." + metadataUpd);
        Log.info("countDataInt =..." + countDataInt);

        String arrDocName[] = dDocName.split("_");
        String arrID[] = dID.split("_");
        String arrMetadata[] = metadataUpd.split(",");
        
        for(int i=0; i<countDataInt; i++) {
            DataBinder requestData = new DataBinder();
            requestData.putLocal("IdcService", "UPDATE_DOCINFO");
            requestData.putLocal("dDocName", arrDocName[i]);
            requestData.putLocal("dID", arrID[i]);
            if (ArrayUtils.contains(arrMetadata, "xOldDocNumber")){
                requestData.putLocal("xOldDocNumber", xOldDocNumberData);
            }
            if (ArrayUtils.contains(arrMetadata, "xAuthor")){
                requestData.putLocal("xAuthor", xAuthorData);
            }
            if (ArrayUtils.contains(arrMetadata, "xAuthorOrganization")){
                requestData.putLocal("xAuthorOrganization", xAuthorOrganizationData);
            }
            if (ArrayUtils.contains(arrMetadata, "xAuthorGroupName")){
                requestData.putLocal("xAuthorGroupName", xAuthorGroupNameData);
            }
            if (ArrayUtils.contains(arrMetadata, "xProjectName")){
                requestData.putLocal("xProjectName", xProjectNameData);
            }
            if (ArrayUtils.contains(arrMetadata, "xContractor")){
                requestData.putLocal("xContractor", xContractorData);
            }
            if (ArrayUtils.contains(arrMetadata, "xWONumber")){
                requestData.putLocal("xWONumber", xWONumberData);
            }
            if (ArrayUtils.contains(arrMetadata, "xComments")){
                requestData.putLocal("xComments", xCommentsData);
            }
            if (ArrayUtils.contains(arrMetadata, "xComments")){
                requestData.putLocal("xComments", xCommentsData);
            }
            if (ArrayUtils.contains(arrMetadata, "xRetentionPeriod")){
                requestData.putLocal("xRetentionPeriod", xRetentionPeriodData);
            }
            if (ArrayUtils.contains(arrMetadata, "xRetentionDate")){
                requestData.putLocal("xRetentionDate", xRetentionDateData);
            }
            if (ArrayUtils.contains(arrMetadata, "dDocAccount")){
                requestData.putLocal("dDocAccount", dDocAccountData);
            }
            if (ArrayUtils.contains(arrMetadata, "dSecurityGroup")){
                requestData.putLocal("dSecurityGroup", dSecurityGroupData);
            }
            if (ArrayUtils.contains(arrMetadata, "xRemarks")){
                requestData.putLocal("xRemarks", xRemarksData);
            }
            if (ArrayUtils.contains(arrMetadata, "xContractNumber")){
                requestData.putLocal("xContractNumber", xContractNumberData);
            }
            requestData.putLocal("dUser", userLogin);
            Log.info("PHE:UpdateDocInfoMultiple:before try:");

            try {
		Log.info("PHE:UpdateDocInfoMultiple:binder:" + requestData);
                executeServiceByUser(requestData, workspace, userLogin);

            } catch (Exception ex) {
                Log.error("PHE:UpdateDocInfoMultiple:ex:" + ex.getMessage().toString());

            }
        }
    }
    
    public void queryDataToUpdateMultiple() throws DataException, ServiceException{
        //Workspace ws = getSystemWorkspace();
        String idcToken = null;
        idcToken = this.m_binder.getLocal("idcToken");
        //String dDocName = null;
        //dDocName = this.m_binder.getLocal("dDocName");
        String dID = null;
        dID = this.m_binder.getLocal("dID");
        String countData = null;
        int countDataInt = 0;
        countData = this.m_binder.getLocal("countData");
        countDataInt = Integer.parseInt(countData);
        //String arrDocName[] = dDocName.split("_");
        String arrID[] = dID.split("_");
        
        DataBinder contentBinder = new DataBinder();

        DatabaseValue dValue = new DatabaseValue();
        //Log.info("PHE:queryInboxTicket:role:" + Role);

        //Log.info("PHE:queryInboxTicket:if:role:" + Role);
        String query = "";
        query = query + "select * from revisions r, docmeta d where r.did=d.did and r.drevrank=0 and r.did in (";
        for(int i=0; i<countDataInt; i++) {
            if (i==0) {
                query = query + arrID[i];
            } else {
                query = query + ", " + arrID[i];
            }
            
        }
        query = query + ")";
        
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        ResultSet resultSet = ws.createResultSetSQL(query);

        /*if ((resultSet == null) || (resultSet.isEmpty())) {
            return resultSet;
        }*/
        
        //ResultSet rSet = dValue.getTicketByAuthor(ItemStatus1, ItemStatus2, User, Role);
        DataResultSet myDataResultSet = new DataResultSet();
        myDataResultSet.copy(resultSet);
        this.m_binder.addResultSet("pheListToUpdateRS", myDataResultSet);
    }
    
    public void deleteMultipleDoc()
            throws DataException, ServiceException {
        Workspace workspace = getWorkspace();
        Log.info("EDMS: Starting Delete Doc Multiple");

        String idcToken = null;
        idcToken = this.m_binder.getLocal("idcToken");
        String dDocName = null;
        dDocName = this.m_binder.getLocal("dDocName");
        String dID = null;
        dID = this.m_binder.getLocal("dID");
        String dDocType = null;
        dDocType = this.m_binder.getLocal("dDocType");
        String dRevLabel = null;
        dRevLabel = this.m_binder.getLocal("revNum");
        String xStatus = null;
        xStatus = this.m_binder.getLocal("xStatus");
        String dStatus = null;
        dStatus = this.m_binder.getLocal("dStatus");
        String isForceDelete = null;
        isForceDelete = this.m_binder.getLocal("isForceDelete");
        String countData = null;
        int countDataInt = 0;
        countData = this.m_binder.getLocal("countData");
        countDataInt = Integer.parseInt(countData);
        String userLogin = null;
        userLogin = this.m_binder.getLocal("userLogin");
        
        Log.info("DeleteMultiple:userLogin =... " + userLogin);
        
        //Log.info("idcToken =..." + idcToken);
        Log.info("dDocName =..." + dDocName);
        Log.info("dID =..." + dID);
        //Log.info("Metadata to Update =..." + metadataUpd);
        Log.info("countDataInt =..." + countDataInt);

        String arrDocName[] = dDocName.split("_");
        String arrID[] = dID.split("_");
        String arrRevLabel[] = dRevLabel.split("_");
        String arrDocType[] = dDocType.split("_");
        String arrxStatus[] = xStatus.split("_");
        String arrdStatus[] = dStatus.split("_");
        String strxStatus = "";
        String strdStatus = "";
        
        String dReason = null;
        dReason = this.m_binder.getLocal("dreason");
        String strQuery = "insert into phe_delete_log (xdocname, xdocnumber, ddoctitle, ddoctype, deletedate, username, xdocpurpose, deletereason, did, deleteStatus) ";
        strQuery = strQuery + "select d.xdocname, d.xdocnumber, r.ddoctitle, r.ddoctype, sysdate, '"+ userLogin +"', d.xdocpurpose, '" + dReason + "', d.did, 'Success' from docmeta d, revisions r where d.did = r.did and d.did in (";
        int countDataIntComa = countDataInt-1;
        for(int i=0; i<countDataInt; i++) {
            strQuery = strQuery + "'" + arrID[i] + "'";
            if (countDataInt != 1) {
                if (i!=countDataIntComa) {
                    strQuery = strQuery + ",";
                }
            }
        }
        strQuery = strQuery + ")";
        Log.info("PHE:DeleteMultipleDoc:queryLog:" + strQuery);
        
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();

        String returnExecute = "";
        try {
            returnExecute = String.valueOf(ws.executeSQL(strQuery));
        } catch (Exception ex) {
            Log.error("PHE:DeleteMultipleDoc:Insert Log" + ex);
        }
        
        DataCollection dCollLog = new DataCollection();
        Workspace wsLog = dCollLog.getSystemWorkspace();

        String logFailed = "";
        String strQueryFailed ="";

        for(int i=0; i<countDataInt; i++) {
            //Log.info("PHE:DeleteMultipleDoc:arrRevLabel:before try:" + arrRevLabel[i]);
            //Log.info("PHE:DeleteMultipleDoc:arrDocType:before try:" + arrDocType[i]);
            if (Integer.parseInt(arrRevLabel[i]) > 1) {
                DataBinder requestData = new DataBinder();
                requestData.putLocal("IdcService", "DELETE_REV");
                requestData.putLocal("dDocName", arrDocName[i]);
                requestData.putLocal("dID", arrID[i]);
                requestData.putLocal("isForceDelete", isForceDelete);
                requestData.putLocal("idcToken", idcToken);
                requestData.putLocal("dUser", userLogin);
                Log.info("PHE:DeleteMultipleDoc:delete rev:before try:");

                try {
                    Log.info("PHE:DeleteMultipleDoc:delete rev 1:binder:" + requestData);
                    executeServiceByUser(requestData, workspace, userLogin);

                } catch (Exception ex) {
                    Log.error("PHE:DeleteMultipleDoc:ex:" + ex.getMessage().toString());

                }
            } else {
                strxStatus = arrxStatus[i];
                strdStatus = arrdStatus[i];
                //Log.info("PHE:DeleteMultipleDoc:strxStatus" + strxStatus + ":strdStatus:" + strdStatus);
                //Log.info("PHE:DeleteMultipleDoc:strxStatus" + strxStatus + ":strdStatus:" + strdStatus);
                if ((arrDocType[i].equals("NonDrawing") || arrDocType[i].equals("Drawing") || arrDocType[i].equals("NonDrawingPassive")) && !strxStatus.equals("Deleted") && !strdStatus.equals("EXPIRED")) {
                    DataBinder requestData = new DataBinder();
                    requestData.putLocal("IdcService", "PHE_CHECKIN_SIMILAR");
                    requestData.putLocal("dDocName", arrDocName[i]);
                    requestData.putLocal("dID", arrID[i]);
                    requestData.putLocal("dUser", userLogin);
                    requestData.putLocal("idcToken", idcToken);
                    Log.info("PHE:DeleteMultipleDoc:phe checkin similar:before try:");

                    try {
                        Log.info("PHE:DeleteMultipleDoc:phe checkin similar:binder:" + requestData);
                        executeServiceByUser(requestData, workspace, userLogin);

                    } catch (Exception ex) {
                        Log.error("PHE:DeleteMultipleDoc:ex:" + ex.getMessage().toString());

                    } 
                } else {
                    DataBinder requestData = new DataBinder();
                    requestData.putLocal("IdcService", "DELETE_REV");
                    requestData.putLocal("dDocName", arrDocName[i]);
                    requestData.putLocal("dID", arrID[i]);
                    requestData.putLocal("isForceDelete", isForceDelete);
                    requestData.putLocal("dUser", userLogin);
                    requestData.putLocal("idcToken", idcToken);
                    Log.info("PHE:DeleteMultipleDoc:delete rev:before try:");

                    try {
                        Log.info("PHE:DeleteMultipleDoc:delete rev2:binder:" + requestData);
                        executeServiceByUser(requestData, workspace, userLogin);
                        
                    } catch (Exception ex) {
                        Log.error("PHE:DeleteMultipleDoc:ex:" + ex.getMessage().toString());

                    }
                }
                
            }
        }
    }
    
    public void queryDataLogDelete() throws DataException, ServiceException{
        //Workspace ws = getSystemWorkspace();
        String idcToken = null;
        //idcToken = this.m_binder.getLocal("idcToken");
        //String dDocName = null;
        //dDocName = this.m_binder.getLocal("dDocName");
        String dID = null;
        dID = this.m_binder.getLocal("dID");
        String countData = null;
        int countDataInt = 0;
        countData = this.m_binder.getLocal("countData");
        countDataInt = Integer.parseInt(countData);
        String arrID[] = dID.split("_");
        
        DataBinder contentBinder = new DataBinder();

        DatabaseValue dValue = new DatabaseValue();
        //Log.info("PHE:queryInboxTicket:role:" + Role);

        //Log.info("PHE:queryInboxTicket:if:role:" + Role);
        String query = "";
        query = query + "select * from phe_delete_log where did in (";
        for(int i=0; i<countDataInt; i++) {
            if (i==0) {
                query = query + arrID[i];
            } else {
                query = query + ", " + arrID[i];
            }
        }
        query = query + ")";
        
        Log.info("PHE:queryDataLogDelete:query:" + query);
        DataCollection dColl = new DataCollection();
        Workspace ws = dColl.getSystemWorkspace();
        ResultSet resultSet = ws.createResultSetSQL(query);

        DataResultSet myDataResultSet = new DataResultSet();
        myDataResultSet.copy(resultSet);
        this.m_binder.addResultSet("pheListDeletedRS", myDataResultSet);
    }
    
}
