package PindahRevisi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import intradoc.common.DataStreamWrapper;
import intradoc.common.ExecutionContext;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.common.SystemUtils;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.provider.Provider;
import intradoc.provider.Providers;
import intradoc.server.FileService;
import intradoc.server.Service;
import intradoc.server.ServiceData;
import intradoc.server.ServiceHandler;
import intradoc.server.ServiceManager;
import intradoc.server.UserStorage;
import intradoc.shared.UserData;


public class PindahRevisi extends Service  {
	private static final String IDC_SERVICE = "IdcService";
	private static final String FORCE_DOWNLOAD_STREAM_TO_FILE_PATH = "ForceDownloadStreamToFilepath";
	private static final String GET_FILE = "GET_FILE";
	private static final String D_ID = "dID";

	public FileService executeFileService(final DataBinder serviceBinder, final Service m_service) throws DataException, ServiceException {
	    final String serviceName = serviceBinder.getLocal(IDC_SERVICE);
	    final ServiceData serviceData = ServiceManager.getFullService(serviceName);
	    final FileService service = (FileService) ServiceManager.createService(serviceData.m_classID, m_service.getWorkspace(), m_service.getOutput(), serviceBinder, serviceData);
	    service.setConditionVar(FORCE_DOWNLOAD_STREAM_TO_FILE_PATH, true);
	    service.initDelegatedObjects();
	    m_service.getRequestImplementor().doRequestInternalEx(service, m_service.getUserData(), false);
	    return service;
	}

//	public void runGetFile() throws DataException, ServiceException {
//	    DataBinder serviceBinder = new DataBinder();
//	    serviceBinder.putLocal(IDC_SERVICE, GET_FILE);
//	    serviceBinder.putLocal(D_ID, "1");
//	    executeFileService(serviceBinder, m_service);
//
//	    final DataStreamWrapper streamWrapper = m_service.getDownloadStream(false);
//	    final String filePath = streamWrapper.m_filePath;
//	    Log.info("filePath="+filePath);

	    // Do whatever we want with the file

	    // Remove this zip file after we exit this service (this assumes we did what we wanted with the file above)
	    //m_binder.m_tempFiles.remove(zipFilePath);
//	}
	
	public void downloadDocument() throws DataException, ServiceException {


	    SystemUtils.trace("system", "downloadDocumentInWorkflow starting");
	    String dID = "1";// m_binder.getLocal("dID");
	    String contentID ="AGITVMAGITCOM1000001";// m_binder.getLocal("dDocName");
	    String serviceName = "GET_FILE";
	    String userName = m_binder.getLocal("dUser");

	    SystemUtils.trace("system", "dID-->" + dID);
	    SystemUtils.trace("system", "contentID-->" + contentID);
	    SystemUtils.trace("system", "User:" + userName);

	    DataBinder serviceBinder = new DataBinder();

	    serviceBinder.putLocal("dID", dID);
	    serviceBinder.putLocal("dDocName", contentID);
	    serviceBinder.putLocal("IdcService", serviceName);
	    Workspace workspace = getSystemWorkspace();

	    try {
	      SystemUtils.trace("system", "started");
	      ServiceData serviceData = ServiceManager.getFullService(serviceName);
	      workspace = getSystemWorkspace();
	      Service service =
	        ServiceManager.createService(serviceData.m_classID, workspace, null,
	                                     serviceBinder, serviceData);
	      UserData fullUserData = getFullUserData("weblogic",service,workspace);
	      service.setUserData(fullUserData);
	      serviceBinder.m_environment.put("REMOTE_USER", userName);
	      service.initDelegatedObjects();
	      service.executeSafeServiceInNewContext(serviceName, true);
	      SystemUtils.trace("system", "succeeded");
	      String path = (String)service.getCachedObject("PrimaryFilePath");
	      SystemUtils.trace("system", "Path-->" + path);
	      Log.info("filePath="+path);
	      
	      File source = new File(path);
	        File dest = new File("/home/oracle/"+contentID+"pdf");
	        copyFileUsingChannel(source, dest);
	      
	    } catch (Exception e) {
	      SystemUtils.trace("system", "failed");
	    }

	  }
	
    public Workspace getSystemWorkspace() {
        Workspace workspace = null;
        Provider wsProvider = Providers.getProvider("SystemDatabase");
        if (wsProvider != null) {
            workspace = (Workspace) wsProvider.getProvider();
        }
        return workspace;
    }
    
    public UserData getFullUserData(String userName, ExecutionContext cxt, Workspace ws) throws DataException, ServiceException {
        if (ws == null) {
            ws = getSystemWorkspace();
        }
        UserData userData = UserStorage.retrieveUserDatabaseProfileDataFull(userName, ws, null, cxt, true, true);
//        ws.releaseConnection();
        return userData;
    }
    
    private static void copyFileUsingChannel(File source, File dest) throws IOException {
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destChannel = new FileOutputStream(dest).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
           }finally{
               sourceChannel.close();
               destChannel.close();
       }
    }
}
