/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Markup;

import static ScheduledEvent.ScheduledEvt.pheSendMail;
import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradocservice.Users;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.provider.Provider;
import intradoc.provider.Providers;
import intradoc.server.Service;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.DataCollection;

/**
 *
 * @author AkhmadH
 */
public class DeleteMarkup extends Service {

    public DeleteMarkup() {
        super();
    }

    public Workspace getSystemWorkspace() {
        Workspace workspace = null;
        Provider wsProvider = Providers.getProvider("SystemDatabase");
        if (wsProvider != null) {
            workspace = (Workspace) wsProvider.getProvider();
        }
        return workspace;
    }

    public String updateMarkupDocInfo() throws DataException, ServiceException {
        Workspace ws = getSystemWorkspace();
        String dID = this.m_binder.getLocal("dID");
        String dDocName = this.m_binder.getLocal("dDocName");
        String dRevLabel = this.m_binder.getLocal("dRevLabel");
        String dSecurityGroup = this.m_binder.getLocal("dSecurityGroup");
        String dUser = this.m_binder.getLocal("dUser");
        String dDocTitle = this.m_binder.getLocal("dDocTitle");
        String dDocAuthor = this.m_binder.getLocal("dDocAuthor");
        String pdDocName = this.m_binder.getLocal("pdID");
        String markupCreateDate = this.m_binder.getLocal("dCreateDate2");
        String returnFunction = "";
        DateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd G 'at' HH:mm:ss z");
        Date date = new Date();

        DataCollection dColl = new DataCollection();
        DatabaseValue dValue = new DatabaseValue();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "UPDATE_DOCINFO");
        binders.putLocal("dID", dID);
        binders.putLocal("dDocName", dDocName);
        binders.putLocal("dSecurityGroup", dSecurityGroup);
        binders.putLocal("dRevLabel", dRevLabel);
        //binders.putLocal("xContractor", xContractor);
        binders.putLocal("xMarkup_BasedID", "0");

        String insertDeleteLog = "insert into phe_deletemarkup_actionlog (dDocName, markupTitle, markupAuthor, markupCreateDate, parentDocName, deleteBy, deleteDate) values ('";
        insertDeleteLog = insertDeleteLog + dDocName + "', '";
        insertDeleteLog = insertDeleteLog + dDocTitle + "', '";
        insertDeleteLog = insertDeleteLog + dDocAuthor + "', ";
        insertDeleteLog = insertDeleteLog + "TO_DATE('" + markupCreateDate + "','yyyy-MM-dd'), '";
        insertDeleteLog = insertDeleteLog + pdDocName + "', '";
        insertDeleteLog = insertDeleteLog + dUser + "', "; //delete by
        insertDeleteLog = insertDeleteLog + "TO_DATE('" + dFormat.format(date) + "','yyyy-MM-dd G 'at' HH:mm:ss') )"; //delete date
        Log.info("PHE:updateMarkupDocInfo:insertLogQuery:" + insertDeleteLog);

        String returnExecute = "";
        try {
            Log.info("PHE:updateMarkupDocInfo:binder:" + binders);
            returnFunction = dColl.executeService(binders, dUser, true);
            Log.info("PHE:updateMarkupDocInfo:returnFunction:" + returnFunction);
            returnExecute = String.valueOf(ws.executeSQL(insertDeleteLog));

        } catch (Exception ex) {
            Log.error("PHE:updateMarkupDocInfo:ex:" + ex.getMessage().toString());
            return "executeFailed:updateStatus";
        }

        return returnFunction;
    }

    public String updateMarkupDocInfoBulk(String interval) throws DataException, ServiceException {
        Workspace ws = getSystemWorkspace();
        DataResultSet result = null;
        String returnFunction = "";
        String returnExecute = "";
        String returnUpdate = "";
        String returnDelete = "";
        String listTemp = "";
        String listMessage = "";
        //nanti ambil dari config
        String strTo="owc.support@pheonwj.pertamina.com";
        String strSubject = "";
        String strMessage = "";
        DateFormat dFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        DateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        Date inDate;
//        Log.info("m_binder print:"+m_binder);
        //comment 300715 gak jalan bikin error
//        String dUser = m_binder.getLocal("dUser");


        String query =
                "select d.did,ddocname,dsecuritygroup,drevlabel,xmarkup_basedid,ddoctitle,dDocAuthor,dindate,(select rev.ddocname from revisions rev where rev.did=d.xmarkup_basedid) as pdDocname,(select rev2.ddoctitle from revisions rev2 where rev2.did=d.xmarkup_basedid) as pdDoctitle \n"
                + "from docmeta d,revisions \n"
                + "where xcomments='AutoVue Markup' and d.did=revisions.did \n"
                + "and ROUND(sysdate-TO_DATE(TO_CHAR(dindate, 'DD-MON-YY'),'DD-MON-YY'))>" + interval + "\n"
                + "and xmarkup_basedid in \n"
                + "(select dc.did from docmeta dc,revisions where (ddoctype='Drawing' or ddoctype='General') \n"
                + "and xdocModel='3D' \n"
                + "and dc.did=revisions.did)";
        Log.info("PHE:updateMarkupDocInfo:querymarkup:" + query);

        ResultSet rs = ws.createResultSetSQL(query);
        result = new DataResultSet();
        result.copy(rs);

        if (!result.isEmpty()) {
            do {
                String dID = result.getStringValue(0);
                String dDocName = result.getStringValue(1);
                String dSecurityGroup = result.getStringValue(2);
                String dRevLabel = result.getStringValue(3);
                String pdID = result.getStringValue(4);
                String dDocTitle = result.getStringValue(5);
                String dDocAuthor = result.getStringValue(6);
                String dInDate = result.getStringValue(7);

                Log.info("dInDate:" + dInDate);
                String pdDocName = result.getStringValue(8);
                String[] tempdInDate = dInDate.split("'");
                String pdInDate = tempdInDate[1];
                Log.info("pdInDate:" + pdInDate);
//            String[] newdInDate = pdInDate.split(".");
                String newdInDate = pdInDate.substring(0, pdInDate.indexOf("."));

                Log.info("newdInDate:" + newdInDate);
                Log.info("pdID:" + pdID);

                DataCollection dColl = new DataCollection();

                DataBinder binders = new DataBinder();
                binders.putLocal("IdcService", "UPDATE_DOCINFO");
                binders.putLocal("dID", dID);
                binders.putLocal("dDocName", dDocName);
                binders.putLocal("dSecurityGroup", dSecurityGroup);
                binders.putLocal("dRevLabel", dRevLabel);
                //binders.putLocal("xContractor", xContractor);
                binders.putLocal("xMarkup_BasedID", "0");

                //updated nanda 210815 - ngelist isi email dimari

                listMessage = listMessage + "<tr>";
                String columnDetailStart = "<td style='background-color:backgroundColor; color:colorText; padding: 8px 10px ;'>";
                String columnDetailEnd = "</td>";

                //markup title
                listMessage = listMessage + columnDetailStart;
                listMessage = listMessage + dDocTitle;
                listMessage = listMessage + columnDetailEnd;

                //markup author
                listMessage = listMessage + columnDetailStart;
                listMessage = listMessage + dDocAuthor;
                listMessage = listMessage + columnDetailEnd;

                //doctitle
                listMessage = listMessage + columnDetailStart;
                listMessage = listMessage + result.getStringValue(9);
                listMessage = listMessage + columnDetailEnd;

                //diindate
                listMessage = listMessage + columnDetailStart;
                listMessage = listMessage + dDocAuthor;
                listMessage = listMessage + columnDetailEnd;

                listMessage = listMessage + "</tr>";

                listTemp = listTemp + listMessage;
                listMessage = "";


                String insertDeleteLog = "insert into phe_deletemarkup_actionlog (dDocName, markupTitle, markupAuthor, markupCreateDate,parentdocname, deleteBy, deleteDate) values ('";
                insertDeleteLog = insertDeleteLog + dDocName + "', '";
                insertDeleteLog = insertDeleteLog + dDocTitle + "', '";
                insertDeleteLog = insertDeleteLog + dDocAuthor + "', ";

                try {
                    inDate = oldFormat.parse(newdInDate);
                    insertDeleteLog = insertDeleteLog + "TO_DATE('" + dFormat.format(inDate) + "','yy-MM-dd HH24:MI:SS'), '";
                } catch (ParseException ex) {
                    Logger.getLogger(DeleteMarkup.class.getName()).log(Level.SEVERE, null, ex);
                    insertDeleteLog = insertDeleteLog + "TO_DATE('" + dFormat.format(date) + "','YY-MM-DD HH24:MI:SS'), '";
                }

                insertDeleteLog = insertDeleteLog + pdDocName + "', '";
                insertDeleteLog = insertDeleteLog + "weblogic" + "', "; //delete by
                insertDeleteLog = insertDeleteLog + "TO_DATE('" + dFormat.format(date) + "','YY-MM-DD HH24:MI:SS') )"; //delete date
                Log.info("PHE:updateMarkupDocInfo:insertLogQuery:" + insertDeleteLog);



                try {

                    Log.info("PHE:updateMarkupDocInfo:binder:" + binders);
                    returnFunction = dColl.executeService(binders, "weblogic", true);
                    Log.info("PHE:updateMarkupDocInfo:returnFunction:" + returnFunction);
                    returnExecute = String.valueOf(ws.executeSQL(insertDeleteLog));


                    Log.info("PHE:updateParentMarkupDocInfoBulk:returnExecute:" + returnExecute);
                    returnUpdate = updateParentMarkupDocInfoBulk(pdID, "weblogic");
                    Log.info("PHE:updateParentMarkupDocInfoBulk:returnUpdate:" + returnUpdate);
                    returnDelete = deleteMarkupDocBulk(dDocName, "weblogic");
                    Log.info("PHE:updateParentMarkupDocInfoBulk:returnDelete:" + returnDelete);

                } catch (Exception ex) {
                    Log.error("PHE:updateMarkupDocInfo:ex:" + ex.getMessage().toString());
                    return "executeFailed:updateStatus";
                }
            } while (result.next());
        }


//updated by nanda 210815 - kirim email di mari
        strSubject = "[PHEONWJ] 3D Document Markup Delete Notification";
        strMessage = "<span style='font-family:Arial'><p>Dear Sir/Madam, <br><br>"
                + "This e-mail is sent to notify you that today is schedule to delete 3D document markup stored in syystem more than " + interval + " days<b>List of deleted markup(s)</b> : ";

        strMessage = strMessage + "<table border='1' style='width:100%'>";
        strMessage = strMessage + "<tr>";
        strMessage = strMessage + "<td style='padding: 8px 10px; background-color:#F0E68C; text-align:center'>";
        strMessage = strMessage + "<h3>Markup Title</h3>";
        strMessage = strMessage + "</td>";

        strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
        strMessage = strMessage + "<h3>Markup Author</h3>";
        strMessage = strMessage + "</td>";

        strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
        strMessage = strMessage + "<h3>Document Title</h3>";
        strMessage = strMessage + "</td>";

        strMessage = strMessage + "<td style='padding: 8px 10px ; background-color:#F0E68C; text-align:center'>";
        strMessage = strMessage + "<h3>In Date</h3>";
        strMessage = strMessage + "</td>";
        strMessage = strMessage + "</tr>";
        strMessage = strMessage + listTemp;

        strMessage = strMessage + "</table>";
        strMessage = strMessage + "<br>";

        strMessage = strMessage + "<p>Thank you for your attention.<br></p>";
        strMessage = strMessage + "<p><b>Technical Document Control</b><br></p></span>";
        
         pheSendMail(strTo, strMessage, strSubject, "");

        return returnFunction;
    }

    public String updateParentMarkupDocInfoBulk(String dID, String dUser) throws DataException, ServiceException {


//        String query = "select docmeta.did,ddocname,xdocname, xmarkupcounter,drevlabel,dsecuritygroup \n"
//                + "from docmeta,revisions \n"
//                + "where (ddoctype='Drawing' or ddoctype='General') \n"
//                + "and xdocModel='3D' \n"
//                + "and docmeta.did=revisions.did\n"
//                + "and docmeta.did in "
//                + "(select xmarkup_basedid from docmeta,revisions where xcomments='AutoVue Markup' and docmeta.did=revisions.did and ROUND(sysdate-TO_DATE(TO_CHAR(dindate, 'DD-MON-YY'),'DD-MON-YY'))>30)";

        Workspace ws = getSystemWorkspace();
        DataResultSet result = null;
//        String dID = this.m_binder.getLocal("pdID");
//        String dDocName = this.m_binder.getLocal("pdDocName");
//        String dRevLabel = this.m_binder.getLocal("pdRevLabel");
//        String dSecurityGroup = this.m_binder.getLocal("pdSecurityGroup");
//        String dUser = this.m_binder.getLocal("dUser");

        String returnFunction = "";

        //update log dikasi ddocname pake where did pasti bisa
        String query = "select xMarkupCounter,ddocname,dsecuritygroup,drevlabel from docmeta,revisions where docmeta.did=revisions.did and docmeta.did = '" + dID + "'";

        Log.info("updateParentMarkupDocInfoBulk:query:" + query);
        ResultSet rs = ws.createResultSetSQL(query);
        result = new DataResultSet();
        result.copy(rs);
        Log.info("updateParentMarkupDocInfoBulk:RS:" + result);
        String rsxMarkupCounter = result.getStringValue(0);
        String dDocName = result.getStringValue(1);
        String dSecurityGroup = result.getStringValue(2);
        String dRevLabel = result.getStringValue(3);
        int xMarkupCount = Integer.parseInt(rsxMarkupCounter) - 1;
        String xMarkupCounter = Integer.toString(xMarkupCount);

        DataCollection dColl = new DataCollection();
        DatabaseValue dValue = new DatabaseValue();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "UPDATE_DOCINFO");
        binders.putLocal("dID", dID);
        binders.putLocal("dDocName", dDocName);
        binders.putLocal("dSecurityGroup", dSecurityGroup);
        binders.putLocal("dRevLabel", dRevLabel);
        binders.putLocal("xMarkupCounter", xMarkupCounter);


        try {
            Log.info("PHE:updateMarkupDocInfo:binder:" + binders);
            returnFunction = dColl.executeService(binders, dUser, true);
            Log.info("PHE:updateMarkupDocInfo:returnFunction:" + returnFunction);
        } catch (Exception ex) {
            Log.error("PHE:updateMarkupDocInfo:ex:" + ex.getMessage().toString());
            return "executeFailed:updateStatus";
        }
        return returnFunction;
    }

    public String updateParentMarkupDocInfo() throws DataException, ServiceException {
        Workspace ws = getSystemWorkspace();
        DataResultSet result = null;
        String dID = this.m_binder.getLocal("pdID");
        String dDocName = this.m_binder.getLocal("pdDocName");
        String dRevLabel = this.m_binder.getLocal("pdRevLabel");
        String dSecurityGroup = this.m_binder.getLocal("pdSecurityGroup");
        String dUser = this.m_binder.getLocal("dUser");

        String returnFunction = "";

        String query = "select xMarkupCounter from docmeta where did = '" + dID + "'";

        ResultSet rs = ws.createResultSetSQL(query);
        result = new DataResultSet();
        result.copy(rs);
        String rsxMarkupCounter = result.getStringValue(0);
        int xMarkupCount = Integer.parseInt(rsxMarkupCounter) - 1;
        String xMarkupCounter = Integer.toString(xMarkupCount);

        DataCollection dColl = new DataCollection();
        DatabaseValue dValue = new DatabaseValue();
        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "UPDATE_DOCINFO");
        binders.putLocal("dID", dID);
        binders.putLocal("dDocName", dDocName);
        binders.putLocal("dSecurityGroup", dSecurityGroup);
        binders.putLocal("dRevLabel", dRevLabel);
        binders.putLocal("xMarkupCounter", xMarkupCounter);


        try {
            Log.info("PHE:updateMarkupDocInfo:binder:" + binders);
            returnFunction = dColl.executeService(binders, dUser, true);
            Log.info("PHE:updateMarkupDocInfo:returnFunction:" + returnFunction);
        } catch (Exception ex) {
            Log.error("PHE:updateMarkupDocInfo:ex:" + ex.getMessage().toString());
            return "executeFailed:updateStatus";
        }
        return returnFunction;
    }

    public String deleteMarkupDoc() {
        Workspace ws = getSystemWorkspace();
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        Log.info("PHE:deleteMarkupDoc");
        String dDocName = this.m_binder.getLocal("dDocName");
        String userLogin = this.m_binder.getLocal("dUser");

        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "DELETE_DOC");
        binders.putLocal("dDocName", dDocName);

        try {
            returnFunction = dColl.executeService(binders, userLogin, true);
            Log.info("PHE:deleteMarkupDoc:returnFunction:" + returnFunction);
            //dColl.executeService(binder, userLogin, false, workspace,false);
        } catch (Exception ex) {
            Log.error("PHE:deleteMarkupDoc:ex:" + ex.getMessage().toString());

            return "executeFailed";
        }

        return returnFunction;
    }

    public String deleteMarkupDocBulk(String dDocName, String userLogin) {
        Workspace ws = getSystemWorkspace();
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        Log.info("PHE:deleteMarkupDoc");
//        String dDocName = this.m_binder.getLocal("dDocName");
//        String userLogin = this.m_binder.getLocal("dUser");

        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "DELETE_DOC");
        binders.putLocal("dDocName", dDocName);

        try {
            returnFunction = dColl.executeService(binders, userLogin, true);
            Log.info("PHE:deleteMarkupDoc:returnFunction:" + returnFunction);
            //dColl.executeService(binder, userLogin, false, workspace,false);
        } catch (Exception ex) {
            Log.error("PHE:deleteMarkupDoc:ex:" + ex.getMessage().toString());

            return "executeFailed";
        }

        return returnFunction;
    }

    public String deleteMarkupRev() {
        Workspace ws = getSystemWorkspace();
        String returnFunction = "";
        DataCollection dColl = new DataCollection();
        Log.info("PHE:deleteMarkupDoc");
        String dDocName = this.m_binder.getLocal("dDocName");
        String userLogin = this.m_binder.getLocal("dUser");

        DataBinder binders = new DataBinder();
        binders.putLocal("IdcService", "DELETE_REV");
        binders.putLocal("dDocName", dDocName);

        try {
            returnFunction = dColl.executeService(binders, userLogin, true);
            Log.info("PHE:deleteMarkupDoc:returnFunction:" + returnFunction);
            //dColl.executeService(binder, userLogin, false, workspace,false);
        } catch (Exception ex) {
            Log.error("PHE:deleteMarkupDoc:ex:" + ex.getMessage().toString());

            return "executeFailed";
        }

        return returnFunction;
    }
}
