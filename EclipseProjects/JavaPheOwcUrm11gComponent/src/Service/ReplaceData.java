/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import data.DatabaseValue;
import intradoc.common.Log;
import intradoc.data.DataException;
import intradoc.server.Service;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Ginanjar
 */
public class ReplaceData extends Service {

	public void replaceDownloadApplet() {
		DatabaseValue dValue = new DatabaseValue();
		String portalhda = "";
		try {
			portalhda = dValue.getPheConfig("PORTAL_HDA_ADDRESS");
		} catch (DataException ex) {
			Log.error("replaceData:ex:" + ex.getMessage());
		}
		String file = "pne_portal.hda";
		String dName = m_binder.getLocal("dUser");
		String profile = "";
		profile = dName.substring(0, 2);
		BufferedReader br = null;
		BufferedWriter bw = null;

		try {
			String line = "", oldtext = "", newtext = "";
			int i = 0;
			File files = new File(portalhda + "\\\\" + profile + "\\\\" + dName + "\\\\" + file);
			BufferedReader reader = new BufferedReader(new FileReader(files));
			while ((line = reader.readLine()) != null) {
				oldtext += line + "\r\n";
				if (line.contains("isDownloadApplet=1")) {
					i++;
				}
			}
			reader.close();
			newtext = oldtext;
			if (i == 0) {
				newtext = oldtext.replaceFirst("@end", "isDownloadApplet=1\n@end");
			}
			FileWriter writer = new FileWriter(portalhda + "\\" + profile + "\\" + dName + "\\pne_portal.hda");
			writer.write(newtext);
			writer.close();
		} catch (Exception e) {
			Log.error("There is no file portal hda for this user+" + e.getMessage());
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				Log.error("No Buffered Reader" + e.getMessage());
			}

			if (bw != null) {
				try {
					bw.close();
				} catch (IOException ex) {
					Log.error("ex.getMessage:" + ex.getMessage());
				}
			}

		}
	}
}
