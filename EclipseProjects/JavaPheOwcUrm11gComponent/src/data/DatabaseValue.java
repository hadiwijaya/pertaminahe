/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import intradoc.common.Log;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.DataResultSet;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradocservice.Folder;
import intradocservice.Util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import util.DataCollection;
import util.Validator;

/**
 *
 * @author Ginanjar
 */
public class DatabaseValue {
	private String uploadTempHeaderMasterlistDrawing = "TDC MASTER LIST DRAWING";
	private String uploadTempHeaderMasterlistNonDrawing = "TDC MASTER LIST NONDRAWING";
	private String uploadTempHeaderGeneral = "DCRMS GENERAL - GENERAL DOCUMENT";
	private String uploadTempHeaderPublic = "DCRMS PHE PUBLIC - PUBLIC DOCUMENT";
	private String uploadTempHeaderDrawing = "DCRMS GENERAL - DRAWING DOCUMENT";
	private String uploadTempHeaderNonDrawing = "DCRMS GENERAL - NONDRAWING DOCUMENT";
	private String uploadTempHeaderExternalResource = "EXTERNAL RESOURCE LIST";

	// add by nanda 180516
	// query semua dokumen selain cover note dapetin ddocname
	// run service buat delete dokumen
	public ResultSet getTransmittalDocName(String transmittalid) throws DataException {
		String query = " SELECT CONTENT_ID AS DDOCNAME FROM PHE_DSWF_DOC WHERE FORMAT IS NOT NULL AND DOC_TITLE NOT LIKE '%Cover Note%' AND TRANSMITTAL_ID='"
				+ transmittalid + "'";
		Log.info("getTransmittalDocName--query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return resultSet;
		}

//        resultSet.first();
//        String ddocname = resultSet.getStringValueByName("DDOCNAME");
		return resultSet;
	}

	public String getPheConfig(String keyconfig) throws DataException {
		String query = "SELECT KEY_VALUE FROM PHE_CONFIG WHERE KEY_CONFIG = '" + keyconfig + "'";
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		// Log.info("PHE:getPheConfig:RS:" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "0";
		}

		resultSet.first();
		String keyValue = resultSet.getStringValueByName("KEY_VALUE");
		// Log.info("PHE:keyValue:" + keyValue);
		return keyValue;
	}

	public String generateRunningNumber(String docClasification, String disciplineCode, String combinationCode)
			throws DataException {
		String query = "select phe_func_set_next_rnumber('" + docClasification + "','" + combinationCode
				+ "') AS NEXTNUMBER from dual";
		Log.info("PHE:generateRunningNumber:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		// Log.info("PHE:getPheConfig:RS:" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			resultSet.first();
			String runningNumber = resultSet.getStringValue(0);
			if (docClasification.equalsIgnoreCase("masterlistdrawing")) {
				String disciplineId = "";
				query = "SELECT ID FROM PHE_D_DISCIPLINE_CODE WHERE CODE = '" + disciplineCode + "'";
				resultSet = ws.createResultSetSQL(query);
				if (!resultSet.isEmpty()) {
					disciplineId = resultSet.getStringValue(0);
					runningNumber = disciplineId + runningNumber;
					return runningNumber;
				} else {
					return "";
				}
			}
			return runningNumber;
		}
	}

	public String getLocation(String area, String type) throws DataException {
		String tableName = "";
		if (type.toUpperCase().contains("NONDRAWING")) {
			tableName = "PHE_NDLOCATION_CODE";
		} else {
			tableName = "PHE_DLOCATION_CODE";
		}
		String query = "SELECT DISTINCT * FROM " + tableName + " WHERE CODE = '" + area + "'";
		// Log.info("PHE:Location:Query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			return area;
		}
	}

	public String getPlatform(String area, String platform) throws DataException {
		String query = "select distinct * from PHE_DPLATFORM_CODE WHERE LOCATION_CODE = '" + area
				+ "' AND PLATFORM_CODE = '" + platform + "'";
		// Log.info("PHE:getPlatform:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			return platform;
		}
	}

	public String getDisciplineCode(String disciplineCode, String type) throws DataException {
		String tableName = "";
		if (type.toUpperCase().contains("NONDRAWING")) {
			tableName = "PHE_ND_DISCIPLINE_CODE";
		} else {
			tableName = "PHE_D_DISCIPLINE_CODE";
		}
		String query = "select distinct * from " + tableName + " WHERE CODE = '" + disciplineCode + "'";
		Log.info("PHE:DisciplineCode:Query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			return disciplineCode;
		}
	}

	public boolean validDisciplineCode(String disciplineCode, String runningNumber, String docType)
			throws DataException {
		if (docType.equalsIgnoreCase("DRAWING") || docType.toLowerCase().contains("masterlistdrawing")
				|| docType.toUpperCase().equals(uploadTempHeaderDrawing)
				|| docType.toUpperCase().equals(uploadTempHeaderMasterlistDrawing)) {
			String tableName = "PHE_D_DISCIPLINE_CODE";
			runningNumber = runningNumber.substring(0, 1);
//        Log.info("PHE:validDisciplineCode:runningNumber:" + runningNumber);
			String query = "SELECT 1 FROM " + tableName + " WHERE ID = '" + runningNumber + "' AND CODE = '" + disciplineCode + "'";
			Log.info("PHE:validDisciplineCode:Query:" + query);
			DataCollection dColl = new DataCollection();
			Workspace ws = dColl.getSystemWorkspace();
			ResultSet resultSet = ws.createResultSetSQL(query);

			if ((resultSet == null) || (resultSet.isEmpty())) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public String getDocumentType(String docType, String type) throws DataException {
		String tableName = "";
		if (type.toUpperCase().contains("NONDRAWING")) {
			tableName = "PHE_NDDOCTYPE";
		} else {
			tableName = "PHE_DDOCTYPE";
		}
		String query = "select distinct * from " + tableName + " WHERE CODE = '" + docType + "'";
		Log.info("PHE:DocumentType:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			return docType;
		}
	}

	public String getDocumentNumberCombination(String area, String platform, String disciplineCode, String docType,
			String runningNumber, String sheetNumber, String type) throws DataException {
		String docNumber = "";

		if (type.equals(uploadTempHeaderMasterlistDrawing)) {
			docNumber = area + "-" + platform + "-" + disciplineCode + "-" + docType;
		} else {
			docNumber = area + "-" + disciplineCode + "-" + docType;
		}
		if (Integer.parseInt(runningNumber) < 10) {
			runningNumber = "000" + Integer.parseInt(runningNumber);
		} else if (Integer.parseInt(runningNumber) < 100) {
			runningNumber = "00" + Integer.parseInt(runningNumber);
		} else if (Integer.parseInt(runningNumber) < 1000) {
			runningNumber = "0" + Integer.parseInt(runningNumber);
		}
		docNumber = docNumber + "-" + runningNumber;

		try {
			if (sheetNumber.length() == 0) {
				sheetNumber = "";
			}
		} catch (Exception ex) {
			sheetNumber = "";
			Log.error("PHE:getDocumentNumberCombination:ex:" + ex.getMessage());
		}
		String query = "select distinct XDOCNUMBER from DOCMETA " + "WHERE XDOCNUMBER = '" + docNumber + "' "
				+ " AND UPPER(XSTATUS) NOT LIKE 'DELETED' ";

		if (sheetNumber.length() > 0) {
			query = query + " AND XSHEETNUMBER = '" + sheetNumber + "'";
		} else {
			query = query + " AND (XSHEETNUMBER IS NULL OR  XSHEETNUMBER = '0')";
		}

		Log.info("PHE:getDocumentNumberCombination:query:" + query);

		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
//            Log.info("PHE:getDocumentNumberCombination:result kosong");
			return "";

		} else {
//            Log.info("PHE:getDocumentNumberCombination:result ada");
			return "already exists";
		}
	}

	public String getDIDDocumentNumberDeteledStatus(String docNumber, String sheetNumber) throws DataException {

		String query = "select distinct DID ,XDOCNUMBER from DOCMETA " + "WHERE XDOCNUMBER = '" + docNumber + "' "
				+ " AND UPPER(XSTATUS) = 'DELETED' ";

		if (sheetNumber.length() > 0) {
			query = query + "AND XSHEETNUMBER = '" + sheetNumber + "'";
		} else {
			query = query + "AND (XSHEETNUMBER IS NULL OR  XSHEETNUMBER = '0')";
		}
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		Log.info("PHE:isDocumentNumberDeteledStatus:query" + query);
		Log.info("PHE:isDocumentNumberDeteledStatus:resultSet" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			resultSet.first();
			return resultSet.getStringValue(0);
		}
	}

	public ResultSet getBatchTemp(String author) throws DataException {

		String query = "select * FROM PHE_BATCH_TEMP WHERE AUTHOR = '" + author + "' ORDER BY TO_NUMBER(INDEXROW)";
		// Log.info("PHE:getMasterListTemp:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		// Log.info("PHE:DocumentNumber:query:" + query);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return resultSet;
		}

		/*
		 * while(resultSet.next()){ Log.info("PHE:insidemethod:" +
		 * resultSet.getStringValue(0)); }
		 */

		return resultSet;
	}

	public String removeBatchTemp(String author) {

		String query = "DELETE FROM PHE_BATCH_TEMP WHERE AUTHOR = '" + author + "' ";
		// Log.info("PHE:getMasterListTemp:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		String returnExecute = "";
		try {
			returnExecute = String.valueOf(ws.executeSQL(query));
		} catch (Exception ex) {
			returnExecute = "FAILED";
			Log.error("PHE:removeMasterListTemp:" + ex);
		}
		// update by nanda - 1510 - disable autocommitmode
//        try {
//            ws.commitTran();
//            ws.releaseConnection();
//        } catch (DataException ex) {
//            Log.error("PHE:DataException:ex:" + ex);
//        }

		return returnExecute;
	}

	public String insertBatchTemp(String typeDrawing, String indexRow, String area, String platform,
			String disciplineCode, String documentType, String runningNumber, String sheetNumber, String documentTitle,
			String oldDocNo, String projectName, String projectYear, String ownerOrganization, String ownerName,
			String author, String remarks, String userLogin, String docTitle, String securityGroup,
			String docClasification, String sheetName, String documentName, String documentNumber,
			String authorOrganization, String company, String retentionPeriod, String buAssetFunction,
			String destinationPath, String documentPurpose, String MOCNo, String authorGroupName, String contractor,
			String contractNo, String WONo, String revision, String status, String activePassive, String retentionDate,
			String error, String docModel, String description) {
		String query = "";
//        Log.info("PHE:insertTDCMasterList:typeDrawing:" + typeDrawing);
		if (typeDrawing.toUpperCase().equals(uploadTempHeaderMasterlistDrawing)) {
			query = "INSERT INTO PHE_BATCH_TEMP " + "(INDEXROW, " + "XDLOCATION," + "XDPLATFORM," + "XDDISCIPLINECODE,"
					+ "XDDOCTYPECODE," + "XRUNNINGNUMBER," + "XSHEETNUMBER," + "XOLDNUMBER,"
					+ "XPROJECTNAME,XPROJECTYEAR,XOWNERORGANIZATION,XOWNERNAME,XAUTHOR,CREATEPRIMARYMETAFILE,XREMARKS,"
					+ "DDOCNAME,DDOCTITLE,DSECURITYGROUP,DDOCAUTHOR,DDOCTYPE,SHEETNAME,TYPE,ERRORREMARKS,AUTHOR) "
					+ "values ('" + indexRow + "','" + area + "','" + platform + "','" + disciplineCode + "','"
					+ documentType + "','" + runningNumber + "','" + sheetNumber + "','" + oldDocNo + "'," + "'"
					+ projectName + "','" + projectYear + "','" + ownerOrganization + "','" + ownerName + "','" + author
					+ "','1','" + remarks + "'," + "'docno','" + docTitle + "','" + securityGroup + "','" + userLogin
					+ "','" + docClasification + "','" + sheetName + "','" + typeDrawing + "','" + error + "','"
					+ userLogin + "')";
		} else if (typeDrawing.toUpperCase().equals(uploadTempHeaderMasterlistNonDrawing)) {
			query = "INSERT INTO PHE_BATCH_TEMP " + "(INDEXROW, " + "XNDLOCATION," + "XNDDISCIPLINECODE,"
					+ "XNDDOCTYPECODE," + "XRUNNINGNUMBER," + "XOLDNUMBER," + "XPROJECTNAME,"
					+ "XPROJECTYEAR,XOWNERORGANIZATION,XOWNERNAME,XAUTHOR,CREATEPRIMARYMETAFILE,XREMARKS,"
					+ "DDOCNAME,DDOCTITLE,DSECURITYGROUP,DDOCAUTHOR,DDOCTYPE,SHEETNAME,TYPE,ERRORREMARKS,AUTHOR) "
					+ "values ('" + indexRow + "','" + area + "','" + disciplineCode + "','" + documentType + "','"
					+ runningNumber + "','" + oldDocNo + "'," + "'" + projectName + "','" + projectYear + "','"
					+ ownerOrganization + "','" + ownerName + "','" + author + "','1','" + remarks + "'," + "'docno','"
					+ docTitle + "','" + securityGroup + "','" + userLogin + "','" + docClasification + "','"
					+ sheetName + "','" + typeDrawing + "','" + error + "','" + userLogin + "')";
		} else if (typeDrawing.toUpperCase().equals(uploadTempHeaderGeneral)) {
			query = "INSERT INTO PHE_BATCH_TEMP " + "(INDEXROW, " + "XDOCNAME, " + "DDOCTITLE," + "XDOCNUMBER,"
					+ "XAUTHOR," + "XAUTHORORGANIZATION," + "XPROJECTNAME," + "XCOMPANY," + "XOLDNUMBER,"
					+ "XRETENTIONPERIOD," + "XBUASSETFUNCTION," + "XDESTINATION, " + "DSECURITYGROUP, " + "DDOCAUTHOR, "
					+ "DDOCTYPE, " + "TYPE," + "ERRORREMARKS," + "AUTHOR," + "XDOCMODEL," + "XCOMMENTS) " + "values ('"
					+ indexRow + "','" + documentName + "','" + docTitle + "','" + documentNumber + "','" + author
					+ "','" + authorOrganization + "','" + projectName + "','" + company + "','" + oldDocNo + "','"
					+ retentionPeriod + "','" + buAssetFunction + "','" + destinationPath + "','" + securityGroup
					+ "','" + userLogin + "','" + docClasification + "','" + typeDrawing + "','" + error + "','"
					+ userLogin + "','" + docModel + "','" + description + "')";
		} // ** Start Added by HadiWijaya 25022019 **//
		else if (typeDrawing.toUpperCase().equals(uploadTempHeaderPublic)) {
			query = "INSERT INTO PHE_BATCH_TEMP " + "(INDEXROW, " + "XDOCNAME, " + "DDOCTITLE," + "XDOCNUMBER,"
					+ "XAUTHOR," + "XAUTHORORGANIZATION," + "XPROJECTNAME," + "XCOMPANY," + "XOLDNUMBER,"
					+ "XRETENTIONPERIOD," + "XBUASSETFUNCTION," + "XDESTINATION, " + "DSECURITYGROUP, " + "DDOCAUTHOR, "
					+ "DDOCTYPE, " + "TYPE," + "ERRORREMARKS," + "AUTHOR," + "XDOCMODEL," + "XCOMMENTS) " + "values ('"
					+ indexRow + "','" + documentName + "','" + docTitle + "','" + documentNumber + "','" + author
					+ "','" + authorOrganization + "','" + projectName + "','" + company + "','" + oldDocNo + "','"
					+ retentionPeriod + "','" + buAssetFunction + "','" + destinationPath + "','" + securityGroup
					+ "','" + userLogin + "','" + docClasification + "','" + typeDrawing + "','" + error + "','"
					+ userLogin + "','" + docModel + "','" + description + "')";
		} // ** End Added by HadiWijaya 25022019 **//
		else if (typeDrawing.toUpperCase().equals(uploadTempHeaderDrawing)) {
			query = "INSERT INTO PHE_BATCH_TEMP " + "(INDEXROW, " + "XDOCNAME, " + "DDOCTITLE," + "XDOCNUMBER,"
					+ "XDOCPURPOSE," + "XMOCNUMBER," + "XAUTHOR," + "XAUTHORORGANIZATION," + "XAUTHORGROUPNAME,"
					+ "XCOMPANY," + "XPROJECTNAME," + "XCONTRACTOR," + "XCONTRACTNUMBER," + "XWONUMBER, " + "XSTATUS, "
					+ "XREVISION, " + "XDLOCATION," + "XDPLATFORM," + "XDDISCIPLINECODE," + "XDDOCTYPECODE,"
					+ "XRUNNINGNUMBER," + "DSECURITYGROUP, " + "DDOCAUTHOR, " + "DDOCTYPE, " + "TYPE," + "ERRORREMARKS,"
					+ "AUTHOR," + "XDOCMODEL," + "XCOMMENTS) " + "values ('" + indexRow + "','" + documentName + "','"
					+ docTitle + "','" + documentNumber + "','" + documentPurpose + "','" + MOCNo + "','" + author
					+ "','" + authorOrganization + "','" + authorGroupName + "','" + company + "','" + projectName
					+ "','" + contractor + "','" + contractNo + "','" + WONo + "','" + status + "','" + revision + "','"
					+ area + "','" + platform + "','" + disciplineCode + "','" + documentType + "','" + runningNumber
					+ "','" + securityGroup + "','" + userLogin + "','" + docClasification + "','" + typeDrawing + "','"
					+ error + "','" + userLogin + "','" + docModel + "','" + description + "')";
		} else if (typeDrawing.toUpperCase().equals(uploadTempHeaderNonDrawing)) {
			query = "INSERT INTO PHE_BATCH_TEMP " + "(INDEXROW, " + "XDOCNAME, " + "DDOCTITLE," + "XDOCNUMBER,"
					+ "XDOCPURPOSE," + "ACTIVEPASSIVE," + "XOLDNUMBER," + "XAUTHOR," + "XAUTHORORGANIZATION,"
					+ "XAUTHORGROUPNAME," + "XCOMPANY," + "XPROJECTNAME," + "XCONTRACTOR," + "XCONTRACTNUMBER,"
					+ "XRETENTIONPERIOD, " + "DOUTDATE, " + "XWONUMBER, " + "XSTATUS, " + "XREVISION, " + "XNDLOCATION,"
					+ "XNDDISCIPLINECODE," + "XNDDOCTYPECODE," + "XRUNNINGNUMBER," + "DSECURITYGROUP, " + "DDOCAUTHOR, "
					+ "DDOCTYPE, " + "TYPE," + "ERRORREMARKS," + "AUTHOR) " + "values ('" + indexRow + "','"
					+ documentName + "','" + docTitle + "','" + documentNumber + "','" + documentPurpose + "','"
					+ activePassive + "','" + oldDocNo + "','" + author + "','" + authorOrganization + "','"
					+ authorGroupName + "','" + company + "','" + projectName + "','" + contractor + "','" + contractNo
					+ "','" + retentionPeriod + "','" + retentionDate + "','" + WONo + "','" + status + "','" + revision
					+ "','" + area + "','" + disciplineCode + "','" + documentType + "','" + runningNumber + "','"
					+ securityGroup + "','" + userLogin + "','" + docClasification + "','" + typeDrawing + "','" + error
					+ "','" + userLogin + "')";
		}
		Log.info("PHE:insertBatchTemp:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		String returnExecute = "";
		try {
			returnExecute = String.valueOf(ws.executeSQL(query));
		} catch (Exception ex) {
			Log.error("PHE:insertBatchTemp:" + ex);
			returnExecute = "FAILED";
		} finally {
//            try {
//                ws.commitTran();
//                ws.releaseConnection();
//            } catch (DataException ex) {
//                Log.error("PHE:insertBatchTemp:Commit:ex:" + ex);
//            }

		}
		return returnExecute;
	}

	public String insertBatchFailedLog(String userLogin, Date date) {
		DateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String query = "INSERT INTO PHE_UPLOAD_FAILED_ML_LOG " + "SELECT     XDLOCATION," + "    XNDLOCATION,"
				+ "    XDPLATFORM," + "    XNDPLATFORM," + "    XDDISCIPLINECODE," + "    XNDDISCIPLINECODE,"
				+ "    XDDOCTYPECODE," + "    XNDDOCTYPECODE ," + "    XRUNNINGNUMBER," + "    XSHEETNUMBER,"
				+ "    XOLDNUMBER," + "    XPROJECTNAME," + "    XPROJECTYEAR," + "    XOWNERORGANIZATION,"
				+ "    XOWNERNAME," + "    XAUTHOR," + "    CREATEPRIMARYMETAFILE," + "    XREMARKS," + "    DDOCNAME,"
				+ "    DDOCTITLE," + "    DSECURITYGROUP," + "    DDOCAUTHOR," + "    DDOCTYPE," + "    SHEETNAME,"
				+ "    TYPE," + "    ERRORREMARKS," + "    AUTHOR," + "    INDEXROW," + "    TO_DATE('"
				+ dFormat.format(date) + "','YYYY-MM-DD HH24:MI:SS') AS TODAY," + "    XDOCNUMBER,"
				+ "    XAUTHORORGANIZATION," + "    XBUASSETFUNCTION," + "    XDESTINATION," + "    XRETENTIONPERIOD,"
				+ "    XCOMPANY," + "    XDOCNAME," + "    XDOCPURPOSE," + "    XMOCNUMBER," + "    XAUTHORGROUPNAME,"
				+ "    XCONTRACTOR," + "    XCONTRACTNUMBER," + "    XWONUMBER," + "    XREVISION," + "    XSTATUS,"
				+ "    ACTIVEPASSIVE, " + "    DOUTDATE," + "    XDOCMODEL, " + "    XCOMMENTS "
				+ " FROM PHE_BATCH_TEMP " + " WHERE AUTHOR = '" + userLogin + "' ";
//                + " AND ERRORREMARS IS NOT NULL";
		Log.info("PHE:insertBatchFailedLog:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();

		String returnExecute = "";
		try {
			returnExecute = String.valueOf(ws.executeSQL(query));
		} catch (Exception ex) {
			Log.error("PHE:insertBatchFailedLog:" + ex);
			returnExecute = "FAILED";
		} finally {
//            try {
//                ws.commitTran();
//                ws.releaseConnection();
//            } catch (DataException ex) {
//                Log.error("PHE:insertBatchFailedLog:Commit:ex:" + ex);
//            }
		}
		return returnExecute;
	}

	public ResultSet getFailedLogML(String author, String date) throws DataException {

		String query = "SELECT * FROM PHE_UPLOAD_FAILED_ML_LOG "
				+ "WHERE AUTHOR = 'weblogic' AND TO_CHAR(UPLOAD_DATE, 'yyyy-mm-dd HH24:MI:SS') || '.0' "
				+ "LIKE '%' ||TO_CHAR('" + date + "') ||'%' ORDER BY INDEXROW ASC, UPLOAD_DATE DESC";
		Log.info("PHE:getFailedLogML:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		// Log.info("PHE:DocumentNumber:query:" + query);
		if ((resultSet == null) || (resultSet.isEmpty())) {
//            Log.info("KOSONG");
			return resultSet;
		}
		return resultSet;
	}

	public String getLastDateFailedLogML(String author) throws DataException {

		String query = "SELECT * FROM PHE_UPLOAD_FAILED_ML_LOG WHERE AUTHOR = '" + author
				+ "' AND ROWNUM =1 ORDER BY UPLOAD_DATE DESC, INDEXROW ASC";

		Log.info("PHE:getFailedLogML:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		// Log.info("PHE:DocumentNumber:query:" + query);
		if (resultSet != null) {
			resultSet.first();

			return String.valueOf(resultSet.getDateValueByName("UPLOAD_DATE"));
		}
		return "";
	}

	public String GetResultQuery(String query, Workspace ws) {
		ResultSet rs;
		Log.info("EDMS: Query : " + query);
		try {
			rs = ws.createResultSetSQL(query);
			if (rs != null) {
				String result = rs.getStringValue(0);
//                Log.info("EDMS: result : " + result);
				return result;
			} else {
				return null;
			}
		} catch (DataException e) {
			Log.error(e.getMessage());
			return null;
		}
	}

	// DOCUMENT
	public boolean UpdateDocMetaStatus(String status, String dID, Workspace ws) {
		boolean queryResult;
		String query;
		query = "UPDATE DOCMETA SET XSTATUS = '" + status + "' WHERE dID = '" + dID + "'";
		Log.info("EDMS: QueryUpdate : " + query);
		try {
			queryResult = this.UpdateData(query, ws);
		} catch (Exception ex) {
			Log.error("PHE:updateDocMetaStatusFailedLog:ex:" + ex);
			queryResult = false;
		}
		return queryResult;
	}

	public boolean UpdateRevisionDocType(String docType, String dID, Workspace ws) {
		boolean queryResult;
		String query;
		query = "UPDATE REVISIONS SET DDOCTYPE = '" + docType + "' WHERE dID = '" + dID + "'";
		Log.info("EDMS: QueryUpdate : " + query);

		try {
			queryResult = this.UpdateData(query, ws);
		} catch (Exception ex) {
			Log.error("PHE:updateDocMetaStatusFailedLog:ex:" + ex);
			queryResult = false;
		}
		return queryResult;
	}

	public boolean UpdateData(String query, Workspace ws) {
		boolean queryResult;
		try {
			ws.executeSQL(query);
			queryResult = true;
		} catch (Exception ex) {
			Log.error("PHE:UpdateDataFailedLog:" + ex);
			queryResult = false;
		} finally {
//            try {
//                ws.commitTran();
//                //ws.releaseConnection();
//            } catch (DataException ex) {
//                Log.error("PHE:UpdateDataFailedLog:Commit:ex:" + ex);
//                queryResult = false;
//            }
		}
		return queryResult;
	}

	public String GetConfigValue(String code, Workspace ws) {
		String query;
		query = "SELECT KEY_VALUE FROM PHE_CONFIG WHERE KEY_CONFIG LIKE '" + code + "'";
		return this.GetResultQuery(query, ws);
	}

	public String getDocNameDocMeta(String docName) throws DataException {
		String query = "select distinct xdocname from docmeta where xDocName= '" + docName + "'";

		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			return docName;
		}
	}

	public String getSpecifyOptionsList(String dKey, String dOption) throws DataException {
		String query = "SELECT DOPTION FROM OPTIONSLIST WHERE DKEY = '" + dKey + "' AND UPPER(DOPTION) = UPPER('"
				+ dOption + "')";
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		Log.info("PHE:getSpecifyOptionList:query:" + query);
		ResultSet resultSet = ws.createResultSetSQL(query);
		// Log.info("PHE:getPheConfig:RS:" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "0";
		} else {
			resultSet.first();
			dOption = resultSet.getStringValueByName("dOption");
			return dOption;
		}
	}

	// add by nanda 1809 - to check docname whther exist in destination path
	public String checkDocumentNameGeneral(String docName, String parentGUID) throws DataException {
		String query = "select xdocname,revisions.ddocname,folderfiles.fparentguid from docmeta,revisions,folderfiles where docmeta.did=revisions.did and revisions.ddocname=folderfiles.ddocname and revisions.ddoctype='General' and folderfiles.fparentguid='"
				+ parentGUID + "' and xdocname='" + docName + "'";
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		Log.info("PHE:checkDocumentNameGeneral:query:" + query);
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			return "document name already exist";
		}
	}

	public String checkDocumentName(String docNumber, Integer revision, String sheetNumber, String docPurpose)
			throws DataException {
//        String docNumber = "";
////        String sheetNumber = "";
//        int revision = 0;
//        Validator validator = new Validator();
//        String splitDocName[] = validator.splitDocName(docName).split(";");
//        docNumber = splitDocName[0];
//
//        Log.info("PHE:checkDocumentName:splitSize:" + splitDocName.length);
//        if (splitDocName[2].equals("-1")) {
//            sheetNumber = "";
//        } else {
//            sheetNumber = splitDocName[2];
//        }
//        try {
//            revision = Integer.parseInt(splitDocName[1]);
//        } catch (Exception ex) {
//            revision = 0;
//            Log.error("PHE:checkDocumentName:invalidParse");
//        }
//
//        Log.info("PHE:checkDocumentName:docNumber:" + docNumber + ";revision:" + revision + ";sheetNumber:" + sheetNumber);
//        if (sheetNumber.length() > 0) {
//            if (!validator.isNumeric(sheetNumber)) {
//                return "invalidFormat:sheetNumberNotNumeric";
//            }
//        }

		String query = "SELECT DISTINCT XDOCNUMBER, XSTATUS, XDOCPURPOSE, XDOCNAME\n " + " FROM DOCMETA, REVISIONS\n"
				+ " WHERE DOCMETA.DID = REVISIONS.DID " + " AND XDOCNUMBER = UPPER('" + docNumber + "') "
				+ " AND UPPER(XSTATUS) <> 'DELETED'" + " AND UPPER(XDOCPURPOSE) = UPPER ('" + docPurpose + "') ";
		if (sheetNumber.length() > 0) {
			query = query + " AND XSHEETNUMBER = '" + sheetNumber + "'";
		} else {
			query = query + "AND (XSHEETNUMBER IS NULL OR  XSHEETNUMBER = '0')";
		}

		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		Log.info("PHE:checkDocumentName:query:" + query);
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			int revisionFromDB = Integer.valueOf(getLatestRevisionDocument(docNumber, sheetNumber, docPurpose));
			Log.info("PHE:checkDocumentName:revisionFromDB:" + revisionFromDB);
			Log.info("PHE:checkDocumentName:revision:" + revision);
//            boolean hasRevisionFromDocName = false;
//            if (sheetNumber.length()>0) {
//                Log.info("PHE:checkDocumentName:revision:" + revision);
//                hasRevisionFromDocName = true;
//            }
//            if (!hasRevisionFromDocName) {
//                return String.valueOf(revisionFromDB + 1); //NANDA KAYAKNYA DISINI BUAT NOLAK DOKUMNE TANPA CACHING
//            }
			// add by nanda 090914 - already exist doc
			if (revisionFromDB == revision) {
				return "revisionAlreadyExist";
			} else if (revisionFromDB > revision) {
				return "revisionError";
			}

		}
		return "";
	}

	public String getLatestRevisionDocument(String docNumber, String sheetNumber, String docPurpose)
			throws DataException {
		String query = "SELECT DISTINCT XSTATUS, XREVISION FROM DOCMETA,REVISIONS\n" + "WHERE XDOCNUMBER = UPPER('"
				+ docNumber + "') " + "AND UPPER( XDOCPURPOSE) = UPPER('" + docPurpose + "')\n"
				// UPDATE BY NANDA 280115
				+ "AND DOCMETA.DID=REVISIONS.DID AND DDOCTYPE LIKE '%Drawing%' \n"; // update by nanda 130615 - tambahin
																					// persen (bugs nondrawing passive)
		if (sheetNumber.length() > 0) {
			query = query + "AND XSHEETNUMBER = '" + sheetNumber + "' \n";
		} else {
			query = query + "AND (XSHEETNUMBER IS NULL OR  XSHEETNUMBER = '0')\n ";
		}
		query = query + "ORDER BY XREVISION DESC";
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		Log.info("PHE:getLatestRevisionDocument:query:" + query);
		ResultSet resultSet2 = ws.createResultSetSQL(query);
		if (resultSet2 == null || (resultSet2.isEmpty())) {
			return "0";
		} else {
			resultSet2.first();
			return resultSet2.getStringValue(1);
		}
	}

	public ArrayList getAllDocPurpose(String docName) throws DataException {
		String docNumber = "";
		String revision = "";
		String sheetNumber = "";
		ArrayList al = new ArrayList();
		Validator validator = new Validator();
		String splitDocName[] = validator.splitDocName(docName).split(";");
		docNumber = splitDocName[0];
		revision = splitDocName[1];
		// edit by nanda - 1207 - untuk ambil value sheetnumber
		if (!splitDocName[2].toString().equalsIgnoreCase("-1")) {
			sheetNumber = splitDocName[2];
		}
//        else if(splitDocName[2].toString().equalsIgnoreCase("-1")){
//            sheetNumber="1";
//        }

		String query = "SELECT XDOCPURPOSE,DDOCNAME,REVISIONS.DID AS MAXDID,DDOCTYPE, XSTATUS,XREVISION,XSHEETNUMBER,XDOCNAME\n"
				+ "FROM DOCMETA, REVISIONS\n" + "WHERE DOCMETA.DID = REVISIONS.DID\n"
				// edit by nanda 0509 - buat status deleted setara sama registered
//                + "AND UPPER(XSTATUS) <> 'DELETED' \n"
				+ "AND XDOCNUMBER = UPPER('" + docNumber + "') \n"
				// modified by nanda - 140115 - untuk solve issue upload technical ngecek
				// dokumen general, akhirnya error
				+ "AND DDOCTYPE LIKE '%Drawing%' \n";

		if (sheetNumber.length() > 0) {
			query = query + "AND XSHEETNUMBER = UPPER('" + sheetNumber + "') \n";
		} else {
			query = query + "AND (XSHEETNUMBER IS NULL OR  XSHEETNUMBER = '0')\n";
		}

		// add by nanda - 1207 - validasi revisi - 2210 buka lagi,cobak lagi
//        if (revision.length() > 0) {
//            query = query + "AND XREVISION IN (SELECT MAX(XREVISION) FROM DOCMETA WHERE XDOCNUMBER = UPPER('" + docNumber + "'))\n";
//        }
//            if (sheetNumber.length() > 0) {
//                query = query + "AND XSHEETNUMBER = UPPER('" + sheetNumber + "')) \n";
//            } else {
//                query = query + "AND (XSHEETNUMBER IS NULL OR  XSHEETNUMBER = '0'))\n";
//            }
//            query = query + "OR XREVISION = UPPER('" + revision + "')) \n";
		query = query + "ORDER BY XREVISION DESC";
//        }
//        query = query + "group by XDOCPURPOSE, DDOCNAME, DDOCTYPE, XSTATUS";

		Log.info("PHE:getAllDocPurpose:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			Log.info("PHE:getAllDocPurpose:resultSet:null");
			return al;
		} else {
			int i = 0;

			resultSet.first();
			if (resultSet.getStringValue(0) == null) {
				Log.info("PHE:getAllDocPurpose:resultSet:getStringValue(0) is null");
				al.add("" + ";" + resultSet.getStringValue(1) + ";" + resultSet.getStringValueByName("MAXDID") + ";"
						+ resultSet.getStringValue(4) + ";" + resultSet.getStringValue(4) + ";"
						+ resultSet.getStringValue(5) + ";" + resultSet.getStringValue(6) + ";"
						+ resultSet.getStringValue(7));
			} else {
				Log.info("PHE:getAllDocPurpose:resultSet:getStringValue(0) is not null");
				al.add(resultSet.getStringValue(0) + ";" + resultSet.getStringValue(1) + ";"
						+ resultSet.getStringValueByName("MAXDID") + ";" + resultSet.getStringValue(4) + ";"
						+ resultSet.getStringValue(4) + ";" + resultSet.getStringValue(5) + ";"
						+ resultSet.getStringValue(6) + ";" + resultSet.getStringValue(7));
			}
			i++;
			while (resultSet.next() && i == 1) {
				if (resultSet.getStringValue(0) == null) {
					al.add("" + ";" + resultSet.getStringValue(1) + ";" + resultSet.getStringValueByName("MAXDID") + ";"
							+ resultSet.getStringValue(4) + ";" + resultSet.getStringValue(5) + ";"
							+ resultSet.getStringValue(5) + ";" + resultSet.getStringValue(6) + ";"
							+ resultSet.getStringValue(7));
				} else {
					al.add(resultSet.getStringValue(0) + ";" + resultSet.getStringValue(1) + ";"
							+ resultSet.getStringValueByName("MAXDID") + ";" + resultSet.getStringValue(4) + ";"
							+ resultSet.getStringValue(5) + ";" + resultSet.getStringValue(5) + ";"
							+ resultSet.getStringValue(6) + ";" + resultSet.getStringValue(7));
				}
			}

			return al;
		}

	}

	public ArrayList getCurrentDocPurpose(String docName) throws DataException {
		String docNumber = "";
		String revision = "";
		String sheetNumber = "";
		ArrayList al = new ArrayList();
		Validator validator = new Validator();
		String splitDocName[] = validator.splitDocName(docName).split(";");
		docNumber = splitDocName[0];
		revision = splitDocName[1];
		// edit by nanda - 1207 - untuk ambil value sheetnumber
		if (!splitDocName[2].toString().equalsIgnoreCase("-1")) {
			sheetNumber = splitDocName[2];
		}
//        else if(splitDocName[2].toString().equalsIgnoreCase("-1")){
//            sheetNumber="1";
//        }

		String query = "SELECT XDOCPURPOSE,DDOCNAME,REVISIONS.DID AS MAXDID,DDOCTYPE, XSTATUS,XREVISION,XSHEETNUMBER\n"
				+ "FROM DOCMETA, REVISIONS\n" + "WHERE DOCMETA.DID = REVISIONS.DID\n"
				+ "AND UPPER(XSTATUS) <> 'DELETED' \n" + "AND XDOCNUMBER = UPPER('" + docNumber + "') \n";
		if (sheetNumber.length() > 0) {
			query = query + "AND XSHEETNUMBER = UPPER('" + sheetNumber + "') \n";
		} else {
			query = query + "AND (XSHEETNUMBER IS NULL OR  XSHEETNUMBER = '0')\n";
		}

		// add by nanda - 1207 - validasi revisi
		if (revision.length() > 0) {
			query = query + "AND (XREVISION IN (SELECT MAX(XREVISION) FROM DOCMETA WHERE XDOCNUMBER = UPPER('"
					+ docNumber + "')\n";
			if (sheetNumber.length() > 0) {
				query = query + "AND XSHEETNUMBER = UPPER('" + sheetNumber + "')) \n";
			} else {
				query = query + "AND (XSHEETNUMBER IS NULL OR  XSHEETNUMBER = '0'))\n";
			}
			query = query + "OR XREVISION = UPPER('" + revision + "')) \n";
			query = query + "ORDER BY XREVISION DESC";
		}
//        query = query + "group by XDOCPURPOSE, DDOCNAME, DDOCTYPE, XSTATUS";

		Log.info("PHE:getAllDocPurpose:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			Log.info("PHE:getAllDocPurpose:resultSet:null");
			return al;
		} else {
			int i = 0;

			resultSet.first();
			if (resultSet.getStringValue(0) == null) {
				Log.info("PHE:getAllDocPurpose:resultSet:getStringValue(0) is null");
				al.add("" + ";" + resultSet.getStringValue(1) + ";" + resultSet.getStringValueByName("MAXDID") + ";"
						+ resultSet.getStringValue(4) + ";" + resultSet.getStringValue(4) + ";"
						+ resultSet.getStringValue(5) + ";" + resultSet.getStringValue(6));
			} else {
				Log.info("PHE:getAllDocPurpose:resultSet:getStringValue(0) is not null");
				al.add(resultSet.getStringValue(0) + ";" + resultSet.getStringValue(1) + ";"
						+ resultSet.getStringValueByName("MAXDID") + ";" + resultSet.getStringValue(4) + ";"
						+ resultSet.getStringValue(4) + ";" + resultSet.getStringValue(5) + ";"
						+ resultSet.getStringValue(6));
			}
			i++;
			while (resultSet.next() && i == 1) {
				if (resultSet.getStringValue(0) == null) {
					al.add("" + ";" + resultSet.getStringValue(1) + ";" + resultSet.getStringValueByName("MAXDID") + ";"
							+ resultSet.getStringValue(4) + ";" + resultSet.getStringValue(5) + ";"
							+ resultSet.getStringValue(5));
				} else {
					al.add(resultSet.getStringValue(0) + ";" + resultSet.getStringValue(1) + ";"
							+ resultSet.getStringValueByName("MAXDID") + ";" + resultSet.getStringValue(4) + ";"
							+ resultSet.getStringValue(5) + ";" + resultSet.getStringValue(5));
				}
			}

			return al;
		}

	}

	public ResultSet getDefaultExpiredDocument() throws DataException {
		String query = "SELECT rs.dID AS dID,\n" + "  rs.DDOCNAME AS dDocName,\n" + "  rs.DDOCTYPE AS dDocType\n"
				+ "FROM REVISIONS rs,\n" + "  DOCMETA\n" + "WHERE rs.dID = docmeta.dID\n" + "AND rs.dRevisionId =\n"
				+ "  (SELECT MAX(rd.drevisionid)\n" + "  FROM revisions rd\n" + "  WHERE rd.ddocname = rs.ddocname\n"
				+ "  )\n" + "AND ddocname NOT IN\n" + "  ( SELECT DISTINCT REVISIONS.DDOCNAME\n" + "  FROM REVISIONS,\n"
				+ "    FOLDERFILES\n" + "  WHERE REVISIONS.DDOCNAME = FOLDERFILES.DDOCNAME\n" + "  )\n"
				+ "AND SYSDATE > xRetentionDate\n" + "AND UPPER(XSTATUS) != 'EXPIRED'";
		Log.info("PHE:getDefaultExpiredDocument:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

//        Log.info("PHE:isEmpty:" + resultSet.isEmpty());

		return resultSet;
	}

	public ResultSet getDefaultObsoleteDocument() throws DataException {
//        Log.info("PHE:getDefaultObsoleteDocument:startquery");
		String query = "SELECT d.dID AS dID, r.DDOCNAME AS dDocName,\n"
				+ "d.XRECORDOBSOLETEDATE AS ObsoleteDate, r.DDOCTYPE AS dDocType \n" + "FROM DOCMETA d, REVISIONS r\n"
				+ "WHERE TO_CHAR(XRECORDOBSOLETEDATE) is not null and d.dID=r.dID and upper(d.xstatus)!='OBSOLETE'";
		Log.info("PHE:getDefaultObsoleteDocument:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

//        Log.info("PHE:isEmpty:" + resultSet.isEmpty());

		return resultSet;
	}

	public ResultSet getDocNameByDocTitle(String docTitle) throws DataException {
		String query = "SELECT DID, DDOCNAME FROM REVISIONS WHERE DDOCTITLE = '" + docTitle + "'";
		Log.info("PHE:getDocNameByDocTitle:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

//        Log.info("PHE:isEmpty:" + resultSet.isEmpty());

		return resultSet;
	}

	public String getEmailFromName(String dName) throws DataException {
		// edit by nanda - 230614 - add or dFullName in Query
		// String query = "SELECT dEmail, dname FROM USERS WHERE dName = '" + dName + "'
		// or dFullName ='" + dName + "'";

		// edit by nanda - 140714
		// add trim() to delete whitespace in split
		String query = "select distinct dEmail from users where (UPPER(dName) = UPPER('" + dName.trim()
				+ "') or UPPER(dFullName) = UPPER('" + dName.trim() + "'))";
		Log.info("PHE:getEmailFromName:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		if (resultSet.isEmpty() || resultSet == null) {
			ws.releaseConnection();
			return "";
		} else {
			DataResultSet dResultSet = new DataResultSet();
			dResultSet.copy(resultSet);
			ws.releaseConnection();
			for (int i = 0; i < dResultSet.getNumRows(); i++) {
				List list = dResultSet.getCurrentRowAsList();
				return list.get(0).toString();
			}

			return "";
		}

	}

	public String getFullNameByName(String dName) throws DataException {
//        String query = "SELECT distinct dFullName FROM USERS WHERE dName = '" + dName + "' or dFullName = '" + dName + "'";

		// edit by nanda - 140714
		// add trim() to delete whitespace in split
		String query = "select distinct dFullName from users where (UPPER(dName) = UPPER('" + dName.trim()
				+ "') or UPPER(dFullName) = UPPER('" + dName.trim() + "'))";
		Log.info("PHE:getFullNameByName:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		if (resultSet.isEmpty() || resultSet == null) {
			ws.releaseConnection();
			return "";
		} else {
			DataResultSet dResultSet = new DataResultSet();
			dResultSet.copy(resultSet);
			ws.releaseConnection();
			for (int i = 0; i < dResultSet.getNumRows(); i++) {
				List list = dResultSet.getCurrentRowAsList();
				return list.get(0).toString();
			}

			return "";
		}

	}

	public boolean InsertKPIUserLogin() {
		return true;
	}

	public String getIDFullNameFromBusinessContact(String fullName) throws DataException {
		String query = "SELECT ID AS IDUSERNAME\n" + "FROM PHE_BUSINESS_CONTACT\n" + "WHERE FULLNAME = '" + fullName
				+ "'\n" + "AND USERNAME  IS NULL\n" + "ORDER BY ID DESC";
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		Log.info("PHE:getIDFullNameFromBusinessContact:query:" + query);
		ResultSet resultSet = ws.createResultSetSQL(query);
		// Log.info("PHE:getPheConfig:RS:" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		}

		resultSet.first();
		String idUsername = resultSet.getStringValueByName("IDUSERNAME");
		Log.info("PHE:getIDFullNameFromBusinessContact:idUsername:" + idUsername);
		return idUsername;
	}

	public String getUsernameBusinessContactByUsername(String username) throws DataException {
		String query = "SELECT USERNAME\n" + "FROM PHE_BUSINESS_CONTACT\n" + "WHERE username = '" + username + "'\n"
				+ "ORDER BY ID DESC";
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		// Log.info("PHE:getPheConfig:RS:" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		}

		resultSet.first();
		String idUsername = resultSet.getStringValueByName("USERNAME");
		// Log.info("PHE:keyValue:" + keyValue);
		return idUsername;
	}

	public boolean updateUsernameBusinessContactByID(String id, String username, Workspace ws) {
		boolean queryResult;
		String query;
		query = "UPDATE PHE_BUSINESS_CONTACT SET username = '" + username + "' WHERE id = '" + id + "'";
		Log.info("PHE:updateUsernameBusinessContactByID:query:" + query);
		try {
			queryResult = this.UpdateData(query, ws);
		} catch (Exception ex) {
			Log.error("PHE:updateUsernameBusinessContactByID:ex:" + ex.getMessage());
			queryResult = false;
		}

		return queryResult;
	}

	public String updateXRObjectID(String dDocName, String dID, Workspace ws) {
		String queryResult = "";
		String query;
		query = "UPDATE DOCMETA SET XROBJECTID='" + dID + "' WHERE DID='" + dID + "'";
		Log.info("PHE:updateXRObjectID:query:" + query);
		try {
			if (this.UpdateData(query, ws)) {
				queryResult = "true";
			} else {
				queryResult = "executeFailed:updateRObjectID";
			}

		} catch (Exception e) {
			Log.info("PHE:updateXRObjectID:Ex:" + query);
		}
		return queryResult;
	}

	public String insertUserSecurityAttributes(String dUserName, String dAttributeName, String dAttributeType,
			String dAttributePrivilege) {
		String query = "";
		query = "insert into UserSecurityAttributes(dUserName,dAttributeName,dAttributeType,dAttributePrivilege) values('"
				+ dUserName + "','" + dAttributeName + "','" + dAttributeType + "'," + dAttributePrivilege + ")";
		Log.info("PHE:insertUserSecurityAttributes:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		String returnExecute = "";
		try {
			returnExecute = String.valueOf(ws.executeSQL(query));
		} catch (Exception ex) {
			Log.info("PHE:insertUserSecurityAttributes:" + ex);
			returnExecute = "FAILED";
		} finally {
//            try {
//                //ws.commitTran();
			ws.releaseConnection(); // nanda 2811
//            } catch (DataException ex) {
//                Log.error("PHE:insertBatchTemp:Commit:ex:" + ex);
//            }
		}
		return returnExecute;
	}

	public String insertUser(String dName, String dFullName, String dEmail, String dPassword, String dUserAuthType) {
		String query = "";
		query = "insert into Users(dName,dFullName,dEmail,dPasswordEncoding,dPassword,dUserAuthType,dUserArriveDate,dUserChangeDate,dUserLocale,dUserTimezone,UClassifiedMarkings,UBarcode, dUserSourceFlags) values ('"
				+ dName + "','" + dFullName + "','" + dEmail + "','SHA1-CB','" + dPassword + "','" + dUserAuthType
				+ "',SYSDATE,SYSDATE,'English-US','Asia/Jakarta','No Markings',UPPER('" + dName + "'), '0')";
		Log.info("PHE:insertUser:query" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		String returnExecute = "";
		try {
			returnExecute = String.valueOf(ws.executeSQL(query));
		} catch (Exception ex) {
			Log.error("PHE:insertUser:" + ex);
			returnExecute = "FAILED";
		} finally {
//            try {
			ws.releaseConnection(); // nanda 2811
//            } catch (Exception ex) {
//                Log.error("PHE:insertUser:Commit:ex:" + ex);
//            }
		}
		return returnExecute;
	}

	public String updateScheduleLog(String keyEmail, String resultValue) {
		String returnExecute = "";
		String query = "update PHE_SCHEDULE_LOG set (LAST_SCHEDULE=sysdate, RESULT_VALUE='" + resultValue
				+ "' where KEY_VALUE='" + keyEmail + "')";
		Log.info("PHE:updateScheduleEmailLog:query" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		try {
			returnExecute = String.valueOf(ws.executeSQL(query));
		} catch (Exception ex) {
			Log.error("PHE:updateScheduleEmailLog:" + ex);
			returnExecute = "FAILED";
		} finally {
//            try {
//                ws.releaseConnection();
//            } catch (Exception ex) {
//                Log.error("PHE:insertScheduleEmailLog:Commit:ex:" + ex);
//            }
//
		}

		return returnExecute;
	}

	public String insertScheduleLog(String keyEmail, String resultValue, String service) {
		String query = "";
		query = "insert into PHE_SCHEDULE_LOG values('" + keyEmail + "', sysdate, '" + resultValue + "', '" + service
				+ "')";
		Log.info("PHE:insertScheduleEmailLog:query" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		String returnExecute = "";
		try {
			returnExecute = String.valueOf(ws.executeSQL(query));
		} catch (Exception ex) {
			Log.error("PHE:insertScheduleEmailLog:" + ex);
			returnExecute = "FAILED";
		} finally {
//            try {
//                ws.releaseConnection();
//            } catch (Exception ex) {
//                Log.error("PHE:insertScheduleEmailLog:Commit:ex:" + ex);
//            }
//
		}
		return returnExecute;
	}

	public String insertEmailLog(String strFrom, String strTo, String strSubject, String resultValue) {
		String query = "";
		query = "insert into phe_email_log values('" + strFrom + "', '" + strTo + "', '" + strSubject + "', sysdate, '"
				+ resultValue + "')";
		Log.info("PHE:insertEmailLog:query" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		String returnExecute = "";
		try {
			returnExecute = String.valueOf(ws.executeSQL(query));
		} catch (Exception ex) {
			Log.error("PHE:insertEmailLog:" + ex);
			returnExecute = "FAILED";
		} finally {
//            try {
//                ws.releaseConnection();
//            } catch (Exception ex) {
//                Log.error("PHE:insertScheduleEmailLog:Commit:ex:" + ex);
//            }
//
		}
		return returnExecute;
	}

	public ResultSet getTicketByAuthor(String Item1, String Item2, String dUser, String Role) throws DataException {
		String query = "";

		if (Role.equalsIgnoreCase("TDC")) {
			// query = query + "select dfullname,reservations.* from reservations,users
			// where drequestor=dname and discomplete=0 " + "and ((drequestid in (select
			// distinct(to_char(drequestid)) from reservationitemmap where " +
			// "dpherequestItemStatus = '" + Item1 + "' ) and (pheTDCAdmin = '" + dUser + "'
			// or pheTDCAdmin is null))" + "or (drequestid in (select
			// distinct(to_char(drequestid)) from reservationitemmap where
			// dpherequestItemStatus = '" + Item2 + "' ) ) ) order by dLastModifiedDate
			// desc";
			query = query + "SELECT USR.DFULLNAME, R.* FROM RESERVATIONS R, USERS USR WHERE R.DREQUESTOR = USR.DNAME"
					+ " AND R.DISCOMPLETE = 0" + " AND ("
					+ " (R.DREQUESTID IN (SELECT DISTINCT (TO_CHAR (DREQUESTID)) FROM RESERVATIONITEMMAP WHERE DPHEREQUESTITEMSTATUS = 'wwRequested'))"
					+ " OR "
					+ " (R.DREQUESTID IN (SELECT DISTINCT (TO_CHAR (DREQUESTID)) FROM RESERVATIONITEMMAP WHERE DPHEREQUESTITEMSTATUS = 'wwRequestExtend')))"
					+ " AND R.PHEBOXTYPE IN (SELECT CASE REGEXP_REPLACE(DATTRIBUTENAME, '_DLS_admin', '')"
					+ " WHEN 'DWS' THEN 'DWS'" + " WHEN 'SUBSURFACE' THEN 'SUBS'" + " WHEN 'COMMFIN' THEN 'CF'"
					+ " WHEN 'MSCM' THEN 'SCM'" + " WHEN 'EXECUTIVES' THEN 'EXL'" + " WHEN 'PRJ' THEN 'PRJ'"
					+ " WHEN 'EI' THEN 'EI'" + " WHEN 'HRR' THEN 'HRR'" + " WHEN 'HSSE' THEN 'HSSE'"
					+ " WHEN 'OPS' THEN 'OPS'" + " ELSE 'STS'" + " END as BOX" + " FROM USERSECURITYATTRIBUTES "
					+ " WHERE DATTRIBUTETYPE = 'role' AND (DATTRIBUTENAME LIKE '%DLS_admin%' OR DATTRIBUTENAME = 'tdcadmin') "
					+ " AND DUSERNAME= '" + dUser + "') ORDER BY R.DLASTMODIFIEDDATE DESC";
		} else if (Role.equalsIgnoreCase("Warehouse")) {
			query = "select dfullname,reservations.* from reservations,users where drequestor=dname and discomplete=0 and drequestid in ";
			query = query
					+ "(select distinct(to_char(drequestid)) from reservationitemmap where dpherequestItemStatus = '"
					+ Item1 + "' or dpherequestItemStatus = '" + Item2 + "' ) ";
			query = query + "and (pheWarehouseAdmin = '" + dUser
					+ "' or pheWarehouseAdmin is null) order by dLastModifiedDate desc";
		} else if (Role.equalsIgnoreCase("TDCWarehouse")) {
			// query = "select dfullname,r.*, '' as ibx from reservations r,users where
			// drequestor=dname and r.discomplete=0 and ( (drequestid in (select
			// distinct(to_char(drequestid)) from reservationitemmap where
			// dpherequestItemStatus = 'wwRequested' or dpherequestItemStatus =
			// 'wwRequestExtend' ) and (pheTDCAdmin = '" + dUser + "' or pheTDCAdmin is
			// null))" + " or (drequestid in (select distinct(to_char(drequestid)) from
			// reservationitemmap where dpherequestItemStatus = 'wwWaiting' ) and
			// (pheWarehouseAdmin = '" + dUser + "' or pheWarehouseAdmin is null)))";
			query = "SELECT dfullname, r.*, '' AS ibx FROM reservations r, users WHERE drequestor = dname AND r.discomplete = 0 "
					+ "AND ( (drequestid IN (SELECT DISTINCT (TO_CHAR (drequestid)) FROM reservationitemmap "
					+ "WHERE dpherequestItemStatus = 'wwRequested' OR dpherequestItemStatus = 'wwRequestExtend') "
					+ "OR (drequestid IN (SELECT DISTINCT (TO_CHAR (drequestid)) FROM reservationitemmap "
					+ "WHERE dpherequestItemStatus = 'wwWaiting') ))) "
					+ "AND R.PHEBOXTYPE IN (SELECT CASE REGEXP_REPLACE(DATTRIBUTENAME, '_DLS_admin', '') "
					+ "WHEN 'DWS' THEN 'DWS' " + "WHEN 'SUBSURFACE' THEN 'SUBS' " + "WHEN 'COMMFIN' THEN 'CF' "
					+ "WHEN 'MSCM' THEN 'SCM' " + "WHEN 'EXECUTIVES' THEN 'EXL' " + "WHEN 'PRJ' THEN 'PRJ' "
					+ "WHEN 'EI' THEN 'EI' " + "WHEN 'HRR' THEN 'HRR' " + "WHEN 'HSSE' THEN 'HSSE' "
					+ "WHEN 'OPS' THEN 'OPS' " + "ELSE 'STS' " + "END as BOX "
					+ "FROM USERSECURITYATTRIBUTES WHERE DATTRIBUTETYPE = 'role' AND (DATTRIBUTENAME LIKE '%DLS_admin%' OR DATTRIBUTENAME = 'tdcadmin') AND DUSERNAME= '"
					+ dUser + "')";

			// commented by nanda - 090115
//      query = query + " union select r.*, 'extend' as ibx from reservations r where r.discomplete=0 and " + " ( (drequestid in (select distinct(to_char(drequestid)) from reservationitemmap where dpherequestItemStatus = 'wwRequestExtend' ) ) )";
		}

		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			return resultSet;
		}

		return resultSet;
	}

	public ResultSet getTicketdID(String dRequestID) throws DataException {
		String query = "select to_char(dID) from reservationitemmap where to_char(drequestid)=" + dRequestID;

		System.out.println(query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		Log.info("PHE:DocumentNumber:query:" + query);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			System.out.println("Result nya Null");
			return resultSet;
		}

		while (resultSet.next()) {
			String dID = resultSet.getStringValueByName("dID");
			System.out.println(dID);
			Log.info("PHE:QuerydID:" + dID);
		}

		System.out.println(resultSet);
		return resultSet;
	}

	public ResultSet pheTicketProcessListData(String Item1, String Item2, String dUser, String adminTable)
			throws DataException {
		// String query = "select dfullname,reservations.* from reservations,users where
		// drequestor=dname and discomplete=0 and drequestid in (select
		// distinct(to_char(drequestid)) from reservationitemmap where
		// dpherequestItemStatus = '" + Item1 + "' or dpherequestItemStatus = '" + Item2
		// + "' ) and (" + adminTable + " = '" + dUser + "') order by dLastModifiedDate
		// desc";
		String query = "";
		if (adminTable.equals("pheWarehouseAdmin")) {
			query = "select dfullname,reservations.* from reservations,users where drequestor=dname and discomplete=0 and drequestid in (select distinct(to_char(drequestid)) from reservationitemmap where dpherequestItemStatus = '"
					+ Item1 + "' or dpherequestItemStatus = '" + Item2 + "' ) order by dLastModifiedDate desc";
		} else {
			query = "SELECT dfullname, reservations.* FROM reservations, users WHERE drequestor = dname AND discomplete = 0 "
					+ "AND drequestid IN (SELECT DISTINCT (TO_CHAR (drequestid)) FROM reservationitemmap WHERE dpherequestItemStatus = '"
					+ Item1 + "' OR dpherequestItemStatus = '" + Item2 + "') "
					+ "AND PHEBOXTYPE IN (SELECT CASE REGEXP_REPLACE(DATTRIBUTENAME, '_DLS_admin', '') "
					+ "WHEN 'DWS' THEN 'DWS' " + "WHEN 'SUBSURFACE' THEN 'SUBS' " + "WHEN 'COMMFIN' THEN 'CF' "
					+ "WHEN 'MSCM' THEN 'SCM' " + "WHEN 'EXECUTIVES' THEN 'EXL' " + "WHEN 'PRJ' THEN 'PRJ' "
					+ "WHEN 'EI' THEN 'EI' " + "WHEN 'HRR' THEN 'HRR' " + "WHEN 'HSSE' THEN 'HSSE' "
					+ "WHEN 'OPS' THEN 'OPS' " + "ELSE 'STS' " + "END as BOX FROM USERSECURITYATTRIBUTES "
					+ "WHERE DATTRIBUTETYPE = 'role' AND (DATTRIBUTENAME LIKE '%DLS_admin%' OR DATTRIBUTENAME = 'tdcadmin') AND DUSERNAME= '"
					+ dUser + "')" + "ORDER BY dLastModifiedDate DESC";
		}

		System.out.println(query);
		Log.info("PHE:pheTicketProcessListData:query: " + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);

		if ((resultSet == null) || (resultSet.isEmpty())) {
			return resultSet;
		}

		return resultSet;
	}

	// 260716 Haries: Fungsi untuk mencari DID, DDOCNAME dan SECURITYGROUP untuk
	// upload Excel + DIS

	public String getDIDDocumentDIS(String docName, String docPurpose, String docType, String securityGroup)
			throws DataException {

		String fParentGUID = "";
		Folder fld = new Folder();
		Util util = new Util();
		util.setUserLogin("weblogic");
		DataBinder binder = new DataBinder();
		ResultSet rs = fld.GetFolderInfo("/Cabinets/Others Documents/Technical Desktop CheckIn", binder,
				util.getSystemWorkspace(), util);

		if (!rs.isEmpty())
			fParentGUID = rs.getStringValueByName("fFolderGUID");

		if (docName.indexOf(".") >= 0) {
			docName = docName.substring(0, docName.indexOf("."));
		}

		String query = "select distinct REVISIONS.DID ,xDocName from DOCMETA, REVISIONS, FOLDERFILES where DOCMETA.DID=REVISIONS.DID and DREVRANK=0 "
				+ " and REVISIONS.DDOCNAME=FOLDERFILES.DDOCNAME AND FPARENTGUID='" + fParentGUID + "' "
				+ " and dDocTitle = '" + docName + "' " + " AND XDOCPURPOSE = '" + docPurpose + "' " + " and DDOCTYPE='"
				+ docType + "' ";

		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		Log.info("PHE:getDIDDocumentDIS:query" + query);
		Log.info("PHE:getDIDDocumentDIS:resultSet" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			resultSet.first();
			return resultSet.getStringValue(0);
		}
	}

	public String getDDocNameDocumentDIS(String docName, String docPurpose, String docType, String securityGroup)
			throws DataException {

		String fParentGUID = "";
		Folder fld = new Folder();
		Util util = new Util();
		util.setUserLogin("weblogic");
		DataBinder binder = new DataBinder();
		ResultSet rs = fld.GetFolderInfo("/Cabinets/Others Documents/Technical Desktop CheckIn", binder,
				util.getSystemWorkspace(), util);

		if (!rs.isEmpty())
			fParentGUID = rs.getStringValueByName("fFolderGUID");

		if (docName.indexOf(".") >= 0) {
			docName = docName.substring(0, docName.indexOf("."));
		}

		String query = "select distinct REVISIONS.DDocName ,xDocName from DOCMETA, REVISIONS, FOLDERFILES where DOCMETA.DID=REVISIONS.DID and DREVRANK=0 "
				+ " and REVISIONS.DDOCNAME=FOLDERFILES.DDOCNAME AND FPARENTGUID='" + fParentGUID + "' "
				+ " and dDocTitle = '" + docName + "' " + " AND XDOCPURPOSE = '" + docPurpose + "' " + " and DDOCTYPE='"
				+ docType + "' ";

		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		Log.info("PHE:getDDocNameDocumentDIS:query" + query);
		Log.info("PHE:getDDocNameDocumentDIS:resultSet" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			resultSet.first();
			return resultSet.getStringValue(0);
		}
	}

	public String getDWebExtensionDocumentDIS(String docName, String docPurpose, String docType, String securityGroup)
			throws DataException {

		String query = "select distinct dWebExtension ,xDocName from DOCMETA, REVISIONS where DOCMETA.DID=REVISIONS.DID and DREVRANK=0 and "
				+ " dDocTitle = '" + docName + "' " + " AND XDOCPURPOSE = '" + docPurpose + "' " + " and DDOCTYPE='"
				+ docType + "' ";

		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		Log.info("PHE:getDDocNameDocumentDIS:query" + query);
		Log.info("PHE:getDDocNameDocumentDIS:resultSet" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			resultSet.first();
			return resultSet.getStringValue(0);
		}
	}

	public String getSecurityGroupDIS(String docName, String docPurpose, String docType, String securityGroup)
			throws DataException {

		String query = "select distinct dSecurityGroup ,xDocName from DOCMETA, REVISIONS where DOCMETA.DID=REVISIONS.DID and DREVRANK=0 and "
				+ " XDOCNAME = '" + docName + "' " + " AND XDOCPURPOSE = '" + docPurpose + "' " + " and DDOCTYPE='"
				+ docType + "' ";

		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		Log.info("PHE:getDDocNameDocumentDIS:query" + query);
		Log.info("PHE:getDDocNameDocumentDIS:resultSet" + resultSet);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "";
		} else {
			resultSet.first();
			return resultSet.getStringValue(0);
		}
	}

	public String insertBatchTempExtResource(String indexRow, String xDocNumber, String xDepartment,
			String xExternalOwner, String xSubmitDate, String xYear, String xStampDate, String xComments,
			String xDocName, String xStatus, String xReferensi, String xAuthor, String createprimarymetafile,
			String dDocname, String dDocTitle, String dSecurityGroup, String dDocAuthor, String docClasification,
			String sheetName, String typeDrawing, String error, String sFolder, String userLogin) {
		String query = "";
		if (typeDrawing.toUpperCase().equals(uploadTempHeaderExternalResource)) {
			/*
			query = "INSERT INTO PHE_BATCH_TEMP (INDEXROW, xDocNumber,xDepartment,xExternalOwner,xSubmitDate,xYear,xStampDate,xComments,xDocName,xStatus,xReferensi,xAuthor,createprimarymetafile,dDocname,dDocTitle,dSecurityGroup,DDOCAUTHOR,DDOCTYPE,xDestination,SHEETNAME,TYPE,ERRORREMARKS,AUTHOR) values ('"
					+ indexRow + "','" + xDocNumber + "','" + xDepartment + "','" + xExternalOwner + "','"
					+ xSubmitDate + "','" + xYear + "','" + xStampDate + "','" + xComments + "','" + xDocName
					+ "','" + xStatus + "','" + xReferensi + "','" + xAuthor + "','" + createprimarymetafile
					+ "','" + dDocname + "','" + dDocTitle + "','" + dSecurityGroup + "','" + dDocAuthor + "','"
					+ docClasification + "','" + sFolder + "','" + sheetName + "','" + typeDrawing + "','"
					+ error + "','" + userLogin + "')";
			*/
			query = "INSERT INTO PHE_BATCH_TEMP (INDEXROW, xDocNumber,xComments,xDocName,xStatus,xAuthor,createprimarymetafile,dDocname,dDocTitle,dSecurityGroup,DDOCAUTHOR,DDOCTYPE,xDestination,SHEETNAME,TYPE,ERRORREMARKS,AUTHOR) values ('"
					+ indexRow + "','" + xDocNumber + "','" + xComments + "','" + xDocName
					+ "','" + xStatus + "','" + xAuthor + "','" + createprimarymetafile
					+ "','" + dDocname + "','" + dDocTitle + "','" + dSecurityGroup + "','" + dDocAuthor + "','"
					+ docClasification + "','" + sFolder + "','" + sheetName + "','" + typeDrawing + "','"
					+ error + "','" + userLogin + "')";
		}
		Log.info("PHE:insertBatchTempExtRes:query:" + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		String returnExecute = "";
		try {
			returnExecute = String.valueOf(ws.executeSQL(query));
			Log.info("PHE:insertBatchTempExtRes:returnExecute:" + returnExecute);
		} catch (Exception ex) {
			Log.error("PHE:insertBatchTemp:" + ex);
			returnExecute = "FAILED";
		}
		return returnExecute;
	}

	public String getPheXcompany(String pheAp) throws DataException {
		String query = "SELECT KEY_DISPLAY FROM PHE_CONFIG WHERE KEY_CONFIG = 'XCOMPANY_" + pheAp.toUpperCase() + "'";
		Log.info("PHE:getPheXcompany:QUERY: " + query);
		DataCollection dColl = new DataCollection();
		Workspace ws = dColl.getSystemWorkspace();
		ResultSet resultSet = ws.createResultSetSQL(query);
		if ((resultSet == null) || (resultSet.isEmpty())) {
			return "0";
		}

		resultSet.first();
		String keyDisplay = resultSet.getStringValueByName("KEY_DISPLAY");
		return keyDisplay;
	}
}
