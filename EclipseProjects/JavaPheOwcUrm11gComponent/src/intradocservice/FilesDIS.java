//@author: Haries
package intradocservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.common.SystemUtils;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradoc.server.ServiceData;
import intradoc.server.ServiceManager;
import intradoc.shared.UserData;

import intradocservice.Util;
public class FilesDIS extends Service{

	public String downloadDocument(String docName, String dID, String contentID, String userName ) throws DataException, ServiceException {
		
		String PrimaryFile = "";
		Util util = new Util();
	   // SystemUtils.trace("system", "downloadDocumentInWorkflow starting");
	    //String dID = "1";// m_binder.getLocal("dID");
	    //String contentID ="AGITVMAGITCOM1000001";// m_binder.getLocal("dDocName");
	    String serviceName = "GET_FILE";
	    //String userName = m_binder.getLocal("dUser");

	   // SystemUtils.trace("system", "dID-->" + dID);
	   // SystemUtils.trace("system", "contentID-->" + contentID);
	   // SystemUtils.trace("system", "User:" + userName);

	    DataBinder serviceBinder = new DataBinder();

	    serviceBinder.putLocal("dID", dID);
	    serviceBinder.putLocal("dDocName", contentID);
	    serviceBinder.putLocal("IdcService", serviceName);
	    Workspace workspace = util.getSystemWorkspace();

	    try {
	      SystemUtils.trace("system", "started");
	      ServiceData serviceData = ServiceManager.getFullService(serviceName);
	      workspace = util.getSystemWorkspace();
	      Service service =
	        ServiceManager.createService(serviceData.m_classID, workspace, null,
	                                     serviceBinder, serviceData);
	      UserData fullUserData = util.getFullUserData("weblogic",service,workspace);
	      service.setUserData(fullUserData);
	      serviceBinder.m_environment.put("REMOTE_USER", userName);
	      service.initDelegatedObjects();
	      service.executeSafeServiceInNewContext(serviceName, true);
	      SystemUtils.trace("system", "succeeded");
	      String path = (String)service.getCachedObject("PrimaryFilePath");
	      SystemUtils.trace("system", "Path-->" + path);
	      Log.info("filePath="+path);
	      
	      
	      
	      File source = new File(path);
	        File dest = new File("D:\\DIStmp\\"+docName);
	        copyFileUsingChannel(source, dest);
	      PrimaryFile = "D:\\DIStmp\\"+docName;
	        return PrimaryFile;
	    } catch (Exception e) {
	      SystemUtils.trace("system", "failed");
	    }
	    if(PrimaryFile ==""){
	    	return "Gagal";
	    }
	    else
	    	return PrimaryFile;

	  }
	
	private static void copyFileUsingChannel(File source, File dest) throws IOException {
        FileChannel sourceChannel = null;
        FileChannel destChannel = null;
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destChannel = new FileOutputStream(dest).getChannel();
            destChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
           }finally{
               sourceChannel.close();
               destChannel.close();
       }
        
    }
	
	public void deleteFile(String filePath){
		
		File files = new File(filePath);
		try{
			files.delete();
			Log.info("File Deleted: " + filePath);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
}
