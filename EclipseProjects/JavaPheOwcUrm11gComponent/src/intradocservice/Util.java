package intradocservice;

import intradoc.common.ExecutionContext;
import intradoc.common.LocaleUtils;
import intradoc.common.Log;
import intradoc.common.ServiceException;

import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;

import intradoc.provider.Provider;
import intradoc.provider.Providers;

import intradoc.server.Service;
import intradoc.server.ServiceData;
import intradoc.server.ServiceManager;

import intradoc.server.UserStorage;

import intradoc.shared.UserData;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;

import java.util.Date;

import oracle.stellent.ridc.IdcClient;
import oracle.stellent.ridc.IdcClientManager;
import oracle.stellent.ridc.IdcContext;
import oracle.stellent.ridc.protocol.ServiceResponse;

import util.DataCollection;

public class Util {
	public Util() {
		super();
	}

	private String UserLogin;

	public void setUserLogin(String UserLogin) {
		this.UserLogin = UserLogin;
	}

	public String getUserLogin() {
		return UserLogin;
	}

	public ResultSet GetExpiredContent(DataBinder binder) {
		DataCollection dc = new DataCollection();
		try {
			binder.putLocal("IdcService", "GET_EXPIRED");
			String executeService = dc.executeService(binder, this.UserLogin, true);
			ResultSet rs = binder.getResultSet("SearchResults");
			return rs;
		} catch (Exception ex) {
			return null;
		}
	}

	public String CheckinNewVersion(DataBinder binder) {
		Log.info("PHE:CheckinNewVersion:enter");
		DataCollection dc = new DataCollection();
		try {
			DataBinder binderCheckout = new DataBinder();
			binderCheckout.putLocal("dDocName", binder.getLocal("dDocName"));
			binderCheckout.putLocal("IdcService", "CHECKOUT_BY_NAME");
			Log.info("PHE:CheckinNewVersion:binderCheckout:" + binderCheckout);
			if (!dc.executeService(binderCheckout, this.UserLogin, true).equalsIgnoreCase("executeFailed")) {
				binder.putLocal("isJavaMethod", "true");
				binder.putLocal("isUpdate", "true");
				binder.putLocal("isCustom", "true");
				binder.putLocal("IdcService", "CHECKIN_BYNAME");

				return dc.executeService(binder, this.UserLogin, true);
			} else
				return "executeFailed";
		} catch (Exception ex) {
			return "executeFailed";
		}
	}

	public String UpdateCheckin(DataBinder binder) {
		DataCollection dc = new DataCollection();
		try {
			binder.putLocal("isCustom", "true");
			binder.putLocal("IdcService", "UPDATE_DOCINFO_BYFORM");
			return dc.executeService(binder, this.UserLogin, true);
		} catch (Exception ex) {
			return "executeFailed";
		}
	}

	public String UpdateCheckin_DocInfo(DataBinder binder) {
		DataCollection dc = new DataCollection();
		try {
			binder.putLocal("IdcService", "UPDATE_DOCINFO");

			return dc.executeService(binder, this.UserLogin, true);
		} catch (Exception ex) {
			Log.error("PHE:UpdateCheckin_DocInfo:ex:" + ex.getMessage());
			return "executeFailed";
		}
	}

	public String NavigateToConfirmCheckin(DataBinder binder, Workspace ws) {
		DataCollection dc = new DataCollection();
		try {
			String dID;
			dID = binder.getLocal("dID");
			binder.putLocal("IdcService", "CHECKIN_CONFIRM_FORM");
			binder.putLocal("RedirectParams", "IdcService=CHECKIN_CONFIRM_FORM&dID=" + dID);
			// Log.info("Binder : " + binder);
			return dc.executeService(binder, this.UserLogin, true);
		} catch (Exception ex) {
			return "executeFailed";
		}
	}

	public String DoCheckin(DataBinder binder, Workspace ws) {
		DataCollection dc = new DataCollection();
		try {
			binder.putLocal("isCustom", "true");
			binder.putLocal("IdcService", "CHECKIN_UNIVERSAL");
			binder.putLocal("dDocAccount", "");
			binder.putLocal("createPrimaryMetaFile", "1");
			Log.info("PHE:docheckin:binder:" + binder);

			return dc.executeService(binder, this.UserLogin, true);
		} catch (Exception ex) {
			return "executeFailed";
		}
	}
	
	public String DoCheckout(DataBinder binder, Workspace ws) {
		DataCollection dc = new DataCollection();
		try {
			binder.putLocal("isCustom", "true");
			binder.putLocal("IdcService", "CHECKOUT");
			Log.info("PHE:docheckout:binder:" + binder);

			return dc.executeService(binder, this.UserLogin, true);
		} catch (Exception ex) {
			return "executeFailed";
		}
	}

	public void executeService(DataBinder binder, String userName, boolean suppressServiceError, Workspace ws)
			throws DataException, ServiceException {
		String cmd = binder.getLocal("IdcService");
		if (cmd == null) {
			throw new DataException("!csIdcServiceMissing");
		}

		ServiceData serviceData = ServiceManager.getFullService(cmd);

		if (serviceData == null) {
			throw new DataException(LocaleUtils.encodeMessage("!csNoServiceDefined", null, cmd));
		}
		Service service = ServiceManager.createService(serviceData.m_classID, ws, null, binder, serviceData);
		UserData fullUserData = getFullUserData(userName, service, ws);
		service.setUserData(fullUserData);
		binder.m_environment.put("REMOTE_USER", userName);
		ServiceException error = null;
		try {
			service.setSendFlags(true, true);
			service.initDelegatedObjects();
			service.globalSecurityCheck();
			service.preActions();
			service.doActions();
			service.postActions();
			service.updateSubjectInformation(true);
			service.updateTopicInformation(binder);
		} catch (ServiceException e) {
			error = e;
			Log.error("PHE:executeService:err:" + e.getMessage());
		} finally {
			service.cleanUp(true);
			// ws.releaseConnection();
		}
		if (error != null) {
			if (suppressServiceError) {
				error.printStackTrace();
				if (binder.getLocal("StatusCode") == null) {
					binder.putLocal("StatusCode", String.valueOf(error.m_errorCode));
					binder.putLocal("StatusMessage", error.getMessage());
				}
			} else {
				throw new ServiceException(error.m_errorCode, error.getMessage());
			}
		}
	}

	public Workspace getSystemWorkspace() {
		Workspace workspace = null;
		Provider wsProvider = Providers.getProvider("SystemDatabase");
		if (wsProvider != null) {
			workspace = (Workspace) wsProvider.getProvider();
		}
		return workspace;
	}

	public UserData getFullUserData(String userName, ExecutionContext cxt, Workspace ws)
			throws DataException, ServiceException {
		if (ws == null) {
			ws = getSystemWorkspace();
		}
		UserData userData = UserStorage.retrieveUserDatabaseProfileDataFull(userName, ws, null, cxt, true, true);
		ws.releaseConnection();
		return userData;
	}

	public static String getCurrentMonth() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	public static String getCurrentDate() {
		SimpleDateFormat sdfDate = new SimpleDateFormat("dd");
		Date now = new Date();
		String strDate = sdfDate.format(now);
		return strDate;
	}

	public String getCurrentHour() {
		SimpleDateFormat sdfTime = new SimpleDateFormat("HH");
		Date now = new Date();
		String strDate = sdfTime.format(now);
		return strDate;
	}

	public String UpdateCheckin(DataBinder binder, Workspace ws) {
		DataCollection dc = new DataCollection();
		try {
			binder.putLocal("IdcService", "UPDATE_DOCINFO_BYFORM");
			Log.info("PHE:UpdateCheckin_DocInfoByForm:binder:" + binder);
			Log.info("this.UserLogin:" + this.UserLogin);
			return dc.executeService(binder, this.UserLogin, true);
		} catch (Exception ex) {
			Log.error("PHE:updateCheckin:ex" + ex.getMessage());
			return "executeFailed";
		}
	}

	// Haries 28-07-2016:Fungsi getfile untuk DIS

	public InputStream getFile(String documentId, String dDocName) {
		ServiceResponse severiceResponse = null;
		IdcClientManager manager = new IdcClientManager();
		try {
			//IdcClient client = manager.createClient("idc://kponwjis013.pheonwj.com:4444");
			IdcClient client = manager.createClient("idc://kponwjap005.pertamina.com:4444");
			DataBinder dataBinderReq = (DataBinder) client.createBinder();
			dataBinderReq.putLocal("IdcService", "GET_FILE");
			dataBinderReq.putLocal("dID", documentId);

			severiceResponse = client.sendRequest(new IdcContext(this.UserLogin),
					(oracle.stellent.ridc.model.DataBinder) dataBinderReq);
			InputStream is = severiceResponse.getResponseStream();
			System.out.println("GET_FILE size: " + is.available());
			return is;
		} catch (Exception ex) {
			System.out.println("Error GetFile(): " + ex.getMessage());
			Log.error("Error GetFile(): " + ex.getMessage());
		}
		return null;
	}

	public String saveDocumentToLocal(String documentName, String documentID, String dDocName) {
		try {
			// ServiceResponse response = idcClient.sendRequest(user, binder);

			// get the response as a string
			InputStream stream = getFile(documentID, dDocName);
			// dispaly the content of the file
			OutputStream ostream = new FileOutputStream("D:\\DIStmp\\" + documentName);
			//
			// int read = 0;
			// byte[] bytes = new byte[1024*256];
			// while ((read = stream.read(bytes)) != -1) {
			// ostream.write(bytes, 0, read);
			// }

			byte[] buf = new byte[1024 * 256];
			long i = 0;
			int len;

			while (true) {
				i++;
				len = stream.read(buf);
				if (len == -1) {
					break;
				}
				ostream.write(buf, 0, len);

			}
			ostream.flush();
			ostream.close();
			stream.close();

		} catch (Exception e) {
			e.printStackTrace();
			Log.error("Error GetFile(): " + e.getMessage());
			return "Gagal";
		}
		return "Sukses";
	}
}
