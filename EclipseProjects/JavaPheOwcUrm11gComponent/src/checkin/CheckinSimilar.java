package checkin;

import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradocservice.Document;
import intradocservice.Util;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.DataCollection;

public class CheckinSimilar extends Service {
	public void updateOneRevision() {
		Util util = new Util();
		Document document = new Document();
		DataBinder binder = new DataBinder();
		// DatabaseValue dVal = new DatabaseValue();
		DataCollection dColl = new DataCollection();
		DataBinder binderSiapDiExecuteChangeFolder = new DataBinder();

		String dID = this.m_binder.getLocal("dID");
		String dDocName = this.m_binder.getLocal("dDocName");
		String lastDID = "";
		String executeResult = "";

		Workspace ws = dColl.getSystemWorkspace();
		binder.putLocal("dUser", this.m_binder.getLocal("dUser"));

		ResultSet rs = document.GetDocInfo(dID, binder, ws, true, util);

		ResultSet rs2 = document.GetDocInfoFolderID(dID, binder, ws, true, util);
		document.deleteDoc(dDocName, this.m_binder.getLocal("dUser"), ws);
		String dDocNameResult = document.checkinSimilar(this.m_binder.getLocal("dUser"), rs, "Deleted");

		lastDID = document.GetDocIDByDocName(ws, dDocNameResult);

		binderSiapDiExecuteChangeFolder.putLocal("fParentGUID", rs2.getStringValueByName("fParentGUID"));
		binderSiapDiExecuteChangeFolder.putLocal("dID", lastDID);
		binderSiapDiExecuteChangeFolder.putLocal("dDocName", dDocNameResult);
		try {
			executeResult = document.executeUpdateDocInfoJustFParentGUID(binderSiapDiExecuteChangeFolder, "",
					this.m_binder.getLocal("dUser"), true);
		} catch (DataException e) {
			Log.error(e.getMessage());
		} catch (ServiceException e) {
			Log.error(e.getMessage());
		}
		this.m_binder.putLocal("IdcService", "DOC_INFO");
		this.m_binder.putLocal("dId", lastDID);
		this.m_binder.putLocal("dDocName", dDocNameResult);
		try {
			dColl.executeService(this.m_binder, this.m_binder.getLocal("dUser"), true);
		} catch (DataException ex) {
			Logger.getLogger(CheckinSimilar.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ServiceException ex) {
			Logger.getLogger(CheckinSimilar.class.getName()).log(Level.SEVERE, null, ex);
		}
		this.m_binder.putLocal("dIDDirect", lastDID);
		this.m_binder.putLocal("dDocNameDirect", dDocNameResult);
	}
}
