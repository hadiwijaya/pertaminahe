package checkin;

import data.DatabaseValue;
import intradoc.common.FileUtils;
import intradoc.common.Log;
import intradoc.common.ServiceException;
import intradoc.data.DataBinder;
import intradoc.data.DataException;
import intradoc.data.ResultSet;
import intradoc.data.Workspace;
import intradoc.server.Service;
import intradocservice.Document;
import intradocservice.Folder;
import intradocservice.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.DataCollection;
import util.ErrorHandler;
import util.Validator;

public class precheck extends Service {
	private ErrorHandler ErrHandler = new ErrorHandler();
	private String secGroupApDrawing = "PHEDrawing";
	private String secGroupApNonDrawing = "PHENonDrawing";
	private String secGroupApTdc = "PHE_TDC";
	private String docTypeApDrawing = "Drawing";
	private String docTypeApNonDrawing = "NonDrawing";
	private String cabinetApMain = "";
	private String folderTechDocONWJ = "Technical Documents";
	private String folderTechOtherAP = "STK Documents";
	private String folderNative = "Native Documents";
	private String uploadTempHeaderMasterlistNonDrawing = "TDC MASTER LIST NONDRAWING";

	public void DoPreCheck() {
		DatabaseValue dValue = new DatabaseValue();
		Validator valid = new Validator();
		String pheAp = m_binder.getLocal("xInternalOwner") == null ? "" : m_binder.getLocal("xInternalOwner");
		if (pheAp.length() <= 0) {
			String apComp = m_binder.getLocal("xCompany") == null ? "" : m_binder.getLocal("xCompany");
			pheAp = valid.verifyApPrefix(apComp);
		} else {
			pheAp = pheAp.replace(" ", "").toUpperCase();
		}

		Log.info("<== START DO PRE CHECK ==>\n" + "PHE AP PRECHECK: " + pheAp);

		if (pheAp.length() > 0) {
			// Find SECURITY GROUP DRAWING for the cabinet based on AP, from PHE_CONFIG
			// table
			String keyConfApSGD = "SECGRP_DRAWING_" + pheAp;
			try {
				secGroupApDrawing = dValue.getPheConfig(keyConfApSGD.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(precheck.class.getName()).log(Level.SEVERE, null, ex);
			}

			// Find SECURITY GROUP NONDRAWING for the cabinet based on AP, from PHE_CONFIG
			// table
			String keyConfApSGND = "SECGRP_NONDRAWING_" + pheAp;
			try {
				secGroupApNonDrawing = dValue.getPheConfig(keyConfApSGND.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(precheck.class.getName()).log(Level.SEVERE, null, ex);
			}

			// Find DOC TYPE DRAWING for the document based on AP, from PHE_CONFIG table
			String keyConfApDTD = "DOCTYP_DRAWING_" + pheAp;
			try {
				docTypeApDrawing = dValue.getPheConfig(keyConfApDTD.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(precheck.class.getName()).log(Level.SEVERE, null, ex);
			}

			// Find DOC TYPE NONDRAWING for the document based on AP, from PHE_CONFIG table
			String keyConfApDTND = "DOCTYP_NONDRAWING_" + pheAp;
			try {
				docTypeApNonDrawing = dValue.getPheConfig(keyConfApDTND.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(precheck.class.getName()).log(Level.SEVERE, null, ex);
			}
			String keyConfApTDC = "SECGRP_TDC_" + pheAp;
			try {
				secGroupApTdc = dValue.getPheConfig(keyConfApTDC.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(precheck.class.getName()).log(Level.SEVERE, null, ex);
			}

			// Find ROOT cabinet name based on AP, from PHE_CONFIG table
			String keyConfApCab = "CABINETS_" + pheAp;
			try {
				cabinetApMain = dValue.getPheConfig(keyConfApCab.toUpperCase().trim());
			} catch (DataException ex) {
				Logger.getLogger(precheck.class.getName()).log(Level.SEVERE, null, ex);
			}
		} else {
			Log.info("PHE:precheck.java:DoPreCheck:99: AP Parameter not defined !");
		}
		Validator validator = new Validator();
		DataCollection dc = new DataCollection();
		Workspace ws = dc.getSystemWorkspace();
		Util util = new Util();
		DataBinder tmpBinder = new DataBinder();
		tmpBinder.putLocal("dUser", this.m_binder.getLocal("dUser"));
		boolean hasError = false;
		boolean HaveNoValidDisciplineCode = false;
		boolean isCreatePartnerNonDrawing = false;
		boolean isOutofScenario = false;
		try {
			String updateStatus = this.m_binder.getLocal("isUpdate");
			boolean isUpdate;
			if (updateStatus == null) {
				isUpdate = false;
			} else {
				if (updateStatus.equalsIgnoreCase("0")) {
					isUpdate = false;
				} else {
					isUpdate = true;
				}
			}
			Document doc = new Document();
			Folder fld = new Folder();
			util.setUserLogin(this.m_binder.getLocal("dUser"));
			DatabaseValue db = new DatabaseValue();
			String type = this.m_binder.getLocal("dDocType");
			if (!isUpdate) {
				if (type.toUpperCase().contains("MASTERLIST")) {
					DataBinder tempBinder = this.m_binder;
					String docNumber = "";
					String xauthorOrg = "";
					String xlocation;
					String xplatform;
					String xdisciplincode;
					String xdoctype;
					String xrunningNumber;
					String xsheetNumber;
					boolean isTDCDrawing;
					if (IsDrawing()) {
						docNumber = this.m_binder.getLocal("xDocNumber");
						xlocation = this.m_binder.getLocal("xDLocation");
						xplatform = this.m_binder.getLocal("xDPlatform");
						xdisciplincode = this.m_binder.getLocal("xDDisciplineCode");
						xdoctype = this.m_binder.getLocal("xDDocTypeCode");
						xrunningNumber = this.m_binder.getLocal("xRunningNumber");
						xsheetNumber = this.m_binder.getLocal("xSheetNumber");
						docNumber = xlocation + "-" + xplatform + "-" + xdisciplincode + "-" + xdoctype + "-"
								+ xrunningNumber;
						isTDCDrawing = true;
						if (xsheetNumber.length() > 0) {
							if (validator.isNumeric(xsheetNumber)) {
								this.m_binder.putLocal("xSheetNumber", xsheetNumber);
							} else {
								this.ErrHandler.ErrSheetNumberNotNumeric(this.m_binder);
								hasError = true;
							}
						}
						if (!dValue.validDisciplineCode(xdisciplincode, xrunningNumber, type)) {
							hasError = true;
							HaveNoValidDisciplineCode = true;
						}
						this.m_binder.putLocal("dSecurityGroup", this.secGroupApDrawing);
					} else {
						docNumber = this.m_binder.getLocal("xDocNumber");
						xlocation = this.m_binder.getLocal("xNdLocation");
						xplatform = "";
						xdisciplincode = this.m_binder.getLocal("xNdDisciplineCode");
						xdoctype = this.m_binder.getLocal("xNdDocTypeCode");
						xrunningNumber = this.m_binder.getLocal("xRunningNumber");
						xsheetNumber = null;
						docNumber = xlocation + "-" + xdisciplincode + "-" + xdoctype + "-" + xrunningNumber;
						xauthorOrg = this.m_binder.getLocal("xAuthorOrganization");
						isTDCDrawing = false;
						this.m_binder.putLocal("dSecurityGroup", secGroupApNonDrawing);
					}
					if (fld.IsContentExist(ws, xlocation, xplatform, xdisciplincode, xdoctype, xrunningNumber,
							xsheetNumber, isTDCDrawing)) {
						this.ErrHandler.ErrDocNumberExists(this.m_binder);
					} else if (HaveNoValidDisciplineCode && pheAp == "PHEONWJ") {
						this.ErrHandler.ErrColaborateDisciplineCode(this.m_binder);
					} else {
						if (!IsValidDocNumber(docNumber, type)) {
							this.ErrHandler.ErrInvalidDocNumber(this.m_binder);
						} else if (!hasError) {
							String executeResult = util.DoCheckin(tempBinder, ws);
							if (executeResult.indexOf("executeFailed") >= 0) {
								this.ErrHandler.ErrCheckin(this.m_binder);
							} else {
								DataBinder binderDocInfo = new DataBinder();

								binderDocInfo.putLocal("dUser", this.m_binder.getLocal("dUser"));

								String[] splitPerIDan = executeResult.split(";");
								String tempdDocName = splitPerIDan[1];
								String tempdID = splitPerIDan[0];
								DataBinder binderUpdateDocInfo = new DataBinder();

								executeResult = doc.executeUpdateDocInfoJustObjectID(binderUpdateDocInfo, "",
										tempdDocName, tempdID, type, this.m_binder.getLocal("dUser"), true);

								if (type.indexOf("NonDrawing") >= 0) {
									isCreatePartnerNonDrawing = true;
								}

								DataBinder binders = new DataBinder();
								binders.putLocal("AutoNumberPrefix", "EDMS");
								binders.putLocal("IsAutoNumber", "true");
								binders.putLocal("xDocNumber", docNumber);
								binders.putLocal("xNdLocation", xlocation);
								binders.putLocal("xNdDisciplineCode", xdisciplincode);
								binders.putLocal("xNdDocTypeCode", xdoctype);
								binders.putLocal("xRunningNumber", xrunningNumber);
								binders.putLocal("xAuthorOrganization", xauthorOrg);

								createPartnerTDCNonDrawing(executeResult, util, isCreatePartnerNonDrawing, ws, doc,
										tempdID, binders, tempdDocName, type);
							}
						}
					}
				} else if ((type.equalsIgnoreCase(docTypeApDrawing)) || (type.equalsIgnoreCase(docTypeApNonDrawing))
						|| (type.equalsIgnoreCase("NonDrawingPassive"))) {
					Log.info("<== INSIDE DOCTYPE DRAWING OR NON DRAWING ==>");
					String docNumber = "";
					String revision = "";
					String sheetNumber = "";
					Boolean HaveNoRevision = Boolean.valueOf(false);
					Boolean HaveNoValid3D = Boolean.valueOf(false);
					String tmpDocNumber = this.m_binder.getLocal("xDocNumber");
					if (!tmpDocNumber.equalsIgnoreCase("invalidDocName")) {
						if (IsDrawing()) {
							if (tmpDocNumber.contains("~")) {
								docNumber = tmpDocNumber.substring(0, tmpDocNumber.indexOf("~"));
								revision = tmpDocNumber.substring(tmpDocNumber.indexOf("~") + 1);
								if (revision.contains("-")) {
									sheetNumber = revision.substring(revision.indexOf("-") + 1);
									revision = revision.substring(0, revision.indexOf("-"));
								}
							} else {
								HaveNoRevision = Boolean.valueOf(true);
							}
							String[] combination = docNumber.split("-");
							String docModel = this.m_binder.getLocal("xDocModel");
							if (!IsValidDoc3D(combination[3], type, docModel)) {
								HaveNoValid3D = Boolean.valueOf(true);
							} else if (!dValue.validDisciplineCode(combination[2], combination[4], type)) {
								HaveNoValidDisciplineCode = true;
							}
						} else {
							if (tmpDocNumber.contains("~")) {
								docNumber = tmpDocNumber.substring(0, tmpDocNumber.indexOf("~"));
								revision = tmpDocNumber.substring(tmpDocNumber.indexOf("~") + 1);
								if (revision.contains("-")) {
									sheetNumber = revision.substring(revision.indexOf("-") + 1);
									revision = revision.substring(0, revision.indexOf("-"));
								}
							} else {
								HaveNoRevision = Boolean.valueOf(true);
							}
						}
					}
					int flagNextAction = 0;
					String docPurpose = this.m_binder.getLocal("xDocPurpose");

					Boolean isDocPurposeNative = Boolean.valueOf(docPurpose.equalsIgnoreCase("NATIVE"));
					String checkDocument = dValue.checkDocumentName(docNumber, Integer.valueOf(revision),
							sheetNumber.toString(), docPurpose.toString());
					if (!IsValidDocNumber(docNumber, type)) {
						if (HaveNoRevision.booleanValue()) {
							this.ErrHandler.ErrInvalidDocName(this.m_binder);
						} else {
							this.ErrHandler.ErrInvalidDocNumber(this.m_binder);
						}
					} else if (tmpDocNumber.equalsIgnoreCase("invalidDocName")) {
						this.ErrHandler.ErrInvalidDocNumber(this.m_binder);
					} else if (HaveNoRevision.booleanValue()) {
						this.ErrHandler.ErrInvalidDocName(this.m_binder);
					} else if (HaveNoValid3D.booleanValue()) {
						this.ErrHandler.errColaborate3DDisciplineCode(this.m_binder);
					} else if (HaveNoValidDisciplineCode && pheAp == "PHEONWJ") {
						this.ErrHandler.ErrColaborateDisciplineCode(this.m_binder);
					} else if (!validator.isNumeric(revision)) {
						this.ErrHandler.ErrRevisionsNotNumeric(this.m_binder);
					} else if (checkDocument.equalsIgnoreCase("revisionError")) {
						this.ErrHandler.ErrLessDocRevision(this.m_binder);
					} else if (checkDocument.equals("revisionAlreadyExist")) {
						this.ErrHandler.ErrDocNumberExists(this.m_binder);
					} else {
						Log.info("<== NO ERROR: CONTINUE ==>");
						String dID = doc.GetDocID(ws, docNumber, sheetNumber, revision, false);
						String dIDNative = doc.GetDocID(ws, docNumber, sheetNumber, revision, true);
						String dIDRegistered = doc.GetDocIDRegistered(ws, docNumber, sheetNumber);
						String dIDLatestPublish = doc.GetDocID(ws, docNumber, sheetNumber,
								isDocPurposeNative.booleanValue());

						Boolean isNative;
						if (dIDNative != null) {
							if (dIDNative.equalsIgnoreCase("0")) {
								isNative = Boolean.valueOf(false);
							} else {
								isNative = Boolean.valueOf(true);
							}
						} else {
							isNative = Boolean.valueOf(false);
						}
						ResultSet rsDocInfo = dID.equalsIgnoreCase("0") ? null
								: dID.isEmpty() ? null : doc.GetDocInfo(dID, tmpBinder, ws, false, util);
						ResultSet rsDocInfoNative = dIDNative.equalsIgnoreCase("0") ? null
								: dIDNative.isEmpty() ? null : doc.GetDocInfo(dIDNative, tmpBinder, ws, false, util);
						ResultSet rsDocInfoRegistered = dIDRegistered.equalsIgnoreCase("0") ? null
								: dIDRegistered.isEmpty() ? null
										: doc.GetDocInfo(dIDRegistered, tmpBinder, ws, false, util);

						Boolean isNeedCheckout = Boolean.valueOf(false);
						ResultSet rsDocInfoLatestPublish = null;
						if (!dIDLatestPublish.equalsIgnoreCase("0")) {
							rsDocInfoLatestPublish = doc.GetDocInfo(dIDLatestPublish, tmpBinder, ws, false, util);
							isNeedCheckout = Boolean.valueOf(true);
						}
						Log.info("*#* PRECHECK NEED CHECK OUT: " + isNeedCheckout.toString());

						int tmpRevision = 0;
						String tmpStatus2;
						ResultSet tmpRS;
						String tmpStatus;
						if (docPurpose.trim().toUpperCase().equals("NATIVE")) {
							tmpRS = rsDocInfoNative;
							tmpStatus = "Native";
							tmpStatus2 = "Native";
						} else {
							tmpRS = rsDocInfo;
							tmpStatus = "Published";
							tmpStatus2 = "Implemented";
						}
						try {
							tmpRevision = tmpRS.getStringValueByName("xRevision") == null ? 0
									: Integer.valueOf(tmpRS.getStringValueByName("xRevision")).intValue();
						} catch (Exception ex) {
							tmpRevision = 0;
						}
						String tmpSheetNumber = "";
						if (tmpRS != null) {
							try {
								tmpSheetNumber = tmpRS.getStringValueByName("xSheetNumber");
							} catch (Exception ex) {
								Log.error("PHE:--a:exception:noSheetNumber");
							}
						}
						if ((tmpRS != null) && (tmpRevision > Integer.valueOf(revision).intValue())
								&& ((tmpRS.getStringValueByName("xStatus").equalsIgnoreCase(tmpStatus))
										|| (tmpRS.getStringValueByName("xStatus").equalsIgnoreCase(tmpStatus2)))) {
							this.ErrHandler.ErrLessDocRevision(this.m_binder);
						} else if ((tmpRS != null) && (tmpRevision == Integer.valueOf(revision).intValue())
								&& (sheetNumber.equals(tmpSheetNumber))
								&& ((tmpRS.getStringValueByName("xStatus").equalsIgnoreCase(tmpStatus))
										|| (tmpRS.getStringValueByName("xStatus").equalsIgnoreCase(tmpStatus2)))) {
							this.ErrHandler.ErrDocNumberExists(this.m_binder);
						} else {
							String[] combination = docNumber.split("-");

							this.m_binder.putLocal("xDocNumber", docNumber);
							this.m_binder.putLocal("xDocName", tmpDocNumber);
							this.m_binder.putLocal("xRevision", revision);
							if (IsDrawing()) {
								this.m_binder.putLocal("xDLocation", combination[0]);
								this.m_binder.putLocal("xDPlatform", combination[1]);
								this.m_binder.putLocal("xDDisciplineCode", combination[2]);
								this.m_binder.putLocal("xDDocTypeCode", combination[3]);
								this.m_binder.putLocal("xRunningNumber", combination[4]);
								if (sheetNumber != null) {
									if (validator.isNumeric(sheetNumber)) {
										this.m_binder.putLocal("xSheetNumber", sheetNumber);
									} else {
										this.ErrHandler.ErrSheetNumberNotNumeric(this.m_binder);
									}
								}

								Log.info("*#* PRECHECK SECGROUP DRAWING: \n" + "secGroupApTdc: " + secGroupApTdc
										+ "secGroupApDrawing: " + secGroupApDrawing);

								if (isNative.booleanValue()) {
									this.m_binder.putLocal("dSecurityGroup", secGroupApTdc);
								} else {
									this.m_binder.putLocal("dSecurityGroup", secGroupApDrawing);
								}
							} else {
								try {
									if (!validator.isNumeric(combination[3])) {
										try {
											combination[3] = combination[3].substring(0, combination[3].indexOf("."));
										} catch (Exception e) {
											Log.error("PHE:--a:runningnumber:combination[3]" + combination[4]);
										}
									}
								} catch (Exception ex) {
									try {
										combination[3] = combination[3].substring(0, combination[3].indexOf("."));
									} catch (Exception e) {
										Log.error("PHE:--a:runningnumber:combination[3]" + combination[4]);
									}
								}
								this.m_binder.putLocal("xNdLocation", combination[0]);
								this.m_binder.putLocal("xNdDisciplineCode", combination[1]);
								this.m_binder.putLocal("xNdDocTypeCode", combination[2]);
								this.m_binder.putLocal("xRunningNumber", combination[3]);

								Log.info("*#* PRECHECK SECGROUP NON DRAWING: \n" + "secGroupApTdc: " + secGroupApTdc
										+ "secGroupApDrawing: " + secGroupApNonDrawing);

								if (isNative.booleanValue()) {
									this.m_binder.putLocal("dSecurityGroup", secGroupApTdc);
								} else {
									this.m_binder.putLocal("dSecurityGroup", secGroupApNonDrawing);
								}
							}
							if (docPurpose.equalsIgnoreCase("Native")) {
								dIDRegistered = "0";
							}
							if (!dIDRegistered.equalsIgnoreCase("0")) {
								flagNextAction = 11;
							} else if (!dID.equalsIgnoreCase("0")) {
								if (docPurpose.toUpperCase().equalsIgnoreCase("PUBLISHED")) {
									this.ErrHandler.ErrDocNumberExists(this.m_binder);
								} else if (docPurpose.toUpperCase().equalsIgnoreCase("NATIVE")) {
									if (IsDrawing()) {
										if (this.m_binder.getLocal("xDocModel").equalsIgnoreCase("3D")) {
											flagNextAction = 2;
										} else {
											flagNextAction = 1;
										}
									} else {
										flagNextAction = 1;
									}
								}
							} else if (docPurpose.toUpperCase().equalsIgnoreCase("PUBLISHED")) {
								if (dIDNative.equalsIgnoreCase("0")) {
									flagNextAction = 2;
								} else {
									flagNextAction = 3;
								}
							} else if (docPurpose.toUpperCase().equalsIgnoreCase("NATIVE")) {
								if ((IsDrawing()) && (this.m_binder.getLocal("xDocModel").equalsIgnoreCase("3D"))) {
									flagNextAction = 2;
								} else {
									flagNextAction = 1;
								}
							}
							switch (flagNextAction) {
							case 1:
								this.m_binder.putLocal("xStatus", "Native");
								if (!dID.equalsIgnoreCase("0")) {
									db.UpdateDocMetaStatus("Implemented", dID, ws);
								}
								break;
							case 2:
								this.m_binder.putLocal("xStatus", "Published");
								this.m_binder.putLocal("xDocPurpose", "Published");
								break;
							case 3:
								this.m_binder.putLocal("xStatus", "Implemented");
								break;
							case 11:
								this.m_binder.putLocal("AutoNumberPrefix", "EDMS");
								this.m_binder.putLocal("dID", rsDocInfoRegistered.getStringValueByName("dID"));
								this.m_binder.putLocal("dDocName",
										rsDocInfoRegistered.getStringValueByName("dDocName"));
								if (docPurpose.equalsIgnoreCase("Published")) {
									if (dIDNative.equalsIgnoreCase("0")) {
										this.m_binder.putLocal("xStatus", "Published");
									} else {
										this.m_binder.putLocal("xStatus", "Implemented");
									}
								} else if (docPurpose.equalsIgnoreCase("Native")) {
									this.m_binder.putLocal("xStatus", "Native");
								}
								String dDocName = rsDocInfoRegistered.getStringValueByName("dDocName");
								String dDocTitle = rsDocInfoRegistered.getStringValueByName("dDocTitle");

								tmpBinder = this.m_binder;
								tmpBinder.putLocal("dDocName", dDocName);

								DataBinder binders = new DataBinder();
								binders.putLocal("isJavaMethod", "true");
								if (this.m_binder.getLocal("dDocType").toUpperCase().contains("NONDRAWING")) {
									Log.info("*#* DOCTYPE NON DRAWING");

									binders.putLocal("xNdLocation", this.m_binder.getLocal("xNdLocation"));
									binders.putLocal("xNdDisciplineCode", this.m_binder.getLocal("xNdDisciplineCode"));
									binders.putLocal("xNdDocTypeCode", this.m_binder.getLocal("xNdDocTypeCode"));
									binders.putLocal("xRunningNumber", this.m_binder.getLocal("xRunningNumber"));
									binders.putLocal("primaryFile", this.m_binder.getLocal("primaryFile"));
									binders.putLocal("primaryFile:path", this.m_binder.getLocal("primaryFile:path"));
									binders.putLocal("xDocName", this.m_binder.getLocal("xDocName"));
									binders.putLocal("xDocNumber", this.m_binder.getLocal("xDocNumber"));
									binders.putLocal("dDocTitle", this.m_binder.getLocal("dDocTitle"));
									dDocTitle = this.m_binder.getLocal("dDocTitle");
									binders.putLocal("xDocPurpose", this.m_binder.getLocal("xDocPurpose"));
									binders.putLocal("xStatus", this.m_binder.getLocal("xStatus"));
									binders.putLocal("xOldDocNumber", this.m_binder.getLocal("xOldDocNumber"));
									binders.putLocal("xAuthor", this.m_binder.getLocal("xAuthor"));
									binders.putLocal("xAuthorOrganization",
											this.m_binder.getLocal("xAuthorOrganization"));
									binders.putLocal("xAuthorGroupName", this.m_binder.getLocal("xAuthorGroupName"));
									binders.putLocal("xCompany", this.m_binder.getLocal("xCompany"));
									binders.putLocal("xProjectName", this.m_binder.getLocal("xProjectName"));
									binders.putLocal("xContractor", this.m_binder.getLocal("xContractor"));
									binders.putLocal("xContractNumber", this.m_binder.getLocal("xContractNumber"));
									binders.putLocal("xWONumber", this.m_binder.getLocal("xWONumber"));
									binders.putLocal("dSecurityGroup", secGroupApNonDrawing);
									binders.putLocal("xRevision", this.m_binder.getLocal("xRevision"));
									binders.putLocal("dDocAccount", "");
									if (!this.m_binder.getLocal("dDocType").toUpperCase()
											.contains("NONDRAWINGPASSIVE")) {
										binders.putLocal("xRetentionPeriod",
												this.m_binder.getLocal("xRetentionPeriod"));
										binders.putLocal("xRetentionDate", this.m_binder.getLocal("xRetentionDate"));
									}
								} else if (this.m_binder.getLocal("dDocType").toUpperCase().contains("DRAWING")) {
									Log.info("*#* PRECHECK SECGROUP DRAWING");

									binders.putLocal("xDLocation", this.m_binder.getLocal("xDLocation"));
									binders.putLocal("xDPlatform", this.m_binder.getLocal("xDPlatform"));
									binders.putLocal("xDDisciplineCode", this.m_binder.getLocal("xDDisciplineCode"));
									binders.putLocal("xDDocTypeCode", this.m_binder.getLocal("xDDocTypeCode"));
									binders.putLocal("xRunningNumber", this.m_binder.getLocal("xRunningNumber"));
									binders.putLocal("primaryFile", this.m_binder.getLocal("primaryFile"));
									binders.putLocal("primaryFile:path", this.m_binder.getLocal("primaryFile:path"));
									binders.putLocal("xDocName", this.m_binder.getLocal("xDocName"));
									binders.putLocal("xDocNumber", this.m_binder.getLocal("xDocNumber"));
									binders.putLocal("dDocTitle", this.m_binder.getLocal("dDocTitle"));
									binders.putLocal("dSecurityGroup", secGroupApDrawing);
									dDocTitle = this.m_binder.getLocal("dDocTitle");
									try {
										binders.putLocal("xSheetNumber", this.m_binder.getLocal("xSheetNumber"));
									} catch (Exception ex) {
									}
									if (this.m_binder.getLocal("xDocModel").equalsIgnoreCase("3D")) {
										binders.putLocal("xStatus", "Published");
										binders.putLocal("xDocPurpose", "Published");
									} else {
										binders.putLocal("xStatus", this.m_binder.getLocal("xStatus"));
										binders.putLocal("xDocPurpose", this.m_binder.getLocal("xDocPurpose"));
									}
									binders.putLocal("xMOCNumber", this.m_binder.getLocal("xMOCNumber"));
									binders.putLocal("xOldDocNumber", this.m_binder.getLocal("xOldDocNumber"));
									binders.putLocal("xAuthor", this.m_binder.getLocal("xAuthor"));
									binders.putLocal("xAuthorOrganization",
											this.m_binder.getLocal("xAuthorOrganization"));
									binders.putLocal("xAuthorGroupName", this.m_binder.getLocal("xAuthorGroupName"));
									binders.putLocal("xCompany", this.m_binder.getLocal("xCompany"));
									binders.putLocal("xProjectName", this.m_binder.getLocal("xProjectName"));
									binders.putLocal("xContractor", this.m_binder.getLocal("xContractor"));
									binders.putLocal("xContractNumber", this.m_binder.getLocal("xContractNumber"));
									binders.putLocal("xWONumber", this.m_binder.getLocal("xWONumber"));

									binders.putLocal("xRevision", this.m_binder.getLocal("xRevision"));
									binders.putLocal("dDocAccount", "");

									binders.putLocal("xDocModel", this.m_binder.getLocal("xDocModel"));
									binders.putLocal("xComments", this.m_binder.getLocal("xComments"));
								}
								binders.putLocal("dID", rsDocInfoRegistered.getStringValueByName("dID"));
								binders.putLocal("dDocName", dDocName);

								binders.putLocal("createPrimaryMetaFile", "0");
								binders.putLocal("isUpdate", "true");

								binders.putLocal("dDocAuthor", this.m_binder.getLocal("dDocAuthor"));
								binders.putLocal("dDocType", this.m_binder.getLocal("dDocType"));
								binders.putLocal("isCustom", "true");
								if (StorePrimaryFile()) {
									String executeResult = util.UpdateCheckin(binders, ws);
									if (executeResult.indexOf("executeFailed") >= 0) {
										this.ErrHandler.ErrCheckin(this.m_binder);
									} else {
										if (util.NavigateToConfirmCheckin(this.m_binder, ws)
												.equalsIgnoreCase("executeFailed")) {
											this.ErrHandler.ErrCheckin(this.m_binder);
										}
									}
								} else {
									this.ErrHandler.ErrFailedUpload(this.m_binder);
								}
								break;
							default:
								isOutofScenario = true;
							}
							if (flagNextAction != 11) {
								if (!isOutofScenario) {
									this.m_binder.putLocal("AutoNumberPrefix", "EDMS");
									if (isNeedCheckout.booleanValue()) {
										this.m_binder.putLocal("dDocName",
												rsDocInfoLatestPublish.getStringValueByName("dDocName"));
										DataBinder binderDocCheckout = new DataBinder();
										binderDocCheckout.putLocal("dID",
												rsDocInfoLatestPublish.getStringValueByName("dID"));
										binderDocCheckout.putLocal("dDocName",
												rsDocInfoLatestPublish.getStringValueByName("dDocName"));
										binderDocCheckout.putLocal("dDocTitle",
												rsDocInfoLatestPublish.getStringValueByName("dDocTitle"));

										String executeResult = util.DoCheckout(binderDocCheckout, ws);
										if (executeResult.indexOf("executeFailed") >= 0) {
											this.ErrHandler.ErrCheckout(binderDocCheckout);
										}
									}
									if (StorePrimaryFile()) {
										String executeResult = util.DoCheckin(this.m_binder, ws);
										if (executeResult.indexOf("executeFailed") >= 0) {
											this.ErrHandler.ErrCheckin(this.m_binder);
										} else {
											this.m_binder.putLocal("RedirectUrl",
													"<$HttpCgiPath$>?IdcService=CHECKIN_CONFIRM_FORM&dID=<$dID$>&dDocTitle=<$url(dDocTitle)$>&dDocName=<$url(dDocName)$>&dDocAuthor=<$url(dDocAuthor)$>&fParentGUID=<$url(fParentGUID)$>&fFolderName=<$url(fFolderName)$>&fApplication=<$url(fApplication)$>&fFileGUID=<$url(fFileGUID)$>");

											DataBinder binderDocInfo = new DataBinder();
											binderDocInfo.putLocal("dUser", this.m_binder.getLocal("dUser"));
											String[] splitPerIDan = executeResult.split(";");
											String tempdDocName = splitPerIDan[1];
											String tempdID = splitPerIDan[0];
											DataBinder binderUpdateDocInfo = new DataBinder();
											executeResult = doc.executeUpdateDocInfoJustObjectID(binderUpdateDocInfo,
													"", tempdDocName, tempdID, type, this.m_binder.getLocal("dUser"),
													true);

											String parentDir = "";
											if (this.m_binder.getLocal("xDocPurpose").toUpperCase()
													.equals("PUBLISHED")) {
												parentDir = "/" + cabinetApMain + "/"
														+ (type.toUpperCase().equals("NONDRAWING") ? folderTechDocONWJ
																: type.toUpperCase().equals("DRAWING")
																		? folderTechDocONWJ
																		: type.toUpperCase().equals("NONDRAWINGPASSIVE")
																				? folderTechDocONWJ
																				: folderTechOtherAP)
														+ "/";
											} else if ((IsDrawing())
													&& (this.m_binder.getLocal("xDocModel").equalsIgnoreCase("3D"))) {
												parentDir = "/" + cabinetApMain + "/"
														+ (type.toUpperCase().equals("NONDRAWING") ? folderTechDocONWJ
																: type.toUpperCase().equals("DRAWING")
																		? folderTechDocONWJ
																		: type.toUpperCase().equals("NONDRAWINGPASSIVE")
																				? folderTechDocONWJ
																				: folderTechOtherAP)
														+ "/";
											} else {
												parentDir = "/" + cabinetApMain + "/" + folderNative + "/";
											}
											String folderPath = "";
											if (this.m_binder.getLocal("dDocType").toUpperCase()
													.contains("NONDRAWING")) {
												parentDir = parentDir + "Non Drawings";
												folderPath = this.m_binder.getLocal("xNdLocation") + "/"
														+ fld.GetDisCodeDesc(type,
																this.m_binder.getLocal("xNdDisciplineCode"),
																util.getSystemWorkspace())
														+ "/" + this.m_binder.getLocal("xNdDocTypeCode");
											} else {
												parentDir = parentDir + "Drawings";
												folderPath = this.m_binder.getLocal("xDLocation") + "/"
														+ this.m_binder.getLocal("xDPlatform") + "/"
														+ fld.GetDisCodeDesc(type,
																this.m_binder.getLocal("xDDisciplineCode"), ws)
														+ "/" + this.m_binder.getLocal("xDDocTypeCode");
											}
											DataBinder binderChangeFolder = new DataBinder();

											ResultSet rs = fld.GetFolderInfo(parentDir + "/" + folderPath,
													this.m_binder, util.getSystemWorkspace(), util);
											binderChangeFolder.putLocal("fParentGUID",
													rs.getStringValueByName("fFolderGUID"));
											binderChangeFolder.putLocal("dID", tempdID);
											binderChangeFolder.putLocal("dDocName", tempdDocName);
											executeResult = doc.executeUpdateDocInfoJustFParentGUID(binderChangeFolder,
													"", this.m_binder.getLocal("dUser"), true);
											if ((type.toUpperCase().contains("NONDRAWING"))
													&& (!isDocPurposeNative.booleanValue())
													&& (!isNeedCheckout.booleanValue())) {
												isCreatePartnerNonDrawing = true;

												DataBinder binderPartner = new DataBinder();

												binderPartner.putLocal("AutoNumberPrefix", "EDMS");
												binderPartner.putLocal("IsAutoNumber", "true");
												binderPartner.putLocal("xDocNumber", docNumber);
												binderPartner.putLocal("xNdLocation",
														this.m_binder.getLocal("xNdLocation"));
												binderPartner.putLocal("xNdDisciplineCode",
														this.m_binder.getLocal("xNdDisciplineCode"));
												binderPartner.putLocal("xNdDocTypeCode",
														this.m_binder.getLocal("xNdDocTypeCode"));
												binderPartner.putLocal("xRunningNumber",
														this.m_binder.getLocal("xRunningNumber"));
												binderPartner.putLocal("xAuthorOrganization",
														this.m_binder.getLocal("xAuthorOrganization"));
												createPartnerTDCNonDrawing(executeResult, util,
														isCreatePartnerNonDrawing, ws, doc, tempdID, binderPartner,
														tempdDocName, type);
											}
										}
									} else {
										this.ErrHandler.ErrFailedUpload(this.m_binder);
									}
								} else {
									this.ErrHandler.ErrFailedUpload(this.m_binder);
								}
							}
						}
					}
				} else if (type.equalsIgnoreCase("General")) {
					DataBinder binders = new DataBinder();
					binders.putLocal("isJavaMethod", "true");
					binders.putLocal("AutoNumberPrefix", "EDMS");
					binders.putLocal("IsAutoNumber", "true");
					binders.putLocal("isCustom", "true");
					binders.putLocal("isUpdate", "false");
					binders.putLocal("primaryFile", this.m_binder.getLocal("primaryFile"));
					binders.putLocal("primaryFile:path", this.m_binder.getLocal("primaryFile:path"));
					binders.putLocal("xDocName", this.m_binder.getLocal("xDocName"));
					binders.putLocal("xDocNumber", this.m_binder.getLocal("xDocNumber"));
					binders.putLocal("dDocTitle", this.m_binder.getLocal("dDocTitle"));
					binders.putLocal("dDocType", this.m_binder.getLocal("dDocType"));
					binders.putLocal("xDocPurpose", "Published");
					binders.putLocal("xStatus", "Published");
					binders.putLocal("xOldDocNumber", this.m_binder.getLocal("xOldDocNumber"));
					binders.putLocal("xAuthor", this.m_binder.getLocal("xAuthor"));
					binders.putLocal("xAuthorOrganization", this.m_binder.getLocal("xAuthorOrganization"));
					binders.putLocal("xCompany", this.m_binder.getLocal("xCompany"));
					binders.putLocal("xProjectName", this.m_binder.getLocal("xProjectName"));
					binders.putLocal("xBUAssetFunction", this.m_binder.getLocal("xBUAssetFunction"));
					binders.putLocal("dDocAccount", "");
					binders.putLocal("xDocModel", this.m_binder.getLocal("xDocModel"));
					binders.putLocal("xComments", this.m_binder.getLocal("xComments"));
					try {
						String documentNameDipotong = binders.getLocal("xDocName");
						if (documentNameDipotong.indexOf(".") >= 0) {
							documentNameDipotong = documentNameDipotong.substring(0, documentNameDipotong.indexOf("."));
							binders.putLocal("xDocName", documentNameDipotong);
						}
					} catch (Exception ex) {
						Log.info("PHE:precheck.java:DoPreCheck:716:ex: " + ex.getLocalizedMessage());
					}
					ResultSet rs = fld.GetFolderInfo(this.m_binder.getLocal("fParentGUID_display"), binders, ws, util);
					if (!rs.isEmpty()) {
						binders.putLocal("fParentGUID", rs.getStringValueByName("fFolderGUID"));
						try {
							if (dValue.checkDocumentNameGeneral(binders.getLocal("xDocName"),
									rs.getStringValueByName("fFolderGUID")) != "") {
								Log.error(dValue.checkDocumentNameGeneral(binders.getLocal("xDocName"),
										rs.getStringValueByName("fFolderGUID")));

								this.ErrHandler.ErrDocNumberExists(this.m_binder);
							} else if (StorePrimaryFile()) {
								String executeResult = util.DoCheckin(binders, ws);
								if (executeResult.indexOf("executeFailed") >= 0) {
									this.ErrHandler.ErrCheckin(this.m_binder);
								} else {
									String[] split = executeResult.split(";");
									String tempdID = split[0];
									String url = "<$HttpCgiPath$>?IdcService=CHECKIN_CONFIRM_FORM&dID=" + tempdID
											+ "&dDocTitle=<$url(dDocTitle)$>&dDocName=<$url(dDocName)$>&dDocAuthor=<$url(dDocAuthor)$>&fParentGUID=<$url(fParentGUID)$>&fFolderName=<$url(fFolderName)$>&fApplication=<$url(fApplication)$>&fFileGUID=<$url(fFileGUID)$>";
									this.m_binder.putLocal("RedirectUrl", url);
								}
							} else {
								this.ErrHandler.ErrFailedUpload(this.m_binder);
							}
						} catch (Exception e) {
							Log.info("PHE:precheck.java:DoPreCheck:747:e: " + e.getLocalizedMessage());
						}
					}
				}
			} else {
				Log.info("PHE:precheck.java:DoPreCheck:749: Checkin filter jika update.");
			}
		} catch (Exception ex) {
			Log.error("PHE:precheck.java:DoPreCheck:754:ex: " + ex.getLocalizedMessage());
			ErrorHandler err = new ErrorHandler();
			err.ErrCheckin(this.m_binder);
		}
	}

	public boolean IsValidDoc3D(String docType, String type, String model) {
		if (type.equalsIgnoreCase("Drawing")) {
			if ((type.equalsIgnoreCase("Drawing")) && (docType.equalsIgnoreCase("D3D"))
					&& (model.equalsIgnoreCase("3D"))) {
				return true;
			}
			if ((model.equalsIgnoreCase("2D")) && (!docType.equalsIgnoreCase("D3D"))) {
				return true;
			}
			if ((model.equalsIgnoreCase("")) && (!docType.equalsIgnoreCase("D3D"))) {
				return true;
			}
			return false;
		}
		return true;
	}

	public boolean IsValidDocNumber(String docNumber, String type) {
		String[] names = docNumber.split("-");
		ArrayList<String> tmpNames = new ArrayList<String>();
		tmpNames.addAll(Arrays.asList(names));
		if (!isCombinationNotNull(tmpNames)) {
			return false;
		}
		if (type.toUpperCase().contains("NONDRAWING")) {
			ArrayList<String> tmpDocNum = new ArrayList<String>();
			String location = names[0];
			String disCode = names[1];
			String docType = names[2];

			DatabaseValue db = new DatabaseValue();
			try {
				location = db.getLocation(location, type);
				disCode = db.getDisciplineCode(disCode, type);
				docType = db.getDocumentType(docType, type);

				tmpDocNum.add(location);
				tmpDocNum.add(disCode);
				tmpDocNum.add(docType);
				if (!isCombinationNotNull(tmpDocNum)) {
					return false;
				}
			} catch (DataException e) {
				return false;
			}
		} else {
			String location = names[0];
			String platform = names[1];
			String disCode = names[2];
			String docType = names[3];

			DatabaseValue db = new DatabaseValue();
			try {
				location = db.getLocation(location, type);
				platform = db.getPlatform(location, platform);
				disCode = db.getDisciplineCode(disCode, type);
				docType = db.getDocumentType(docType, type);

				ArrayList<String> tmpDocNum = new ArrayList<String>();

				tmpDocNum.add(location);
				tmpDocNum.add(platform);
				tmpDocNum.add(disCode);
				tmpDocNum.add(docType);
				if (!isCombinationNotNull(tmpDocNum)) {
					return false;
				}
			} catch (DataException e) {
				return false;
			}
		}
		return true;
	}

	public boolean isCombinationNotNull(ArrayList<String> names) {
		for (String name : names) {
			if (name.isEmpty()) {
				return false;
			}
		}
		return true;
	}

	private boolean IsDrawing() {
		return !this.m_binder.getLocal("dDocType").toUpperCase().contains("NONDRAWING");
	}

	private boolean StorePrimaryFile() {
		Util util = new Util();
		DatabaseValue db = new DatabaseValue();

		String tmpprimaryFile = this.m_binder.getLocal("primaryFile");
		if (tmpprimaryFile.contains("\\")) {
			tmpprimaryFile = tmpprimaryFile.substring(tmpprimaryFile.lastIndexOf('\\') + 1, tmpprimaryFile.length());
		}
		String src = this.m_binder.getLocal("primaryFile:path").replace("\\", "/");
		String dst = db.GetConfigValue("TEMP_FOLDER", util.getSystemWorkspace()) + "/" + tmpprimaryFile;
		try {
			FileUtils.copyFile(src, dst);
		} catch (ServiceException ex) {
			Log.error("phe:precheck:StorePrimaryFile:ServiceException:" + ex.getMessage());
			return false;
		}
		this.m_binder.putLocal("primaryFile", dst.replace("/", "\\"));
		this.m_binder.putLocal("primaryFile:path", dst.replace("/", "\\"));
		this.m_binder.putLocal("GetFileBack", "true");
		return true;
	}

	public DataBinder setPartnerNonDrawing(DataBinder binder, String docType, String secGroup) {
		binder.putLocal("dDocAuthor", this.m_binder.getLocal("dUser"));
		binder.putLocal("dDocType", docType);
		binder.putLocal("dSecurityGroup", secGroup);
		binder.putLocal("dDocTitle", this.m_binder.getLocal("dDocTitle"));
		binder.putLocal("xDocPurpose", "");
		binder.putLocal("xStatus", "Registered");
		binder.putLocal("xSheetName", "");
		binder.putLocal("xSheetNumber", "");
		binder.putLocal("xRetentionDate", "");
		binder.putLocal("xRetentionPeriod", "");
		binder.putLocal("xDocName", "");
		binder.putLocal("dID", "");

		binder.putLocal("xRevision", "");
		binder.putLocal("createPrimaryMetaFile", "1");
		binder.putLocal("AutoNumberPrefix", "EDMS");

		String runningNumber = binder.getLocal("xRunningNumber");
		Validator validator = new Validator();
		String area = "";
		String disciplineCode = "";
		String documentType = "";
		String sheetNumber = "";
		String type = uploadTempHeaderMasterlistNonDrawing;
		if (!this.m_binder.getLocal("dDocName").isEmpty()) {
			binder.putLocal("dDocName", "");
		}
		String jadiDiCekin = "true";
		try {
			String tempNumeric = runningNumber.substring(1, runningNumber.length());
			if (Integer.valueOf(runningNumber.substring(0, 1)).intValue() < 5) {
				runningNumber = String.valueOf(Integer.valueOf(runningNumber.substring(0, 1)).intValue() + 5);
			} else {
				runningNumber = String.valueOf(Integer.valueOf(runningNumber.substring(0, 1)).intValue() - 5);
			}
			runningNumber = runningNumber + tempNumeric;

			String tempError = validator.validateDocumentNumberForTDCNonDrawing(area, disciplineCode, documentType,
					runningNumber, sheetNumber, type);
			if (tempError.length() > 0) {
				jadiDiCekin = "false";
			}
		} catch (Exception ex) {
			jadiDiCekin = "false";
			Log.error("PHE:errorParseRunningNumberPasangannya:" + ex.getMessage());
		}
		binder.putLocal("xRunningNumber", runningNumber);
		binder.putLocal("jadiDiCekin", jadiDiCekin);
		return binder;
	}

	public void createPartnerTDCNonDrawing(String executeResult, Util util, boolean isCreatePartnerNonDrawing,
			Workspace ws, Document doc, String tempdID, DataBinder tempBinder, String tempdDocName, String type)
			throws DataException, ServiceException {
		if (isCreatePartnerNonDrawing) {
			DataBinder tempBinder2 = new DataBinder();
			tempBinder2 = setPartnerNonDrawing(tempBinder, type, secGroupApNonDrawing);
			if (tempBinder2.getLocal("jadiDiCekin").toString().equalsIgnoreCase("true")) {
				executeResult = util.DoCheckin(tempBinder2, ws);
				if (executeResult.indexOf("executeFailed") < 0) {
					DataBinder binderDocInfoAgain = new DataBinder();

					binderDocInfoAgain.putLocal("dUser", this.m_binder.getLocal("dUser"));

					String[] splitPerIDanLagi = executeResult.split(";");
					String tempdDocNameLagi = splitPerIDanLagi[1];
					String tempdIDLagi = splitPerIDanLagi[0];
					DataBinder binderUpdateDocInfoAgain = new DataBinder();

					executeResult = doc.executeUpdateDocInfoJustObjectID(binderUpdateDocInfoAgain, "", tempdDocNameLagi,
							tempdIDLagi, type, this.m_binder.getLocal("dUser"), true);
					this.m_binder.putLocal("dDocTitle", tempBinder.getLocal("dDocTitle").toString());
					this.m_binder.putLocal("dID", tempdID);
					this.m_binder.putLocal("dDocName", tempdDocName);
				}
			}
		}
	}
}
